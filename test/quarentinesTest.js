var assert = require("chai").assert;
var app = require("../server/app.js");
var session = require("supertest-session");
var request = session(app);

const adminCredentials = { email: "admin@admin.pt", password: "admin123" };
const employeeCreedentials = {
  email: "adilson@gmail.pt",
  password: "admin123"
};
const volunteerCreedentials = { email: "hugo@email.pt", password: "admin123" };
const invalidCredentials = { email: "invalid@email.pt", password: "admin123" };

const quarentineBody = [
  "id",
  "animal",
  "startDate",
  "endDate",
  "status",
  "info",
  "cause",
  "removed"
];

const invalidQuarentineAnimal = {
  animalId: 0,
  info: "Animal inválido",
  cause: "undefined",
  errorType: "O campo animal precisa ser preenchido."
};

const invalidQuarentineInfoEmpty = {
  animalId: 1,
  info: "",
  cause: "undefined",
  errorType: "O campo informação precisa ser preenchido."
};

const invalidQuarentineInfoOverSize = {
  animalId: 1,
  info:
    "YaY888ZzFuEFSmGSOnbHvJyxR92JtdenpKBExgigW0nZ3vHIYZlXzG7IBjldRFsXm8k5SKb9SSpIKvuO0px6GE4gqypy3DkzZco8A64yQlzCtxayAqJBn5HqjklogH8YEs8lJQvGLwLAARjA8tgR32E1S92b5eUBru3rsCfKdJQFanBQjbiDwiBdlBpg5vRpPPGNCYeXb1nOyiYF7lBJWGhWk6uPHr21xD7GEsyr7yBOrJFOmUrXZrKShXwhKTkA",
  cause: "undefined",
  errorType: "O campo informação deve conter no máximo 255 caracteres."
};

const invalidQuarentineCauseInvalid = {
  animalId: 1,
  info: "Test",
  cause: "Test",
  errorType: "Insira um tipo de causa válido."
};

const validQuarentine = {
  animalId: 1,
  info: "Doente",
  cause: "undefined"
};

const invalidFinishStatusQuarentine = {
  id: 1,
  status: "pending"
};

const invalidFinishEmptyStatusQuarentine = {
  id: 1,
  status: "pending"
};

const invalidFinishQuarentineId = {
  id: 0,
  status: "finished"
};

const validCancelQuarentine = {
  id: 1,
  status: "finished"
};

const validFinishQuarentine = {
  id: 1,
  status: "finished"
};

describe("Unit tests - Quarentines [NotLoggedIn]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(invalidCredentials)
      .expect(400)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - view quarentines - invalid", function() {
    return request.get("/api/quarentines/").expect(403);
  });

  it("POST - create quarentine - invalid", function() {
    return request.post("/api/quarentines").expect(403);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Quarentines [ADMIN]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(adminCredentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - view quarentine - valid", function() {
    return request
      .get("/api/quarentines/")
      .expect(200)
      .then(result => {
        result.body.data.forEach(quarentine => {
          for (var propertyName in quarentine) {
            assert.isTrue(quarentineBody.includes(propertyName));
          }
        });
      });
  });

  it("POST - create quarentine - invalid animal", function() {
    return request
      .post("/api/quarentines")
      .send(invalidQuarentineAnimal)
      .expect(400)
      .then(result =>
        assert.isTrue(result.text === invalidQuarentineAnimal.errorType)
      );
  });

  it("POST - create quarentine - invalid empty info", function() {
    return request
      .post("/api/quarentines")
      .send(invalidQuarentineInfoEmpty)
      .expect(400)
      .then(result =>
        assert.isTrue(result.text === invalidQuarentineInfoEmpty.errorType)
      );
  });

  it("POST - create quarentine - invalid over size info", function() {
    return request
      .post("/api/quarentines")
      .send(invalidQuarentineInfoOverSize)
      .expect(400)
      .then(result =>
        assert.isTrue(result.text === invalidQuarentineInfoOverSize.errorType)
      );
  });

  it("POST - create quarentine - invalid cause", function() {
    return request
      .post("/api/quarentines")
      .send(invalidQuarentineCauseInvalid)
      .expect(400)
      .then(result =>
        assert.isTrue(result.text === invalidQuarentineCauseInvalid.errorType)
      );
  });

  it("POST - create quarentine - valid", function() {
    return request
      .post("/api/quarentines")
      .send(validQuarentine)
      .expect(200);
  });

  it("GET - edit quarentine - valid", function() {
    return request.get("/api/quarentines/edit/1").expect(200);
  });

  it("POST - edit quarentine - invalid animal", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(invalidQuarentineAnimal)
      .expect(400);
  });

  it("POST - edit quarentine - invalid empty info", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(invalidQuarentineInfoEmpty)
      .expect(400);
  });

  it("POST - edit quarentine - invalid over size info", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(invalidQuarentineInfoOverSize)
      .expect(400);
  });

  it("POST - edit quarentine - invalid cause", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(invalidQuarentineCauseInvalid)
      .expect(400);
  });

  it("POST - edit quarentine - valid", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(validQuarentine)
      .expect(200);
  });

  it("PUT - finish quarentine - invalid status value", function() {
    return request
      .put("/api/quarentines")
      .send(invalidFinishStatusQuarentine)
      .expect(400);
  });

  it("PUT - finish quarentine - invalid status empty", function() {
    return request
      .put("/api/quarentines")
      .send(invalidFinishEmptyStatusQuarentine)
      .expect(400);
  });

  it("PUT - finish quarentine - invalid id", function() {
    return request
      .put("/api/quarentines")
      .send(invalidFinishQuarentineId)
      .expect(400);
  });

  it("PUT - finish quarentine - valid", function() {
    return request
      .put("/api/quarentines")
      .send(validFinishQuarentine)
      .expect(200);
  });

  it("PUT - finish quarentine - valid", function() {
    return request
      .put("/api/quarentines")
      .send(validCancelQuarentine)
      .expect(200);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Quarentines [EMPLOYEE]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(employeeCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - view quarentine - valid", function() {
    return request
      .get("/api/quarentines/")
      .expect(200)
      .then(result => {
        result.body.data.forEach(quarentine => {
          for (var propertyName in quarentine) {
            assert.isTrue(quarentineBody.includes(propertyName));
          }
        });
      });
  });

  it("POST - create quarentine - invalid animal", function() {
    return request
      .post("/api/quarentines")
      .send(invalidQuarentineAnimal)
      .expect(400)
      .then(result =>
        assert.isTrue(result.text === invalidQuarentineAnimal.errorType)
      );
  });

  it("POST - create quarentine - invalid empty info", function() {
    return request
      .post("/api/quarentines")
      .send(invalidQuarentineInfoEmpty)
      .expect(400)
      .then(result =>
        assert.isTrue(result.text === invalidQuarentineInfoEmpty.errorType)
      );
  });

  it("POST - create quarentine - invalid over size info", function() {
    return request
      .post("/api/quarentines")
      .send(invalidQuarentineInfoOverSize)
      .expect(400)
      .then(result =>
        assert.isTrue(result.text === invalidQuarentineInfoOverSize.errorType)
      );
  });

  it("POST - create quarentine - invalid cause", function() {
    return request
      .post("/api/quarentines")
      .send(invalidQuarentineCauseInvalid)
      .expect(400)
      .then(result =>
        assert.isTrue(result.text === invalidQuarentineCauseInvalid.errorType)
      );
  });

  it("POST - create quarentine - valid", function() {
    return request
      .post("/api/quarentines")
      .send(validQuarentine)
      .expect(200);
  });

  it("GET - edit quarentine - valid", function() {
    return request.get("/api/quarentines/edit/1").expect(200);
  });

  it("POST - edit quarentine - invalid animal", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(invalidQuarentineAnimal)
      .expect(400);
  });

  it("POST - edit quarentine - invalid empty info", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(invalidQuarentineInfoEmpty)
      .expect(400);
  });

  it("POST - edit quarentine - invalid over size info", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(invalidQuarentineInfoOverSize)
      .expect(400);
  });

  it("POST - edit quarentine - invalid cause", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(invalidQuarentineCauseInvalid)
      .expect(400);
  });

  it("POST - edit quarentine - valid", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(validQuarentine)
      .expect(200);
  });

  it("PUT - finish quarentine - invalid status value", function() {
    return request
      .put("/api/quarentines")
      .send(invalidFinishStatusQuarentine)
      .expect(400);
  });

  it("PUT - finish quarentine - invalid status empty", function() {
    return request
      .put("/api/quarentines")
      .send(invalidFinishEmptyStatusQuarentine)
      .expect(400);
  });

  it("PUT - finish quarentine - invalid id", function() {
    return request
      .put("/api/quarentines")
      .send(invalidFinishQuarentineId)
      .expect(400);
  });

  it("PUT - finish quarentine - valid", function() {
    return request
      .put("/api/quarentines")
      .send(validFinishQuarentine)
      .expect(200);
  });

  it("PUT - finish quarentine - valid", function() {
    return request
      .put("/api/quarentines")
      .send(validCancelQuarentine)
      .expect(200);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Quarentines [VOLUNTEER]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(volunteerCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - view quarentine - valid", function() {
    return request
      .get("/api/quarentines/")
      .expect(200)
      .then(result => {
        result.body.data.forEach(quarentine => {
          for (var propertyName in quarentine) {
            assert.isTrue(quarentineBody.includes(propertyName));
          }
        });
      });
  });

  it("POST - create quarentine - invalid animal", function() {
    return request
      .post("/api/quarentines")
      .send(invalidQuarentineAnimal)
      .expect(403);
  });

  it("POST - create quarentine - invalid empty info", function() {
    return request
      .post("/api/quarentines")
      .send(invalidQuarentineInfoEmpty)
      .expect(403);
  });

  it("POST - create quarentine - invalid over size info", function() {
    return request
      .post("/api/quarentines")
      .send(invalidQuarentineInfoOverSize)
      .expect(403);
  });

  it("POST - create quarentine - invalid cause", function() {
    return request
      .post("/api/quarentines")
      .send(invalidQuarentineCauseInvalid)
      .expect(403);
  });

  it("POST - create quarentine - invalid", function() {
    return request
      .post("/api/quarentines")
      .send(validQuarentine)
      .expect(403);
  });

  it("GET - edit quarentine - invalid", function() {
    return request.get("/api/quarentines/edit/1").expect(403);
  });

  it("POST - edit quarentine - invalid animal", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(invalidQuarentineAnimal)
      .expect(403);
  });

  it("POST - edit quarentine - invalid empty info", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(invalidQuarentineInfoEmpty)
      .expect(403);
  });

  it("POST - edit quarentine - invalid over size info", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(invalidQuarentineInfoOverSize)
      .expect(403);
  });

  it("POST - edit quarentine - invalid cause", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(invalidQuarentineCauseInvalid)
      .expect(403);
  });

  it("POST - edit quarentine - invalid", function() {
    return request
      .put("/api/quarentines/edit/1")
      .send(validQuarentine)
      .expect(403);
  });

  it("PUT - finish quarentine - invalid status value", function() {
    return request
      .put("/api/quarentines")
      .send(invalidFinishStatusQuarentine)
      .expect(403);
  });

  it("PUT - finish quarentine - invalid status empty", function() {
    return request
      .put("/api/quarentines")
      .send(invalidFinishEmptyStatusQuarentine)
      .expect(403);
  });

  it("PUT - finish quarentine - invalid id", function() {
    return request
      .put("/api/quarentines")
      .send(invalidFinishQuarentineId)
      .expect(403);
  });

  it("PUT - finish quarentine - invalid", function() {
    return request
      .put("/api/quarentines")
      .send(validFinishQuarentine)
      .expect(403);
  });

  it("PUT - finish quarentine - invalid", function() {
    return request
      .put("/api/quarentines")
      .send(validCancelQuarentine)
      .expect(403);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});
