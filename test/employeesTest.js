var assert = require("chai").assert;
var app = require("../server/app.js");
var session = require("supertest-session");
var request = session(app);

const adminCredentials = { email: "admin@admin.pt", password: "admin123" };
const volunteerCreedentials = { email: "hugo@email.pt", password: "admin123" };
const employeeCreedentials = { email: "rui@email.pt", password: "admin123" };

getEmployee = () => {
  return {
    name: "Tester",
    photoUrl: "",
    birthdate: new Date("1990-01-01"),
    contact: "960000000",
    address: "Rua Luisa Todi",
    info: "A employee created to test",
    gender: "male",
    email: "test@gmail.com",
    type: "admin"
  };
};

getEmployeeToEdit = () => {
  return {
    name: "Tester",
    photoUrl: "",
    birthdate: "1990-01-01",
    contact: "960000001",
    address: "Rua Luisa Todi",
    info: "A employee edited to test",
    gender: "male",
    email: "tes2t@gmail.com",
    type: "employee",
    employeeId: 8
  };
};

describe("Unit tests - Employees [ADMIN]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(adminCredentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - View accounts", function() {
    return request
      .get("/api/employees/")
      .expect(200)
      .then(response => {
        let data = JSON.parse(response.text).data;
        assert.typeOf(data, "array", "Data returned is not an array");
        assert.containsAllKeys(
          data[0],
          [
            "employeeId",
            "name",
            "email",
            "photoUrl",
            "birthdate",
            "gender",
            "contact",
            "address",
            "info",
            "type",
            "accountId",
            "archived",
            "removed"
          ],
          "The returned data doesn't contains all information about collaborators."
        );
      });
  });

  it("POST - create account", function() {
    return request
      .post("/api/employees/")
      .send(getEmployee())
      .expect(200);
  });

  it("POST - create account - invalid email already exists", function() {
    return request
      .post("/api/employees/")
      .send(getEmployee())
      .expect(400);
  });

  it("POST - create account - invalid photo url invalid", function() {
    var employee = getEmployee();
    employee.photoUrl =
      "FpVCM9yQKYHFRw3vy1N0eGSS2GmodsPoOxGRcI3h0TeaAgng2BKq1GEZS07xBsM3BvcBxF6jclPhUwfslAJmnQzXV7fkcJqdpEEqKYcuAB6NfMDyolc02g0pCn1sPH1FXapKScmIsz6Y7stWTHaL9KFKvshii3w5u1WRH29khNp8nLFC7t5NXPnVoLElTcXuK1gKTP7Ure7MkNa76NjepCxr5eqp7YTMf9y3MW5HNjxFEK87dXTYAIjOuG6";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - pre-used email", function() {
    var employee = getEmployee();
    employee.email = "admin@admin.pt";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - null photo url ", function() {
    var employee = getEmployee();
    employee.photoUrl = null;
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - overlarge name", function() {
    var employee = getEmployee();
    employee.name = "JejVSH8N122Up5tj215rcX2u4O046F59VLB3HM7ceAx569NjjFD";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - empty name", function() {
    var employee = getEmployee();
    employee.name = "";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - invalid birthdate", function() {
    var employee = getEmployee();
    employee.birthdate = new Date("2200-01-01");
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - null birthdate", function() {
    var employee = getEmployee();
    employee.birthdate = null;
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - overlarge address", function() {
    var employee = getEmployee();
    employee.address =
      "4036Vv8fle9uMJ36mnPA0fCc48L41ILwlCnO49MrkteGhDiKyvpsI4ZDCAv78KKUS34QSF5Rq33M4bboTxQY7ud4NHj5V9g7N6Bj7pXn9Mj4h3iDIxVqJNs9qsVkdlBtubO4h0scSD5W33756HbpG8hAKPkcJjdHa9NelLvBUq3T04Ut0g83N5uv9pTsRtaC7c1Dy6RxIN84dFkxPam58was3ylYN17xVFzxdBPOawfaAj2dtItyz9gfva4DpGkTut09vR70E0OwKLuNsh1ncnre0SJsL4wBD0I9yvU1wssd7A2lUY1rVQ2L7xlZ0cRu8lqKl8buF943eaE9L5Gz12Rkt877WgDy2xJY1Ij24fzZ9Jhjw2N0v5a";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - empty address", function() {
    var employee = getEmployee();
    employee.address = "";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - orverlage contact", function() {
    var employee = getEmployee();
    employee.contact = "653160725853184421400791576587442286740689";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - empty contact", function() {
    var employee = getEmployee();
    employee.contact = "";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - empty contact", function() {
    var employee = getEmployee();
    employee.gender = "teste";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - invalid format email", function() {
    var employee = getEmployee();
    employee.email = "testes2testes.com";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - overlage email", function() {
    var employee = getEmployee();
    employee.email =
      "M38HSJv5nez2Ed1b4r9Df23k2U6F6og0k81NyfHZD9R4Q43POX78DpPr311rtuGk0KLK3u02GbBtIbv4He8JYPJt95GuPPqOl9S4ZqOmNMjuaj1oKZrDJFfJ1Lk0Nchm5yBm723fmD9t@gmail.com";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - create account - invalid - invalid type", function() {
    var employee = getEmployee();
    employee.type = "test";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account", function() {
    return request
      .put("/api/employees/")
      .send(getEmployeeToEdit())
      .expect(200);
  });

  it("PUT - edit account - invalid email already exists", function() {
    var employee = getEmployeeToEdit();
    employee.email = "paulaCandeias@gmail.pt";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account - invalid photo url invalid", function() {
    var employee = getEmployeeToEdit();
    employee.photoUrl =
      "FpVCM9yQKYHFRw3vy1N0eGSS2GmodsPoOxGRcI3h0TeaAgng2BKq1GEZS07xBsM3BvcBxF6jclPhUwfslAJmnQzXV7fkcJqdpEEqKYcuAB6NfMDyolc02g0pCn1sPH1FXapKScmIsz6Y7stWTHaL9KFKvshii3w5u1WRH29khNp8nLFC7t5NXPnVoLElTcXuK1gKTP7Ure7MkNa76NjepCxr5eqp7YTMf9y3MW5HNjxFEK87dXTYAIjOuG6";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account - invalid - overlarge name", function() {
    var employee = getEmployeeToEdit();
    employee.name = "JejVSH8N122Up5tj215rcX2u4O046F59VLB3HM7ceAx569NjjFD";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account - invalid - empty name", function() {
    var employee = getEmployeeToEdit();
    employee.name = "";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account - invalid - invalid birthdate", function() {
    var employee = getEmployeeToEdit();
    employee.birthdate = new Date("2200-01-01");
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account - invalid - null birthdate", function() {
    var employee = getEmployeeToEdit();
    employee.birthdate = null;
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account - invalid - overlarge address", function() {
    var employee = getEmployeeToEdit();
    employee.address =
      "4036Vv8fle9uMJ36mnPA0fCc48L41ILwlCnO49MrkteGhDiKyvpsI4ZDCAv78KKUS34QSF5Rq33M4bboTxQY7ud4NHj5V9g7N6Bj7pXn9Mj4h3iDIxVqJNs9qsVkdlBtubO4h0scSD5W33756HbpG8hAKPkcJjdHa9NelLvBUq3T04Ut0g83N5uv9pTsRtaC7c1Dy6RxIN84dFkxPam58was3ylYN17xVFzxdBPOawfaAj2dtItyz9gfva4DpGkTut09vR70E0OwKLuNsh1ncnre0SJsL4wBD0I9yvU1wssd7A2lUY1rVQ2L7xlZ0cRu8lqKl8buF943eaE9L5Gz12Rkt877WgDy2xJY1Ij24fzZ9Jhjw2N0v5a";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account - invalid - empty address", function() {
    var employee = getEmployeeToEdit();
    employee.address = "";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account - invalid - orverlage contact", function() {
    var employee = getEmployeeToEdit();
    employee.contact = "653160725853184421400791576587442286740689";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account - invalid - empty contact", function() {
    var employee = getEmployeeToEdit();
    employee.contact = "";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account - invalid - empty contact", function() {
    var employee = getEmployeeToEdit();
    employee.gender = "teste";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account - invalid - invalid format email", function() {
    var employee = getEmployeeToEdit();
    employee.email = "testes2testes.com";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account - invalid - overlage email", function() {
    var employee = getEmployeeToEdit();
    employee.email =
      "M38HSJv5nez2Ed1b4r9Df23k2U6F6og0k81NyfHZD9R4Q43POX78DpPr311rtuGk0KLK3u02GbBtIbv4He8JYPJt95GuPPqOl9S4ZqOmNMjuaj1oKZrDJFfJ1Lk0Nchm5yBm723fmD9t@gmail.com";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("PUT - edit account - invalid - invalid type", function() {
    var employee = getEmployeeToEdit();
    employee.type = "test";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(400);
  });

  it("POST - upload image when create account - valid", function() {
    var employee = getEmployeeToEdit();
    employee.email = "uploadImage@hotmail.com";
    employee.image =
      "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAmUlEQVQ4je2TsQ3CMBBFnxMa08WR2IQKJskIUNwMZAcYwWIQMs65JCUpEEIYW4pJy6v+6e6+/hVnnGsAzsCBMi7AsbbW/rIMsAU2xrnmkeruuzW7zgIw+JGbv6fGQpWzfy3HOsJlDQY/AlCv3jpF9oS5ZBOICKoB1YCIlCdQDR9127qyBHP5Gyw3CBXPr/qi709JHXE1S995AsqoJu8x78GsAAAAAElFTkSuQmCC";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(200);
  });

  it("POST - upload image when create account - invalid - empty", function() {
    var employee = getEmployeeToEdit();
    employee.email = "uploadImage2@hotmail.com";
    employee.image = "";
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(200);
  });

  it("POST - upload image when create account - invalid - undefined", function() {
    var employee = getEmployeeToEdit();
    employee.email = "uploadImage3@hotmail.com";
    employee.image = undefined;
    return request
      .post("/api/employees/")
      .send(employee)
      .expect(200);
  });

  it("PUT - upload image when edit account - valid", function() {
    var employee = getEmployeeToEdit();
    employee.email = "uploadImage@hotmail.com";
    employee.employeeId = 12;
    employee.image =
      "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAmUlEQVQ4je2TsQ3CMBBFnxMa08WR2IQKJskIUNwMZAcYwWIQMs65JCUpEEIYW4pJy6v+6e6+/hVnnGsAzsCBMi7AsbbW/rIMsAU2xrnmkeruuzW7zgIw+JGbv6fGQpWzfy3HOsJlDQY/AlCv3jpF9oS5ZBOICKoB1YCIlCdQDR9127qyBHP5Gyw3CBXPr/qi709JHXE1S995AsqoJu8x78GsAAAAAElFTkSuQmCC";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(200);
  });

  it("PUT - upload image when edit account - invalid - empty", function() {
    var employee = getEmployeeToEdit();
    employee.email = "uploadImage2@hotmail.com";
    employee.employeeId = 13;
    employee.image = "";
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(200);
  });

  it("PUT - upload image when edit account - invalid - undefined", function() {
    var employee = getEmployeeToEdit();
    employee.email = "uploadImage3@hotmail.com";
    employee.employeeId = 14;
    employee.image = undefined;
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(200);
  });

  it("DELETE - employee - success", function() {
    return request
      .delete("/api/employees/")
      .send({ id: 6 })
      .expect(200);
  });

  it("DELETE - employee - invalid id", function() {
    return request
      .delete("/api/employees/")
      .send({ id: -1 })
      .expect(400);
  });

  it("ARCHIVE - employee - success", function() {
    return request
      .post("/api/employees/archive")
      .send({ id: 6 })
      .expect(200);
  });

  it("ARCHIVE - employee - invalid id", function() {
    return request
      .post("/api/employees/archive")
      .send({ id: -1 })
      .expect(400);
  });

  it("REACTIVATE - employee - success", function() {
    return request
      .post("/api/employees/reactivate")
      .send({ id: 6 })
      .expect(200);
  });

  it("REACTIVATE - employee - invalid id", function() {
    return request
      .post("/api/employees/reactivate")
      .send({ id: -1 })
      .expect(400);
  });

  it("GET - own profile - success", function() {
    return request.get("/api/employees/1").expect(200);
  });

  it("GET - other profile - success", function() {
    return request.get("/api/employees/3").expect(200);
  });

  it("GET - other profile - does not exists", function() {
    return request.get("/api/employees/302010").expect(404);
  });

  it("GET - other profile - does not exists - string id", function() {
    return request.get("/api/employees/av20").expect(404);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          console.log(err);
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit test - Employees [VOLUNTEER]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(volunteerCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("PUT - edit account - forbidden", function() {
    return request
      .put("/api/employees/")
      .send(getEmployeeToEdit())
      .expect(403);
  });

  it("PUT - edit account", function() {
    var employee = getEmployeeToEdit();
    employee.email = "hugo@email.pt";
    employee.employeeId = 4;
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(200);
  });

  it("DELETE - employee - forbidden", function() {
    return request
      .delete("/api/employees/")
      .send({ id: 4 })
      .expect(403);
  });

  it("ARCHIVE - employee - forbidden", function() {
    return request
      .post("/api/employees/archive")
      .send({ id: 4 })
      .expect(403);
  });

  it("REACTIVATE - employee - forbidden", function() {
    return request
      .post("/api/employees/reactivate")
      .send({ id: 4 })
      .expect(403);
  });

  it("GET - own profile - success", function() {
    return request.get("/api/employees/4").expect(200);
  });

  it("GET - other profile - success", function() {
    return request.get("/api/employees/3").expect(403);
  });

  it("GET - other profile - does not exists", function() {
    return request.get("/api/employees/302010").expect(403);
  });

  it("GET - other profile - does not exists - string id", function() {
    return request.get("/api/employees/av20").expect(404);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit test - Employees [EMPLOYEE]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(employeeCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("PUT - edit account", function() {
    var employee = getEmployeeToEdit();
    employee.email = "rui@email.pt";
    employee.employeeId = 3;
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(200);
  });

  it("PUT - edit account", function() {
    var employee = getEmployeeToEdit();
    employee.email = "rui@email.pt";
    employee.employeeId = 4;
    return request
      .put("/api/employees/")
      .send(employee)
      .expect(403);
  });

  it("GET - own profile - success", function() {
    return request.get("/api/employees/3").expect(200);
  });

  it("GET - other profile - success", function() {
    return request.get("/api/employees/1").expect(403);
  });

  it("GET - other profile - does not exists", function() {
    return request.get("/api/employees/302010").expect(403);
  });

  it("GET - other profile - does not exists - string id", function() {
    return request.get("/api/employees/av20").expect(404);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});
