var assert = require("chai").assert;
var app = require("../server/app.js");
var session = require("supertest-session");
var request = session(app);

const adminCredentials = { email: "admin@admin.pt", password: "admin123" };
const employeeCreedentials = {
  email: "adilson@gmail.pt",
  password: "admin123"
};
const volunteerCreedentials = { email: "hugo@email.pt", password: "admin123" };

getAnimal = () => {
  return {
    name: "teddy",
    birthdate: 1558473733631,
    breed: "Bull Dog",
    size: "Big",
    info: "Apenas um teste",
    gender: "male",
    type: "dog"
  };
};

// ========================== ADMIN ================================
describe("Unit tests - Animals [ADMIN]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(adminCredentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  // =============== CREATE ==============
  it("POST - create animal - valid", function() {
    return request
      .post("/api/animals/")
      .send(getAnimal())
      .expect(200);
  });

  it("POST - create animal - invalid name", function() {
    let animal = getAnimal();
    animal.name = "";

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  it("POST - create animal - invalid birthdate", function() {
    let animal = getAnimal();
    animal.birthdate = new Date().setDate(new Date().getDate() + 1);

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  it("POST - create animal - invalid breed", function() {
    let animal = getAnimal();
    animal.breed = null;

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  it("POST - create animal - invalid size", function() {
    let animal = getAnimal();
    animal.size = undefined;

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  it("POST - create animal - invalid info", function() {
    let animal = getAnimal();
    animal.info =
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,.";

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  it("POST - create animal - invalid gender", function() {
    let animal = getAnimal();
    animal.gender = "Macho man";

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  it("POST - create animal - invalid type", function() {
    let animal = getAnimal();
    animal.gender = "Coelhinho";

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  // =============== EDIT ==============
  it("PUT - edit animal - valid", function() {
    let animal = getAnimal();
    animal.name = "Zeca";

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(200);
  });

  it("PUT - edit animal - invalid name", function() {
    let animal = getAnimal();
    animal.name = "";

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  it("PUT - edit animal - invalid birthdate", function() {
    let animal = getAnimal();
    animal.birthdate = new Date().setDate(new Date().getDate() + 1);

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  it("PUT - edit animal - invalid breed", function() {
    let animal = getAnimal();
    animal.breed = null;

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  it("PUT - edit animal - invalid size", function() {
    let animal = getAnimal();
    animal.size = undefined;

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  it("PUT - edit animal - invalid info", function() {
    let animal = getAnimal();
    animal.info =
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,.";

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  it("PUT - edit animal - invalid gender", function() {
    let animal = getAnimal();
    animal.gender = "Macho man";

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  it("PUT - edit animal - invalid type", function() {
    let animal = getAnimal();
    animal.gender = "Coelhinho";

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  // ============================= GET ALL ====================
  it("GET - get all animals", function() {
    return request
      .get("/api/animals/")
      .expect(200)
      .then(response => {
        let data = JSON.parse(response.text).data;
        assert.typeOf(data, "array", "Data returned is not an array");
        assert.containsAllKeys(
          data[0],
          [
            "animalId",
            "name",
            "type",
            "gender",
            "size",
            "breed",
            "photoUrl",
            "inQuarentine"
          ],
          "The returned data doesn't contains all necessary information about animals."
        );
      });
  });
  // =============== VIEW ==============

  it("GET - view animal - invalid animal", function() {
    return request.get("/api/animals/100").expect(404);
  });

  it("GET - view animal - valid", function() {
    return request
      .get("/api/animals/1")
      .expect(200)
      .then(response => {
        let data = JSON.parse(response.text).data;
        assert.containsAllKeys(
          data,
          [
            "animalId",
            "name",
            "photoUrl",
            "birthdate",
            "breed",
            "size",
            "gender",
            "type",
            "inQuarentine"
          ],
          "The returned data doesn't contains all information about animals."
        );
      });
  });

  // ============================= DELETE ====================
  it("DELETE - animal - success", function() {
    return request.delete("/api/animals/2").expect(200);
  });

  it("DELETE - animal - invalid id", function() {
    return request.delete("/api/animals/-1").expect(404);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

// ================================ EMPLOYEE =================================
describe("Unit tests - Animals [EMPLOYEE]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(employeeCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  // =============== CREATE ==============
  it("POST - create animal - valid", function() {
    return request
      .post("/api/animals/")
      .send(getAnimal())
      .expect(200);
  });

  it("POST - create animal - invalid name", function() {
    let animal = getAnimal();
    animal.name = "";

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  it("POST - create animal - invalid birthdate", function() {
    let animal = getAnimal();
    animal.birthdate = new Date().setDate(new Date().getDate() + 1);

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  it("POST - create animal - invalid breed", function() {
    let animal = getAnimal();
    animal.breed = null;

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  it("POST - create animal - invalid size", function() {
    let animal = getAnimal();
    animal.size = undefined;

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  it("POST - create animal - invalid info", function() {
    let animal = getAnimal();
    animal.info =
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,.";

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  it("POST - create animal - invalid gender", function() {
    let animal = getAnimal();
    animal.gender = "Macho man";

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  it("POST - create animal - invalid type", function() {
    let animal = getAnimal();
    animal.gender = "Coelhinho";

    return request
      .post("/api/animals/")
      .send(animal)
      .expect(400);
  });

  // =============== EDIT ==============
  it("PUT - edit animal - valid", function() {
    let animal = getAnimal();
    animal.name = "Zeca";
    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(200);
  });

  it("PUT - edit animal - invalid name", function() {
    let animal = getAnimal();
    animal.name = "";

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  it("PUT - edit animal - invalid birthdate", function() {
    let animal = getAnimal();
    animal.birthdate = new Date().setDate(new Date().getDate() + 1);

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  it("PUT - edit animal - invalid breed", function() {
    let animal = getAnimal();
    animal.breed = null;

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  it("PUT - edit animal - invalid size", function() {
    let animal = getAnimal();
    animal.size = undefined;

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  it("PUT - edit animal - invalid info", function() {
    let animal = getAnimal();
    animal.info =
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,.";

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  it("PUT - edit animal - invalid gender", function() {
    let animal = getAnimal();
    animal.gender = "Macho man";

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  it("PUT - edit animal - invalid type", function() {
    let animal = getAnimal();
    animal.gender = "Coelhinho";

    return request
      .put("/api/animals/1")
      .send(animal)
      .expect(400);
  });

  // ============================= GET ALL ====================
  it("GET - get all animals", function() {
    return request
      .get("/api/animals/")
      .expect(200)
      .then(response => {
        let data = JSON.parse(response.text).data;
        assert.typeOf(data, "array", "Data returned is not an array");
        assert.containsAllKeys(
          data[0],
          [
            "animalId",
            "name",
            "type",
            "gender",
            "size",
            "breed",
            "photoUrl",
            "inQuarentine"
          ],
          "The returned data doesn't contains all necessary information about animals."
        );
      });
  });

  // ============================= DELETE ====================
  it("DELETE - animal - success", function() {
    return request.delete("/api/animals/2").expect(403);
  });
  // =============== VIEW ==============

  it("GET - view animal - valid permission", function() {
    return request.get("/api/animals/1").expect(200);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

// ================================ VOLUNTEER =================================

describe("Unit tests - Animals [VOLUNTEER]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(volunteerCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  // =============== CREATE ==============
  it("POST - create animal - forbbiden", function() {
    return request
      .post("/api/animals/")
      .send(getAnimal())
      .expect(403);
  });

  // =============== EDIT ==============
  it("PUT - edit animal - forbbiden", function() {
    return request
      .put("/api/animals/1")
      .send(getAnimal())
      .expect(403);
  });

  // ============================= GET ALL ====================
  it("GET - get all animals", function() {
    return request
      .get("/api/animals/")
      .expect(200)
      .then(response => {
        let data = JSON.parse(response.text).data;
        assert.typeOf(data, "array", "Data returned is not an array");
        assert.containsAllKeys(
          data[0],
          [
            "animalId",
            "name",
            "type",
            "gender",
            "size",
            "breed",
            "photoUrl",
            "inQuarentine"
          ],
          "The returned data doesn't contains all necessary information about animals."
        );
      });
  });

  // ============================= DELETE ====================
  it("DELETE - animal - success", function() {
    return request.delete("/api/animals/2").expect(403);
  });
  // =============== VIEW ==============

  it("GET - view animal - valid permission", function() {
    return request.get("/api/animals/1").expect(200);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

// ================================ NOLOGGEDIN =================================
describe("Unit test - Employees [NOLOGGEDIN]", function() {
  before(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  // =============== CREATE ==============
  it("POST - create animal - forbbiden", function() {
    return request
      .post("/api/animals/")
      .send(getAnimal())
      .expect(403);
  });

  // =============== EDIT ==============
  it("PUT - edit animal - forbbiden", function() {
    return request
      .put("/api/animals/1")
      .send(getAnimal())
      .expect(403);
  });

  // ============================= DELETE ====================
  it("DELETE - animal - success", function() {
    return request.delete("/api/animals/2").expect(403);
  });

  // ============================= GET ALL ====================
  it("GET - get all animals", function() {
    return request.get("/api/animals/").expect(403);
  });
  // =============== VIEW ==============

  it("GET - view animal - forbidden", function() {
    return request.get("/api/animals/1").expect(403);
  });
});
