var assert = require("chai").assert;
var app = require("../server/app.js");
var session = require("supertest-session");
var request = session(app);

const adminCredentials = { email: "admin@admin.pt", password: "admin123" };
const employeeCreedentials = {
  email: "adilson@gmail.pt",
  password: "admin123"
};
const volunteerCreedentials = { email: "hugo@email.pt", password: "admin123" };
const invalidCredentials = { email: "invalid@email.pt", password: "admin123" };

describe("Unit tests - Kennels [ADMIN]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(adminCredentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("POST - create kennel - valid permission", function() {
    return request
      .post("/api/kennels/")
      .send({ capacity: 3, name: "kennel a", animals: [1, 2] })
      .expect(200);
  });

  it("PUT - edit kennel - valid permission", function() {
    return request
      .put("/api/kennels/")
      .send({ kennelId: 1, capacity: 3, name: "kennel a", animals: [3] })
      .expect(200);
  });

  it("GET - get kennel - valid permission", function() {
    return request
      .get("/api/kennels/1")
      .send()
      .expect(200);
  });

  it("GET - get all kennels - valid permission", function() {
    return request
      .get("/api/kennels/")
      .send()
      .expect(200);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Kennels [EMPLOYEE]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(employeeCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("POST - create kennel - valid permission", function() {
    return request
      .post("/api/kennels/")
      .send({ capacity: 3, name: "kennel a", animals: [1, 2] })
      .expect(200);
  });

  it("PUT - edit kennel - valid permission", function() {
    return request
      .put("/api/kennels/")
      .send({ kennelId: 1, capacity: 3, name: "kennel a", animals: [3] })
      .expect(200);
  });

  it("GET - get kennel - valid permission", function() {
    return request
      .get("/api/kennels/1")
      .send()
      .expect(200);
  });

  it("GET - get all kennels - valid permission", function() {
    return request
      .get("/api/kennels/")
      .send()
      .expect(200);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Kennels [VOLUNTEER]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(volunteerCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("POST - create kennel - invalid permission", function() {
    return request
      .post("/api/kennels/")
      .send({ capacity: 3, name: "kennel a", animals: [1, 2] })
      .expect(403);
  });

  it("PUT - edit kennel - invalid permission", function() {
    return request
      .put("/api/kennels/")
      .send({ kennelId: 1, capacity: 3, name: "kennel a", animals: [3] })
      .expect(403);
  });

  it("GET - get kennel - valid permission", function() {
    return request
      .get("/api/kennels/1")
      .send()
      .expect(200);
  });

  it("GET - get all kennels - valid permission", function() {
    return request
      .get("/api/kennels/")
      .send()
      .expect(200);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Kennels [NotLoggedIn]", function() {
  it("POST - create kennel - invalid permission", function() {
    return request
      .post("/api/kennels/")
      .send({ capacity: 3, name: "kennel a", animals: [1, 2] })
      .expect(403);
  });

  it("PUT - edit kennel - invalid permission", function() {
    return request
      .put("/api/kennels/")
      .send({ kennelId: 1, capacity: 3, name: "kennel a", animals: [3] })
      .expect(403);
  });

  it("GET - get kennel - invalid permission", function() {
    return request
      .get("/api/kennels/1")
      .send()
      .expect(403);
  });

  it("GET - get all kennels - invalid permission", function() {
    return request
      .get("/api/kennels/")
      .send()
      .expect(403);
  });
});
