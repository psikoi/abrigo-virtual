const app = require("../server/app.js");
var chai = require("chai");
var session = require("supertest-session");
var request = session(app);

const userCredentials = { email: "admin@admin.pt", password: "admin123" };

describe("Unit tests - Session", () => {
  it("POST - login - valid", function() {
    request
      .post("/api/sessions/login")
      .send(userCredentials)
      .expect(200)
      .then(response => {
        chai.assert(response.text, "OK");
      });
  });

  it("POST - login - invalid - wrong password", function() {
    return request
      .post("/api/sessions/login")
      .send({ email: "admin@admin.pt", password: "wrongPassword" })
      .expect(400)
      .then(response => {
        chai.expect(response.error).to.be.not.NaN;
      });
  });

  it("POST - change password - valid", function() {
    return request
      .post("/api/sessions/changePassword")
      .send({
        currentPassword: userCredentials.password,
        password: "changePasswordTest"
      })
      .expect(200);
  });

  it("Replace password", function() {
    return request
      .post("/api/sessions/changePassword")
      .send({
        currentPassword: "changePasswordTest",
        password: userCredentials.password
      })
      .expect(200);
  });

  it("POST - block account ", function() {
    return request
      .post("/api/sessions/block")
      .send({ id: 5 })
      .expect(200);
  });

  it("POST - block account that not exists", function() {
    return request
      .post("/api/sessions/block")
      .send({ id: 1000 })
      .expect(400);
  });

  it("POST - change password - invalid - empty password", function() {
    return request
      .post("/api/sessions/changePassword")
      .send({ currentPassword: "admin123", password: "" })
      .expect(400);
  });

  it("GET - logout - valid", function() {
    return request.get("/api/sessions/logout").expect(200);
  });

  it("GET - logout - valid twice logout", function() {
    return request.get("/api/sessions/logout").expect(200);
  });
});

describe("Unit test - Employees [NoLoggedIn]", function() {
  it("POST - login blocked", function() {
    return request
      .post("/api/sessions/login")
      .send({ email: "paulaCandeias@gmail.pt", password: "admin123" })
      .expect(403);
  });

  it("POST - forgot password - valid", function() {
    return request
      .post("/api/sessions/forgotPassword")
      .send({ email: userCredentials.email })
      .expect(200);
  });

  it("POST - forgot password - invalid - nonexistent email", function() {
    return request
      .post("/api/employees/forgotPassword")
      .send({ email: "emailInexistente@qwerty.com" })
      .expect(404);
  });
});
