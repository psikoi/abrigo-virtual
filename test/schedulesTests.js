var assert = require("chai").assert;
var app = require("../server/app.js");
var session = require("supertest-session");
var request = session(app);

const adminCredentials = { email: "admin@admin.pt", password: "admin123" };
const employeeCreedentials = {
  email: "adilson@gmail.pt",
  password: "admin123"
};
const volunteerCreedentials = { email: "hugo@email.pt", password: "admin123" };
const invalidCredentials = { email: "invalid@email.pt", password: "admin123" };

describe("Unit tests - Schedule [ADMIN]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(adminCredentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("POST - create schedule - empty", function() {
    return request
      .post("/api/schedule/")
      .send([])
      .expect(400);
  });

  it("POST - create schedule - valid", function() {
    return request
      .post("/api/schedule/")
      .send([
        {
          day: 1,
          time: {
            hours: 7,
            minutes: 30
          },
          employeeId: 3
        },
        {
          day: 1,
          time: {
            hours: 8,
            minutes: 0
          },
          employeeId: 3
        },
        {
          day: 1,
          time: {
            hours: 8,
            minutes: 30
          },
          employeeId: 3
        },
        {
          day: 1,
          time: {
            hours: 9,
            minutes: 0
          },
          employeeId: 3
        }
      ])
      .expect(200);
  });

  it("POST - override/view schedule", function() {
    request.post("/api/schedule/").send([
      {
        day: 1,
        time: {
          hours: 10,
          minutes: 30
        },
        employeeId: 3
      },
      {
        day: 1,
        time: {
          hours: 11,
          minutes: 0
        },
        employeeId: 3
      },
      {
        day: 1,
        time: {
          hours: 11,
          minutes: 30
        },
        employeeId: 3
      },
      {
        day: 1,
        time: {
          hours: 12,
          minutes: 0
        },
        employeeId: 3
      }
    ]);
    return request
      .get("/api/schedule/3")
      .expect(200)
      .then(response => {
        let data = JSON.parse(response.text);
        assert.typeOf(data, "array", "Data returned is not an array");
        assert.containsAllKeys(
          data[0],
          ["day", "hours", "minutes"],
          "The returned data doesn't contain all information about the schedule."
        );
      });
  });

  it("GET - view tasks - invalid id", function() {
    return request.get("/api/schedule/200000").expect(404);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Schedule [NotLoggedIn]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(invalidCredentials)
      .expect(400)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - view schedule - invalid", function() {
    return request.get("/api/schedule/1").expect(403);
  });

  it("GET - view schedule - invalid id", function() {
    return request.get("/api/schedule/200000").expect(403);
  });

  it("POST - create schedule - invalid", function() {
    return request.post("/api/schedule/").expect(403);
  });
});

describe("Unit tests - Schedule [EMPLOYEE]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(employeeCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - view schedule - invalid", function() {
    return request.get("/api/schedule/1").expect(403);
  });

  it("GET - view schedule - valid", function() {
    return request.get("/api/schedule/2").expect(200);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Schedule [VOLUNTEER]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(volunteerCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - view schedule - invalid", function() {
    return request.get("/api/schedule/1").expect(403);
  });

  it("GET - view schedule - valid", function() {
    return request.get("/api/schedule/4").expect(200);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});
