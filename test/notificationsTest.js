const app = require("../server/app.js");
var assert = require("chai").assert;
var session = require("supertest-session");
var request = session(app);

const adminId1 = { email: "admin@admin.pt", password: "admin123" };
const employeeId2 = { email: "adilson@gmail.pt", password: "admin123" };

describe("Unit tests - Notifications - Not logged in", () => {
  it("GET - get notification by employee id - invalid permission", function() {
    return request
      .get("/api/notifications/employee/1")
      .send([])
      .expect(403);
  });

  it("PUT - read notification by id - invalid permission", function() {
    return request
      .put("/api/notifications/read/1")
      .send([])
      .expect(403);
  });
});

describe("Unit tests - Notifications - Admin (id 1)", () => {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(adminId1)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - get notification by employee id - invalid permission", function() {
    return request
      .get("/api/notifications/employee/2")
      .send([])
      .expect(403);
  });

  it("GET - get notification by employee id - valid permission", function() {
    return request
      .get("/api/notifications/employee/1")
      .send([])
      .expect(200)
      .then(response => {
        let data = JSON.parse(response.text).data;
        assert.typeOf(data, "array", "Data returned is not an array");
        assert.containsAllKeys(
          data[0],
          [
            "notificationId",
            "creationDate",
            "readDate",
            "message",
            "url",
            "employeeId"
          ],
          "The returned data doesn't contains all information about the notification."
        );
      });
  });

  it("PUT - read notification by id - invalid permission", function() {
    return request
      .put("/api/notifications/read/2")
      .send([])
      .expect(403);
  });

  it("PUT - read notification by id - valid permission", function() {
    return request
      .put("/api/notifications/read/1")
      .send([])
      .expect(200);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Notifications - Employee (id 2)", () => {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(employeeId2)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - get notification by employee id - invalid permission", function() {
    return request
      .get("/api/notifications/employee/1")
      .send([])
      .expect(403);
  });

  it("GET - get notification by employee id - valid permission", function() {
    return request
      .get("/api/notifications/employee/2")
      .send([])
      .expect(200);
  });

  it("PUT - read notification by id - invalid permission", function() {
    return request
      .put("/api/notifications/read/1")
      .send([])
      .expect(403);
  });

  it("PUT - read notification by id - valid permission", function() {
    return request
      .put("/api/notifications/read/2")
      .send([])
      .expect(200);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});
