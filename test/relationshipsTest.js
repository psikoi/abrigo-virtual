var assert = require("chai").assert;
var app = require("../server/app.js");
var session = require("supertest-session");
var request = session(app);

const adminCredentials = { email: "admin@admin.pt", password: "admin123" };
const employeeCreedentials = {
  email: "adilson@gmail.pt",
  password: "admin123"
};
const volunteerCreedentials = { email: "hugo@email.pt", password: "admin123" };
const invalidCredentials = { email: "invalid@email.pt", password: "admin123" };

function buildRelationship(animalA, animalB, value) {
  return {
    animalAId: animalA,
    animalBId: animalB,
    value: value
  };
}

describe("Unit tests - Relationships [ADMIN]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(adminCredentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("POST - create relationship - missing animal a", function() {
    return request
      .post("/api/relationships/")
      .send({ animalBId: 1, value: 1 })
      .expect(400);
  });

  it("POST - create relationship - missing animal b", function() {
    return request
      .post("/api/relationships/")
      .send({ animalAId: 1, value: 1 })
      .expect(400);
  });

  it("POST - create relationship - missing value", function() {
    return request
      .post("/api/relationships/")
      .send({ animalAId: 1, animalB: 1 })
      .expect(400);
  });

  it("POST - create relationship - undefined animal a", function() {
    return request
      .post("/api/relationships/")
      .send(buildRelationship(undefined, 1, 1))
      .expect(400);
  });

  it("POST - create relationship - undefined animal b", function() {
    return request
      .post("/api/relationships/")
      .send(buildRelationship(1, undefined, 1))
      .expect(400);
  });

  it("POST - create relationship - undefined value", function() {
    return request
      .post("/api/relationships/")
      .send(buildRelationship(1, 1, undefined))
      .expect(400);
  });

  it("POST - create relationship - animal a doesn't exist", function() {
    return request
      .post("/api/relationships/")
      .send(buildRelationship(123123, 1, 1))
      .expect(400);
  });

  it("POST - create relationship - animal b doesn't exist", function() {
    return request
      .post("/api/relationships/")
      .send(buildRelationship(1, 123123, 1))
      .expect(400);
  });

  it("POST - create relationship - value not a number", function() {
    return request
      .post("/api/relationships/")
      .send(buildRelationship(1, 123123, "1"))
      .expect(400);
  });

  it("POST - create relationship - same animal", function() {
    return request
      .post("/api/relationships/")
      .send(buildRelationship(1, 1, 1))
      .expect(400);
  });

  it("POST - create relationship - valid", function() {
    return request
      .post("/api/relationships/")
      .send(buildRelationship(1, 2, 1))
      .expect(200);
  });

  it("PUT - edit relationship - missing animal a", function() {
    return request
      .put("/api/relationships/1")
      .send({ animalBId: 1, value: 1 })
      .expect(400);
  });

  it("PUT - edit relationship - missing animal b", function() {
    return request
      .put("/api/relationships/1")
      .send({ animalAId: 1, value: 1 })
      .expect(400);
  });

  it("PUT - edit relationship - missing value", function() {
    return request
      .put("/api/relationships/1")
      .send({ animalAId: 1, animalB: 1 })
      .expect(400);
  });

  it("PUT - edit relationship - undefined animal a", function() {
    return request
      .put("/api/relationships/1")
      .send(buildRelationship(undefined, 1, 1))
      .expect(400);
  });

  it("PUT - edit relationship - undefined animal b", function() {
    return request
      .put("/api/relationships/1")
      .send(buildRelationship(1, undefined, 1))
      .expect(400);
  });

  it("PUT - edit relationship - undefined value", function() {
    return request
      .put("/api/relationships/1")
      .send(buildRelationship(1, 1, undefined))
      .expect(400);
  });

  it("PUT - edit relationship - animal a doesn't exist", function() {
    return request
      .put("/api/relationships/1")
      .send(buildRelationship(123123, 1, 1))
      .expect(400);
  });

  it("PUT - edit relationship - animal b doesn't exist", function() {
    return request
      .put("/api/relationships/1")
      .send(buildRelationship(1, 123123, 1))
      .expect(400);
  });

  it("PUT - edit relationship - value not a number", function() {
    return request
      .put("/api/relationships/1")
      .send(buildRelationship(1, 123123, "1"))
      .expect(400);
  });

  it("PUT - edit relationship - same animal", function() {
    return request
      .put("/api/relationships/1")
      .send(buildRelationship(1, 1, 1))
      .expect(400);
  });

  it("PUT - edit relationship - invalid relationship", function() {
    return request
      .put("/api/relationships/123")
      .send(buildRelationship(1, 2, 1))
      .expect(400);
  });

  it("PUT - edit relationship - valid", function() {
    return request
      .put("/api/relationships/1")
      .send(buildRelationship(3, 2, 1))
      .expect(200);
  });

  it("GET - get animal relationship - invalid animal", function() {
    return request
      .get("/api/relationships/animal/123123")
      .send()
      .expect(200)
      .then(response => {
        let data = JSON.parse(response.text).data;

        assert.typeOf(data, "array", "Data returned is not an array");
        assert.isEmpty(data, "there is no relationships for this animal");
      });
  });

  it("GET - get animal relationship - no relationships for animal", function() {
    return request
      .get("/api/relationships/animal/1")
      .send()
      .expect(200)
      .then(response => {
        let data = JSON.parse(response.text).data;

        assert.typeOf(data, "array", "Data returned is not an array");
        assert.isEmpty(data, "there is no relationships for this animal");
      });
  });

  it("GET - get animal relationship - valid", function() {
    return request
      .get("/api/relationships/animal/2")
      .send()
      .expect(200)
      .then(response => {
        let data = JSON.parse(response.text).data;
        var sampleObj = data[0];

        assert.typeOf(data, "array", "Data returned is not an array");
        assert.containsAllKeys(
          sampleObj,
          ["name", "value"],
          "Relationship doesn't contain the other animal and a value."
        );
      });
  });

  it("DELETE - delete relationship - invalid relationship", function() {
    return request
      .delete("/api/relationships/123123")
      .send()
      .expect(400);
  });

  it("DELETE - delete relationship - valid", function() {
    return request
      .delete("/api/relationships/1")
      .send()
      .expect(200);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Relationships [EMPLOYEE]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(employeeCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("POST - create relationship - invalid permission", function() {
    return request
      .post("/api/relationships/")
      .send(buildRelationship(1, 2, 1))
      .expect(403);
  });

  it("PUT - edit relationship - invalid permission", function() {
    return request
      .put("/api/relationships/1")
      .send(buildRelationship(1, 2, 1))
      .expect(403);
  });

  it("GET - get animal relationship - valid permission", function() {
    return request
      .get("/api/relationships/animal/2")
      .send()
      .expect(200);
  });

  it("DELETE - delete relationship - invalid permission", function() {
    return request
      .delete("/api/relationships/1")
      .send()
      .expect(403);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Relationships [VOLUNTEER]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(volunteerCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("POST - create relationship - invalid permission", function() {
    return request
      .post("/api/relationships/")
      .send(buildRelationship(1, 2, 1))
      .expect(403);
  });

  it("PUT - edit relationship - invalid permission", function() {
    return request
      .put("/api/relationships/1")
      .send(buildRelationship(1, 2, 1))
      .expect(403);
  });

  it("GET - get animal relationship - valid permission", function() {
    return request
      .get("/api/relationships/animal/2")
      .send()
      .expect(200);
  });

  it("DELETE - delete relationship - invalid permission", function() {
    return request
      .delete("/api/relationships/1")
      .send()
      .expect(403);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Tasks [NotLoggedIn]", function() {
  it("POST - create relationship - invalid permission", function() {
    return request
      .post("/api/relationships/")
      .send(buildRelationship(1, 2, 1))
      .expect(403);
  });

  it("PUT - edit relationship - invalid permission", function() {
    return request
      .put("/api/relationships/1")
      .send(buildRelationship(1, 2, 1))
      .expect(403);
  });

  it("GET - get animal relationship - invalid permission", function() {
    return request
      .get("/api/relationships/animal/2")
      .send()
      .expect(403);
  });

  it("DELETE - delete relationship - invalid permission", function() {
    return request
      .delete("/api/relationships/1")
      .send()
      .expect(403);
  });
});
