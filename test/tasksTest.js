var assert = require("chai").assert;
var app = require("../server/app.js");
var session = require("supertest-session");
var request = session(app);

const adminCredentials = { email: "admin@admin.pt", password: "admin123" };
const employeeCreedentials = {
  email: "adilson@gmail.pt",
  password: "admin123"
};
const volunteerCreedentials = { email: "hugo@email.pt", password: "admin123" };
const invalidCredentials = { email: "invalid@email.pt", password: "admin123" };

describe("Unit tests - Tasks [NotLoggedIn]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(invalidCredentials)
      .expect(400)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - view tasks - invalid", function() {
    return request.get("/api/tasks/").expect(403);
  });

  it("PUT - finish task - invalid", function() {
    return request.put("/api/tasks/").expect(403);
  });

  it("GET - view tasks profile - invalid", function() {
    return request.get("/api/tasks/employee/1").expect(403);
  });
});

describe("Unit tests - Tasks [ADMIN]", function() {
  const tasksId1 = [];

  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(adminCredentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - view tasks - valid", function() {
    return request
      .get("/api/tasks/")
      .expect(200)
      .then(result => {
        result.body.data.forEach(task => {
          if (task.employee.id === 1) {
            tasksId1.push(task);
          }
        });
      });
  });

  // Since admin will always have access to finish task, permissions will be tested as admin
  // Other account types will only test if they have permission to execute the action

  it("PUT - finish task - invalid status", function() {
    return request
      .put("/api/tasks/")
      .send({
        id: 1,
        status: "",
        comment: ""
      })
      .expect(400);
  });

  it("PUT - finish task - invalid comment", function() {
    return request
      .put("/api/tasks/")
      .send({
        id: 1,
        status: "completed",
        comment:
          "long comment with over 255 letters aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
      })
      .expect(400);
  });

  it("PUT - finish task - invalid id", function() {
    return request
      .put("/api/tasks/")
      .send({
        id: -1,
        status: "completed",
        comment: ""
      })
      .expect(400);
  });

  it("PUT - finish task - status not sent", function() {
    return request
      .put("/api/tasks/")
      .send({
        id: 1,
        comment: ""
      })
      .expect(400);
  });

  it("PUT - finish task - id not sent", function() {
    return request
      .put("/api/tasks/")
      .send({
        status: "completed",
        comment: ""
      })
      .expect(400);
  });

  it("PUT - finish task - id is not number", function() {
    return request
      .put("/api/tasks/")
      .send({
        id: "invalid",
        status: "completed",
        comment: ""
      })
      .expect(400);
  });

  it("PUT - finish task - valid", function() {
    return request
      .put("/api/tasks/")
      .send({
        id: 1,
        status: "completed",
        comment: "valid"
      })
      .expect(200);
  });

  it("GET- view tasks profile - valid", function() {
    return request
      .get("/api/tasks/employee/1")
      .expect(200)
      .then(result => {
        assert.equal(result.body.data.length, tasksId1.length);
      });
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Tasks [EMPLOYEE]", function() {
  const tasksId1 = [];

  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(employeeCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - view tasks - valid", function() {
    return request
      .get("/api/tasks/")
      .expect(200)
      .then(result => {
        result.body.data.forEach(task => {
          if (task.employee.id === 1) {
            tasksId1.push(task);
          }
        });
      });
  });

  it("PUT - finish task - valid", function() {
    return request
      .put("/api/tasks/")
      .send({
        id: 1,
        status: "completed",
        comment: "valid"
      })
      .expect(200);
  });

  it("GET - view tasks profile - valid", function() {
    return request
      .get("/api/tasks/employee/1")
      .expect(200)
      .then(result => {
        assert.equal(result.body.data.length, tasksId1.length);
      });
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit tests - Tasks [VOLUNTEER]", function() {
  const tasksId1 = [];

  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(volunteerCreedentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - view tasks - valid", function() {
    return request
      .get("/api/tasks/")
      .expect(200)
      .then(result => {
        result.body.data.forEach(task => {
          if (task.employee.id === 1) {
            tasksId1.push(task);
          }
        });
      });
  });

  it("PUT - finish task - valid", function() {
    return request
      .put("/api/tasks/")
      .send({
        id: 1,
        status: "completed",
        comment: "valid"
      })
      .expect(200);
  });

  it("GET - view tasks profile - valid", function() {
    return request
      .get("/api/tasks/employee/1")
      .expect(200)
      .then(result => {
        assert.equal(result.body.data.length, tasksId1.length);
      });
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});
