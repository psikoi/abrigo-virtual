var assert = require("chai").assert;
var app = require("../server/app.js");
var session = require("supertest-session");
var request = session(app);

const adminCredentials = { email: "admin@admin.pt", password: "admin123" };
const employeeCredentials = { email: "rui@email.pt", password: "admin123" };
const volunteerCredentials = { email: "hugo@email.pt", password: "admin123" };
const invalidCredentials = { email: "invalid@email.pt", password: "admin123" };

const employeeId1 = { email: "admin@admin.pt", password: "admin123" };
const employeeId2 = { email: "adilson@gmail.pt", password: "admin123" };

describe("Unit test - Employees [NoLoggedIn]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(invalidCredentials)
      .expect(400)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - View reinforcements", function() {
    return request.get("/api/reinforcements/").expect(403);
  });
});

describe("Unit tests - Employees [ADMIN]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(adminCredentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - View reinforcements", function() {
    return request
      .get("/api/reinforcements/")
      .expect(200)
      .then(response => {
        let data = JSON.parse(response.text).data;
        var sampleObj = data[0];

        assert.typeOf(data, "array", "Data returned is not an array");
        assert.containsAllKeys(
          sampleObj,
          "employee",
          "Reinforcement doesn't contain an employee."
        );
        assert.containsAllKeys(
          sampleObj.employee,
          [
            "id",
            "name",
            "email",
            "photoUrl",
            "birthdate",
            "gender",
            "contact",
            "address",
            "info",
            "type",
            "archived",
            "removed"
          ],
          "Reinforcement doesn't contain a valid employee."
        );
        assert.containsAllKeys(
          sampleObj,
          [
            "id",
            "status",
            "startDate",
            "endDate",
            "replyDate",
            "token",
            "tokenExpiration"
          ],
          "Reinforcement is not valid"
        );
      });
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit test - Employees [EMPLOYEE]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(employeeCredentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - View reinforcements", function() {
    return request.get("/api/reinforcements/").expect(403);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit test - Employees [VOLUNTEER]", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(volunteerCredentials)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - View reinforcements", function() {
    return request.get("/api/reinforcements/").expect(403);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit test - Accept Reinforcement Request - EmployeeId 1", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(employeeId1)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - View reinforcement", function() {
    return request.get("/api/reinforcements/t1").expect(200);
  });

  it("GET - View reinforcement - Invalid token", function() {
    return request.get("/api/reinforcements/t0").expect(400);
  });

  it("GET - View reinforcement - Invalid employee id", function() {
    return request.get("/api/reinforcements/2").expect(403);
  });

  it("POST - Accept reinforcement - Invalid token", function() {
    return request.post("/api/reinforcements/accept/0000000000000").expect(400);
  });

  it("POST - Accept reinforcement - Invalid employee id", function() {
    return request.post("/api/reinforcements/accept/2").expect(403);
  });

  it("POST - Accept reinforcement - valid", function() {
    return request
      .post("/api/reinforcements/accept/1")
      .send([
        {
          day: 1,
          time: {
            hours: 8,
            minutes: 30
          }
        },
        {
          day: 1,
          time: {
            hours: 9,
            minutes: 0
          }
        },
        {
          day: 1,
          time: {
            hours: 9,
            minutes: 30
          }
        }
      ])
      .expect(200);
  });

  it("POST - Accept reinforcement - already accepted", function() {
    return request.post("/api/reinforcements/accept/1").expect(400);
  });

  it("POST - Decline reinforcement - already accepted", function() {
    return request.post("/api/reinforcements/decline/1").expect(400);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});

describe("Unit test - Accept Reinforcement Request - EmployeeId 2", function() {
  before(function(done) {
    this.enableTimeouts(false);
    request
      .post("/api/sessions/login")
      .send(employeeId1)
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });

  it("GET - View reinforcement - Expired token", function() {
    return request.get("/api/reinforcements/2").expect(403);
  });

  it("POST - Accept reinforcement - Expired token", function() {
    return request.post("/api/reinforcements/accept/2").expect(403);
  });

  it("POST - Decline reinforcement - Expired token", function() {
    return request.post("/api/reinforcements/decline/2").expect(403);
  });

  after(function(done) {
    request
      .get("/api/sessions/logout")
      .send()
      .expect(200)
      .end(function(err) {
        if (err) {
          return done(err);
        }
        return done();
      });
  });
});
