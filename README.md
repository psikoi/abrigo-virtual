# Abrigo Virtual
### GP Project 18/19


<br/>


### How to run

**Run the server:**
- Press F5 to start the node server (entry-point: server/app.js) (running on localhost/3000)

**Run the client:**
- Open a new terminal
- Insert the following terminal commands:
```
cd client
npm start
```


API modules to develop:
```
animals
quarentines
employee
tasks
walks
kennel
user
shelter
relationship
```
