import React, { Component } from "react";
import Avatar from "./Avatar.js";
import AvatarCounter from "./AvatarCounter.js";

export class AvatarGroup extends Component {
  render() {
    const { animals, maxAvatars } = this.props;

    return animals.map((animal, index) => {
      if (index < maxAvatars) {
        return (
          <Avatar
            key={"e" + animal.animalId + Math.random()}
            size="30px"
            object={animal}
            zIndex={animals.length - index}
            leftMargin={index * -10}
            position={"relative"}
            imgBorder={"2px solid #FFFFFF"}
            imgBorderRadius={"20px"}
            isAnimal={true}
            notAnchor={false}
          />
        );
      } else if (index === maxAvatars) {
        return (
          <AvatarCounter
            key={"t" + animal.animalId}
            leftMargin={index * -5}
            maxAvatars={maxAvatars}
            number={animals.length - maxAvatars}
            animals={animals}
          />
        );
      } else {
        return null;
      }
    });
  }
}

export default AvatarGroup;
