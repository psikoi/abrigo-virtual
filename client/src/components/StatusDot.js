import React, { Component } from "react";

export class StatusDot extends Component {
  render() {
    return (
      <span
        style={{
          color: this.props.color,
          fontSize: "15px",
          marginRight: "7px"
        }}
      >
        &#9679;
      </span>
    );
  }
}

export default StatusDot;
