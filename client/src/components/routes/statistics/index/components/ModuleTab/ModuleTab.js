import React, { Component } from "react";
import PropTypes from "prop-types";
import "./ModuleTab.css";

export class ModuleTab extends Component {
  render() {
    return (
      <div
        className={`module-tab ${this.props.active ? "active" : ""}`}
        onClick={this.props.onClick}
      >
        <span>{this.props.title}</span>
      </div>
    );
  }
}

ModuleTab.propTypes = {
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  active: PropTypes.bool
};

export default ModuleTab;
