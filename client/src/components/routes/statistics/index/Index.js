import React, { Component } from "react";
import PageHeader from "../../../PageHeader";
import ModuleTab from "./components/ModuleTab/ModuleTab";
import Statistic from "../../../Statistic";
import ErrorPage from "../../errors/ErrorPage";
import { hasPermission } from "../../../../utils/permissions";
import { AppContext } from "../../../../contexts/AppContext";
import dataParser from "./util/dataParser.js";
import "./Index.css";

const tasksTitle = "Tarefas";
const animalsTitle = "Animais";
const quarentinesTitle = "Quarentenas";

const tasksStatistics = [
  <Statistic
    key="tasksPerStatus"
    baseUrl={`${
      ""
    }/api/statistics/tasks-per-status/`}
    title="Tarefas por estado"
    type="PieChart"
    parseData={dataParser.taskPerStatus}
  />,
  <Statistic
    key="completedTasksByEmployee"
    baseUrl={`${
      ""
    }/api/statistics/completed-tasks-by-employee/`}
    title="Tarefas concluídas por funcionário"
    type="BarChart"
    parseData={dataParser.completedTasksByEmployee}
  />,
  <Statistic
    key="pendingTasksByEmployee"
    baseUrl={`${
      ""
    }/api/statistics/pending-tasks-by-employee/`}
    title="Tarefas pendentes por funcionário"
    type="BarChart"
    parseData={dataParser.pendingTasksByEmployee}
  />
];

const animalsStatistics = [
  <Statistic
    key="animalsBySize"
    baseUrl={`${
      ""
    }/api/statistics/get-animals-by-size/`}
    title="Animais por porte"
    type="BarChart"
    parseData={dataParser.animalsBySize}
    hideDateSelector
  />,
  <Statistic
    key="animalsByAge"
    baseUrl={`${
      ""
    }/api/statistics/get-animals-by-age/`}
    title="Animais por idade"
    type="BarChart"
    parseData={dataParser.animalsByAge}
    hideDateSelector
  />,
  <Statistic
    key="animalsByRegister"
    baseUrl={`${
      ""
    }/api/statistics/get-animals-by-register-date/`}
    title="Registo de animais"
    type="LineChart"
    parseData={dataParser.animalsByRegister}
  />
];

const quarentinesStatistics = [
  <Statistic
    key="quarentinesByAnimal"
    baseUrl={`${
      ""
    }/api/statistics/get-quarentines-by-animal/`}
    title="Quarentenas por animal"
    type="BarChart"
    parseData={dataParser.quarentinesByAnimal}
  />,
  <Statistic
    key="quarentinesByRegister"
    baseUrl={`${
      ""
    }/api/statistics/get-quarentines-by-register-date/`}
    title="Registo de quarentenas"
    type="LineChart"
    parseData={dataParser.quarentinesByRegister}
  />,
  <Statistic
    key="quarentinesByCause"
    baseUrl={`${
      ""
    }/api/statistics/get-quarentines-by-cause/`}
    title="Causas das quarentenas"
    type="PieChart"
    parseData={dataParser.quarentinesByCause}
    hideDateSelector
  />
];

export class Index extends Component {
  state = {
    activeTitle: tasksTitle
  };

  componentWillMount() {
    document.body.style = "background: #eaeced !important";
  }

  getCurrentStatistics = () => {
    switch (this.state.activeTitle) {
      case tasksTitle:
        return tasksStatistics;
      case animalsTitle:
        return animalsStatistics;
      case quarentinesTitle:
        return quarentinesStatistics;
      default:
        return tasksStatistics;
    }
  };

  onTasksClick = () => {
    this.setState({
      activeTitle: tasksTitle
    });
  };

  onAnimalsClick = () => {
    this.setState({
      activeTitle: animalsTitle
    });
  };

  onQuarentinesClick = () => {
    this.setState({
      activeTitle: quarentinesTitle
    });
  };

  render() {
    if (!hasPermission(this.context, "statistics", "view")) {
      return <ErrorPage statusCode="403" />;
    }

    const { activeTitle } = this.state;

    return (
      <div className="page" style={{ background: "#eaeced" }}>
        <PageHeader title="Estatísticas" />
        <div className="module-container">
          <ul>
            <li>
              <ModuleTab
                title={tasksTitle}
                onClick={this.onTasksClick}
                active={activeTitle === tasksTitle}
              />
            </li>
            <li>
              <ModuleTab
                title={animalsTitle}
                onClick={this.onAnimalsClick}
                active={activeTitle === animalsTitle}
              />
            </li>
            <li>
              <ModuleTab
                title={quarentinesTitle}
                onClick={this.onQuarentinesClick}
                active={activeTitle === quarentinesTitle}
              />
            </li>
          </ul>
        </div>
        <div className="statistic-side">{this.getCurrentStatistics()}</div>
      </div>
    );
  }
}

Index.contextType = AppContext;

export default Index;
