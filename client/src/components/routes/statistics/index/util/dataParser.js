import { translateStatus } from "../../../../../utils/translation/task.translation";
import { translateSize } from "../../../../../utils/translation/animal.translation";
import { translateQuarentineCause } from "../../../../../utils/translation/quarentine.translation";

function taskPerStatus(data) {
  var arr = [["Tarefas", "Tarefas por estado"]];

  data.forEach(function(e) {
    var ins = [];
    ins.push(translateStatus(e.status));
    ins.push(e.count);

    arr.push(ins);
  });

  return arr;
}

function completedTasksByEmployee(data) {
  var arr = [["Tarefas", "Tarefas concluídas por funcionário"]];

  data.forEach(function(e) {
    var ins = [];
    ins.push(e.name);
    ins.push(e.count);

    arr.push(ins);
  });

  return arr;
}

function pendingTasksByEmployee(data) {
  var arr = [["Tarefas", "Tarefas pendentes por funcionário"]];

  data.forEach(function(e) {
    var ins = [];
    ins.push(e.name);
    ins.push(e.count);

    arr.push(ins);
  });

  return arr;
}

function animalsBySize(data) {
  var arr = [["Animais", "Animais por porte"]];

  data.forEach(function(e) {
    var ins = [];
    ins.push(translateSize(e));
    ins.push(e.count);

    arr.push(ins);
  });

  return arr;
}

function animalsByAge(data) {
  var arr = [["Animais", "Animais por idade"]];

  data.forEach(function(e) {
    var ins = [];
    var str;

    if (e.age === 0) {
      str = "> 1 ano";
    } else if (e.age === 1) {
      str = e.age + " ano";
    } else {
      str = e.age + " anos";
    }

    ins.push(str);
    ins.push(e.count);

    arr.push(ins);
  });

  return arr;
}

function animalsByRegister(data) {
  return byRegister(data, "Animais", "Registo de animais");
}

function quarentinesByRegister(data) {
  return byRegister(data, "Quarentenas", "Registo de quarentenas");
}

function byRegister(data, title, desc) {
  var arr = [[title, desc]];

  var objStr = "register";
  var type = data[0].type;

  if (type) {
    switch (type) {
      case "week":
        objStr = "day";
        break;
      case "month":
        objStr = "interval";
        break;
      case "year":
        objStr = "month";
        break;
      case "all":
        objStr = "year";
        break;
      default:
        break;
    }
  }

  data.forEach(function(e) {
    var ins = [];

    ins.push(e[objStr] + "");
    ins.push(e.count);

    arr.push(ins);
  });

  return arr;
}

function quarentinesByAnimal(data) {
  var arr = [["Quarentenas", "Quarentenas por animal"]];

  data.forEach(function(e) {
    var ins = [];
    ins.push(e.name);
    ins.push(e.count);

    arr.push(ins);
  });

  return arr;
}

function quarentinesByCause(data) {
  var arr = [["Quarentenas", "Causa das quarentenas"]];

  data.forEach(function(e) {
    var ins = [];
    ins.push(translateQuarentineCause(e.cause));
    ins.push(e.count);

    arr.push(ins);
  });

  return arr;
}

export default {
  taskPerStatus,
  completedTasksByEmployee,
  pendingTasksByEmployee,
  animalsBySize,
  animalsByAge,
  animalsByRegister,
  quarentinesByAnimal,
  quarentinesByRegister,
  quarentinesByCause
};
