import React, { Component } from "react";
import "../../../styles/Form.css";
import Loading from "../../../Loading";
import ErrorPage from "../../errors/ErrorPage";
import { connect } from "react-redux";
import { hasPermission } from "../../../../utils/permissions";
import { AppContext } from "../../../../contexts/AppContext";
import {
  createQuarentine,
  fetchAnimalsToQuarentine
} from "../../../../store/actions/quarentines.action";
import QuarentineForm from "../_general/QuarentineForm";

export class Create extends Component {
  state = {
    success: undefined,
    errorMessage: undefined,
    animalId: 0,
    description: "",
    cause: ""
  };

  causes = [
    { value: "sick", label: "Doença" },
    { value: "vaccinated", label: "Vacinação" },
    { value: "medicalWatch", label: "Vigia médica" },
    { value: "fight", label: "Agressividade" },
    { value: "undefined", label: "Outro" }
  ];

  componentWillMount() {
    document.body.style = "background: #FFF !important";
    this.setState({ errorMessage: null, success: null });
    this.props.fetchAnimalsToQuarentine();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ errorMessage: nextProps.error.message });
    }

    if (nextProps.success) {
      this.props.history.push("/quarentenas");
    }

    if (nextProps.availability && this.state.availabilityRequested) {
      this.setState({ availability: nextProps.availability });
    }
  }

  onSelectedAnimalsChange(selected) {
    this.setState({ animalId: selected.value });
  }

  isStringValid(str) {
    return str !== null && str !== "";
  }

  onSubmit() {
    const { animalId, description, cause } = this.state;

    if (
      this.isStringValid(description) &&
      animalId > 0 &&
      this.isStringValid(cause)
    ) {
      this.props.createQuarentine({ animalId, info: description, cause });
    } else {
      this.setState({ errorMessage: "Preencha todos os campos corretamente." });
    }
  }

  onDescriptionChange(e) {
    this.setState({ description: e.target.value });
  }

  onCauseChange(e) {
    this.setState({ cause: e.value });
  }

  render() {
    if (this.props.isLoading) {
      return <Loading />;
    }

    if (!hasPermission(this.context, "employee", "create")) {
      return <ErrorPage statusCode="403" />;
    }

    return (
      <QuarentineForm
        titlePage="Criar quarentena"
        animals={this.props.animals}
        causes={this.causes}
        errorMessage={this.state.errorMessage}
        onSelectedAnimalsChange={this.onSelectedAnimalsChange.bind(this)}
        onDescriptionChange={this.onDescriptionChange.bind(this)}
        onCauseChange={this.onCauseChange.bind(this)}
        onSubmit={this.onSubmit.bind(this)}
      />
    );
  }
}

Create.contextType = AppContext;

const mapStateToProps = ({ createQuarentine }) => ({
  success: createQuarentine && createQuarentine.success,
  error: createQuarentine && createQuarentine.error,
  isLoading: createQuarentine && createQuarentine.isLoading,
  animals: createQuarentine && createQuarentine.animals
});

export default connect(
  mapStateToProps,
  { createQuarentine, fetchAnimalsToQuarentine }
)(Create);
