import React, { Component } from "react";
import StatusDot from "../../../StatusDot";
import { convertToColor } from "../../../../utils/translation/task.translation";
import {
  translateQuarentineStatus,
  translateQuarentineCause
} from "../../../../utils/translation/quarentine.translation";
import "../../../styles/Popup.css";
import Avatar from "../../../Avatar";
import { formatDate } from "../../../../utils/dates";
import Dropdown from "../../../Dropdown";
import { hasPermission } from "../../../../utils/permissions";
import { AppContext } from "../../../../contexts/AppContext";
import FinishPopup from "../finish/FinishPopup";
import QuarentineDetails from "../details/QuarentineDetails";

export class QuarentineTable extends Component {
  state = {
    isFinishPopupOpen: false,
    quarentine: null,
    isDetailsPopupOpen: false
  };

  sameIdFinishPermission = currentUser => {
    return currentUser.type === "admin" || currentUser.type === "employee";
  };

  quarentineActionLinks(quarentine, permission) {
    var links = [
      {
        title: "Editar",
        url: "/quarentenas/editar/" + quarentine.id
      }
    ];

    if (
      permission.finish &&
      quarentine.status === "pending" &&
      this.sameIdFinishPermission(this.context.user)
    ) {
      links.push({
        title: "Terminar",
        onClick: () => this.openFinishPopup(quarentine)
      });
    }

    return links;
  }

  openFinishPopup = quarentine => {
    this.setState({
      isFinishPopupOpen: true,
      quarentine
    });
  };

  openDetailsPopup = quarentine => {
    this.setState({
      isDetailsPopupOpen: true,
      quarentine
    });
  };

  closeFinishPopup = () => {
    this.setState({
      isFinishPopupOpen: false,
      quarentine: null
    });
  };

  closeDetailsPopup(e) {
    const { className } = e.target;
    if (className === "popup-background" || className === "close") {
      this.setState({ isDetailsPopupOpen: false });
    }
  }

  finishQuarentineConfirm = () => {
    const { quarentine } = this.state;

    if (quarentine) {
      this.setState({
        newRemoveRequest: true
      });
      this.props.finishQuarentine(quarentine);
    }
  };

  updateList = () => {
    this.closeFinishPopup();
    this.props.loadQuarentines();
  };

  dateFormatter(quarentine) {
    if (quarentine.status === "pending") {
      return formatDate(new Date(quarentine.startDate));
    }
    return `${formatDate(new Date(quarentine.startDate))} até ${formatDate(
      new Date(quarentine.endDate)
    )}`;
  }

  render() {
    const { quarentines } = this.props;
    const { isFinishPopupOpen, quarentine, isDetailsPopupOpen } = this.state;

    const emptyTableWarning = (
      <tr>
        <td>Nenhum resultado encontrado</td>
      </tr>
    );

    var permission = {
      finish: false
    };

    if (hasPermission(this.context, "quarentines", "finish")) {
      permission.finish = true;
    }

    return (
      <>
        {isDetailsPopupOpen ? (
          <div
            className="popup-background"
            onClick={this.closeDetailsPopup.bind(this)}
          >
            <div className="popup quarentine-popup">
              <QuarentineDetails
                onClose={e => this.closeDetailsPopup(e)}
                quarentine={quarentine}
              />
            </div>
          </div>
        ) : null}

        <table className="table">
          <colgroup>
            {this.props.displayAvatar ? <col width="50px" /> : null}
            <col width="100px" />
            <col width="180px" />
            <col />
            <col width="120px" />
            <col width="30px" />
          </colgroup>

          <tbody>
            {quarentines.length === 0 ? emptyTableWarning : null}
            {quarentines.map(quarentine => (
              <tr
                key={quarentine.id}
                onClick={() => this.openDetailsPopup(quarentine)}
              >
                {this.props.displayAvatar ? (
                  <td>
                    <Avatar
                      size="40px"
                      object={quarentine.animal}
                      isAnimal={true}
                    />
                  </td>
                ) : null}
                <td
                  style={
                    this.props.displayAvatar ? { paddingLeft: "5px" } : null
                  }
                >
                  <b>{quarentine.animal.name}</b>
                </td>
                <td style={{ fontFamily: "Work Sans", fontSize: "13px" }}>
                  {this.dateFormatter(quarentine)}
                </td>
                <td>{translateQuarentineCause(quarentine.cause)}</td>
                <td>
                  <StatusDot color={convertToColor(quarentine.status)} />
                  {translateQuarentineStatus(quarentine.status)}
                </td>
                <td>
                  <Dropdown
                    toggle={<img src="/img/icon_more_vert.png" alt="More" />}
                    links={this.quarentineActionLinks(quarentine, permission)}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {permission.finish && this.state.isFinishPopupOpen ? (
          <FinishPopup
            quarentine={quarentine}
            isOpen={isFinishPopupOpen}
            confirmClass="btn-danger"
            confirmText="Confirmar"
            title="Terminar quarentena"
            onConfirm={this.updateList}
            onCancel={this.closeFinishPopup}
          />
        ) : null}
      </>
    );
  }
}

QuarentineTable.contextType = AppContext;

export default QuarentineTable;
