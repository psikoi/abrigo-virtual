import React, { Component } from "react";
import PageHeader from "../../../PageHeader";
import ReactTooltip from "react-tooltip";
import MessageContainer from "../../../MessageContainer";
import { withRouter } from "react-router";
import Selector from "../../../Selector";
import "../../tasks/_general/TaskForm.css";
import helper from "../../../../utils/helper";

export class QuarentineForm extends Component {
  formatAnimals(animals) {
    if (!animals) {
      return [];
    }

    return animals.map(animal => {
      return { value: animal.animalId, label: animal.name };
    });
  }

  render() {
    const {
      animals,
      onDescriptionChange,
      onSelectedAnimalsChange,
      onSubmit,
      description,
      titlePage,
      errorMessage,
      value,
      causes,
      cause,
      onCauseChange
    } = this.props;

    return (
      <div className="page">
        <PageHeader title={titlePage} />

        <MessageContainer
          message={errorMessage}
          className="alert alert-danger"
        />
        <div className="form task-form">
          <Selector
            className="task-form-select"
            options={this.formatAnimals(animals)}
            onSelectorChange={onSelectedAnimalsChange}
            placeholder="Animal"
            isSearchable={true}
            value={value}
            dataTip={helper.QUARENTINE_ANIMAL}
          />

          <Selector
            className="task-form-select"
            options={causes}
            onSelectorChange={onCauseChange}
            placeholder="Causa"
            isSearchable={true}
            value={cause}
            dataTip={helper.QUARENTINE_CAUSE}
          />

          <textarea
            onChange={onDescriptionChange}
            value={description || undefined}
            placeholder="Descrição"
            data-tip={helper.QUARENTINE_DESCRIPTION}
          />

          <div className="actions-container">
            <button className="btn-medium" onClick={this.props.history.goBack}>
              Voltar
            </button>
            <button className="btn-confirm btn-medium" onClick={onSubmit}>
              Confirmar
            </button>
          </div>
          <ReactTooltip delayShow={300} />
        </div>
      </div>
    );
  }
}

export default withRouter(QuarentineForm);
