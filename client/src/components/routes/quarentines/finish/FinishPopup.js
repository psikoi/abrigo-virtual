import React, { Component } from "react";
import { connect } from "react-redux";
import Selector from "../../../Selector";
import {
  finishQuarentine,
  clearFinishProps
} from "../../../../store/actions/quarentines.action";
import MessageContainer from "../../../MessageContainer";
import { notifySuccess } from "../../../../utils/notifications";
import "../../../styles/Popup.css";
import "./FinishPopup.css";

export class FinishPopup extends Component {
  state = {
    status: "",
    errorMessage: ""
  };

  quarentineStatus = [
    { value: "canceled", label: "Cancelada" },
    { value: "finished", label: "Concluída" }
  ];

  componentWillReceiveProps(nextProps) {
    if (nextProps.finishError) {
      this.setState({
        status: "",
        errorMessage: nextProps.finishError
      });
    } else if (nextProps.finishSuccess) {
      notifySuccess("Quarentena terminada com sucesso!");
      this.setState({
        status: ""
      });
      this.props.onConfirm();
    }
  }

  onStatusChanged = selectedOption => {
    this.setState({ status: selectedOption.value });
  };

  submit = () => {
    var reqBody = this.state;
    reqBody.id = this.props.quarentine.id;
    this.props.finishQuarentine(reqBody);
  };

  onCancel = () => {
    this.props.clearFinishProps();
    this.props.onCancel();
  };

  render() {
    return (
      <div className="finish-quarentine-popup">
        <div className="popup-background">
          <div className="popup">
            <h6>{this.props.title}</h6>

            <div className="form">
              <MessageContainer
                message={this.state.errorMessage}
                className="alert alert-danger"
              />

              <Selector
                options={this.quarentineStatus}
                placeholder="Estado"
                onSelectorChange={this.onStatusChanged}
              />

              <div className="popup-buttons">
                <button onClick={this.onCancel} style={{ marginRight: "20px" }}>
                  Cancelar
                </button>

                <button
                  onClick={this.submit}
                  className={this.props.confirmClass}
                >
                  {this.props.confirmText || "Confirmar"}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ quarentines }) => ({
  finishSuccess: quarentines && quarentines.finishSuccess,
  finishError: quarentines && quarentines.finishError
});

export default connect(
  mapStateToProps,
  { finishQuarentine, clearFinishProps }
)(FinishPopup);
