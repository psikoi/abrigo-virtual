import React, { Component } from "react";
import "../../../styles/Form.css";
import ErrorPage from "../../errors/ErrorPage";
import Loading from "../../../Loading";
import { connect } from "react-redux";
import QuarentineForm from "../_general/QuarentineForm";

import {
  editQuarentine,
  getQuarentine
} from "../../../../store/actions/quarentines.action";
import { AppContext } from "../../../../contexts/AppContext";

export class Edit extends Component {
  state = {
    success: undefined,
    errorMessage: undefined,
    animalId: 0,
    cause: "",
    description: ""
  };

  causes = [
    { value: "sick", label: "Doença" },
    { value: "vaccinated", label: "Vacinação" },
    { value: "medicalWatch", label: "Vigia médica" },
    { value: "fight", label: "Agressividade" },
    { value: "undefined", label: "Outro" }
  ];

  componentWillMount() {
    document.body.style = "background: #FFF !important";
    this.setState({ errorMessage: null, success: null });
    this.props.getQuarentine(this.props.match.params.id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ errorMessage: nextProps.error.message });
    } else if (nextProps.success) {
      this.props.history.push("/quarentenas");
    } else if (nextProps.quarentine) {
      var { animalId, cause, info } = nextProps.quarentine;
      this.setState({
        animalId,
        cause,
        description: info
      });
    }
  }

  onSelectedAnimalsChange = selectedOption => {
    this.setState({ animalId: selectedOption.value });
  };

  onCauseChange = selectedOption => {
    this.setState({ cause: selectedOption.value });
  };

  onDescriptionChange(e) {
    this.setState({ description: e.target.value });
  }

  isStringValid(str) {
    return str !== null && str !== "";
  }

  onSubmit() {
    const { animalId, description, cause } = this.state;

    if (
      this.isStringValid(description) &&
      animalId > 0 &&
      this.isStringValid(cause)
    ) {
      this.props.editQuarentine(this.props.match.params.id, {
        animalId,
        info: description,
        cause
      });
    } else {
      this.setState({ errorMessage: "Preencha todos os campos corretamente." });
    }
  }

  render() {
    const { permission, isLoading, animals, error } = this.props;
    const { errorMessage, cause, description, animalId } = this.state;

    if (permission) {
      return (
        <>
          <QuarentineForm
            titlePage="Editar quarentena"
            animals={animals}
            causes={this.causes}
            value={animalId}
            cause={cause}
            description={description}
            errorMessage={errorMessage}
            onSelectedAnimalsChange={this.onSelectedAnimalsChange.bind(this)}
            onDescriptionChange={this.onDescriptionChange.bind(this)}
            onCauseChange={this.onCauseChange.bind(this)}
            onSubmit={this.onSubmit.bind(this)}
          />
        </>
      );
    } else if (isLoading === undefined) {
      return <ErrorPage statusCode={error.code} />;
    } else {
      return <Loading />;
    }
  }
}

Edit.contextType = AppContext;

const mapStateToProps = ({ editQuarentine }) => ({
  success: editQuarentine && editQuarentine.success,
  error: editQuarentine && editQuarentine.error,
  isLoading: editQuarentine && editQuarentine.isLoading,
  permission: editQuarentine && editQuarentine.permission,
  quarentine: editQuarentine && editQuarentine.quarentine,
  animals: editQuarentine && editQuarentine.animals
});

export default connect(
  mapStateToProps,
  { editQuarentine, getQuarentine }
)(Edit);
