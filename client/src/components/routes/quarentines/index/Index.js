import React, { Component } from "react";
import { connect } from "react-redux";
import ErrorPage from "../../errors/ErrorPage";
import { Link } from "react-router-dom";
import PageHeader from "../../../PageHeader";
import Loading from "../../../Loading";

import { fetchQuarentines } from "../../../../store/actions/quarentines.action";
import { QuarentineTable } from "../_general/QuarentineTable";
import { translateQuarentineStatus } from "../../../../utils/translation/quarentine.translation";

export class Index extends Component {
  state = {
    data: [],
    isLoading: true,
    errorMessage: undefined
  };

  componentWillMount() {
    this.props.fetchQuarentines();
    document.body.style = "background: #eaeced !important";
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      const { data } = nextProps.data;
      this.setState({
        data: data,
        isLoading: false
      });
    } else if (nextProps.error) {
      this.setState({
        isLoading: false,
        errorMessage: nextProps.error
      });
    }
  }

  onSearchChanged = e => {
    this.setState({ searchQuery: e.target.value });
  };

  filteredResults(results) {
    const { searchQuery } = this.state;

    if (!searchQuery || searchQuery.length === 0) {
      return results;
    }

    const query = searchQuery.toLowerCase();

    return results.filter(result => {
      const { status } = result;
      const { name } = result.animal;
      const tStatus = translateQuarentineStatus(status);

      return (
        name.toLowerCase().includes(query) ||
        tStatus.toLowerCase().includes(query)
      );
    });
  }

  render() {
    if (this.state.isLoading) {
      return <Loading />;
    }

    if (this.state.errorMessage) {
      return <ErrorPage statusCode="403" />;
    }

    if (!this.state.isLoading && !this.state.errorMessage)
      return (
        <div className="page" style={{ background: "#eaeced" }}>
          <PageHeader title="Quarentenas" />
          <div className="search-container">
            <img src="/img/icons/search.svg" alt="" />
            <input
              onChange={this.onSearchChanged}
              type="search"
              placeholder="Pesquisar"
            />
          </div>
          <Link className="btn-confirm action-link-btn" to="/quarentenas/criar">
            Criar nova
          </Link>
          <QuarentineTable
            quarentines={this.filteredResults(this.state.data)}
            displayAvatar={true}
            loadQuarentines={this.props.fetchQuarentines}
          />
        </div>
      );
  }
}

const mapStateToProps = ({ quarentines }) => ({
  data: quarentines && quarentines.data,
  error: quarentines && quarentines.error,
  isLoading: quarentines && quarentines.isLoading
});

export default connect(
  mapStateToProps,
  { fetchQuarentines }
)(Index);
