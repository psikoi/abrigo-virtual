import React, { Component } from "react";
import "./QuarentineDetails.css";
import Avatar from "../../../Avatar";
import {
  translateQuarentineStatus,
  translateQuarentineCause
} from "../../../../utils/translation/quarentine.translation";
import { formatDate } from "../../../../utils/dates";
import { convertToColor } from "../../../../utils/translation/task.translation";

export class QuarentineDetails extends Component {
  dateFormatter(quarentine) {
    if (quarentine.status === "pending") {
      return `${formatDate(new Date(quarentine.startDate))} até ao momento`;
    }
    return `${formatDate(new Date(quarentine.startDate))} até ${formatDate(
      new Date(quarentine.endDate)
    )}`;
  }

  render() {
    const { quarentine, onClose } = this.props;
    const { animal } = quarentine;
    const color = convertToColor(quarentine.status);

    return (
      <div className="quarentine-details">
        <img
          src="/img/icons/clear.svg"
          alt="x"
          onClick={onClose}
          className="close"
        />
        <div className="top">
          <div className="animal">
            <label>Animal</label>
            <div className="avatar-span">
              <Avatar size="20px" object={animal} isAnimal={true} />
              <span style={{ marginLeft: "10px" }}>{animal.name}</span>
            </div>
          </div>
          <div className="status">
            <label>Estado</label>
            <span style={{ color: color }}>
              {translateQuarentineStatus(quarentine.status)}
            </span>
          </div>
          <div className="cause">
            <label>Causa</label>
            <span>{translateQuarentineCause(quarentine.cause)}</span>
          </div>
        </div>
        <div className="date">
          <label>Data</label>
          <span>{this.dateFormatter(quarentine)}</span>
        </div>
        <hr />
        <div className="description">
          <label>Descrição</label>
          <span>{quarentine.info ? quarentine.info : "---"}</span>
        </div>
      </div>
    );
  }
}

export default QuarentineDetails;
