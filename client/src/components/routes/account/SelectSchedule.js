import React, { Component } from "react";
import { Schedule } from "../../Schedule";
import {
  createSchedule,
  getSchedule
} from "../../../store/actions/schedule.action";
import { connect } from "react-redux";
import ErrorPage from "../errors/ErrorPage";
import "../../styles/SelectSchedule.css";
import { AppContext } from "../../../contexts/AppContext";

class SelectSchedule extends Component {
  state = {
    employeeId: this.props.employee.employeeId,
    type: this.props.employee.type,
    isAvailable: this.props.employee.type === "volunteer",
    fetchedEmployeeSchedule: []
  };

  onConfirmSelection = blocks => {
    blocks.forEach(element => {
      element.employeeId = this.state.employeeId;
    });

    this.props.createSchedule(blocks, this.state.isAvailable);
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.getSuccess) {
      this.setState({
        fetchedEmployeeSchedule: nextProps.getSuccess
      });
    }

    if (nextProps.createError) {
      this.setState({
        createError: nextProps.createError
      });
    }

    if (nextProps.createSuccess && !this.state.createSuccess) {
      this.setState({ createSuccess: true });
      this.props.getSchedule(this.state.employeeId, this.state.isAvailable);
      this.setState({ createSuccess: false });
    }

    if (nextProps.getError) {
      this.setState({
        getError: nextProps.getError
      });
    }
  }

  componentWillMount() {
    const { employeeId, isAvailable } = this.state;
    this.props.getSchedule(employeeId, isAvailable);
    document.body.style = "background: #eaeced !important";
  }

  render() {
    var error = this.state.getError;
    if (error) {
      return <ErrorPage statusCode={error.code} />;
    }

    const loggedUser = this.context.user;
    const { employeeId, type } = this.props.employee;
    const canEdit =
      (type === "employee" && loggedUser.type === "admin") ||
      ((type === "volunteer" || type === "admin") &&
        loggedUser.employeeId === employeeId);

    return (
      <div className="schedule-container">
        <Schedule
          allowCrossDay={true}
          excludeWeekend={true}
          dateLessMode={true}
          reducedBlocks={true}
          importedBlocks={this.state.fetchedEmployeeSchedule}
          onConfirmSelection={this.onConfirmSelection}
          readOnly={!canEdit}
        />
      </div>
    );
  }
}

const mapStateToProps = ({ schedule }) => ({
  createSuccess: schedule && schedule.createSuccess,
  createError: schedule && schedule.createError,
  userError: schedule && schedule.userError,
  userSuccess: schedule && schedule.userSuccess,
  getSuccess: schedule && schedule.getSuccess
});

SelectSchedule.contextType = AppContext;

export default connect(
  mapStateToProps,
  { createSchedule, getSchedule }
)(SelectSchedule);
