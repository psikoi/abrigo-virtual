import React, { Component } from "react";
import SessionForm from "./_general/SessionForm";
import SessionBanner from "./_general/SessionBanner";
import { Redirect } from "react-router-dom";
import { login } from "../../../store/actions/sessions.action";
import { connect } from "react-redux";
import { AppContext } from "../../../contexts/AppContext";
import ReactTooltip from "react-tooltip";
import helper from "../../../utils/helper";

import "./_general/SessionActions.css";

class Login extends Component {
  constructor() {
    super();

    this.state = {
      loginSuccess: false,
      isLoginLoading: false
    };
    this.message = "Bem vindo ao AbrigoVirtual";
    this.buttonText = "Registe-se";
    this.url = "/registo";
    this.title = "Iniciar sessão";
    this.onSubmit = this.onSubmitCredentials.bind(this);
    this.onEmailChanged = this.onEmailChanged.bind(this);
    this.onPasswordChanged = this.onPasswordChanged.bind(this);
    this.submitText = "Entrar";
    this.inputs = [
      {
        icon: "/img/icons/email.svg",
        type: "email",
        name: "email",
        placeholder: "Email",
        onChange: this.onEmailChanged,
        helper: helper.LOGIN_EMAIL
      },
      {
        icon: "/img/icons/password.svg",
        type: "password",
        name: "password",
        placeholder: "Palavra passe",
        onChange: this.onPasswordChanged,
        helper: helper.LOGIN_PASSWORD
      }
    ];
    this.recoverPassword = "Recuperar palavra passe";
    this.anchor = "/recuperar-palavra-passe";
    this.method = "GET";
    this.action = "/colaboradores";
  }

  onEmailChanged(evt) {
    this.setState({ email: evt.target.value });
  }

  onPasswordChanged(evt) {
    this.setState({ password: evt.target.value });
  }

  onSubmitCredentials(e) {
    e.preventDefault();
    var credentials = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.login(credentials);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      this.setState({
        loginSuccess: nextProps.loginSuccess,
        isLoginLoading: nextProps.isLoginLoading,
        error: nextProps.error
      });

      if (nextProps.loginSuccess) {
        this.context.update();
      }
    }
  }

  render() {
    // If is already logged in
    if (this.context.user) {
      return <Redirect to="/colaboradores" />;
    }

    if (this.state.loginSuccess) {
      return <Redirect to={this.action} />;
    }

    const { error } = this.state;

    return (
      <div className="page">
        <div className="session-container">
          <SessionBanner
            message={this.message}
            buttonText={this.buttonText}
            url={this.url}
          />
          <SessionForm
            title={this.title}
            onClick={this.onSubmit}
            inputs={this.inputs}
            submitText={this.submitText}
            recoverPassword={this.recoverPassword}
            anchor={this.anchor}
            method={this.method}
            action={this.action}
            isLoading={this.state.isLoginLoading}
            errorMessage={error ? error : undefined}
          />
        </div>
        <ReactTooltip delayShow={300} />
      </div>
    );
  }
}

Login.contextType = AppContext;

const mapStateToProps = ({ login }) => ({
  loginSuccess: login && login.loginSuccess,
  error: login && login.error,
  isLoginLoading: login && login.isLoginLoading
});

export default connect(
  mapStateToProps,
  { login }
)(Login);
