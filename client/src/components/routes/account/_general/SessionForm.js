import React, { Component } from "react";

class SessionForm extends Component {
  render() {
    var error = this.props.errorMessage;

    return (
      <div className={`form-container ${this.props.additionalClass}`}>
        <div className="form-content">
          <p>{this.props.title}</p>
          <label
            className="lbl-error"
            role="alert"
            style={{ display: error ? "block" : "none" }}
          >
            {error}
          </label>

          {this.props.inputs.map(input => (
            <div key={input.name} className="input-container">
              {input.icon ? <img src={input.icon} alt="" /> : null}
              <input
                type={input.type}
                name={input.name}
                placeholder={input.placeholder}
                onChange={input.onChange}
                data-tip={input.helper}
              />
            </div>
          ))}

          {this.props.anchor ? (
            <a href={this.props.anchor}>{this.props.recoverPassword}</a>
          ) : (
            ""
          )}

          <button
            className="button"
            onClick={this.props.onClick}
            disabled={this.props.isLoading}
          >
            {this.props.isLoading ? "A carregar..." : this.props.submitText}
          </button>
        </div>
      </div>
    );
  }
}

export default SessionForm;
