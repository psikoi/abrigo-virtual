import React, { Component } from "react";

class SessionBanner extends Component {
  render() {
    return (
      <div className={`session-banner ${this.props.additionalClass}`}>
        <div>
          <p>{this.props.message}</p>
          <a href={this.props.url}>{this.props.buttonText}</a>
        </div>
      </div>
    );
  }
}

export default SessionBanner;
