import React, { Component } from "react";
import SessionForm from "./_general/SessionForm";
import SessionBanner from "./_general/SessionBanner";
import { connect } from "react-redux";
import "./_general/SessionActions.css";
import { notifySuccess } from "../../../utils/notifications.js";
import { fetchForgotPassword } from "../../../store/actions/sessions.action";
import ReactTooltip from "react-tooltip";
import helper from "../../../utils/helper";

export class ForgotPassword extends Component {
  constructor() {
    super();
    this.state = {};
    this.onEmailChanged = this.onEmailChanged.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.message = "Já sei a minha palavra-passe";
    this.buttonText = "Iniciar sessão";
    this.url = "/";
    this.title = "Recuperar palavra-passe";
    this.submitText = "Confirmar";
    this.inputs = [
      {
        icon: "/img/icons/email.svg",
        type: "email",
        name: "email",
        placeholder: "Email",
        onChange: this.onEmailChanged,
        helper: helper.LOGIN_EMAIL
      }
    ];
    this.recoverPassword = "";
    this.anchor = "";
    this.method = "";
    this.action = "";
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      if (nextProps.error) {
        this.setState({ ...this.state, errorMessage: nextProps.error });
      } else if (nextProps.message) {
        this.setState({ ...this.state, errorMessage: undefined });
        notifySuccess(nextProps.message);
      }

      this.setState({ ...this.state, isLoading: nextProps.isLoading });
    }
  }

  render() {
    var error = "";

    if (this.state) {
      error = this.state.errorMessage || "";
    }

    return (
      <div className="page">
        <div className="session-container">
          <SessionBanner
            message={this.message}
            buttonText={this.buttonText}
            url={this.url}
          />
          <SessionForm
            title={this.title}
            inputs={this.inputs}
            submitText={this.submitText}
            recoverPassword={this.recoverPassword}
            anchor={this.anchor}
            method={this.method}
            action={this.action}
            onClick={this.onSubmit}
            errorMessage={error}
            isLoading={this.state.isLoading}
          />
        </div>
        <ReactTooltip delayShow={300} />
      </div>
    );
  }

  onEmailChanged(evt) {
    this.setState({ email: evt.target.value });
  }

  isEmailValid(email) {
    var regex = /.+@.+\.[A-Za-z]+$/;
    return regex.test(email);
  }

  // Event on click submit
  onSubmit = async () => {
    if (this.isEmailValid(this.state.email)) {
      await this.props.fetchForgotPassword(this.state.email);
    } else {
      this.setState({ errorMessage: "Email inválido" });
    }
  };
}

const mapStateToProps = ({ forgotPassword }) => ({
  message: forgotPassword && forgotPassword.message,
  error: forgotPassword && forgotPassword.error,
  isLoading: forgotPassword && forgotPassword.isLoading
});

export default connect(
  mapStateToProps,
  { fetchForgotPassword }
)(ForgotPassword);
