import React, { Component } from "react";
import SessionForm from "./_general/SessionForm";
import SessionBanner from "./_general/SessionBanner";
import { Redirect } from "react-router-dom";
import { register } from "../../../store/actions/shelters.action";
import { connect } from "react-redux";
import { AppContext } from "../../../contexts/AppContext";
import ReactTooltip from "react-tooltip";
import Loading from "../../Loading";
import helper from "../../../utils/helper";
import { notifySuccess } from "../../../utils/notifications.js";

import "./_general/SessionActions.css";

class Register extends Component {
  constructor() {
    super();

    this.state = {
      success: false,
      isLoading: false
    };

    this.message = "Bem vindo ao AbrigoVirtual";
    this.buttonText = "Já tem conta?";
    this.url = "/iniciar-sessao";
    this.title = "Registar albergue";
    this.submitText = "Registar";
    this.inputs = [
      {
        type: "name",
        name: "shelterName",
        placeholder: "Nome do albergue",
        onChange: this.onShelterNameChange,
        helper: helper.SHELTER_NAME
      },
      {
        type: "address",
        name: "shelterAddress",
        placeholder: "Morada do albergue",
        onChange: this.onShelterAddressChange,
        helper: helper.SHELTER_ADDRESS
      },
      {
        type: "email",
        name: "adminEmail",
        placeholder: "Email do Administrador",
        onChange: this.onAdminEmailChange,
        helper: helper.EMP_EMAIL
      },
      {
        type: "name",
        name: "adminName",
        placeholder: "Nome do Administrador",
        onChange: this.onAdminNameChange,
        helper: helper.EMP_NAME
      }
    ];
    this.method = "GET";
    this.action = "/colaboradores";
  }

  onShelterNameChange = e => {
    this.setState({ shelterName: e.target.value });
  };

  onShelterAddressChange = e => {
    this.setState({ shelterAddress: e.target.value });
  };

  onAdminEmailChange = e => {
    this.setState({ adminEmail: e.target.value });
  };

  onAdminNameChange = e => {
    this.setState({ adminName: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    var submit = {
      shelter: {
        name: this.state.shelterName,
        address: this.state.shelterAddress
      },
      user: {
        email: this.state.adminEmail,
        name: this.state.adminName
      }
    };
    this.props.register(submit);
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      this.setState({
        success: nextProps.success,
        isLoading: nextProps.isLoading,
        error: nextProps.error
      });

      if (nextProps.error) {
        this.clearInputs();
      }
    }
  }

  clearInputs = () => {
    this.setState({
      shelterName: undefined,
      shelterAddress: undefined,
      adminEmail: undefined,
      adminName: undefined
    });
  };

  render() {
    // If is already logged in
    if (this.context.user) {
      return <Redirect to="/colaboradores" />;
    }

    if (this.state.success) {
      notifySuccess(
        "Albergue criado com successo. Verifique o seu email para obter a palavra passe"
      );
      return <Redirect to="/iniciar-sessao" />;
    }

    if (this.state.isLoading) {
      return <Loading />;
    }

    const { error } = this.state;

    return (
      <div className="page">
        <div className="session-container register">
          <SessionBanner
            message={this.message}
            buttonText={this.buttonText}
            url={this.url}
            additionalClass={"register"}
          />
          <SessionForm
            title={this.title}
            onClick={this.onSubmit}
            inputs={this.inputs}
            submitText={this.submitText}
            method={this.method}
            action={this.action}
            isLoading={this.state.isLoginLoading}
            errorMessage={error ? error : undefined}
            additionalClass={"register"}
          />
        </div>
        <ReactTooltip delayShow={300} />
      </div>
    );
  }
}

Register.contextType = AppContext;

const mapStateToProps = ({ register }) => ({
  success: register && register.success,
  error: register && register.error,
  isLoading: register && register.isLoading
});

export default connect(
  mapStateToProps,
  { register }
)(Register);
