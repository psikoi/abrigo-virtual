import React, { Component } from "react";
import { connect } from "react-redux";
import "../../styles/Form.css";
import PageHeader from "../../PageHeader";
import MessageContainer from "../../MessageContainer";
import { notifySuccess } from "../../../utils/notifications.js";
import { AppContext } from "../../../contexts/AppContext";
import { fetchChangePassword } from "../../../store/actions/sessions.action";
import ReactTooltip from "react-tooltip";
import helper from "../../../utils/helper";

export class ChangePassword extends Component {
  constructor() {
    super();
    this.state = {
      errorMessage: null
    };
    this.onCurrentPasswordChanged = this.onCurrentPasswordChanged.bind(this);
    this.onNewPasswordChanged = this.onNewPasswordChanged.bind(this);
    this.onConfirmPasswordChanged = this.onConfirmPasswordChanged.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount() {
    document.body.style = "background: white !important";
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ errorMessage: nextProps.error });
    } else if (nextProps.success) {
      // Add success toast
      this.setState({ errorMessage: undefined });
      notifySuccess("Palavra-passe alterada com sucesso.");

      // Redirect to login page
      setTimeout(
        () =>
          this.props.history.push(
            "colaboradores/" + this.context.user.employeeId
          ),
        2000
      );
    }
  }

  render() {
    var error = this.state.errorMessage || "";

    return (
      <div id="change-password" className="page" style={{ width: "400px" }}>
        <PageHeader title="Alterar palavra-passe" />
        <MessageContainer message={error} className="alert alert-danger" />

        <div className="form">
          <input
            type="password"
            name="actualPassword"
            placeholder="Palavra-passe atual"
            onChange={this.onCurrentPasswordChanged}
            data-tip={helper.LOGIN_PASSWORD}
          />

          <input
            type="password"
            name="password"
            placeholder="Nova palavra-passe"
            onChange={this.onNewPasswordChanged}
            data-tip={helper.RECOVER_NEW_PASSWORD}
          />

          <input
            type="password"
            name="confirmPassword"
            placeholder="Confirmar palavra-passe"
            onChange={this.onConfirmPasswordChanged}
            data-tip={helper.RECOVER_CONFIRM_PASSWORD}
          />

          <div className="actions-container">
            <button className="btn-medium" onClick={this.props.history.goBack}>
              Voltar
            </button>
            <button className="btn-confirm btn-medium" onClick={this.onSubmit}>
              Confirmar
            </button>
          </div>
        </div>
        <ReactTooltip delayShow={300} />
      </div>
    );
  }

  onCurrentPasswordChanged(evt) {
    this.setState({ currentPassword: evt.target.value });
  }

  onNewPasswordChanged(evt) {
    this.setState({ newPassword: evt.target.value });
  }

  onConfirmPasswordChanged(evt) {
    this.setState({ confirmPassword: evt.target.value });
  }

  // Event on click submit
  onSubmit = async () => {
    if (this.allFieldsAreValid()) {
      this.errorMessage = null;

      await this.props.fetchChangePassword(
        this.state.currentPassword,
        this.state.newPassword
      );
    } else {
      this.setState({
        errorMessage: "As novas palavras-passe que inseriu não coincidem"
      });
    }
  };

  allFieldsAreValid() {
    return (
      this.state.newPassword === this.state.confirmPassword &&
      this.state.newPassword !== ""
    );
  }
}

const mapStateToProps = ({ changePassword }) => ({
  success: changePassword && changePassword.success,
  error: changePassword && changePassword.error,
  isLoading: changePassword && changePassword.isLoading
});

ChangePassword.contextType = AppContext;

export default connect(
  mapStateToProps,
  { fetchChangePassword }
)(ChangePassword);
