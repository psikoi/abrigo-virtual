import React, { Component } from "react";
import SessionForm from "./_general/SessionForm";
import SessionBanner from "./_general/SessionBanner";
import { connect } from "react-redux";
import "./_general/SessionActions.css";
import { notifySuccess } from "../../../utils/notifications.js";
import { fetchResetPassword } from "../../../store/actions/sessions.action";
import ReactTooltip from "react-tooltip";
import helper from "../../../utils/helper";

export class ResetPassword extends Component {
  constructor() {
    super();
    this.state = {
      token: window.location.search.substring(7)
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onPasswordChanged = this.onPasswordChanged.bind(this);
    this.onConfirmPasswordChanged = this.onConfirmPasswordChanged.bind(this);

    this.message = "Já sei a minha palavra-passe";
    this.buttonText = "Iniciar sessão";
    this.url = "/";
    this.title = "Alterar palavra-passe";
    this.submitText = "Confirmar";
    this.inputs = [
      {
        icon: "/img/icons/password.svg",
        type: "password",
        name: "password",
        placeholder: "Palavra-passe",
        onChange: this.onPasswordChanged,
        helper: helper.RECOVER_NEW_PASSWORD
      },
      {
        icon: "/img/icons/password.svg",
        type: "password",
        name: "passwordConfirmation",
        placeholder: "Confirmação palavra-passe",
        onChange: this.onConfirmPasswordChanged,
        helper: helper.RECOVER_CONFIRM_PASSWORD
      }
    ];
    this.recoverPassword = "";
    this.anchor = "";
    this.method = "";
    this.action = "";
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      if (nextProps.error) {
        this.setErrorMessage(nextProps.error);
      } else if (nextProps.message) {
        // Reset error message
        this.setErrorMessage(undefined);

        // Add success toast
        notifySuccess("Palavra-passe alterada com sucesso.");

        // Redirect to login page
        setTimeout(function() {
          window.location = "/iniciar-sessao";
        }, 1000);
      }
    }

    this.setState({ ...this.state, isLoading: nextProps.isLoading });
  }

  render() {
    return (
      <div className="page">
        <div className="session-container">
          <SessionBanner
            message={this.message}
            buttonText={this.buttonText}
            url={this.url}
          />
          <SessionForm
            title={this.title}
            onSubmit={this.onSubmit}
            inputs={this.inputs}
            submitText={this.submitText}
            recoverPassword={this.recoverPassword}
            anchor={this.anchor}
            method={this.method}
            action={this.action}
            onClick={this.onSubmit}
            isLoading={this.state.isLoading}
            errorMessage={this.state.errorMessage}
          />
        </div>
        <ReactTooltip delayShow={300} />
      </div>
    );
  }

  onPasswordChanged(evt) {
    this.setState({ password: evt.target.value });
  }

  onConfirmPasswordChanged(evt) {
    this.setState({ passwordConfirmation: evt.target.value });
  }

  allFieldsAreValid() {
    if (!this.state.password || !this.state.passwordConfirmation) {
      return false;
    }

    if (this.state.password === "") {
      return false;
    }

    return this.state.password === this.state.passwordConfirmation;
  }

  // Event on click submit
  onSubmit = async () => {
    if (this.allFieldsAreValid())
      await this.props.fetchResetPassword(
        this.state.token,
        this.state.password
      );
    else {
      this.setErrorMessage("Palavras-passe não correspondem");
    }
  };

  setErrorMessage(text) {
    this.setState({ ...this.state, errorMessage: text });
  }
}

const mapStateToProps = ({ resetPassword }) => ({
  message: resetPassword && resetPassword.message,
  error: resetPassword && resetPassword.error,
  isLoading: resetPassword && resetPassword.isLoading
});

export default connect(
  mapStateToProps,
  { fetchResetPassword }
)(ResetPassword);
