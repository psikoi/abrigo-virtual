import React, { Component } from "react";
import SelectSchedule from "./SelectSchedule";

export class Profile extends Component {
  render() {
    return (
      <div className="page">
        <SelectSchedule
          employee={{
            employeeId: this.props.match.params.id,
            type: "volunteer"
          }}
        />
      </div>
    );
  }
}

export default Profile;
