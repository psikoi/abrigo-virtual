import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import PageHeader from "../../../PageHeader";
import Selector from "../../../Selector";
import "../../../styles/Form.css";
import ErrorPage from "../../errors/ErrorPage";
import Loading from "../../../Loading";
import {
  editEmployee,
  getEmployee
} from "../../../../store/actions/employees.action";
import { connect } from "react-redux";
import MessageContainer from "../../../MessageContainer";
import UploadImage from "../../../UploadImage";
import { hasPermission } from "../../../../utils/permissions";
import { AppContext } from "../../../../contexts/AppContext";
import ReactTooltip from "react-tooltip";
import helper from "../../../../utils/helper";

export class Edit extends Component {
  state = {
    editSuccess: undefined,
    errorMessage: undefined,
    isLoaded: false,
    name: "",
    type: "",
    gender: "",
    email: "",
    contact: "",
    address: "",
    info: "",
    birthdate: "",
    image: ""
  };

  genders = [
    { value: "male", label: "Masculino" },
    { value: "female", label: "Feminino" },
    { value: "undefined", label: "Outro" }
  ];

  employeeTypes = [
    { value: "employee", label: "Funcionário" },
    { value: "volunteer", label: "Voluntário" },
    { value: "admin", label: "Administrador" }
  ];

  onSubmit = () => {
    const {
      name,
      type,
      gender,
      email,
      contact,
      address,
      info,
      birthdate,
      image
    } = this.state;

    this.setState({ editRequested: true });

    this.props.editEmployee({
      name,
      type,
      gender,
      email,
      contact,
      address,
      info,
      birthdate,
      employeeId: this.props.match.params.id,
      image
    });
  };

  onTypeChanged = selectedOption => {
    this.setState({ type: selectedOption.value });
  };

  onGenderChanged = selectedOption => {
    this.setState({ gender: selectedOption.value });
  };

  onNameChanged = e => {
    this.setState({ name: e.target.value });
  };

  onEmailChanged = e => {
    this.setState({ email: e.target.value });
  };

  onContactChanged = e => {
    this.setState({ contact: e.target.value });
  };

  onAddressChanged = e => {
    this.setState({ address: e.target.value });
  };

  onInfoChanged = e => {
    this.setState({ info: e.target.value });
  };

  onBirthdateChanged = e => {
    this.setState({ birthdate: e.target.value });
  };

  onImageUploaded = data => {
    this.setState({ image: data });
  };

  renderRedirect = () => {
    if (this.state.editSuccess) {
      this.setState({ isLoaded: false });
      return (
        <Redirect
          to={{
            pathname: "/colaboradores",
            state: { editEmployee: this.state.editSuccess }
          }}
        />
      );
    }
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ errorMessage: nextProps.error });
    } else if (nextProps.editSuccess && this.state.editRequested) {
      this.setState({ editSuccess: nextProps.editSuccess });
    } else if (nextProps.loadSuccess && !nextProps.isLoading) {
      this.fillField(nextProps.loadSuccess);
    }
  }

  fillField = employee => {
    this.setState({
      type: employee.type,
      gender: employee.gender,
      name: employee.name,
      email: employee.email,
      contact: employee.contact,
      address: employee.address,
      info: employee.info,
      birthdate: employee.birthdate,
      image: employee.photoUrl
    });
  };

  componentWillMount() {
    this.getEmployee(this.props.match.params.id);
  }

  getEmployee = id => {
    if (!this.state.isLoaded) {
      this.props.getEmployee(id);
      this.setState({ isLoaded: true });
    }
  };

  render() {
    let id = this.props.match.params.id;

    if (this.props.isLoading || this.props.isEmployeeLoading) {
      return <Loading />;
    }

    if (!hasPermission(this.context, "employee", "edit", id)) {
      return <ErrorPage statusCode="403" />;
    }

    return (
      <div id="create-employee" className="page" style={{ width: "500px" }}>
        {this.renderRedirect()}

        <PageHeader title="Editar colaborador" />

        <MessageContainer
          message={this.state.errorMessage}
          className="alert alert-danger"
        />
        {this.props.isLoading ? (
          <Loading />
        ) : (
          <div className="form">
            <div className="left-30">
              <UploadImage
                onImageUploaded={this.onImageUploaded}
                url={this.state.image}
                dataTip={helper.EMP_PHOTO}
              />
            </div>
            <div className="right-70">
              <input
                type="text"
                name="name"
                placeholder="Nome"
                onChange={this.onNameChanged}
                value={this.state.name}
                data-tip={helper.EMP_NAME}
              />
              <input
                onChange={this.onEmailChanged}
                type="email"
                name="email"
                placeholder="Email"
                value={this.state.email}
                data-tip={helper.EMP_EMAIL}
              />
            </div>
            <div className="form-group">
              <Selector
                className="left"
                options={this.genders}
                placeholder="Sexo"
                onSelectorChange={this.onGenderChanged}
                value={this.state.gender}
                dataTip={helper.EMP_GENDER}
              />
              <Selector
                className="right"
                options={this.employeeTypes}
                placeholder="Tipo"
                onSelectorChange={this.onTypeChanged}
                value={this.state.type}
                dataTip={helper.EMP_ROLE}
              />
            </div>

            <div className="form-group">
              <input
                className="left"
                type="tel"
                name="contact"
                placeholder="Contacto"
                onChange={this.onContactChanged}
                value={this.state.contact}
                data-tip={helper.EMP_CONTACT}
              />
              <input
                className="right"
                type="date"
                name="birthdate"
                placeholder="Data de nascimento"
                onChange={this.onBirthdateChanged}
                value={
                  this.state.birthdate ? this.state.birthdate.split("T")[0] : ""
                }
                data-tip={helper.EMP_BIRTHDATE}
              />
            </div>

            <input
              type="text"
              name="address"
              placeholder="Morada"
              onChange={this.onAddressChanged}
              value={this.state.address}
              data-tip={helper.EMP_ADDRESS}
            />

            <textarea
              onChange={this.onInfoChanged}
              placeholder="Informação adicional"
              value={this.state.info || ""}
              data-tip={helper.EMP_ADDITIONALINFO}
            />

            <div className="actions-container">
              <button
                className="btn-medium"
                onClick={this.props.history.goBack}
              >
                Voltar
              </button>
              <button
                className="btn-confirm btn-medium"
                onClick={this.onSubmit}
              >
                Confirmar
              </button>
            </div>
          </div>
        )}
        <ReactTooltip delayShow={300} />
      </div>
    );
  }
}

Edit.contextType = AppContext;

const mapStateToProps = ({ editEmployee, employee }) => ({
  editSuccess: editEmployee && editEmployee.success,
  error: editEmployee && editEmployee.error,
  isLoading:
    (editEmployee && editEmployee.isLoading) ||
    (employee && employee.isEmployeeLoading),
  loadSuccess: employee && employee.data
});

export default connect(
  mapStateToProps,
  { editEmployee, getEmployee }
)(Edit);
