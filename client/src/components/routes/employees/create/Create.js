import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import PageHeader from "../../../PageHeader";
import Selector from "../../../Selector";
import "../../../styles/Form.css";
import Loading from "../../../Loading";
import ErrorPage from "../../errors/ErrorPage";
import { createEmployee } from "../../../../store/actions/employees.action";
import { connect } from "react-redux";
import MessageContainer from "../../../MessageContainer";
import UploadImage from "../../../UploadImage";
import { hasPermission } from "../../../../utils/permissions";
import { AppContext } from "../../../../contexts/AppContext";
import ReactTooltip from "react-tooltip";
import helper from "../../../../utils/helper";

export class Create extends Component {
  state = {
    success: undefined,
    errorMessage: undefined
  };

  genders = [
    { value: "male", label: "Masculino" },
    { value: "female", label: "Feminino" },
    { value: "undefined", label: "Outro" }
  ];

  employeeTypes = [
    { value: "employee", label: "Funcionário" },
    { value: "volunteer", label: "Voluntário" },
    { value: "admin", label: "Administrador" }
  ];

  onSubmit() {
    const {
      name,
      type,
      gender,
      email,
      contact,
      address,
      info,
      birthdate,
      image
    } = this.state;
    this.props.createEmployee({
      name,
      type,
      gender,
      email,
      contact,
      address,
      info,
      birthdate,
      photoUrl: "",
      image
    });
  }

  onTypeChanged = selectedOption => {
    this.setState({ type: selectedOption.value });
  };

  onGenderChanged = selectedOption => {
    this.setState({ gender: selectedOption.value });
  };

  onNameChanged = e => {
    this.setState({ name: e.target.value });
  };

  onEmailChanged = e => {
    this.setState({ email: e.target.value });
  };

  onContactChanged = e => {
    this.setState({ contact: e.target.value });
  };

  onAddressChanged = e => {
    this.setState({ address: e.target.value });
  };

  onInfoChanged = e => {
    this.setState({ info: e.target.value });
  };

  onBirthdateChanged = e => {
    this.setState({ birthdate: e.target.value });
  };

  onImageUploaded = data => {
    this.setState({ image: data });
  };

  renderRedirect = () => {
    if (this.state.success) {
      return (
        <Redirect
          to={{
            pathname: "/colaboradores",
            state: { createEmployee: this.state.success }
          }}
        />
      );
    }
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ errorMessage: nextProps.error });
    }

    if (nextProps.success) {
      this.setState({ success: nextProps.success });
    }
  }

  componentWillMount() {
    document.body.style = "background: #FFF !important";
    // HERE
  }

  render() {
    if (this.props.isLoading) {
      return <Loading />;
    }

    if (!hasPermission(this.context, "employee", "create")) {
      return <ErrorPage statusCode="403" />;
    }

    return (
      <div id="create-employee" className="page" style={{ width: "500px" }}>
        {this.renderRedirect()}
        <PageHeader title="Criar colaborador" />

        <MessageContainer
          message={this.state.errorMessage}
          className="alert alert-danger"
        />

        <div className="form">
          <div className="left-30">
            <UploadImage
              onImageUploaded={this.onImageUploaded}
              dataTip={helper.EMP_PHOTO}
              url={this.state.image}
            />
          </div>
          <div className="right-70">
            <input
              type="text"
              name="name"
              placeholder="Nome"
              onChange={this.onNameChanged}
              data-tip={helper.EMP_NAME}
            />
            <input
              onChange={this.onEmailChanged}
              type="email"
              name="email"
              placeholder="Email"
              data-tip={helper.EMP_EMAIL}
            />
          </div>
          <div className="form-group">
            <Selector
              className="left"
              options={this.genders}
              placeholder="Sexo"
              onSelectorChange={this.onGenderChanged}
              dataTip={helper.EMP_GENDER}
            />
            <Selector
              className="right"
              options={this.employeeTypes}
              placeholder="Tipo"
              onSelectorChange={this.onTypeChanged}
              dataTip={helper.EMP_ROLE}
            />
          </div>

          <div className="form-group">
            <input
              className="left"
              type="tel"
              name="contact"
              placeholder="Contacto"
              onChange={this.onContactChanged}
              data-tip={helper.EMP_CONTACT}
            />
            <input
              className="right"
              type="date"
              name="birthdate"
              placeholder="Data de nascimento"
              onChange={this.onBirthdateChanged}
              data-tip={helper.EMP_BIRTHDATE}
            />
          </div>

          <input
            type="text"
            name="address"
            placeholder="Morada"
            onChange={this.onAddressChanged}
            data-tip={helper.EMP_ADDRESS}
          />

          <textarea
            onChange={this.onInfoChanged}
            placeholder="Informação adicional"
            data-tip={helper.EMP_ADDITIONALINFO}
          />

          <div className="actions-container">
            <button className="btn-medium" onClick={this.props.history.goBack}>
              Voltar
            </button>
            <button
              className="btn-confirm btn-medium"
              onClick={this.onSubmit.bind(this)}
            >
              Confirmar
            </button>
          </div>
        </div>
        <ReactTooltip delayShow={300} />
      </div>
    );
  }
}

Create.contextType = AppContext;

const mapStateToProps = ({ createEmployee }) => ({
  success: createEmployee && createEmployee.success,
  error: createEmployee && createEmployee.error,
  isLoading: createEmployee && createEmployee.isLoading
});

export default connect(
  mapStateToProps,
  { createEmployee }
)(Create);
