import React, { Component } from "react";
import { connect } from "react-redux";
import PageHeader from "../../../PageHeader";
import EmployeeTable from "../_general/EmployeeTable";
import Loading from "../../../Loading";
import { Link } from "react-router-dom";
import "../../../styles/ListPage.css";
import ErrorPage from "../../errors/ErrorPage";

import { fetchEmployees } from "../../../../store/actions/employees.action";
import { translateType } from "../../../../utils/translation/employee.translation";

export class Index extends Component {
  state = {
    data: [],
    isLoading: true,
    errorMessage: undefined
  };

  onSearchChanged = e => {
    this.setState({ searchQuery: e.target.value });
  };

  filteredResults(results) {
    const { searchQuery } = this.state;

    if (!searchQuery || searchQuery.length === 0) {
      return results;
    }

    const query = searchQuery.toLowerCase();

    return results.filter(result => {
      const { name } = result;
      const type = translateType(result);

      return (
        name.toLowerCase().includes(query) || type.toLowerCase().includes(query)
      );
    });
  }

  componentWillMount() {
    this.props.fetchEmployees();
    document.body.style = "background: #eaeced !important";
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      const { data } = nextProps.data;
      this.setState({
        data,
        isLoading: false
      });
    } else if (nextProps.error) {
      this.setState({
        isLoading: false,
        errorMessage: nextProps.error
      });
    }
  }

  render() {
    if (this.state.isLoading) {
      return <Loading />;
    }

    if (this.state.errorMessage) {
      return <ErrorPage statusCode="403" />;
    }

    if (!this.state.isLoading && !this.state.errorMessage)
      return (
        <div className="page" style={{ background: "#eaeced" }}>
          <PageHeader title="Colaboradores" />
          <div className="search-container">
            <img src="/img/icons/search.svg" alt="" />
            <input
              onChange={this.onSearchChanged}
              type="search"
              placeholder="Pesquisar"
            />
          </div>
          <Link
            className="btn-confirm action-link-btn"
            to="/colaboradores/criar"
          >
            Criar novo
          </Link>
          <EmployeeTable
            loadEmployees={this.props.fetchEmployees}
            employees={this.filteredResults(this.state.data)}
          />
        </div>
      );
  }
}

const mapStateToProps = ({ employees }) => ({
  data: employees && employees.data,
  error: employees && employees.error,
  isEmployeesLoading: employees && employees.isEmployeesLoading
});

export default connect(
  mapStateToProps,
  { fetchEmployees }
)(Index);
