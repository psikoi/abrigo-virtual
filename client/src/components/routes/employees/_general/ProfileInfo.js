import React, { Component } from "react";
import "../../../styles/ProfileInfo.css";
import { translateGender } from "../../../../utils/translation/employee.translation";
import { formatDateLong, formatDatetimeLong } from "../../../../utils/dates";

export class RootMenu extends Component {
  render() {
    const { employee } = this.props;
    return (
      <div className="info">
        <div>
          <section>
            <div className="info-field">
              <p>Sexo</p>
              <b>{translateGender(employee.gender)}</b>
            </div>

            <div className="info-field">
              <p>Contacto</p>
              <b className="number-format">{employee.contact}</b>
            </div>

            <div className="info-field">
              <p>Data de nascimento</p>
              <b className="number-format">
                {formatDateLong(new Date(employee.birthdate))}
              </b>
            </div>
          </section>
          <section>
            <div className="info-field">
              <p>Email</p>
              <b>{employee.email}</b>
            </div>

            <div className="info-field">
              <p>Morada</p>
              <b>{employee.address}</b>
            </div>

            <div className="info-field">
              <p>Ultima sessão</p>
              <b className="number-format">
                {formatDatetimeLong(new Date(employee.lastLogin))}
              </b>
            </div>
          </section>
        </div>
        <div>
          <p>Informação adicional</p>
          <b>{employee.info ? employee.info : "---"}</b>
        </div>
      </div>
    );
  }
}

export default RootMenu;
