import React, { Component } from "react";
import "../../../styles/ProfileCard.css";
import Dropdown from "../../../Dropdown";
import Popup from "../../../Popup";
import { connect } from "react-redux";
import {
  translateType,
  translateStatus,
  convertToColor
} from "../../../../utils/translation/employee.translation";
import Avatar from "../../../Avatar";
import { AppContext } from "../../../../contexts/AppContext";
import {
  removeEmployee,
  archiveEmployee
} from "../../../../store/actions/employees.action";
import { notifySuccess } from "../../../../utils/notifications";
import { withRouter } from "react-router";

export class ProfileCard extends Component {
  state = {
    isRemovePopupOpen: false,
    isArchivePopupOpen: false,
    employee: null
  };

  componentWillReceiveProps(nextProps) {
    const { newRemoveRequest, newArchiveRequest } = this.state;

    if (nextProps.error) {
      this.setState({ errorMessage: nextProps.error });
    }

    if (nextProps.removeEmployeeSuccess && newRemoveRequest) {
      notifySuccess(nextProps.removeEmployeeSuccess);
      setTimeout(() => {
        window.location.href = "/colaboradores";
      }, 1500);
      this.setState({
        newRemoveRequest: false
      });
    }

    if (nextProps.archiveEmployeeSuccess && newArchiveRequest) {
      notifySuccess(nextProps.archiveEmployeeSuccess);
      window.location.reload();
      this.setState({
        newArchiveRequest: false
      });
    }
  }

  userActionLinks(employee, isSelf, isAdmin) {
    var actions = [
      {
        title: "Editar",
        url: "/colaboradores/editar/" + employee.employeeId
      }
    ];

    if (isSelf) {
      actions.push({
        title: "Alterar palavra passe",
        url: "/alterar-palavra-passe"
      });
    }

    if (isAdmin) {
      actions.push({
        title: employee.archived ? "Reativar" : "Arquivar",
        onClick: () => this.openArchivePopup(employee)
      });
      actions.push({
        title: "Remover",
        onClick: () => this.openRemovePopup(employee)
      });
    }

    return actions;
  }

  openRemovePopup = employee => {
    this.setState({
      isRemovePopupOpen: true,
      employee: employee
    });
  };

  openArchivePopup = employee => {
    this.setState({
      isArchivePopupOpen: true,
      employee: employee
    });
  };

  closeRemovePopup = () => {
    this.setState({
      isRemovePopupOpen: false,
      employee: null
    });
  };

  closeArchivePopup = () => {
    this.setState({
      isArchivePopupOpen: false,
      employee: null
    });
  };

  removeEmployeeConfirm = () => {
    const { employee } = this.state;

    if (employee) {
      this.setState({
        newRemoveRequest: true
      });
      this.props.removeEmployee(employee.employeeId);
    }
  };

  archiveEmployeeConfirm = archive => {
    const { employee } = this.state;

    if (employee) {
      this.setState({
        newArchiveRequest: true
      });
      this.props.archiveEmployee(employee.employeeId, archive);
    }
  };

  render() {
    const { employee } = this.props;
    const { isRemovePopupOpen, isArchivePopupOpen } = this.state;
    const toArchived = employee && !employee.archived;
    const color = convertToColor(employee);

    const loggedUser = this.context.user;

    const actions = this.userActionLinks(
      employee,
      loggedUser.employeeId === employee.employeeId,
      loggedUser.type === "admin"
    );

    return (
      <>
        <div className="card-container">
          <Avatar size="85px" object={employee} notAnchor={true} />

          <Dropdown
            toggle={<img src="/img/icon_more_vert.png" alt="More" />}
            links={actions}
          />

          <div>
            <b>{employee.name}</b>
            <p>{translateType(employee)}</p>
          </div>

          <div>
            <span
              className="status"
              style={{ borderColor: color, color: color }}
            >
              <b>{translateStatus(employee)}</b>
            </span>
          </div>
        </div>
        <Popup
          isOpen={isRemovePopupOpen}
          confirmClass="btn-danger"
          confirmText="Remover"
          title={`Tem a certeza que pretende remover o utilizador ${
            employee ? employee.name : ""
          } ?`}
          onConfirm={this.removeEmployeeConfirm}
          onCancel={this.closeRemovePopup}
        />
        <Popup
          isOpen={isArchivePopupOpen}
          confirmClass={toArchived ? "btn-danger" : "btn-confirm"}
          confirmText={toArchived ? "Arquivar" : "Reativar"}
          title={`Tem a certeza que pretende ${
            toArchived ? "arquivar" : "reativar"
          } o utilizador ${employee ? employee.name : ""} ?`}
          onConfirm={() => this.archiveEmployeeConfirm(toArchived)}
          onCancel={this.closeArchivePopup}
        />
      </>
    );
  }
}

ProfileCard.contextType = AppContext;

const mapStateToProps = ({ removeEmployee, archiveEmployee }) => ({
  removeEmployeeSuccess: removeEmployee && removeEmployee.success,
  error: removeEmployee && removeEmployee.error,
  isRemoveEmployeeLoading:
    removeEmployee && removeEmployee.isRemoveEmployeeLoading,
  isArchiveEmployeeLoading:
    archiveEmployee && archiveEmployee.isArchiveEmployeeLoading,
  archiveEmployeeSuccess: archiveEmployee && archiveEmployee.success
});

export default withRouter(
  connect(
    mapStateToProps,
    { removeEmployee, archiveEmployee }
  )(ProfileCard)
);
