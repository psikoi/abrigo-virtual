import React, { Component } from "react";
import "../../../styles/TabMenu.css";
import Dropdown from "../../../Dropdown";

export class TabMenu extends Component {
  render() {
    const { notifications } = this.props;

    var notificationBadge;

    if (notifications) {
      const unreadNotifications = notifications.filter(e => !e.readDate);
      if (unreadNotifications.length > 0) {
        notificationBadge = (
          <div className="notifications-badge">
            {unreadNotifications.length}
          </div>
        );
      }
    }

    return (
      <div className="tab-menu">
        <ul>
          {this.props.items.map(item => (
            <li
              key={`rm${item.title}`}
              className={item.active ? "active" : ""}
              onClick={item.onClick}
            >
              {item.title}
              {item.title === "Notificações" ? notificationBadge : null}
            </li>
          ))}
          <li>
            <Dropdown
              toggle={<img src="/img/icon_more_vert.png" alt="More" />}
              links={this.props.items}
            />
          </li>
        </ul>
      </div>
    );
  }
}

export default TabMenu;
