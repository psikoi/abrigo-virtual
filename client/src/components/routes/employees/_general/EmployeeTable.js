import React, { Component } from "react";
import "../../../styles/Table.css";
import Avatar from "../../../Avatar";
import StatusDot from "../../../StatusDot";
import { Link } from "react-router-dom";
import Dropdown from "../../../Dropdown";
import { connect } from "react-redux";
import Popup from "../../../Popup";
import {
  translateType,
  convertToColor,
  translateStatus
} from "../../../../utils/translation/employee.translation";
import {
  removeEmployee,
  archiveEmployee
} from "../../../../store/actions/employees.action";
import { notifySuccess } from "../../../../utils/notifications";
import { withRouter } from "react-router";

export class EmployeeTable extends Component {
  state = {
    isRemovePopupOpen: false,
    isArchivePopupOpen: false,
    employee: null,
    newRemoveRequest: false,
    newArchiveRequest: false
  };

  openRemovePopup = employee => {
    this.setState({
      isRemovePopupOpen: true,
      employee: employee
    });
  };

  openArchivePopup = employee => {
    this.setState({
      isArchivePopupOpen: true,
      employee: employee
    });
  };

  closeRemovePopup = () => {
    this.setState({
      isRemovePopupOpen: false,
      employee: null
    });
  };

  closeArchivePopup = () => {
    this.setState({
      isArchivePopupOpen: false,
      employee: null
    });
  };

  userProfileLink(employee) {
    return `/colaboradores/${employee.employeeId}`;
  }

  userActionLinks(employee) {
    return [
      {
        title: "Ver perfil",
        url: "/colaboradores/" + employee.employeeId
      },
      {
        title: "Editar",
        url: "/colaboradores/editar/" + employee.employeeId
      },
      {
        title: employee.archived ? "Reativar" : "Arquivar",
        onClick: () => this.openArchivePopup(employee)
      },
      {
        title: "Remover",
        onClick: () => this.openRemovePopup(employee)
      }
    ];
  }

  removeEmployeeConfirm = () => {
    const { employee } = this.state;

    if (employee) {
      this.setState({
        newRemoveRequest: true
      });
      this.props.removeEmployee(employee.employeeId);
    }
  };

  archiveEmployeeConfirm = archive => {
    const { employee } = this.state;

    if (employee) {
      this.setState({
        newArchiveRequest: true
      });
      this.props.archiveEmployee(employee.employeeId, archive);
    }
  };

  componentWillReceiveProps(nextProps) {
    const { newRemoveRequest, newArchiveRequest } = this.state;

    if (nextProps.error) {
      this.setState({ errorMessage: nextProps.error });
    }

    if (nextProps.removeEmployeeSuccess && newRemoveRequest) {
      notifySuccess(nextProps.removeEmployeeSuccess);
      this.props.loadEmployees();
      this.setState({
        newRemoveRequest: false
      });
    }

    if (nextProps.archiveEmployeeSuccess && newArchiveRequest) {
      notifySuccess(nextProps.archiveEmployeeSuccess);
      this.props.loadEmployees();
      this.setState({
        newArchiveRequest: false
      });
    }
  }

  render() {
    const { isRemovePopupOpen, employee, isArchivePopupOpen } = this.state;
    const toArchived = employee && !employee.archived;

    // Sort the data by descending id
    const employees = this.props.employees.sort((a, b) => {
      return b.employeeId - a.employeeId;
    });

    const emptyTableWarning = (
      <tr>
        <td>Nenhum resultado encontrado</td>
      </tr>
    );

    return (
      <>
        <table className="table">
          <colgroup>
            <col width="60px" />
            <col width="30%" />
            <col />
            <col />
            <col width="20px" />
          </colgroup>
          <tbody>
            {employees.length === 0 ? emptyTableWarning : null}
            {employees.map(employee => (
              <tr key={employee.employeeId}>
                <td>
                  <Avatar size="40px" object={employee} />
                </td>
                <td>
                  <Link to={this.userProfileLink(employee)}>
                    <b>{employee.name}</b>
                  </Link>
                </td>
                <td>{translateType(employee)}</td>
                <td>
                  <StatusDot color={convertToColor(employee)} />
                  {translateStatus(employee)}
                </td>
                <td>
                  <Dropdown
                    toggle={<img src="/img/icon_more_vert.png" alt="More" />}
                    links={this.userActionLinks(employee)}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <Popup
          isOpen={isRemovePopupOpen}
          confirmClass="btn-danger"
          confirmText="Remover"
          title={`Tem a certeza que pretende remover o utilizador ${
            employee ? employee.name : ""
          } ?`}
          onConfirm={this.removeEmployeeConfirm}
          onCancel={this.closeRemovePopup}
        />
        <Popup
          isOpen={isArchivePopupOpen}
          confirmClass={toArchived ? "btn-danger" : "btn-confirm"}
          confirmText={toArchived ? "Arquivar" : "Reativar"}
          title={`Tem a certeza que pretende ${
            toArchived ? "arquivar" : "reativar"
          } o utilizador ${employee ? employee.name : ""} ?`}
          onConfirm={() => this.archiveEmployeeConfirm(toArchived)}
          onCancel={this.closeArchivePopup}
        />
      </>
    );
  }
}

const mapStateToProps = ({ removeEmployee, archiveEmployee }) => ({
  removeEmployeeSuccess: removeEmployee && removeEmployee.success,
  error: removeEmployee && removeEmployee.error,
  isRemoveEmployeeLoading:
    removeEmployee && removeEmployee.isRemoveEmployeeLoading,
  isArchiveEmployeeLoading:
    archiveEmployee && archiveEmployee.isArchiveEmployeeLoading,
  archiveEmployeeSuccess: archiveEmployee && archiveEmployee.success
});

export default withRouter(
  connect(
    mapStateToProps,
    { removeEmployee, archiveEmployee }
  )(EmployeeTable)
);
