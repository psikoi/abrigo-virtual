import React, { Component } from "react";
import { connect } from "react-redux";
import { readNotification } from "../../../../../store/actions/notifications.action";

import "./Notification.css";

export class Notification extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.readSuccess && this.props.onRead) {
      this.props.onRead();
    }
  }

  onClick = () => {
    this.props.readNotification(this.props.notificationId);
  };

  render() {
    var readClass;
    if (this.props.read) {
      readClass = "read";
    }

    return (
      <div className={`notification ${readClass}`} onClick={this.onClick}>
        {this.props.text}
      </div>
    );
  }
}

const mapStateToProps = ({ readNotifications }) => ({
  readSuccess: readNotifications && readNotifications.readSuccess,
  readError: readNotifications && readNotifications.readError
});

export default connect(
  mapStateToProps,
  { readNotification }
)(Notification);
