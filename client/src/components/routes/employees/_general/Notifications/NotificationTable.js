import React, { Component } from "react";
import { connect } from "react-redux";
import Notification from "./Notification";
import { readAllNotifications } from "../../../../../store/actions/notifications.action";

export class NotificationTable extends Component {
  notificationIds = [];

  componentWillUnmount() {
    this.readAll();
  }

  handleData = (notifications, filterRead) => {
    if (!notifications) {
      return null;
    }

    return notifications
      .filter(e => (filterRead && !e.readDate) || (!filterRead && e.readDate))
      .map(e => {
        this.notificationIds.push(e.notificationId);

        return (
          <Notification
            key={e.notificationId}
            notificationId={e.notificationId}
            text={e.message}
            onRead={this.props.onRead}
          />
        );
      });
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.readAllSuccess && this.props.onRead) {
      this.props.onRead();
    }
  }

  readAll = () => {
    const body = this.notificationIds.map(e => {
      return { id: e };
    });
    this.props.readAllNotifications(body);
  };

  render() {
    const { notifications, notificationsError } = this.props;

    const readNotifications = this.handleData(notifications, false);
    const unreadNotifications = this.handleData(notifications, true);
    if (notificationsError && !notifications) {
      return (
        <div className="info" style={{ padding: "10px 20px" }}>
          {notificationsError}
        </div>
      );
    } else {
      return (
        <div>
          {unreadNotifications}
          <div className="table-section" style={{ marginTop: "20px" }}>
            {!readNotifications || readNotifications.length === 0
              ? ""
              : "Notificações lidas"}
          </div>
          {readNotifications}
        </div>
      );
    }
  }
}

const mapStateToProps = ({ readNotifications }) => ({
  readAllSuccess: readNotifications && readNotifications.readAllSuccess,
  readAllError: readNotifications && readNotifications.readAllError
});

export default connect(
  mapStateToProps,
  { readAllNotifications }
)(NotificationTable);
