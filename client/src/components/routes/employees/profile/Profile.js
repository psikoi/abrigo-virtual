import React, { Component } from "react";
import { connect } from "react-redux";
import Loading from "../../../Loading";
import ErrorPage from "../../errors/ErrorPage";
import ProfileCard from "../_general/ProfileCard";
import TabMenu from "../_general/TabMenu";
import ProfileInfo from "../_general/ProfileInfo";
import TaskTable from "../../tasks/_general/TaskTable";
import NotificationTable from "../_general/Notifications/NotificationTable";
import "./Profile.css";
import SelectSchedule from "../../account/SelectSchedule";

import { getEmployee } from "../../../../store/actions/employees.action";
import { fetchEmployeeTasks } from "../../../../store/actions/tasks.action";
import { getNotifications } from "../../../../store/actions/notifications.action";

export class Profile extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      items: [
        {
          title: "Informação",
          active: true,
          onClick: () => this.setActive(0)
        },
        {
          title: "Horário",
          active: false,
          onClick: () => this.setActive(1)
        },
        {
          title: "Tarefas",
          active: false,
          onClick: () => this.setActive(2)
        },
        {
          title: "Notificações",
          active: false,
          onClick: () => this.setActive(3)
        }
      ]
    };
  }

  componentWillMount() {
    document.body.style = "background: #eaeced !important";
    let id = this.props.match.params.id;
    this.fetchEmployee(id);
  }

  fetchEmployee(id) {
    this.props.getEmployee(id);
    this.props.fetchEmployeeTasks(id);
    this.props.getNotifications(id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      let id = nextProps.match.params.id;
      if (id !== nextProps.data.employeeId.toString()) {
        this.fetchEmployee(id);
      } else this.setState({ employee: nextProps.data, isLoading: false });
    }

    if (nextProps.tasks) {
      this.setState({ tasks: nextProps.tasks });
    }
    if (nextProps.error) {
      this.setState({ error: nextProps.error, isLoading: false });
    }

    if (nextProps.notifications) {
      this.setState({ notifications: nextProps.notifications.data });
    } else if (nextProps.notificationsError) {
      this.setState({
        notifications: undefined,
        notificationsError: nextProps.notificationsError
      });
    }
  }

  render() {
    if (this.state.isLoading) {
      return <Loading />;
    }

    if (this.state.error) {
      return <ErrorPage statusCode={this.state.error.code} />;
    }

    if (this.state.employee) {
      return (
        <div className="page" style={{ width: "850px" }}>
          <div className="profile-info">
            <ProfileCard employee={this.state.employee} />
            <div className="main">
              <TabMenu
                items={this.state.items}
                notifications={this.state.notifications}
              />

              {this.getActiveContent()}
            </div>
          </div>
        </div>
      );
    }
  }

  setActive(i) {
    let items = this.state.items;
    if (!items[i].active) {
      items.forEach((item, index) => {
        if (index === i) {
          item.active = true;
        } else {
          item.active = false;
        }
      });
    }
    this.setState({ items: items });
  }

  refreshNotifications = () => {
    this.props.getNotifications(this.props.match.params.id);
  };

  getActiveContent() {
    const { employee } = this.state;
    if (this.state.items[0].active) {
      return <ProfileInfo employee={this.state.employee} />;
    } else if (this.state.items[1].active) {
      return <SelectSchedule employee={employee} />;
    } else if (this.state.items[2].active) {
      return (
        <TaskTable
          tasks={this.state.tasks}
          displayAvatar={false}
          className="profile-task"
        />
      );
    } else if (this.state.items[3].active) {
      return (
        <NotificationTable
          notifications={this.state.notifications}
          onRead={this.refreshNotifications}
          notificationsError={this.state.notificationsError}
        />
      );
    }
  }
}

const mapStateToProps = ({ employee, employeeTasks, notifications }) => ({
  data: employee && employee.data,
  error: employee && employee.error,
  isLoading: employee && employee.isLoading,
  tasks: employeeTasks && employeeTasks.tasks,
  notifications: notifications && notifications.notifications,
  notificationsError: notifications && notifications.notificationsError
});

export default connect(
  mapStateToProps,
  { getEmployee, fetchEmployeeTasks, getNotifications }
)(Profile);
