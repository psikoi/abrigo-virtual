import React, { Component } from "react";
import { connect } from "react-redux";
import PageHeader from "../../../PageHeader";
import TaskTable from "../_general/TaskTable";
import Loading from "../../../Loading";
import { Link } from "react-router-dom";
import ErrorPage from "../../errors/ErrorPage";
import "../../../styles/ListPage.css";

import { fetchTasks } from "../../../../store/actions/tasks.action";
import { translateStatus } from "../../../../utils/translation/task.translation";

export class Index extends Component {
  state = {
    data: [],
    isLoading: true,
    errorMessage: undefined
  };

  componentWillMount() {
    this.props.fetchTasks();
    document.body.style = "background: #eaeced !important";
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      const { data } = nextProps.data;
      this.setState({
        data: data,
        isLoading: false
      });
    } else if (nextProps.error) {
      this.setState({
        isLoading: false,
        errorMessage: nextProps.error
      });
    }
  }

  onSearchChanged = e => {
    this.setState({ searchQuery: e.target.value });
  };

  filteredResults(results) {
    const { searchQuery } = this.state;

    if (!searchQuery || searchQuery.length === 0) {
      return results;
    }

    const query = searchQuery.toLowerCase();

    return results.filter(result => {
      const { title, status } = result;
      const tStatus = translateStatus(status);

      return (
        title.toLowerCase().includes(query) ||
        tStatus.toLowerCase().includes(query)
      );
    });
  }

  render() {
    if (this.state.isLoading) {
      return <Loading />;
    }

    if (this.state.errorMessage) {
      return <ErrorPage statusCode="403" />;
    }

    if (!this.state.isLoading && !this.state.errorMessage)
      return (
        <div className="page" style={{ background: "#eaeced" }}>
          <PageHeader title="Tarefas" />
          <div className="search-container">
            <img src="/img/icons/search.svg" alt="" />
            <input
              onChange={this.onSearchChanged}
              type="search"
              placeholder="Pesquisar"
            />
          </div>
          <Link className="btn-confirm action-link-btn" to="/tarefas/criar">
            Criar nova
          </Link>
          <TaskTable
            tasks={this.filteredResults(this.state.data)}
            loadTasks={this.props.fetchTasks}
            displayAvatar={true}
          />
        </div>
      );
  }
}

const mapStateToProps = ({ tasks }) => ({
  data: tasks && tasks.data,
  error: tasks && tasks.error,
  isLoading: tasks && tasks.isLoading
});

export default connect(
  mapStateToProps,
  { fetchTasks }
)(Index);
