import React, { Component } from "react";
import PageHeader from "../../../PageHeader";
import MessageContainer from "../../../MessageContainer";
import { withRouter } from "react-router";
import Schedule from "../../../Schedule";
import Selector from "../../../Selector";
import "./TaskForm.css";
import { appendZero } from "../../../../utils/general";

export class TaskForm extends Component {
  state = {
    showSchedule: false,
    schedulelabelText: null
  };

  getSchedule() {
    var { importedStaticBlocks, startDate, endDate } = this.props;
    var selectedBlocks = this.convertDatesToBlock(startDate, endDate);

    if (importedStaticBlocks) {
      importedStaticBlocks = this.convertStrToDate(importedStaticBlocks);
    }

    if (this.state.showSchedule) {
      return (
        <div
          className="popup-background task-form"
          onClick={e => this.displaySchedule(e)}
        >
          <div className="popup">
            <button type="button" className="close" aria-label="Close">
              <span aria-hidden="true">Fechar</span>
            </button>
            <Schedule
              maxHeight="calc(100vh - 280px)"
              returnDays={false}
              excludeWeekend={false}
              reducedBlocks={true}
              onConfirmSelection={this.onConfirmSelection}
              importedStaticBlocks={importedStaticBlocks}
              blockPastDates={true}
              baseDay={startDate ? new Date(startDate) : undefined}
              importedBlocks={
                selectedBlocks.length > 0 ? selectedBlocks : undefined
              }
            />
          </div>
        </div>
      );
    } else {
      return null;
    }
  }

  convertStrToDate(blocks) {
    if (!blocks) {
      return null;
    }
    return blocks.map(block => {
      if (block.date.length > 1) {
        return {
          date: new Date(block.date),
          hours: block.hours,
          minutes: block.minutes
        };
      } else {
        return block;
      }
    });
  }

  componentWillMount() {
    const { importedBlocks, startDate, endDate } = this.props;

    if (startDate && endDate) {
      this.setScheduleText(this.convertDatesToBlock(startDate, endDate));
    } else if (importedBlocks) {
      this.setScheduleText(importedBlocks);
    } else {
      this.setState({ allowChangeScheduleText: true });
    }
  }

  componentWillReceiveProps(nextProps) {
    const { startDate, endDate } = nextProps;

    if (this.state.allowChangeScheduleText) {
      this.setScheduleText(this.convertDatesToBlock(startDate, endDate));
      this.setState({ allowChangeScheduleText: false });
    }
  }

  componentWillUnmount() {
    this.setState({ allowChangeScheduleText: false });
  }

  onConfirmSelection = async blocks => {
    this.setState({ showSchedule: false });

    this.setScheduleText(blocks);

    this.props.onConfirmSelection(
      this.getScheduleStartDate(blocks),
      this.getScheduleEndDate(blocks)
    );
  };

  convertDatesToBlock(startDate, endDate) {
    var blocks = [];
    var date = new Date(startDate);
    endDate = new Date(endDate);

    for (date; date < endDate; date = new Date(date.getTime() + 30 * 60000)) {
      blocks.push({
        date: date,
        hours: date.getHours(),
        minutes: date.getMinutes()
      });
    }

    date = new Date(endDate);

    return blocks;
  }

  setScheduleText(blocks) {
    if (blocks && blocks.length > 0) {
      this.setState({ schedulelabelText: this.getFormattedDate(blocks) });
    }
  }

  getScheduleStartDate(blocks) {
    let block = blocks[0];
    let date = block.date;
    block = block.time || block;

    return new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      block.hours,
      block.minutes
    );
  }

  getScheduleEndDate(blocks) {
    let block = blocks[blocks.length - 1];
    let date = block.date;
    block = block.time || block;

    return new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      block.hours,
      block.minutes + 30
    );
  }

  getFormattedDate(blocks) {
    let startDate = this.getScheduleStartDate(blocks);
    let endDate = this.getScheduleEndDate(blocks);

    // Start date
    let day = appendZero(startDate.getDate());
    let month = appendZero(startDate.getMonth() + 1);
    let year = startDate.getFullYear();
    let hour = appendZero(startDate.getHours());
    let minutes = appendZero(startDate.getMinutes());

    // End hour
    let endHour = appendZero(endDate.getHours());
    let endMinutes = appendZero(endDate.getMinutes());

    if (year === new Date().getFullYear())
      return `${day}/${month}, ${hour}:${minutes} - ${endHour}:${endMinutes}`;
    else {
      return `${day}/${month}/${year}, ${hour}:${minutes} - ${endHour}:${endMinutes}`;
    }
  }

  formatEmployee(employees) {
    if (!employees) {
      return [];
    }

    return employees.map(employee => {
      return { value: employee.employeeId, label: employee.name };
    });
  }

  displaySchedule(e) {
    if (
      e.target.className === "popup-background task-form" ||
      e.target.tagName === "SPAN" ||
      e.target.tagName === "LABEL" ||
      e.target.id === "cancel"
    )
      this.setState({ showSchedule: !this.state.showSchedule });
  }

  render() {
    const {
      employees,
      onSelectedEmployeesChange,
      onTitleChange,
      onDescriptionChange,
      onSubmit,
      title,
      description,
      titlePage,
      errorMessage,
      value
    } = this.props;

    return (
      <div className="page">
        <PageHeader title={titlePage} />

        <MessageContainer
          message={errorMessage}
          className="alert alert-danger"
        />
        {this.getSchedule()}
        <div className="form task-form">
          <input
            type="text"
            name="title"
            placeholder="Título"
            onChange={onTitleChange}
            value={title}
            data-tip={this.props.helper.TASK_TITLE}
          />

          <textarea
            onChange={onDescriptionChange}
            value={description}
            placeholder="Descrição"
            data-tip={this.props.helper.TASK_DESCRIPTION}
          />

          <Selector
            className="task-form-select"
            options={this.formatEmployee(employees)}
            placeholder="Colaborador"
            onSelectorChange={onSelectedEmployeesChange}
            isSearchable={true}
            value={value}
            dataTip={this.props.helper.TASK_EMPLOYEE}
          />

          <label
            className={this.state.schedulelabelText ? "lbl-text" : ""}
            onClick={this.displaySchedule.bind(this)}
            data-tip={this.props.helper.TASK_DATE}
          >
            {this.state.schedulelabelText
              ? this.state.schedulelabelText
              : "DATA"}
          </label>
          <div className="actions-container">
            <button className="btn-medium" onClick={this.props.history.goBack}>
              Voltar
            </button>
            <button className="btn-confirm btn-medium" onClick={onSubmit}>
              Confirmar
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(TaskForm);
