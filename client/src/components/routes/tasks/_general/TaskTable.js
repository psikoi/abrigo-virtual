import React, { Component } from "react";
import StatusDot from "../../../StatusDot";
import Dropdown from "../../../Dropdown";
import {
  translateStatus,
  convertToColor
} from "../../../../utils/translation/task.translation";
import { appendZero } from "../../../../utils/general";
import FinishPopup from "../finish/FinishPopup";
import { hasPermission } from "../../../../utils/permissions";
import { AppContext } from "../../../../contexts/AppContext";
import TaskDetails from "../details/TaskDetails";
import "../../../styles/Popup.css";
import "../details/TaskDetails.css";
import Avatar from "../../../Avatar";

export class TaskTable extends Component {
  state = {
    isFinishPopupOpen: false,
    selectedTask: null,
    taskDetails: undefined
  };

  closeDetailsPopup() {
    this.setState({ taskDetails: undefined });
  }

  openFinishPopup = task => {
    this.setState({
      isFinishPopupOpen: true,
      selectedTask: task
    });
  };

  closeFinishPopup = () => {
    this.setState({
      isFinishPopupOpen: false
    });
  };

  convertDate(strDate) {
    var date = new Date(strDate);

    let day = appendZero(date.getDate());
    let month = appendZero(date.getMonth() + 1);
    let hours = appendZero(date.getHours());
    let minutes = appendZero(date.getMinutes());

    let finalDate = `${day}/${month}`;
    return (finalDate += ` às ${hours}:${minutes}`);
  }

  sameIdFinishPermission = (taskEmployeeId, currentUser) => {
    return (
      currentUser.type === "admin" || taskEmployeeId === currentUser.employeeId
    );
  };

  taskActionLinks(task, permission) {
    var links = [
      {
        title: "Editar",
        url: "/tarefas/editar/" + task.id
      }
    ];

    if (
      permission.finish &&
      task.status === "pending" &&
      this.sameIdFinishPermission(task.employee.id, this.context.user)
    ) {
      links.push({
        title: "Terminar",
        onClick: () => this.openFinishPopup(task)
      });
    }

    return links;
  }

  openPopup(task) {
    this.setState({ taskDetails: task });
  }

  closePopup(e) {
    const { className } = e.target;
    if (className === "popup-background" || className === "close") {
      this.setState({ taskDetails: undefined });
    }
  }

  updateList = () => {
    this.closeFinishPopup();
    this.props.loadTasks();
  };

  render() {
    const emptyTableWarning = (
      <tr>
        <td>Nenhum resultado encontrado</td>
      </tr>
    );

    // Sort the data by descending id
    const tasks = this.props.tasks.sort((a, b) => {
      return b.id - a.id;
    });

    var permission = {
      finish: false
    };

    if (hasPermission(this.context, "tasks", "finish")) {
      permission.finish = true;
    }

    const { taskDetails } = this.state;

    return (
      <>
        {taskDetails ? (
          <div
            className="popup-background"
            onClick={this.closePopup.bind(this)}
          >
            <div className="popup task-popup">
              <TaskDetails
                onClose={e => this.closePopup(e)}
                task={taskDetails}
              />
            </div>
          </div>
        ) : null}

        <table className="table">
          <colgroup>
            {this.props.displayAvatar ? <col width="30px" /> : null}
            <col width="30%" />
            <col width="180px" />
            <col />
            <col width="30px" />
          </colgroup>

          <tbody>
            {tasks.length === 0 ? emptyTableWarning : null}
            {tasks.map(task => (
              <tr key={task.id} onClick={() => this.openPopup(task)}>
                {this.props.displayAvatar ? (
                  <td>
                    <Avatar size="40px" object={task.employee} />
                  </td>
                ) : null}
                <td>
                  <b>{task.title}</b>
                </td>
                <td style={{ fontFamily: "Work Sans", fontSize: "13px" }}>
                  {this.convertDate(task.startDate)}
                </td>
                <td>
                  <StatusDot color={convertToColor(task.status)} />
                  <span style={{ fontSize: "13px" }}>
                    {translateStatus(task.status)}
                  </span>
                </td>
                <td>
                  <Dropdown
                    toggle={<img src="/img/icon_more_vert.png" alt="More" />}
                    links={this.taskActionLinks(task, permission)}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {permission.finish && this.state.isFinishPopupOpen ? (
          <FinishPopup
            task={this.state.selectedTask}
            confirmClass="btn-confirm action-btn"
            confirmText="Confirmar"
            title="Terminar tarefa"
            onConfirm={this.updateList}
            onCancel={this.closeFinishPopup}
          />
        ) : null}
      </>
    );
  }
}

TaskTable.contextType = AppContext;

export default TaskTable;
