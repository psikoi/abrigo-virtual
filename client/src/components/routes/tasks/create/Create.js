import React, { Component } from "react";
import Loading from "../../../Loading";
import ErrorPage from "../../errors/ErrorPage";
import { connect } from "react-redux";
import TaskForm from "../_general/TaskForm";
import ReactTooltip from "react-tooltip";
import helper from "../../../../utils/helper";

import {
  createTask,
  createTaskPermission
} from "../../../../store/actions/tasks.action";
import { employeeAvailability } from "../../../../store/actions/schedule.action";

export class Create extends Component {
  state = {
    isLoading: true,
    error: null,
    errorMessage: null,
    title: null,
    description: null,
    startDate: null,
    endDate: null,
    employeeId: 0,
    availability: undefined
  };

  componentWillMount() {
    document.body.style = "background: #FFF !important";
    this.setState({ errorMessage: null, success: null });
    this.props.createTaskPermission();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ errorMessage: nextProps.error.message });
    }

    if (nextProps.success) {
      this.props.history.push("/tarefas");
    }

    if (nextProps.availability && this.state.availabilityRequested) {
      this.setState({ availability: nextProps.availability });
    }
  }

  onSelectedEmployeesChange(selected) {
    this.setState({ employeeId: selected.value });

    let startDate = new Date();
    let endDate = new Date().setDate(new Date().getDate() + 60);

    this.setState({ availabilityRequested: true });
    this.props.employeeAvailability({
      employeeId: selected.value,
      startDate,
      endDate
    });
  }

  onConfirmSelection = async (startDate, endDate) => {
    this.setState({ startDate: startDate, endDate: endDate });
  };

  onTitleChange(e) {
    this.setState({ title: e.target.value });
  }

  onDescriptionChange(e) {
    this.setState({ description: e.target.value });
  }

  onSubmit() {
    const { title, description, startDate, endDate, employeeId } = this.state;

    if (
      this.isStringValid(title) &&
      this.isStringValid(description) &&
      startDate &&
      endDate &&
      employeeId > 0
    ) {
      this.props.createTask({
        title,
        description,
        startDate,
        endDate,
        employeeId
      });
    } else {
      this.setState({ errorMessage: "Preencha todos os campos corretamente." });
    }
  }

  isStringValid(str) {
    return str !== null && str !== "";
  }

  render() {
    const { employees, permission, isLoading, error } = this.props;

    if (permission) {
      return (
        <>
          <TaskForm
            titlePage="Criar tarefa"
            employees={employees}
            onSelectedEmployeesChange={this.onSelectedEmployeesChange.bind(
              this
            )}
            onConfirmSelection={this.onConfirmSelection.bind(this)}
            onSubmit={this.onSubmit.bind(this)}
            errorMessage={this.state.errorMessage}
            onTitleChange={this.onTitleChange.bind(this)}
            onDescriptionChange={this.onDescriptionChange.bind(this)}
            importedStaticBlocks={this.state.availability}
            startDate={this.state.startDate}
            endDate={this.state.endDate}
            helper={helper}
          />
          <ReactTooltip delayShow={300} />
        </>
      );
    } else if (isLoading === undefined) {
      return <ErrorPage statusCode={error.code} />;
    } else {
      return <Loading />;
    }
  }
}

const mapStateToProps = ({ createTask, employeeAvailability }) => ({
  success: createTask && createTask.success,
  error: createTask && createTask.error,
  isLoading: createTask && createTask.isLoading,
  permission: createTask && createTask.permission,
  employees: createTask && createTask.employees,
  availability: employeeAvailability && employeeAvailability.data
});

export default connect(
  mapStateToProps,
  { createTask, createTaskPermission, employeeAvailability }
)(Create);
