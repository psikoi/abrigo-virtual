import React, { Component } from "react";
import { connect } from "react-redux";
import Selector from "../../../Selector";
import {
  finishTask,
  clearFinishProps
} from "../../../../store/actions/tasks.action";
import MessageContainer from "../../../MessageContainer";
import { notifySuccess } from "../../../../utils/notifications";
import "../../../styles/Popup.css";
import "./FinishPopup.css";

export class FinishPopup extends Component {
  state = {
    status: "",
    comment: "",
    errorMessage: ""
  };

  taskStatus = [
    { value: "canceled", label: "Cancelada" },
    { value: "completed", label: "Concluida" }
  ];

  componentWillReceiveProps(nextProps) {
    if (nextProps.finishError) {
      this.setState({
        status: "",
        comment: "",
        errorMessage: nextProps.finishError
      });
    } else if (nextProps.finishSuccess) {
      notifySuccess("Tarefa terminada com sucesso!");
      this.setState({
        status: "",
        comment: ""
      });
      this.props.onConfirm();
    }
  }

  onStatusChanged = selectedOption => {
    this.setState({ status: selectedOption.value });
  };

  onCommentChanged = e => {
    this.setState({ comment: e.target.value });
  };

  submit = () => {
    var reqBody = this.state;
    reqBody.id = this.props.task.id;
    this.props.finishTask(reqBody);
  };

  onCancel = () => {
    this.props.clearFinishProps();
    this.props.onCancel();
  };

  render() {
    return (
      <div className="finish-task-popup">
        <div className="popup-background">
          <div className="popup">
            <h6>{this.props.title}</h6>

            <div className="form">
              <MessageContainer
                message={this.state.errorMessage}
                className="alert alert-danger"
              />

              <Selector
                options={this.taskStatus}
                placeholder="Estado"
                onSelectorChange={this.onStatusChanged}
              />

              <textarea
                type="text"
                onChange={this.onCommentChanged}
                placeholder="Comentário"
              />

              <div className="popup-buttons">
                <button onClick={this.onCancel} style={{ marginRight: "20px" }}>
                  Cancelar
                </button>

                <button
                  onClick={this.submit}
                  className={this.props.confirmClass}
                >
                  {this.props.confirmText || "Confirmar"}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ tasks }) => ({
  finishSuccess: tasks && tasks.finishSuccess,
  finishError: tasks && tasks.finishError
});

export default connect(
  mapStateToProps,
  { finishTask, clearFinishProps }
)(FinishPopup);
