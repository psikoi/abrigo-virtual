import React, { Component } from "react";
import Loading from "../../../Loading";
import ErrorPage from "../../errors/ErrorPage";
import { connect } from "react-redux";
import TaskForm from "../_general/TaskForm";

import { editTask, getTask } from "../../../../store/actions/tasks.action";
import { employeeAvailability } from "../../../../store/actions/schedule.action";
import ReactTooltip from "react-tooltip";
import helper from "../../../../utils/helper";

export class Create extends Component {
  state = {
    isLoading: true,
    error: null,
    errorMessage: null,
    title: "",
    description: "",
    startDate: null,
    endDate: null,
    employeeId: 0,
    lastEmployeedId: 0,
    availability: undefined
  };

  componentWillMount() {
    document.body.style = "background: #FFF !important";
    this.setState({ errorMessage: null, success: null });
    this.props.getTask(this.props.match.params.id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ errorMessage: nextProps.error.message });
    } else if (nextProps.success) {
      this.props.history.push("/tarefas");
    } else if (nextProps.availability && this.state.availabilityRequested) {
      this.setState({ availability: nextProps.availability });
    } else if (nextProps.task) {
      var { title, description, employeeId, taskId } = nextProps.task;
      var startDate = new Date(nextProps.task.startDate);
      var endDate = new Date(nextProps.task.endDate);

      this.setState(
        {
          title,
          description,
          startDate,
          endDate,
          employeeId,
          lastEmployeedId: employeeId,
          taskId
        },
        () => this.getEmployeeAvailability(employeeId, true)
      );
    }
  }

  onSelectedEmployeesChange(selected) {
    this.setState({ employeeId: selected.value });

    this.getEmployeeAvailability(selected.value, true);
  }

  getEmployeeAvailability(employeeId, availabilityRequested) {
    let startDate = new Date();
    let endDate = new Date().setDate(new Date().getDate() + 60);
    let taskId = this.state.taskId;

    this.setState({ availabilityRequested });
    this.props.employeeAvailability({
      employeeId,
      taskId,
      startDate,
      endDate
    });
  }

  onConfirmSelection = async (startDate, endDate) => {
    this.setState({ startDate: startDate, endDate: endDate });
  };

  onTitleChange(e) {
    this.setState({ title: e.target.value });
  }

  onDescriptionChange(e) {
    this.setState({ description: e.target.value });
  }

  onSubmit() {
    const {
      title,
      description,
      startDate,
      endDate,
      employeeId,
      lastEmployeedId,
      status,
      comment
    } = this.state;

    if (
      this.isStringValid(title) &&
      this.isStringValid(description) &&
      startDate &&
      endDate &&
      employeeId > 0
    ) {
      this.props.editTask(this.props.match.params.id, {
        title,
        description,
        startDate,
        endDate,
        employeeId,
        lastEmployeedId,
        status,
        comment
      });
    } else {
      this.setState({ errorMessage: "Preencha todos os campos corretamente." });
    }
  }

  isStringValid(str) {
    return str !== null && str !== "";
  }

  render() {
    const { employees, permission, isLoading, error } = this.props;
    const { title, description, startDate, endDate, employeeId } = this.state;

    if (permission) {
      return (
        <>
          <TaskForm
            titlePage="Editar tarefa"
            employees={employees}
            onSelectedEmployeesChange={this.onSelectedEmployeesChange.bind(
              this
            )}
            value={employeeId}
            onSubmit={this.onSubmit.bind(this)}
            errorMessage={this.state.errorMessage}
            onTitleChange={this.onTitleChange.bind(this)}
            onDescriptionChange={this.onDescriptionChange.bind(this)}
            importedStaticBlocks={this.state.availability}
            onConfirmSelection={this.onConfirmSelection.bind(this)}
            title={title}
            description={description}
            startDate={startDate}
            endDate={endDate}
            helper={helper}
          />
          <ReactTooltip delayShow={300} />
        </>
      );
    } else if (isLoading === undefined) {
      return <ErrorPage statusCode={error.code} />;
    } else {
      return <Loading />;
    }
  }
}

const mapStateToProps = ({ editTask, employeeAvailability }) => ({
  success: editTask && editTask.success,
  error: editTask && editTask.error,
  isLoading: editTask && editTask.isLoading,
  permission: editTask && editTask.permission,
  employees: editTask && editTask.employees,
  task: editTask && editTask.task,
  availability: employeeAvailability && employeeAvailability.data
});

export default connect(
  mapStateToProps,
  { editTask, getTask, employeeAvailability }
)(Create);
