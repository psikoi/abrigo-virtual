import React, { Component } from "react";
import "./TaskDetails.css";
import StatusDot from "../../../StatusDot";
import {
  convertToColor,
  translateStatus
} from "../../../../utils/translation/task.translation";
import { formatTime, formatDateLong } from "../../../../utils/dates";
import Avatar from "../../../Avatar";

export class TaskDetails extends Component {
  render() {
    const { task, onClose } = this.props;

    return (
      <div className="task-details">
        <div className="title">
          <StatusDot color={convertToColor(task.status)} />
          <b style={{ marginLeft: "5px" }}>{task.title}</b>
          <img
            src="/img/icons/clear.svg"
            alt="x"
            onClick={onClose}
            className="close"
          />
        </div>
        <hr />
        <div className="main">
          <div className="top">
            <div className="description">
              <label>Descrição</label>
              <span>{task.description}</span>
            </div>
          </div>
          <div className="bot">
            <div className="members">
              <label>Colaborador</label>
              <Avatar size="30px" object={task.employee} />
            </div>
            <div className="dates">
              <div className="date">
                <label>Data de criação</label>
                <span>{formatDateLong(new Date(task.creationDate))}</span>
                <span className="hours">
                  {formatTime(new Date(task.creationDate))}
                </span>
              </div>
              <div className="date">
                <label>Data de conclusão</label>
                <span>
                  {task.finishDate
                    ? formatDateLong(new Date(task.finishDate))
                    : "---"}
                </span>
                <span className="hours">
                  {task.finishDate ? formatTime(new Date(task.finishDate)) : ""}
                </span>
              </div>
            </div>
          </div>
        </div>
        <br />
        <hr />
        <br />
        <div className="bottom">
          <div className="top">
            <div className="status">
              <label>Estado</label>
              <span style={{ color: convertToColor(task.status) }}>
                {translateStatus(task.status)}
              </span>
            </div>
            <div className="dates">
              <div className="date">
                <label>Data de início</label>
                <span>{formatDateLong(new Date(task.startDate))}</span>
                <span className="hours">
                  {formatTime(new Date(task.startDate))}
                </span>
              </div>
              <div className="date">
                <label>Data final</label>
                <span>{formatDateLong(new Date(task.endDate))}</span>
                <span className="hours">
                  {formatTime(new Date(task.endDate))}
                </span>
              </div>
            </div>
          </div>
          <div className="bot">
            <div className="comment">
              <label>Comentário</label>
              <span>{task.comment ? task.comment : "---"}</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TaskDetails;
