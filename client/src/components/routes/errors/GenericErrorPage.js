import React, { Component } from "react";
import "./ErrorPages.css";

export class GenericErrorPage extends Component {
  render() {
    var denied = this.props.statusCode.toString() === "403";

    var title = denied ? "Acesso negado" : "Ooops!";
    var message = denied
      ? "Não tens permissão para aceder a este conteúdo"
      : "Alguma coisa correu mal";
    var image = denied ? "/img/acesso.png" : "/img/erro.png";

    return (
      <div className="error-container">
        <div>
          <img src={image} alt="Imagem de erro" className="error-image" />
        </div>
        <div>
          <h1>{title}</h1>
          <h2>{message}</h2>
          <button onClick={() => this.props.goBack()} className="link-button">
            VOLTAR
          </button>
        </div>
      </div>
    );
  }
}

export default GenericErrorPage;
