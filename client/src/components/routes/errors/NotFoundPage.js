import React, { Component } from "react";
import "./ErrorPages.css";

export class NotFoundPage extends Component {
  render() {
    return (
      <div className="error404-container">
        <img src="/img/404.png" alt="Imagem de erro" className="error-image" />
        <h1>Ooops!</h1>
        <h2>Página não encontrada</h2>
        <button onClick={() => this.props.goBack()} className="link-button">
          Voltar
        </button>
      </div>
    );
  }
}

export default NotFoundPage;
