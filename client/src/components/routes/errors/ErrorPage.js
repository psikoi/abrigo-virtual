import React, { Component } from "react";
import NotFoundPage from "./NotFoundPage";
import GenericErrorPage from "./GenericErrorPage";
import { withRouter } from "react-router";

export class ErrorPage extends Component {
  render() {
    return this.props.statusCode.toString() === "404" ? (
      <NotFoundPage goBack={this.props.history.goBack} />
    ) : (
      <GenericErrorPage
        statusCode={this.props.statusCode}
        goBack={this.props.history.goBack}
      />
    );
  }
}

export default withRouter(ErrorPage);
