import React, { Component } from "react";
import PageHeader from "../../../PageHeader";
import MessageContainer from "../../../MessageContainer";
import { withRouter } from "react-router";
import Selector from "../../../Selector";

export class KennelForm extends Component {
  formatAnimals = animals => {
    if (!animals) {
      return [];
    }

    return animals.map(animal => {
      return { value: animal.animalId, label: animal.name };
    });
  };

  render() {
    const {
      animals,
      onSelectedAnimalsChange,
      onNameChange,
      onCapacityChange,
      onSubmit,
      name,
      titlePage,
      errorMessage,
      animalsSelected,
      capacity
    } = this.props;
    const formatAnimals = this.formatAnimals(animals);
    const values = this.formatAnimals(animalsSelected);
    return (
      <div className="page">
        <PageHeader title={titlePage} />

        <MessageContainer
          message={errorMessage}
          className="alert alert-danger"
        />
        <div className="form kennel-form">
          <input
            type="text"
            name="name"
            placeholder="NOME"
            onChange={onNameChange}
            value={name}
            data-tip={this.props.helper.KENNEL_NAME}
          />
          <input
            type="number"
            name="capacity"
            placeholder="CAPACIDADE MÁXIMA"
            onChange={onCapacityChange}
            value={capacity}
            data-tip={this.props.helper.KENNEL_CAPACITY}
          />

          <Selector
            className="kennel-form-select"
            options={formatAnimals}
            placeholder="ANIMAIS"
            onSelectorChange={onSelectedAnimalsChange}
            isSearchable={true}
            value={values}
            dataTip={this.props.helper.KENNEL_ANIMALS}
            isMulti={true}
          />

          <div className="actions-container">
            <button className="btn-medium" onClick={this.props.history.goBack}>
              Voltar
            </button>
            <button className="btn-confirm btn-medium" onClick={onSubmit}>
              Confirmar
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(KennelForm);
