import React, { Component } from "react";
import StatusDot from "../../../StatusDot";
import Dropdown from "../../../Dropdown";
import {
  convertToColor,
  translateSize
} from "../../../../utils/translation/kennel.translation";
import { deleteKennel } from "../../../../store/actions/kennels.action";
import { AppContext } from "../../../../contexts/AppContext";
import "../../../styles/Popup.css";
import AvatarGroup from "../../../AvatarGroup";
import Popup from "../../../Popup";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { notifySuccess } from "../../../../utils/notifications";

export class KennelsTable extends Component {
  state = {
    isFinishPopupOpen: false,
    selectedKennel: null,
    newRemoveRequest: false
  };

  openRemovePopup = kennel => {
    this.setState({
      isRemovePopupOpen: true,
      kennel: kennel
    });
  };

  kennelActionLinks(kennel) {
    var links = [
      {
        title: "Editar",
        url: "/casotas/editar/" + kennel.kennelId
      },
      {
        title: "Remover",
        onClick: () => this.openRemovePopup(kennel)
      }
    ];

    return links;
  }

  updateList = () => {
    this.props.loadKennels();
  };

  removeKennelConfirm = () => {
    const { kennel } = this.state;

    if (kennel) {
      this.setState({
        newRemoveRequest: true
      });
      this.props.deleteKennel(kennel.kennelId);
    }
  };

  closeRemovePopup = () => {
    this.setState({
      isRemovePopupOpen: false,
      kennel: null
    });
  };

  componentWillReceiveProps(nextProps) {
    const { newRemoveRequest } = this.state;

    if (nextProps.error) {
      this.setState({ errorMessage: nextProps.error });
    }

    if (nextProps.success && newRemoveRequest) {
      notifySuccess(nextProps.success);
      this.props.loadKennels();
      this.setState({
        newRemoveRequest: false
      });
    }
  }

  render() {
    const emptyTableWarning = (
      <tr>
        <td>Nenhum resultado encontrado</td>
      </tr>
    );

    var sign = value => {
      return value <= 0 ? "" : "+";
    };

    var numClass = value => {
      return value === 0
        ? { color: "gray" }
        : value < 0
        ? { color: "#9b2828" }
        : { color: "#36a890" };
    };

    // Sort the data by descending id
    const kennels = this.props.kennels.sort((a, b) => {
      return b.id - a.id;
    });
    const { isRemovePopupOpen, kennel } = this.state;
    return (
      <>
        <table className="table">
          <colgroup>
            {this.props.displayAvatar ? <col width="140px" /> : null}
            <col width="30%" />
            <col width="150px" />
            {this.props.disableActions ? (
              <col width="90px" />
            ) : (
              <col width="30px" />
            )}
          </colgroup>

          <tbody>
            {kennels.length === 0 ? emptyTableWarning : null}
            {kennels.map(kennel => (
              <tr key={kennel.kennelId || kennel.id}>
                {this.props.displayAvatar ? (
                  <td>
                    {kennel.animals.length === 0 ? "Nenhum animal" : null}
                    <AvatarGroup animals={kennel.animals} maxAvatars={3} />
                  </td>
                ) : null}
                <td>
                  <b>{kennel.name}</b>
                </td>
                <td>
                  <StatusDot
                    color={convertToColor(
                      kennel.capacity,
                      kennel.animals.length
                    )}
                  />
                  <span style={{ fontSize: "13px" }}>
                    {translateSize(kennel.capacity, kennel.animals.length)}
                  </span>
                </td>
                <td>
                  {this.props.disableActions ? (
                    <b style={numClass(kennel.score)}>
                      {sign(kennel.score)}
                      {kennel.score} pts
                    </b>
                  ) : (
                    <Dropdown
                      toggle={<img src="/img/icon_more_vert.png" alt="More" />}
                      links={this.kennelActionLinks(kennel)}
                    />
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <Popup
          isOpen={isRemovePopupOpen}
          confirmClass="btn-danger"
          confirmText="Remover"
          title={`Tem a certeza que pretende remover a casota ${
            kennel ? kennel.name : ""
          } ?`}
          onConfirm={this.removeKennelConfirm}
          onCancel={this.closeRemovePopup}
        />
      </>
    );
  }
}

const mapStateToProps = ({ kennelsActions }) => ({
  success: kennelsActions && kennelsActions.success,
  error: kennelsActions && kennelsActions.error,
  isLoading: kennelsActions && kennelsActions.isLoading
});

KennelsTable.contextType = AppContext;
export default withRouter(
  connect(
    mapStateToProps,
    { deleteKennel }
  )(KennelsTable)
);
