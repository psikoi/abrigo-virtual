import React, { Component } from "react";
import { connect } from "react-redux";
import PageHeader from "../../../PageHeader";
import KennelsTable from "../_general/KennelsTable";
import Loading from "../../../Loading";
import { Link } from "react-router-dom";
import ErrorPage from "../../errors/ErrorPage";
import "../../../styles/ListPage.css";
import { fetchKennels } from "../../../../store/actions/kennels.action";
import { AppContext } from "../../../../contexts/AppContext";
import { isLicensePlus } from "../../../../utils/licenseType";

export class Index extends Component {
  state = {
    data: [],
    isLoading: true,
    errorMessage: undefined
  };

  componentWillMount() {
    this.props.fetchKennels();
    document.body.style = "background: #eaeced !important";
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      this.setState({
        data: nextProps.data,
        isLoading: false
      });
    } else if (nextProps.error) {
      this.setState({
        isLoading: false,
        errorMessage: nextProps.error
      });
    }
  }

  onSearchChanged = e => {
    this.setState({ searchQuery: e.target.value });
  };

  filteredResults(results) {
    const { searchQuery } = this.state;
    if (!searchQuery || searchQuery.length === 0) {
      return results;
    }

    const query = searchQuery.toLowerCase();

    return results.filter(result => {
      const { name } = result;
      return name.toLowerCase().includes(query);
    });
  }

  render() {
    const { shelter } = this.context;

    if (this.state.isLoading) {
      return <Loading />;
    }

    if (this.state.errorMessage) {
      return <ErrorPage statusCode="403" />;
    }

    if (!this.state.isLoading && !this.state.errorMessage)
      return (
        <div className="page" style={{ background: "#eaeced" }}>
          <PageHeader title="Casotas" />
          <div className="search-container">
            <img src="/img/icons/search.svg" alt="" />
            <input
              onChange={this.onSearchChanged}
              type="search"
              placeholder="Pesquisar"
            />
          </div>
          <Link className="btn-confirm action-link-btn" to="/casotas/criar">
            Registar nova
          </Link>
          {isLicensePlus(shelter.licenseType) ? (
            <Link
              className="action-link-btn"
              to="/casotas/calcular"
              style={{ marginRight: "10px" }}
            >
              Calcular nova distribuição
            </Link>
          ) : null}
          <KennelsTable
            kennels={this.filteredResults(this.state.data)}
            loadKennels={this.props.fetchKennels}
            displayAvatar={true}
          />
        </div>
      );
  }
}

Index.contextType = AppContext;

const mapStateToProps = ({ kennels }) => ({
  data: kennels && kennels.data,
  error: kennels && kennels.error,
  isLoading: kennels && kennels.isLoading
});

export default connect(
  mapStateToProps,
  { fetchKennels }
)(Index);
