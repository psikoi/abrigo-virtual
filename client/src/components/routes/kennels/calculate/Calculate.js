import React, { Component } from "react";
import "./Calculate.css";
import { connect } from "react-redux";
import Avatar from "../../../Avatar";
import {
  calculateKennels,
  redistributeKennels
} from "../../../../store/actions/kennels.action";
import KennelTable from "../_general/KennelsTable";
import Popup from "../../../Popup";
import { AppContext } from "../../../../contexts/AppContext";
import ErrorPage from "../../errors/ErrorPage";
import { isLicensePlus } from "../../../../utils/licenseType";

export class Calculate extends Component {
  state = {
    stage: 0,
    data: [],
    isLoading: true,
    errorMessage: undefined,
    isPopupOpen: false
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      if (this.state.stage === 2) {
        this.props.history.push("/casotas");
      } else {
        this.setState({
          data: nextProps.data,
          isLoading: false,
          stage: nextProps.data.changes.length === 0 ? 0 : 2,
          isPopupOpen: nextProps.data.changes.length === 0
        });
      }
    } else if (nextProps.error) {
      this.setState({
        isLoading: false,
        errorMessage: nextProps.error
      });
    }
  }

  componentWillMount() {
    document.body.style = "background: #eaeced !important";
  }

  start = () => {
    this.setState({ stage: 1, timestamp: new Date().getTime() }, () => {
      this.updateTimer();
      this.props.calculateKennels();
    });
  };

  onApply = () => {
    const changes = this.state.data.changes;
    this.props.redistributeKennels(changes);
  };

  updateTimer = () => {
    const duration = 5000;
    var elapsed = new Date().getTime() - this.state.timestamp;

    if ((!elapsed || elapsed < duration * 1.1) && this.state.stage === 1) {
      if (!elapsed) {
        elapsed = 0;
      }
      this.setState({
        progress: Math.min(100, Math.floor((elapsed / duration) * 100))
      });

      setTimeout(() => {
        this.updateTimer();
      }, duration / 10);
    }
  };

  closePopup = () => {
    this.setState({
      isPopupOpen: false
    });
  };

  getActiveStage() {
    if (this.state.stage === 0) {
      return (
        <div className="before panel">
          <h1>Nova distribuição</h1>
          <hr />
          <p>
            Clique abaixo para calcular uma nova distribuição de casotas
            otimizada
          </p>
          <button onClick={this.start}>Calcular</button>
        </div>
      );
    } else if (this.state.stage === 1) {
      return (
        <div className="during panel">
          <h1>Nova distribuição</h1>
          <hr />
          <div className="content">
            <b>Calculando.. ({this.state.progress}%)</b>
            <div className="progress">
              <div
                className="inner"
                style={{ width: this.state.progress + "%" }}
              />
            </div>
          </div>
        </div>
      );
    } else if (this.state.stage === 2) {
      const { data } = this.state;

      if (!data) {
        return null;
      }

      const { fitness, worst, best, changes, kennels } = data;

      var numClass = value => {
        return value === 0 ? "" : value < 0 ? "neg" : "pos";
      };

      return (
        <div className="side-panel-container">
          <div className="after">
            <div className="panel">
              <h1>Nova distribuição</h1>
              <hr />
              <div className="content">
                <b>Pontuação</b>
                <p>{Math.round(fitness * 100) / 100}</p>
                <hr />
                <div className="cells">
                  <div className="cell">
                    <b>Pior casota</b>
                    <p>
                      {worst.name} (
                      <span className={numClass(worst.score)}>
                        {worst.score} pts
                      </span>
                      )
                    </p>
                  </div>
                  <div className="cell">
                    <b>Mudanças</b>
                    <p>{changes.length}</p>
                  </div>
                  <div className="cell">
                    <b>Melhor casota</b>
                    <p>
                      {best.name} (
                      <span className={numClass(best.score)}>
                        {best.score} pts
                      </span>
                      )
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <button onClick={this.onApply}>APLICAR</button>
          </div>
          <div className="info">
            <b>MUDANÇAS NECESSÁRIAS</b>
            {changes.map(c => (
              <div key={c.animal.animalId} className="change">
                <Avatar object={c.animal} isAnimal={true} size="30px" />
                <b>{c.animal.name}</b>
                <span>
                  Trocar de {c.from.name} para {c.to.name}
                </span>
              </div>
            ))}

            <br />
            <b>CASOTAS</b>
            <KennelTable
              displayAvatar={true}
              kennels={kennels}
              disableActions={true}
            />
          </div>
        </div>
      );
    }
  }

  render() {
    const { shelter } = this.context;

    return isLicensePlus(shelter.licenseType) ? (
      <>
        <div className="page calculate">{this.getActiveStage()}</div>
        <Popup
          isOpen={this.state.isPopupOpen}
          confirmClass="btn-danger"
          cancelText="Fechar"
          title={`Nenhuma alteração recomendada`}
          onConfirm={this.closePopup}
          onCancel={this.closePopup}
          dontNeedConfirmButton={true}
        />
      </>
    ) : (
      <ErrorPage statusCode="403" />
    );
  }
}

Calculate.contextType = AppContext;

const mapStateToProps = ({ kennelsActions }) => ({
  data: kennelsActions && kennelsActions.data,
  error: kennelsActions && kennelsActions.error,
  isLoading: kennelsActions && kennelsActions.isLoading
});

export default connect(
  mapStateToProps,
  { calculateKennels, redistributeKennels }
)(Calculate);
