import React, { Component } from "react";
import Loading from "../../../Loading";
import ErrorPage from "../../errors/ErrorPage";
import { connect } from "react-redux";
import KennelForm from "../_general/KennelForm";
import ReactTooltip from "react-tooltip";
import helper from "../../../../utils/helper";
import { hasPermission } from "../../../../utils/permissions";
import { AppContext } from "../../../../contexts/AppContext";
import {
  editKennel,
  fetchAnimalsAvailable,
  fetchKennel
} from "../../../../store/actions/kennels.action";

export class Edit extends Component {
  state = {
    isLoading: true,
    error: null,
    errorMessage: null,
    name: "",
    capacity: "",
    animals: []
  };

  componentWillMount() {
    document.body.style = "background: #FFF !important";
    var kennelId = this.props.match.params.id;
    this.props.fetchKennel(kennelId);
    this.props.fetchAnimalsAvailable(kennelId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ errorMessage: nextProps.error.message });
    }

    if (nextProps.success) {
      this.props.history.push("/casotas");
    }

    if (nextProps.kennel) {
      this.setState({
        name: nextProps.kennel.name,
        animals: nextProps.kennel.animals,
        capacity: nextProps.kennel.capacity
      });
    }
  }

  formatAnimalSelection = animals => {
    return animals.map(animal => {
      return { animalId: animal.value, name: animal.label };
    });
  };

  onSelectedAnimalsChange = animals => {
    this.setState({ animals: this.formatAnimalSelection(animals) });
  };

  onNameChange = e => {
    this.setState({ name: e.target.value });
  };

  onCapacityChange = e => {
    this.setState({ capacity: e.target.value });
  };

  onSubmit = () => {
    const { capacity, name, animals } = this.state;
    this.props.editKennel({
      kennelId: Number(this.props.match.params.id),
      capacity: Number(capacity),
      name,
      animals: animals.map(function(a) {
        return a.animalId || a;
      })
    });
  };

  render() {
    const { animalsAvailable, isLoading, error } = this.props;
    const { name, capacity, animals } = this.state;
    if (!hasPermission(this.context, "kennels", "create")) {
      return <ErrorPage statusCode="403" />;
    } else if (error && error.code) {
      return <ErrorPage statusCode={error.code} />;
    } else if (isLoading) {
      return <Loading />;
    }
    return (
      <>
        <KennelForm
          titlePage="EDITAR CASOTA"
          animals={animalsAvailable}
          onSelectedAnimalsChange={this.onSelectedAnimalsChange}
          onNameChange={this.onNameChange}
          onSubmit={this.onSubmit}
          errorMessage={this.props.error}
          onCapacityChange={this.onCapacityChange}
          helper={helper}
          name={name}
          capacity={capacity}
          animalsSelected={animals}
        />
        <ReactTooltip delayShow={300} />
      </>
    );
  }
}

Edit.contextType = AppContext;

const mapStateToProps = ({ kennelsActions }) => ({
  success: kennelsActions && kennelsActions.success,
  error: kennelsActions && kennelsActions.error,
  isLoading: kennelsActions && kennelsActions.isLoading,
  permission: kennelsActions && kennelsActions.permission,
  animalsAvailable: kennelsActions && kennelsActions.animals,
  kennel: kennelsActions && kennelsActions.kennel
});

export default connect(
  mapStateToProps,
  { editKennel, fetchAnimalsAvailable, fetchKennel }
)(Edit);
