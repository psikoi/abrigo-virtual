import React, { Component } from "react";
import { connect } from "react-redux";
import PageHeader from "../../../PageHeader";
import AnimalTable from "../_general/AnimalTable";
import Loading from "../../../Loading";
import { Link } from "react-router-dom";
import "../../../styles/ListPage.css";
import ErrorPage from "../../errors/ErrorPage";

import { fetchAnimals } from "../../../../store/actions/animals.action";
import {
  translateType,
  translateGender
} from "../../../../utils/translation/animal.translation";

export class Index extends Component {
  state = {
    data: [],
    isLoading: true,
    error: undefined
  };

  onSearchChanged = e => {
    this.setState({ searchQuery: e.target.value });
  };

  filteredResults(results) {
    const { searchQuery } = this.state;

    if (!searchQuery || searchQuery.length === 0) {
      return results;
    }

    const query = searchQuery.toLowerCase();

    return results.filter(result => {
      const { name } = result;
      const type = translateType(result);
      const gender = translateGender(result);

      return (
        name.toLowerCase().includes(query) ||
        type.toLowerCase().includes(query) ||
        gender.toLowerCase().includes(query)
      );
    });
  }

  componentWillMount() {
    document.body.style = "background: #eaeced !important";
    this.props.fetchAnimals();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      this.setState({ data: nextProps.data, isLoading: false });
    } else if (nextProps.error) {
      this.setState({ isLoading: false, error: nextProps.error });
    }
  }

  render() {
    if (this.state.isLoading) {
      return <Loading />;
    }

    if (this.state.error) {
      return <ErrorPage statusCode={this.state.error.code} />;
    }

    if (!this.state.isLoading && !this.state.error)
      return (
        <div className="page" style={{ background: "#eaeced" }}>
          <PageHeader title="Animais" />
          <div className="search-container">
            <img src="/img/icons/search.svg" alt="" />
            <input
              onChange={this.onSearchChanged}
              type="search"
              placeholder="Pesquisar"
            />
          </div>
          <Link className="btn-confirm action-link-btn" to="/animais/criar">
            Criar novo
          </Link>
          <AnimalTable
            loadAnimals={this.props.fetchAnimals}
            animals={this.filteredResults(this.state.data)}
          />
        </div>
      );
  }
}

const mapStateToProps = ({ fetchAnimals }) => ({
  data: fetchAnimals && fetchAnimals.data,
  error: fetchAnimals && fetchAnimals.error,
  isLoading: fetchAnimals && fetchAnimals.isLoading
});

export default connect(
  mapStateToProps,
  { fetchAnimals }
)(Index);
