import React, { Component } from "react";
import Loading from "../../../Loading";
import ErrorPage from "../../errors/ErrorPage";
import { connect } from "react-redux";
import ReactTooltip from "react-tooltip";
import UploadImage from "../../../UploadImage";
import { hasPermission } from "../../../../utils/permissions";
import { AppContext } from "../../../../contexts/AppContext";
import helper from "../../../../utils/helper";
import Selector from "../../../Selector";
import PageHeader from "../../../PageHeader";
import MessageContainer from "../../../MessageContainer";

import {
  getAnimal,
  editAnimal
} from "../../../../store/actions/animals.action";

export class Edit extends Component {
  state = {
    success: undefined,
    errorMessage: undefined,
    name: "",
    type: "",
    gender: "",
    info: "",
    size: "",
    breed: "",
    birthdate: "",
    image: ""
  };

  genders = [
    { value: "male", label: "Macho" },
    { value: "female", label: "Fêmea" },
    { value: "other", label: "Outro" }
  ];

  size = [
    { value: "micro", label: "Micro" },
    { value: "small", label: "Pequeno" },
    { value: "medium", label: "Médio" },
    { value: "big", label: "Grande" },
    { value: "veryBig", label: "Muito Grande" },
    { value: "other", label: "Outro" }
  ];

  types = [
    { value: "dog", label: "Cão" },
    { value: "cat", label: "Gato" },
    { value: "other", label: "Outro" }
  ];

  componentWillMount() {
    document.body.style = "background: #FFF !important";
    this.setState({ errorMessage: null, success: null });
    this.props.getAnimal(this.props.match.params.id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ errorMessage: nextProps.error.message });
    } else if (nextProps.success) {
      this.props.history.push("/animais");
    } else if (nextProps.animal) {
      this.fillField(nextProps.animal);
    }
  }

  fillField = animal => {
    this.setState({
      name: animal.name,
      defaultName: animal.name,
      type: animal.type,
      gender: animal.gender,
      info: animal.info,
      size: animal.size,
      breed: animal.breed,
      birthdate: animal.birthdate,
      image: animal.photoUrl
    });
  };

  onTypeChanged = selectedOption => {
    this.setState({ type: selectedOption.value });
  };

  onGenderChanged = selectedOption => {
    this.setState({ gender: selectedOption.value });
  };

  onSizeChanged = selectedOption => {
    this.setState({ size: selectedOption.value });
  };

  onNameChanged = e => {
    this.setState({ name: e.target.value });
  };

  onInfoChanged = e => {
    this.setState({ info: e.target.value });
  };

  onBreedChanged = e => {
    this.setState({ breed: e.target.value });
  };

  onBirthdateChanged = e => {
    this.setState({ birthdate: e.target.value });
  };

  onImageUploaded = data => {
    this.setState({ image: data });
  };

  onSubmit() {
    const {
      name,
      type,
      gender,
      info,
      size,
      breed,
      birthdate,
      image
    } = this.state;

    this.props.editAnimal(this.props.match.params.id, {
      name,
      type,
      gender,
      info,
      size,
      breed,
      birthdate,
      image
    });
  }

  render() {
    const {
      name,
      defaultName,
      type,
      gender,
      info,
      size,
      breed,
      birthdate,
      image
    } = this.state;

    if (this.props.isLoading) {
      return <Loading />;
    }

    if (!hasPermission(this.context, "employee", "create")) {
      return <ErrorPage statusCode="403" />;
    }

    return (
      <div id="create-animal" className="page" style={{ width: "500px" }}>
        <PageHeader title={"Editar ficha de animal - " + defaultName} />

        <MessageContainer
          message={this.state.errorMessage}
          className="alert alert-danger"
        />

        <div className="form">
          <div className="left-30">
            <UploadImage
              onImageUploaded={this.onImageUploaded}
              dataTip={helper.ANI_PHOTO}
              url={image}
            />
          </div>
          <div className="right-70">
            <input
              type="text"
              name="name"
              placeholder="Nome"
              onChange={this.onNameChanged}
              data-tip={helper.ANI_NAME}
              value={name}
            />
            <input
              onChange={this.onBreedChanged}
              type="text"
              name="breed"
              placeholder="Raça"
              data-tip={helper.ANI_BREED}
              value={breed}
            />
          </div>
          <div className="form-group">
            <Selector
              className="left"
              options={this.genders}
              placeholder="Sexo"
              onSelectorChange={this.onGenderChanged}
              dataTip={helper.ANI_GENDER}
              value={gender}
            />
            <Selector
              className="right"
              options={this.types}
              placeholder="Tipo"
              onSelectorChange={this.onTypeChanged}
              dataTip={helper.ANI_TYPE}
              value={type}
            />
          </div>

          <div className="form-group">
            <input
              className="left"
              type="date"
              name="birthdate"
              placeholder="Data de nascimento"
              onChange={this.onBirthdateChanged}
              data-tip={helper.EMP_BIRTHDATE}
              value={birthdate ? birthdate.split("T")[0] : ""}
            />
            <Selector
              className="right"
              options={this.size}
              placeholder="Porte"
              onSelectorChange={this.onSizeChanged}
              dataTip={helper.ANI_SIZE}
              value={size}
            />
          </div>

          <textarea
            onChange={this.onInfoChanged}
            placeholder="Informação adicional"
            data-tip={helper.EMP_ADDITIONALINFO}
            value={info}
          />

          <div className="actions-container">
            <button className="btn-medium" onClick={this.props.history.goBack}>
              Voltar
            </button>
            <button
              className="btn-confirm btn-medium"
              onClick={this.onSubmit.bind(this)}
            >
              Confirmar
            </button>
          </div>
        </div>
        <ReactTooltip delayShow={300} />
      </div>
    );
  }
}

Edit.contextType = AppContext;

const mapStateToProps = ({ editAnimal }) => ({
  success: editAnimal && editAnimal.success,
  error: editAnimal && editAnimal.error,
  isLoading: editAnimal && editAnimal.isLoading,
  permission: editAnimal && editAnimal.permission,
  animal: editAnimal && editAnimal.animal
});

export default connect(
  mapStateToProps,
  { getAnimal, editAnimal }
)(Edit);
