import React, { Component } from "react";
import Loading from "../../../Loading";
import { Redirect } from "react-router-dom";
import Create from "./Create";
import ErrorPage from "../../errors/ErrorPage";
import { hasPermission } from "../../../../utils/permissions";
import { AppContext } from "../../../../contexts/AppContext";
import { connect } from "react-redux";
import {
  editRelationship,
  getRelationship
} from "../../../../store/actions/relationships.action";

export class Edit extends Component {
  state = {
    data: undefined,
    isLoading: true,
    error: "",
    editError: "",
    editLoading: false,
    editSuccess: undefined
  };

  componentWillMount() {
    this.props.getRelationship(this.props.match.params.id);
  }

  onSubmit = params => {
    const { animalAId, animalBId, relationshipId } = this.state.data;

    params.animalAId = animalAId;
    params.animalBId = animalBId;

    this.setState({ editRequested: true });

    this.props.editRelationship(params, relationshipId);
  };

  componentWillReceiveProps(nextProps) {
    if (!nextProps) {
      return;
    }

    if (nextProps.error) {
      this.setState({
        error: nextProps.error,
        isLoading: false
      });
    }

    if (nextProps.isLoading) {
      this.setState({
        isLoading: nextProps.isLoading
      });
    }

    if (nextProps.data) {
      this.setState({
        data: nextProps.data[0],
        isLoading: false
      });
    }

    if (nextProps.editError) {
      this.setState({
        editError: nextProps.editError,
        editLoading: false
      });
    }

    if (nextProps.editLoading) {
      this.setState({
        editLoading: nextProps.editLoading
      });
    }

    if (nextProps.editSuccess && this.state.editRequested) {
      this.setState({
        editSuccess: nextProps.editSuccess,
        editLoading: false
      });
    }
  }

  renderRedirect = () => {
    if (this.state.editSuccess) {
      return (
        <Redirect
          to={{
            pathname: `/animais/${this.state.data.animalAId}`,
            state: {
              active: "relationships"
            }
          }}
        />
      );
    }
  };

  render() {
    if (!hasPermission(this.context, "relationships", "edit")) {
      return <ErrorPage statusCode="403" />;
    }

    if (this.state.error) {
      return <ErrorPage statusCode={this.state.error.code} />;
    }

    if (
      this.state.isLoading ||
      this.props.editLoading ||
      this.state.editLoading
    ) {
      return <Loading />;
    }

    const { animalAId, animalBId, value } = this.state.data;

    return (
      <Create
        animalAId={animalAId}
        animalBId={animalBId}
        value={value}
        redirect={this.renderRedirect}
        errorMessage={this.state.editError}
        title={"Editar relação entre animais"}
        onSubmit={this.onSubmit}
        history={this.props.history}
        overrideValidation={true}
      />
    );
  }
}

Edit.contextType = AppContext;

const mapStateToProps = ({ getRelationship, editRelationship }) => ({
  data: getRelationship && getRelationship.data,
  isLoading: getRelationship && getRelationship.isLoading,
  error: getRelationship && getRelationship.error,
  editSuccess: editRelationship && editRelationship.editSuccess,
  editError: editRelationship && editRelationship.editError,
  editLoading: editRelationship && editRelationship.editLoading
});

export default connect(
  mapStateToProps,
  { getRelationship, editRelationship }
)(Edit);
