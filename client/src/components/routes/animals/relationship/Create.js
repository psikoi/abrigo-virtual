import React, { Component } from "react";
import Loading from "../../../Loading";
import { Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import ErrorPage from "../../errors/ErrorPage";
import { connect } from "react-redux";
import ReactTooltip from "react-tooltip";
import { hasPermission } from "../../../../utils/permissions";
import { AppContext } from "../../../../contexts/AppContext";
import helper from "../../../../utils/helper";
import Selector from "../../../Selector";
import PageHeader from "../../../PageHeader";
import MessageContainer from "../../../MessageContainer";

import {
  getCreateRelationship,
  createRelationship
} from "../../../../store/actions/relationships.action";

export class Create extends Component {
  state = {
    getCreateRelationshipSuccess: undefined,
    getCreateRelationshipFailure: undefined,
    errorMessage: "",
    loading: "",
    success: undefined,
    animals: [],
    animalA: 0,
    animalB: 0,
    value: 0
  };

  values = [
    { value: -10, label: "Relação muito má" },
    { value: -5, label: "Relação má" },
    { value: 2, label: "Relação boa" },
    { value: 5, label: "Relação muito boa" }
  ];

  componentWillMount() {
    document.body.style = "background: #FFF !important";

    this.setState({
      animalA: this.props.match
        ? this.props.match.params.id
        : this.props.animalAId
    });

    this.props.getCreateRelationship();
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps) {
      return;
    }

    if (nextProps.getCreateRelationshipSuccess) {
      this.setState({
        animals: nextProps.getCreateRelationshipSuccess,
        loading: false
      });
    }

    if (nextProps.errorMessage) {
      this.setState({
        errorMessage: nextProps.errorMessage,
        loading: false
      });
    }

    if (nextProps.loading) {
      this.setState({
        loading: nextProps.loading
      });
    }

    if (nextProps.success && this.state.createRequested) {
      this.setState({
        success: nextProps.success,
        loading: false
      });
    }
  }

  onAnimalAChange = selectedOption => {
    this.setState({ animalA: selectedOption.value });
  };

  onAnimalBChange = selectedOption => {
    this.setState({ animalB: selectedOption.value });
  };

  onValueChange = selectedOption => {
    this.setState({ value: selectedOption.value });
  };

  removeFromArray(array, element) {
    var newArray = [...array];
    var index;

    newArray.forEach(function(e, i) {
      if (e.id === element) {
        index = i;
      }
    });

    if (index > -1) {
      newArray.splice(index, 1);
    }

    return newArray;
  }

  prepareSelection = arr => {
    if (!arr || !arr.length) {
      return [];
    }

    return arr.map(function(e) {
      return {
        value: e.id,
        label: e.name,
        photo: e.photo
      };
    });
  };

  findDefaultValue = id => {
    var defaultArr = [];

    this.state.animals.forEach(function(e) {
      if (e.id === id) {
        defaultArr.push(e);
      }
    });

    return defaultArr;
  };

  getAnimalAValues = () => {
    var arr = this.findDefaultValue(Number.parseInt(this.state.animalA));
    return this.prepareSelection(arr);
  };

  getAnimalBValues = () => {
    var arr;

    if (this.props.animalBId) {
      arr = this.findDefaultValue(this.props.animalBId);
    } else {
      arr = this.removeFromArray(
        this.state.animals,
        Number.parseInt(this.state.animalA)
      );
    }

    return this.prepareSelection(arr);
  };

  getDefaultValue = () => {
    var def;
    var propValue = this.props.value;

    this.values.forEach(function(e) {
      if (e.value === propValue) {
        def = e;
      }
    });

    return def;
  };

  onSubmit() {
    if (this.props.onSubmit) {
      this.props.onSubmit({
        value: this.state.value || this.props.value
      });
    } else {
      this.setState({ createRequested: true });

      this.props.createRelationship({
        animalAId: Number.parseInt(this.state.animalA),
        animalBId: this.props.animalBId || this.state.animalB,
        value: this.state.value
      });
    }
  }

  renderRedirect = () => {
    if (this.state.success) {
      return (
        <Redirect
          to={{
            pathname: `/animais/${this.state.animalA}`,
            state: {
              active: "relationships"
            }
          }}
        />
      );
    }
  };

  render() {
    if (!this.props.overrideValidation) {
      if (!hasPermission(this.context, "relationships", "create")) {
        return <ErrorPage statusCode="403" />;
      }
    }

    if (this.props.getCreateRelationshipLoading || this.state.loading) {
      return <Loading />;
    }

    const animalAValues = this.getAnimalAValues();
    const animalBValues = this.getAnimalBValues();

    return (
      <div id="create-animal" className="page" style={{ width: "500px" }}>
        {this.props.redirect ? this.props.redirect() : this.renderRedirect()}
        <PageHeader title={this.props.title || "Criar relação entre animais"} />

        <MessageContainer
          message={this.props.errorMessage || this.state.errorMessage}
          className="alert alert-danger"
        />

        <div className="form">
          <div className="form-group">
            <Selector
              options={animalAValues ? animalAValues : []}
              placeholder="Animal 1"
              onSelectorChange={this.onAnimalAChange}
              dataTip={helper.RELATIONSHIP_ANIMAL}
              defaultValue={animalAValues}
            />

            <Selector
              options={animalBValues ? animalBValues : []}
              placeholder="Animal 2"
              onSelectorChange={this.onAnimalBChange}
              dataTip={helper.RELATIONSHIP_ANIMAL}
              defaultValue={this.props.animalBId ? animalBValues : undefined}
            />

            <Selector
              options={this.values}
              placeholder="Tipo de relação"
              onSelectorChange={this.onValueChange}
              dataTip={helper.RELATIONSHIP_VALUE}
              defaultValue={
                this.props.value ? this.getDefaultValue() : undefined
              }
            />
          </div>
          <div className="actions-container">
            <button className="btn-medium" onClick={this.props.history.goBack}>
              Voltar
            </button>
            <button
              className="btn-confirm btn-medium"
              onClick={this.onSubmit.bind(this)}
            >
              Confirmar
            </button>
          </div>
        </div>
        <ReactTooltip delayShow={300} />
      </div>
    );
  }
}

Create.contextType = AppContext;

Create.propTypes = {
  // Props used in Edit
  // Animal A Id. Default value for the first selector.
  animalAId: PropTypes.number,
  // Overrides loading and error functions
  overrideValidation: PropTypes.bool,
  // Animal B Id. Default value for the second selector.
  animalBId: PropTypes.number,
  // Page Header title.
  title: PropTypes.string,
  // Error message to be displayed
  errorMessage: PropTypes.string,
  // Redirect function
  redirect: PropTypes.func,
  // Relationship value. Default value for the value selector.
  value: PropTypes.number,
  // OnSubmit function. Edit calls a different action.
  onSubmit: PropTypes.func
};

const mapStateToProps = ({ createRelationship }) => ({
  getCreateRelationshipSuccess:
    createRelationship && createRelationship.getCreateRelationshipSuccess,
  getCreateRelationshipFailure:
    createRelationship && createRelationship.getCreateRelationshipFailure,
  getCreateRelationshipLoading:
    createRelationship && createRelationship.getCreateRelationshipLoading,

  success: createRelationship && createRelationship.success,
  errorMessage: createRelationship && createRelationship.errorMessage,
  loading: createRelationship && createRelationship.loading
});

export default connect(
  mapStateToProps,
  { getCreateRelationship, createRelationship }
)(Create);
