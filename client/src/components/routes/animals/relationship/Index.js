import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getAnimalRelationships,
  removeRelationship
} from "../../../../store/actions/relationships.action";
import { Link } from "react-router-dom";
import { Loading } from "../../../Loading";
import {
  translateValue,
  convertToColor
} from "../../../../utils/translation/relationship.translation";
import Avatar from "../../../Avatar";
import Dropdown from "../../../Dropdown";
import StatusDot from "../../../StatusDot";
import Popup from "../../../Popup";
import { notifySuccess } from "../../../../utils/notifications";

import "./Index.css";

export class Index extends Component {
  state = {
    data: [],
    error: "",
    isLoading: true,
    isRemovePopupOpen: false,
    removeRequest: false,
    relationship: undefined
  };

  componentWillMount() {
    this.props.getAnimalRelationships(this.props.id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isLoading) {
      this.setState({
        isLoading: true
      });
    }

    if (nextProps.data) {
      this.setState({
        isLoading: false,
        data: nextProps.data
      });
    }

    if (nextProps.error) {
      this.setState({
        isLoading: false,
        error: nextProps.error
      });
    }

    if (nextProps.removeData && this.state.removeRequest) {
      notifySuccess("Relação removida com successo");
      this.setState({
        removeRequest: false
      });
      this.props.getAnimalRelationships(this.props.id);
    }
  }

  relationshipLinks = relationship => {
    return [
      {
        title: "Editar",
        url: "/animais/relacoes/editar/" + relationship.relationshipId
      },
      {
        title: "Remover",
        onClick: () => this.openRemovePopup(relationship)
      }
    ];
  };

  openRemovePopup = relationship => {
    this.setState({
      isRemovePopupOpen: true,
      relationship: relationship
    });
  };

  closeRemovePopup = () => {
    this.setState({
      isRemovePopupOpen: false,
      relationship: null
    });
  };

  removeRelationshipConfirm = () => {
    const { relationship } = this.state;

    if (relationship) {
      this.setState({
        removeRequest: true
      });
      this.props.removeRelationship(relationship.relationshipId);
    }
  };

  render() {
    const {
      data,
      error,
      isLoading,
      isRemovePopupOpen,
      relationship
    } = this.state;

    const { animal } = this.props;

    if (isLoading) {
      return <Loading />;
    }

    if (error) {
      return (
        <div className="info" style={{ padding: "10px 20px" }}>
          {error}
        </div>
      );
    }

    const emptyTableWarning = (
      <tr>
        <td>Este animal não tem nenhuma relação</td>
      </tr>
    );

    return (
      <>
        <div className="relationships-header">
          <span>Relações de {animal.name}</span>
          <Link
            className="create-relationship-button"
            to={`/animais/${this.props.id}/relacoes/criar`}
          >
            <span>+</span> Criar nova
          </Link>
        </div>
        <div className="relationships-content">
          <table className="table">
            <colgroup>
              <col width="80px" />
              <col width="40%" />
              <col />
              <col width="20px" />
            </colgroup>
            <tbody>
              {data.length === 0 ? emptyTableWarning : null}
              {data.map(r => (
                <tr key={r.relationshipId}>
                  <td>
                    <Avatar
                      size="35px"
                      object={{ photoUrl: r.photoUrl }}
                      notAnchor={true}
                    />
                  </td>
                  <td>
                    <b>{r.name}</b>
                  </td>
                  <td>
                    <StatusDot color={convertToColor(r)} />
                    <span>Relação {translateValue(r)}</span>
                  </td>
                  <td>
                    <Dropdown
                      toggle={<img src="/img/icon_more_vert.png" alt="More" />}
                      links={this.relationshipLinks(r)}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <Popup
            isOpen={isRemovePopupOpen}
            confirmClass="btn-danger"
            confirmText="Remover"
            title={`Tem a certeza que pretende remover a relação com ${
              relationship ? relationship.name : ""
            }?`}
            onConfirm={this.removeRelationshipConfirm}
            onCancel={this.closeRemovePopup}
          />
        </div>
      </>
    );
  }
}

const mapStateToProps = ({ getAnimalRelationships, removeRelationship }) => ({
  data: getAnimalRelationships && getAnimalRelationships.data,
  error: getAnimalRelationships && getAnimalRelationships.error,
  isLoading: getAnimalRelationships && getAnimalRelationships.isLoading,
  removeData: removeRelationship && removeRelationship.removeData,
  removeError: removeRelationship && removeRelationship.removeError,
  removeLoading: removeRelationship && removeRelationship.removeLoading
});

export default connect(
  mapStateToProps,
  { getAnimalRelationships, removeRelationship }
)(Index);
