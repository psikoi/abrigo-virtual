import React, { Component } from "react";
import { connect } from "react-redux";
import Loading from "../../../Loading";
import ErrorPage from "../../errors/ErrorPage";
import TabMenu from "../../employees/_general/TabMenu";
import ProfileCard from "../_general/ProfileCard";
import ProfileInfo from "../_general/ProfileInfo";
import Relationships from "../relationship/Index";
import { AppContext } from "../../../../contexts/AppContext";
import { getAnimal } from "../../../../store/actions/animals.action";
import { fetchAnimalQuarentines } from "../../../../store/actions/quarentines.action";
import { QuarentineTable } from "../../quarentines/_general/QuarentineTable";
import { hasPermission } from "../../../../utils/permissions";

export class Profile extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      items: [
        {
          title: "Informação",
          active: true,
          onClick: () => this.setActive(0)
        },
        {
          title: "Quarentenas",
          active: false,
          onClick: () => this.setActive(1)
        },
        {
          title: "Relações",
          active: false,
          onClick: () => this.setActive(2)
        }
      ],
      data: undefined,
      error: undefined
    };
  }

  componentWillMount() {
    document.body.style = "background: #eaeced !important";
    let id = this.props.match.params.id;
    this.props.getAnimal(id);
    this.props.fetchAnimalQuarentines(id);

    if (
      this.props.location.state &&
      this.props.location.state.active === "relationships"
    ) {
      this.props.location.state.active = null;

      this.activateRelationshipTab();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isLoading) {
      this.setState({
        isLoading: true
      });
    }

    if (nextProps.data) {
      this.setState({
        isLoading: false,
        data: nextProps.data
      });
    }

    if (nextProps.error) {
      this.setState({
        isLoading: false,
        error: nextProps.error
      });
    }

    if (nextProps.quarentines) {
      this.setState({ quarentines: nextProps.quarentines });
    }
  }

  render() {
    if (this.state.error) {
      return <ErrorPage statusCode={this.state.error.code} />;
    }

    if (this.state.isLoading || !this.state.data) {
      return <Loading />;
    }

    if (!hasPermission(this.context, "animal", "view")) {
      return <ErrorPage statusCode="403" />;
    }

    return (
      <div className="page" style={{ width: "850px" }}>
        <div className="profile-info">
          <ProfileCard animal={this.state.data} />
          <div className="main">
            <TabMenu
              items={this.state.items}
              notifications={this.state.notifications}
            />

            {this.getActiveContent()}
          </div>
        </div>
      </div>
    );
  }

  setActive(i) {
    let items = this.state.items;
    if (!items[i].active) {
      items.forEach((item, index) => {
        if (index === i) {
          item.active = true;
        } else {
          item.active = false;
        }
      });
    }
    this.setState({ items: items });
  }

  activateRelationshipTab = () => {
    this.setActive(2);
  };

  getActiveContent() {
    if (this.state.items[0].active) {
      return <ProfileInfo animal={this.state.data} />;
    } else if (this.state.items[1].active) {
      return (
        <QuarentineTable
          quarentines={this.state.quarentines}
          displayAvatar={false}
        />
      );
    } else if (this.state.items[2].active) {
      return (
        <Relationships
          id={this.props.match.params.id}
          animal={this.props.data}
        />
      );
    }
  }
}

Profile.contextType = AppContext;

const mapStateToProps = ({ getAnimal, animalQuarentines }) => ({
  data: getAnimal && getAnimal.data,
  error: getAnimal && getAnimal.error,
  isLoading: getAnimal && getAnimal.isLoading,
  quarentines: animalQuarentines && animalQuarentines.quarentines
});

export default connect(
  mapStateToProps,
  { getAnimal, fetchAnimalQuarentines }
)(Profile);
