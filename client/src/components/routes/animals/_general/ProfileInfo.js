import React, { Component } from "react";
import "../../../styles/ProfileInfo.css";
import {
  translateType,
  translateSize,
  translateGender
} from "../../../../utils/translation/animal.translation";

export class ProfileInfo extends Component {
  getAnimalAge = () => {
    const birthdate = this.props.animal.birthdate;
    const date = new Date(birthdate);

    var ageDifMs = Date.now() - date.getTime();
    var ageDate = new Date(ageDifMs);
    var age = Math.abs(ageDate.getUTCFullYear() - 1970);

    var ageString = age === 1 ? "ano" : "anos";

    return age + " " + ageString;
  };

  render() {
    const { animal } = this.props;

    return (
      <div className="info">
        <div>
          <section>
            <div className="info-field">
              <p>Tipo</p>
              <b>{translateType(animal)}</b>
            </div>

            <div className="info-field">
              <p>Porte</p>
              <b>{translateSize(animal)}</b>
            </div>
          </section>
          <section>
            <div className="info-field">
              <p>Raça</p>
              <b>{animal.breed}</b>
            </div>

            <div className="info-field">
              <p>Idade</p>
              <b>{this.getAnimalAge()}</b>
            </div>
          </section>
          <section>
            <div className="info-field">
              <p>Sexo</p>
              <b>{translateGender(animal)}</b>
            </div>

            <div className="info-field">
              <p>Casota</p>
              <b>{animal.kennelName}</b>
            </div>
          </section>
        </div>
        <div>
          <p>Informação adicional</p>
          <b>{animal.info ? animal.info : "---"}</b>
        </div>
      </div>
    );
  }
}

export default ProfileInfo;
