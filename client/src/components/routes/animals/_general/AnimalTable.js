import React, { Component } from "react";
import StatusDot from "../../../StatusDot";
import Dropdown from "../../../Dropdown";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { AppContext } from "../../../../contexts/AppContext";
import Avatar from "../../../Avatar";
import Popup from "../../../Popup";
import {
  translateType,
  translateGender,
  translateQuarentineStatus,
  convertToColor
} from "../../../../utils/translation/animal.translation";
import "../../../styles/Table.css";
import "./AnimalTable.css";
import { notifySuccess, notifyError } from "../../../../utils/notifications";

import { removeAnimal } from "../../../../store/actions/animals.action";

export class AnimalTable extends Component {
  state = {
    isFinishPopupOpen: false,
    selectedTask: null,
    taskDetails: undefined
  };

  state = {
    isRemovePopupOpen: false,
    isArchivePopupOpen: false,
    employee: null,
    newRemoveRequest: false,
    newArchiveRequest: false
  };

  openRemovePopup = animal => {
    this.setState({
      isRemovePopupOpen: true,
      animal
    });
  };

  closeRemovePopup = () => {
    this.setState({
      isRemovePopupOpen: false,
      animal: null
    });
  };

  removeAnimalConfirm = () => {
    const { animal } = this.state;

    if (animal) {
      this.setState({
        newRemoveRequest: true
      });
      this.props.removeAnimal(animal.animalId);
    }
  };

  animalActionLinks(animal) {
    return [
      {
        title: "Ver perfil",
        url: "/animais/" + animal.animalId
      },
      {
        title: "Editar",
        url: "/animais/editar/" + animal.animalId
      },
      {
        title: "Remover",
        onClick: () => this.openRemovePopup(animal)
      }
    ];
  }

  animalProfileLink(animal) {
    return `/animais/${animal.animalId}`;
  }

  componentWillReceiveProps(nextProps) {
    const { newRemoveRequest } = this.state;

    if (nextProps.error) {
      notifyError(nextProps.error);
    }

    if (nextProps.success && newRemoveRequest) {
      notifySuccess(nextProps.success);
      this.props.loadAnimals();
      this.setState({ newRemoveRequest: false });
    }
  }

  render() {
    const { isRemovePopupOpen, animal } = this.state;
    const emptyTableWarning = (
      <tr>
        <td>Nenhum resultado encontrado</td>
      </tr>
    );

    // Sort the data by descending id
    const animals = this.props.animals.sort((a, b) => {
      return b.animalId - a.animalId;
    });

    return (
      <>
        <table className="table animal-table">
          <colgroup>
            <col width="60px" />
            <col width="20%" />
            <col />
            <col />
            <col />
            <col />
            <col width="20px" />
          </colgroup>
          <tbody>
            {animals.length === 0 ? emptyTableWarning : null}
            {animals.map(animal => (
              <tr key={animal.animalId}>
                <td>
                  <Avatar size="40px" object={animal} isAnimal={true} />
                </td>
                <td>
                  <Link to={this.animalProfileLink(animal)}>
                    <b>{animal.name}</b>
                  </Link>
                </td>
                <td>{translateType(animal)}</td>
                <td>{translateGender(animal)}</td>
                <td>{animal.breed}</td>
                <td>
                  <StatusDot color={convertToColor(animal)} />
                  {translateQuarentineStatus(animal)}
                </td>
                <td>
                  <Dropdown
                    toggle={<img src="/img/icon_more_vert.png" alt="More" />}
                    links={this.animalActionLinks(animal)}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <Popup
          isOpen={isRemovePopupOpen}
          confirmClass="btn-danger"
          confirmText="Remover"
          title={`Tem a certeza que pretende remover o animal "${
            animal ? animal.name : ""
          }"?`}
          onConfirm={this.removeAnimalConfirm}
          onCancel={this.closeRemovePopup}
        />
      </>
    );
  }
}

AnimalTable.contextType = AppContext;

const mapStateToProps = ({ removeAnimal }) => ({
  success: removeAnimal && removeAnimal.success,
  error: removeAnimal && removeAnimal.error,
  isLoading: removeAnimal && removeAnimal.isLoading
});

export default withRouter(
  connect(
    mapStateToProps,
    { removeAnimal }
  )(AnimalTable)
);
