import React, { Component } from "react";
import "../../../styles/ProfileCard.css";
import { convertToColor } from "../../../../utils/translation/animal.translation";
import Avatar from "../../../Avatar";

export class ProfileCard extends Component {
  render() {
    const { animal } = this.props;
    const color = convertToColor(animal);

    return (
      <>
        <div className="card-container">
          <Avatar
            size="85px"
            object={animal}
            isAnimal={true}
            notAnchor={true}
          />

          <div>
            <b>{animal.name}</b>
            <p>{animal.breed}</p>
          </div>

          <div>
            <span
              className="status"
              style={{ borderColor: color, color: color }}
            >
              <b>{animal.inQuarentine ? "Em quarentena" : animal.kennelName}</b>
            </span>
          </div>
        </div>
      </>
    );
  }
}

export default ProfileCard;
