import React, { Component } from "react";
import { fetchLicense } from "../../../../store/actions/shelters.action";
import { connect } from "react-redux";
import Loading from "../../../Loading";
import ErrorPage from "../../errors/ErrorPage";
import { translateType } from "../../../../utils/translation/shelters.translation";
import { formatDateLong } from "../../../../utils/dates";
import Dropdown from "../../../Dropdown";
import { AppContext } from "../../../../contexts/AppContext";
import "./Details.css";

export class Details extends Component {
  state = {
    license: undefined,
    isLoading: true,
    errorMessage: undefined
  };

  licenseActionLinks(permission) {
    var links = [
      {
        title: "Alterar licença",
        url: "/canil/licenca"
      }
    ];

    return links;
  }

  componentWillMount() {
    this.props.fetchLicense();
    document.body.style = "background: #eaeced !important";
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.license) {
      this.setState({
        license: nextProps.license,
        isLoading: false
      });
    } else if (nextProps.error) {
      this.setState({
        isLoading: false,
        errorMessage: nextProps.error
      });
    }
  }

  render() {
    const { license } = this.state;

    if (this.state.isLoading) {
      return <Loading />;
    }

    if (this.state.errorMessage) {
      return <ErrorPage statusCode="403" />;
    }

    if (!this.state.isLoading && !this.state.errorMessage)
      return (
        <div className="shelters-details">
          <div className="top">
            <div className="text">
              <h4>{license.name}</h4>
              <span>{license.address}</span>
            </div>
            <div className="dropdown">
              <Dropdown
                toggle={<img src="/img/icon_more_vert.png" alt="More" />}
                links={this.licenseActionLinks()}
              />
            </div>
          </div>
          <hr />
          <div className="bot">
            <div className="left">
              <span>Id de licença:</span>
              <span>Tipo de licença:</span>
              <span>Último pagamento:</span>
              <span>Data de expiração:</span>
            </div>
            <div className="right">
              <span>{license._id}</span>
              <span>{translateType(license)}</span>
              <span>
                {formatDateLong(new Date(license.paymentDate), false)}
              </span>
              <span>
                {formatDateLong(new Date(license.expirationDate), false)}
              </span>
            </div>
          </div>
        </div>
      );
  }
}

Details.contextType = AppContext;

const mapStateToProps = ({ fetchLicense }) => ({
  license: fetchLicense && fetchLicense.license,
  error: fetchLicense && fetchLicense.error,
  isLoading: fetchLicense && fetchLicense.isLoading
});

export default connect(
  mapStateToProps,
  { fetchLicense }
)(Details);
