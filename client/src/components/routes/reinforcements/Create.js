import React, { Component } from "react";
import PageHeader from "../../PageHeader";
import { connect } from "react-redux";
import Schedule from "../../Schedule";
import ReinforcementLoading from "./_general/ReinforcementLoading";
import ReinforcementOptions from "./_general/ReinforcementOptions";
import { notifySuccess, notifyError } from "../../../utils/notifications";
import {
  fetchAvailableEmployees,
  createReinforcement
} from "../../../store/actions/reinforcements.action";

export class Create extends Component {
  state = {
    hasOptions: false,
    selectedEmployee: null
  };

  componentWillMount() {
    document.body.style = "background: #eaeced !important";
  }

  onConfirmSelection = async blocks => {
    this.setState({ blocks });
    await this.props.fetchAvailableEmployees(blocks);
  };

  onSelectEmployee = employee => {
    const { blocks } = this.state;
    const startBlock = blocks[0];
    const endBlock = blocks[blocks.length - 1];

    this.setState({ selectedEmployee: employee });
    this.props.createReinforcement(employee, startBlock, endBlock);
  };

  onSelectionClose = () => {
    this.setState({ hasOptions: false });
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.success && this.state.selectedEmployee !== null) {
      notifySuccess("Pedido de reforço adicionado com sucesso.");
      this.setState({ selectedEmployee: null });
      this.props.history.push("/reforços");
    } else if (nextProps.error) {
      notifyError(nextProps.error);
    } else {
      if (!nextProps.isAvailableLoading && !nextProps.isCreationLoading) {
        var thisScope = this;
        setTimeout(function() {
          thisScope.setState({
            isAvailableLoading: false,
            hasOptions: true
          });
        }, 1000);
      } else {
        if (!nextProps.isCreationLoading) {
          this.setState({
            isAvailableLoading: true
          });
        }
      }

      if (nextProps.data) {
        const { data } = nextProps.data;
        this.setState({ data });
      }
    }
  }

  render() {
    const { hasOptions, isAvailableLoading, data } = this.state;

    return (
      <div className="page">
        <PageHeader title="Solicitar Reforços" />
        <Schedule
          maxHeight="calc(100vh - 280px)"
          returnDays={false}
          excludeWeekend={true}
          reducedBlocks={true}
          onConfirmSelection={this.onConfirmSelection}
          blockPastDates={true}
        />
        {hasOptions ? (
          <ReinforcementOptions
            employees={data}
            onSelectEmployee={this.onSelectEmployee}
            onClose={this.onSelectionClose}
          />
        ) : null}
        {isAvailableLoading ? <ReinforcementLoading /> : null}
      </div>
    );
  }
}

const mapStateToProps = ({ availableEmployees, createReinforcement }) => ({
  data: availableEmployees && availableEmployees.data,
  isAvailableLoading:
    availableEmployees && availableEmployees.isAvailableLoading,
  success: createReinforcement && createReinforcement.success,
  error: createReinforcement && createReinforcement.error,
  isCreationLoading:
    createReinforcement && createReinforcement.isCreationLoading
});

export default connect(
  mapStateToProps,
  { fetchAvailableEmployees, createReinforcement }
)(Create);
