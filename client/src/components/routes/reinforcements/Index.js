import React, { Component } from "react";
import { connect } from "react-redux";
import PageHeader from "../../PageHeader";
import Loading from "../../Loading";
import ErrorPage from "../errors/ErrorPage";
import { Link } from "react-router-dom";
import "../../styles/ListPage.css";
import { notifySuccess } from "../../../utils/notifications";
import {
  fetchReinforcements,
  cancelReinforcement
} from "../../../store/actions/reinforcements.action";
import ReinforcementsTable from "./_general/ReinforcementTable";
import { translateStatus } from "../../../utils/translation/reinforcement.translation";

export class Index extends Component {
  state = {
    data: [],
    isLoading: true,
    errorMessage: undefined
  };

  componentWillMount() {
    this.props.fetchReinforcements();
    document.body.style = "background: #eaeced !important";
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.success && this.state.reload) {
      this.props.fetchReinforcements();
      this.setState({ reload: false });
      notifySuccess("Pedido de reforço cancelado com sucesso.");
    } else if (nextProps.data) {
      const { data } = nextProps.data;
      this.setState({
        data: data,
        isLoading: false
      });
    } else if (nextProps.error) {
      this.setState({
        isLoading: false,
        errorMessage: nextProps.error
      });
    }
  }

  onSearchChanged = e => {
    this.setState({ searchQuery: e.target.value });
  };

  filteredResults(results) {
    const { searchQuery } = this.state;

    if (!searchQuery || searchQuery.length === 0) {
      return results;
    }

    const query = searchQuery.toLowerCase();

    return results.filter(result => {
      const { status, employee } = result;
      const tStatus = translateStatus(status);

      return (
        tStatus.toLowerCase().includes(query) ||
        employee.name.toLowerCase().includes(query)
      );
    });
  }

  onCancelReinforcement = async reinforcement => {
    this.setState({ reload: true });
    await this.props.cancelReinforcement(reinforcement.id);
  };

  filterReinforcements(active) {
    const curDate = new Date();

    var hasPassed = function(date) {
      return date < curDate;
    };

    var isCanceled = function(status) {
      return status === "canceled";
    };

    if (active) {
      return this.state.data.filter(
        r => !hasPassed(new Date(r.endDate)) && !isCanceled(r.status)
      );
    }

    return this.state.data.filter(
      r => hasPassed(new Date(r.endDate)) || isCanceled(r.status)
    );
  }

  render() {
    if (this.state.isLoading) {
      return <Loading />;
    }

    if (this.state.errorMessage) {
      return <ErrorPage statusCode="403" />;
    }

    const activeReinforcements = this.filteredResults(
      this.filterReinforcements(true)
    );
    const inactiveReinforcements = this.filteredResults(
      this.filterReinforcements(false)
    );

    if (!this.state.isLoading && !this.state.errorMessage)
      return (
        <div className="page" style={{ background: "#eaeced" }}>
          <PageHeader title="Reforços" />
          <div className="search-container">
            <img src="/img/icons/search.svg" alt="" />
            <input
              onChange={this.onSearchChanged}
              type="search"
              placeholder="Pesquisar"
            />
          </div>
          <Link
            className="btn-confirm action-link-btn"
            to="/reforços/solicitar"
          >
            Solicitar
          </Link>
          <ReinforcementsTable
            onCancelReinforcement={this.onCancelReinforcement}
            loadReinforcements={this.props.fetchReinforcements}
            active={true}
            reinforcements={activeReinforcements}
          />
          <div className="table-section">
            {inactiveReinforcements.length === 0 ? "" : "Pedidos Inativos"}
          </div>
          <ReinforcementsTable
            onCancelReinforcement={this.onCancelReinforcement}
            loadReinforcements={this.props.fetchReinforcements}
            reinforcements={inactiveReinforcements}
          />
        </div>
      );
  }
}

const mapStateToProps = ({ reinforcements, cancelReinforcement }) => ({
  data: reinforcements && reinforcements.data,
  error:
    (reinforcements && reinforcements.error) ||
    (cancelReinforcement && cancelReinforcement.error),
  isReinforcementsLoading:
    reinforcements && reinforcements.isReinforcementsLoading,
  success: cancelReinforcement && cancelReinforcement.success,
  isCancelLoading: cancelReinforcement && cancelReinforcement.isCancelLoading
});

export default connect(
  mapStateToProps,
  { fetchReinforcements, cancelReinforcement }
)(Index);
