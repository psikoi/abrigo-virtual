import React, { Component } from "react";
import "../../../styles/Table.css";
import Avatar from "../../../Avatar";
import { Link } from "react-router-dom";
import StatusDot from "../../../StatusDot";
import {
  translateStatus,
  convertToColor
} from "../../../../utils/translation/reinforcement.translation";
import Dropdown from "../../../Dropdown";
import { appendZero } from "../../../../utils/general";
import { connect } from "react-redux";
import Popup from "../../../Popup";

export class ReinforcementTable extends Component {
  state = { isCancelPopupOpen: false };

  userActionLinks(reinforcement) {
    return [
      {
        title: "Cancelar",
        onClick: () => this.openCancelPopup(reinforcement)
      }
    ];
  }

  onCancelReinforcement = () => {
    this.props.onCancelReinforcement(this.state.toCancel);
  };

  openCancelPopup(reinforcement) {
    this.setState({
      isCancelPopupOpen: true,
      toCancel: reinforcement
    });
  }

  closeCancelPopup = () => {
    this.setState({
      isCancelPopupOpen: false,
      toCancel: null
    });
  };

  convertToString(reinforcement) {
    var startDate = new Date(reinforcement.startDate);
    var endDate = new Date(reinforcement.endDate);

    var z = appendZero;

    var date = z(startDate.getDate()) + "/" + z(startDate.getMonth() + 1);
    var timeStart = z(startDate.getHours()) + ":" + z(startDate.getMinutes());
    var timeEnd = z(endDate.getHours()) + ":" + z(endDate.getMinutes());

    return date + ", " + timeStart + " - " + timeEnd;
  }

  userProfileLink(employee) {
    return "/colaboradores/" + employee.id;
  }

  render() {
    const { isCancelPopupOpen } = this.state;
    const { active } = this.props;

    // Sort the data by descending id
    const reinforcements = this.props.reinforcements.sort((a, b) => {
      return b.id - a.id;
    });

    const emptyTableWarning = (
      <tr>
        <td>Nenhum resultado encontrado</td>
      </tr>
    );

    return (
      <>
        <table className="table">
          <colgroup>
            <col width="60px" />
            <col width="25%" />
            <col width="250px" />
            <col />
            <col width="20px" />
          </colgroup>
          <tbody>
            {reinforcements.length === 0 && active ? emptyTableWarning : null}
            {reinforcements.map(reinforcement => (
              <tr key={reinforcement.id}>
                <td>
                  <Avatar size="40px" object={reinforcement.employee} />
                </td>
                <td>
                  <Link to={this.userProfileLink(reinforcement.employee)}>
                    <b>{reinforcement.employee.name}</b>
                  </Link>
                </td>
                <td
                  style={{
                    fontFamily: "Work Sans"
                  }}
                >
                  {this.convertToString(reinforcement)}
                </td>
                <td>
                  <StatusDot color={convertToColor(reinforcement.status)} />
                  {translateStatus(reinforcement.status)}
                </td>
                <td>
                  <Dropdown
                    toggle={<img src="/img/icon_more_vert.png" alt="More" />}
                    links={this.userActionLinks(reinforcement)}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <Popup
          isOpen={isCancelPopupOpen}
          confirmClass="btn-danger"
          confirmText="Confirmar"
          title={"Tem a certeza que pretende cancelar este pedido de reforço?"}
          onConfirm={() => this.onCancelReinforcement()}
          onCancel={this.closeCancelPopup}
        />
      </>
    );
  }
}

const mapStateToProps = ({ removeEmployee }) => ({
  success: removeEmployee && removeEmployee.success,
  error: removeEmployee && removeEmployee.error,
  isRemoveEmployeeLoading:
    removeEmployee && removeEmployee.isRemoveEmployeeLoading
});

export default connect(mapStateToProps)(ReinforcementTable);
