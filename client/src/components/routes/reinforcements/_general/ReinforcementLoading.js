import React, { Component } from "react";
import Loading from "../../../Loading";
import "./ReinforcementPopups.css";

export class ReinforcementLoading extends Component {
  render() {
    return (
      <div className="popup-background">
        <div className="popup reinforcement-popup">
          <h6>Verificando voluntários disponíveis</h6>
          <Loading />
        </div>
      </div>
    );
  }
}

export default ReinforcementLoading;
