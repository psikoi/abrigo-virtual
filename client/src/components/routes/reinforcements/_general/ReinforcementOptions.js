import React, { Component } from "react";
import "./ReinforcementPopups.css";
import Avatar from "../../../Avatar";

export class ReinforcementOptions extends Component {
  state = {};

  onSelect(employee) {
    this.props.onSelectEmployee(employee);
    this.setState({ selectedEmployeeId: employee.employeeId });
  }

  onClose() {
    this.props.onClose();
  }

  render() {
    const { selectedEmployeeId } = this.state;

    return (
      <div className="popup-background">
        <div
          className="popup reinforcement-popup"
          style={{ marginTop: "100px" }}
        >
          <h6 style={{ marginBottom: "20px" }}>Voluntários disponíveis</h6>
          <button id="close-btn" onClick={() => this.onClose()}>
            <img src="/img/icons/clear.svg" alt="x" width="20px" />
          </button>
          {this.props.employees.map(employee => (
            <div key={employee.employeeId} className="reinforcement-option">
              <Avatar size="30px" object={employee} />
              <span>{employee.name}</span>
              <button
                disabled={selectedEmployeeId === employee.employeeId}
                onClick={() => this.onSelect(employee)}
                className="btn-confirm"
              >
                {selectedEmployeeId === employee.employeeId
                  ? "Enviando.."
                  : "Escolher"}
              </button>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default ReinforcementOptions;
