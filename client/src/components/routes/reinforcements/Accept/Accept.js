import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import PageHeader from "../../../PageHeader";
import { connect } from "react-redux";
import Schedule from "../../../Schedule";
import {
  getReinforcementByToken,
  acceptReinforcement,
  declineReinforcement
} from "../../../../store/actions/reinforcements.action";
import ErrorPage from "../../errors/ErrorPage";
import { notifySuccess, notifyError } from "../../../../utils/notifications";
import "./Accept.css";
import "../../../styles/Form.css";

export class Accept extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mounted: false
    };

    this.Schedule = React.createRef();
  }

  componentWillMount() {
    document.body.style = "background: #eaeced !important";
    var token = this.getTokenFromUrl();
    //check if token is null
    this.props.getReinforcementByToken(token);
  }

  getTokenFromUrl = () => {
    let search = window.location.search;
    let params = new URLSearchParams(search);
    return params.get("token");
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.success) {
      this.setState({
        mounted: true,
        reinforcement: nextProps.success.data
      });
    } else if (nextProps.error) {
      this.setState({
        mounted: true,
        invalid: true
      });
    }

    if (nextProps.acceptSuccess) {
      this.reinforcementActionSuccess(
        "Reforço aceite com sucesso",
        nextProps.acceptSuccess
      );
    } else if (nextProps.acceptError) {
      notifyError(nextProps.acceptError);
    }

    if (nextProps.declineSuccess) {
      this.reinforcementActionSuccess(
        "Reforço recusado com sucesso",
        nextProps.declineSuccess
      );
    } else if (nextProps.acceptError) {
      notifyError(nextProps.declineError);
    }
  }

  reinforcementActionSuccess = (message, success) => {
    notifySuccess(message);
    this.setState({
      actionSuccess: success
    });
  };

  refuse = () => {
    this.props.declineReinforcement(this.getTokenFromUrl());
  };

  accept = () => {
    this.props.acceptReinforcement(
      this.getTokenFromUrl(),
      this.Schedule.current.convertedBlocks()
    );
  };

  render() {
    if (!this.state.mounted) {
      return null;
    } else if (
      this.state.invalid ||
      !this.state.reinforcement ||
      !isNaN(this.getTokenFromUrl())
    ) {
      return <ErrorPage statusCode="404" />;
    } else if (this.state.actionSuccess) {
      return (
        <Redirect
          to={{
            pathname: "/"
          }}
        />
      );
    }

    var reinforcement = this.state.reinforcement;
    var startDate = new Date(reinforcement.startDate);
    var endDate = new Date(reinforcement.endDate);
    return (
      <div className="page">
        <PageHeader title="Pedido de reforço" />
        <div className="reinforcement-request">
          <h6>
            O abrigo virtual solicitou a sua ajuda para um período de reforços.
            Verifique abaixo o seu novo horário parcial sugerido.
          </h6>
        </div>
        <br />
        <Schedule
          ref={this.Schedule}
          maxHeight="calc(100vh - 220px)"
          excludeWeekend={true}
          reducedBlocks={true}
          readOnly={true}
          returnDays={true}
          importedRange={{ startDate, endDate }}
        />
        <div className="form">
          <div className="actions-container">
            <button className="btn-danger btn-medium" onClick={this.refuse}>
              Recusar
            </button>
            <button className="btn-confirm btn-medium" onClick={this.accept}>
              Aceitar
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ acceptReinforcement }) => ({
  success: acceptReinforcement && acceptReinforcement.success,
  error: acceptReinforcement && acceptReinforcement.error,
  isGetReinforcementLoading:
    acceptReinforcement && acceptReinforcement.isGetReinforcementLoading,
  acceptSuccess: acceptReinforcement && acceptReinforcement.acceptSuccess,
  acceptError: acceptReinforcement && acceptReinforcement.acceptError,
  declineSuccess: acceptReinforcement && acceptReinforcement.declineSuccess,
  declineError: acceptReinforcement && acceptReinforcement.declineError
});

export default connect(
  mapStateToProps,
  { getReinforcementByToken, acceptReinforcement, declineReinforcement }
)(Accept);
