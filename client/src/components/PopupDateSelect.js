import React, { Component } from "react";
import "./styles/Form.css";

export class PopupDateSelect extends Component {
  state = {
    selectedDate: new Date()
  };

  onConfirm = () => {
    this.props.onConfirm(this.state.selectedDate);
    this.setState({
      selectedDate: new Date()
    });
  };

  onDateChanged = e => {
    this.setState({
      selectedDate: new Date(e.target.value)
    });
  };

  stopPropagation = e => {
    e.stopPropagation();
  };

  render() {
    if (!this.props.isOpen) {
      return null;
    }

    return (
      <div className="popup-background" onClick={this.props.onCancel}>
        <div
          className="popup"
          onClick={this.stopPropagation}
          style={{ textAlign: "center" }}
        >
          <b>Selecionar data</b>
          <br /> <br />
          <input
            onChange={this.onDateChanged}
            className="custom-input"
            value={this.state.selectedDate.toISOString().substr(0, 10)}
            type="date"
          />
          <br /> <br />
          <div className="popup-buttons">
            <button
              onClick={this.props.onCancel}
              style={{ marginRight: "20px" }}
            >
              Cancelar
            </button>
            <button onClick={this.onConfirm} className="btn-confirm">
              Confirmar
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default PopupDateSelect;
