import React, { Component } from "react";

export class PageHeader extends Component {
  render() {
    const { title, subTitle } = this.props;
    return (
      <div className="page-header">
        {title ? <h1>{title}</h1> : <h2>{subTitle}</h2>}
        <hr />
      </div>
    );
  }
}

export default PageHeader;
