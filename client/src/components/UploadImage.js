import React, { Component } from "react";
import "./styles/Form.css";

export class UploadImage extends Component {
  state = {
    src: this.props.url
  };

  render() {
    return (
      <div className="image-content" data-tip={this.props.dataTip}>
        <img
          id="imageViewer"
          alt="Carregar foto"
          src={this.state.src || this.props.url}
          style={{ display: this.getImageDisplay() }}
          onClick={this.openChooseFileDialog}
        />
        <button
          style={{ display: this.getImageDisplay() }}
          onClick={() => {
            this.setSrcImage(undefined);
            this.props.onImageUploaded(undefined);
          }}
        >
          Remover foto
        </button>

        <label
          style={{ display: this.getLabelDisplay() }}
          onClick={this.openChooseFileDialog}
        >
          Carregar foto
        </label>

        <input
          type="file"
          id="chooseFile"
          name="image"
          accept="image/png, image/jpeg"
          style={{ display: "none" }}
          onChange={() => {
            this.uploadImage(this.setSrcImage, this.props.onImageUploaded);
          }}
        />
      </div>
    );
  }

  getImageDisplay() {
    return this.state.src || this.props.url ? "" : "none";
  }

  getLabelDisplay() {
    return this.state.src || this.props.url ? "none" : "";
  }

  openChooseFileDialog() {
    document.getElementById("chooseFile").click();
  }

  setSrcImage = data => {
    this.setState({ src: data });
  };

  uploadImage(setSrc, onImageUploaded) {
    let input = document.getElementById("chooseFile");

    let reader = new FileReader();
    reader.onloadend = function() {
      onImageUploaded(reader.result);
      setSrc(reader.result);

      // Reset input to allow upload when image is removed
      input.value = "";
    };

    if (input.files[0]) {
      reader.readAsDataURL(input.files[0]);
    }
  }
}

export default UploadImage;
