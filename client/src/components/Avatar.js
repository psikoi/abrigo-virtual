import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./styles/Avatar.css";

export class Avatar extends Component {
  render() {
    const avatarStyle = {
      width: this.props.size || "35px",
      height: this.props.size || "35px",
      zIndex: this.props.zIndex,
      position: this.props.position,
      left: this.props.leftMargin
    };

    const imageStyle = {
      border: this.props.imgBorder,
      borderRadius: this.props.imgBorderRadius
    };

    const url = this.getUrl(this.props.object, this.props.isAnimal);
    const photoUrl = this.getProfilePhoto(
      this.props.object,
      this.props.isAnimal
    );

    return (
      <div className="avatar" style={avatarStyle}>
        {this.props.notAnchor || !url ? (
          <img src={photoUrl} style={imageStyle} alt="" />
        ) : (
          <Link to={url}>
            <img src={photoUrl} style={imageStyle} alt="" />
          </Link>
        )}
      </div>
    );
  }

  getUrl(object, isAnimal) {
    if (!object) return;
    return isAnimal
      ? "/animais/" + (object.animalId || object.id)
      : "/colaboradores/" + (object.id ? object.id : object.employeeId);
  }

  getProfilePhoto(object, isAnimal) {
    if (object && object.photoUrl) return this.props.object.photoUrl;

    return isAnimal ? "/img/default_animal.png" : "/img/default_user.png";
  }
}

export default Avatar;
