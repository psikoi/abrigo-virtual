import React, { Component } from "react";
import "./styles/Popup.css";

export class Popup extends Component {
  render() {
    if (!this.props.isOpen) {
      return null;
    }

    return (
      <div className="popup-background" onClick={this.props.onCancel}>
        <div className="popup">
          <h6>{this.props.title}</h6>
          <p>{this.props.message}</p>

          <div className="popup-buttons">
            <button
              onClick={this.props.onCancel}
              style={{ marginRight: "20px" }}
            >
              {this.props.cancelText || "Cancelar"}
            </button>
            {!this.props.dontNeedConfirmButton ? (
              <button
                onClick={this.props.onConfirm}
                className={this.props.confirmClass}
              >
                {this.props.confirmText || "Confirmar"}
              </button>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

export default Popup;
