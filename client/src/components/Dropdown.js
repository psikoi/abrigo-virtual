import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./styles/Dropdown.css";

export class Dropdown extends Component {
  // Prevents the dropdown from propagating
  // the toggle click to any higher up elements
  stopPropagation(e) {
    e.stopPropagation();
  }

  onButtonClick(e, link) {
    this.stopPropagation(e);
    link.onClick();
  }

  render() {
    return (
      <div className="btn-group">
        <button
          className="dropdown-toggle"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
          onClick={e => this.stopPropagation(e)}
        >
          {this.props.toggle}
        </button>
        <div className="dropdown-menu dropdown-menu-right">
          {this.props.links.map((link, i) =>
            link.url ? (
              <Link
                key={i}
                to={link.url}
                className="dropdown-item"
                onClick={e => this.stopPropagation(e)}
              >
                {link.title}
              </Link>
            ) : (
              <button
                key={i}
                onClick={e => this.onButtonClick(e, link)}
                className="dropdown-item"
              >
                {link.title}
              </button>
            )
          )}
        </div>
      </div>
    );
  }
}

export default Dropdown;
