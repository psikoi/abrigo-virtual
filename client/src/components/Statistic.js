import React, { Component } from "react";
import "./styles/Statistic.css";
import Selector from "./Selector";
import PropTypes from "prop-types";
import Chart from "react-google-charts";
import Loading from "./Loading";
import PopupDateSelect from "./PopupDateSelect";

export class Statistic extends Component {
  state = {
    currentDateType: 0,
    isDateSelectionOpen: false
  };

  noData = "no_data";

  dateOptions = [
    { value: "week", label: "Esta semana" },
    { value: "month", label: "Este mês" },
    { value: "year", label: "Este ano" },
    { value: "all", label: "Sempre" },
    { value: "select", label: "Selecionar data" }
  ];

  componentWillReceiveProps(nextProps) {
    this.loadStatistic(this.timeParam(), nextProps.baseUrl);
  }

  async componentDidMount() {
    this.loadStatistic(this.timeParam());
  }

  timeParam = () => {
    return this.props.hideDateSelector ? undefined : this.dateOptions[0].value;
  };

  buildUrl = date => {
    var url = this.state.url ? this.state.url : this.props.baseUrl;

    return date ? this.props.baseUrl + date : url;
  };

  buildNewUrl = (date, url) => {
    return date ? url + date : url;
  };

  async loadStatistic(date, newUrl) {
    const url = newUrl ? this.buildNewUrl(date, newUrl) : this.buildUrl(date);

    if (url) {
      try {
        let payload = await fetch(url, {
          credentials: "include",
          method: "GET"
        });

        var response = await payload.json();
        this.setState({ data: this.props.parseData(response.data) });
      } catch (errorDic) {
        this.setState({ data: this.noData });
      }
    }

    this.setState({
      url: date ? this.props.baseUrl + date : url
    });
  }

  onDateTypeChanged = dateValue => {
    var typeIndex = -1;

    this.dateOptions.forEach((e, index) => {
      if (e.value === dateValue.value) {
        typeIndex = index;
        this.setState({ currentDateType: index });
      }
    });

    this.setState({ isDateSelectionOpen: typeIndex === 4 });

    if (typeIndex !== 4) {
      this.loadStatistic(dateValue.value);
    }
  };

  onDateSelected = d => {
    var dateUrl = d instanceof Date ? d.toISOString().substr(0, 10) : d;

    this.loadStatistic(dateUrl);
    this.setState({ isDateSelectionOpen: false });
  };

  onDateCanceled = () => {
    this.setState({ isDateSelectionOpen: false });
  };

  buildChart = (data, type, showLegend) => {
    if (!data) {
      return <Loading />;
    }

    if (data === this.noData) {
      return (
        <div className="statistic-no-data">
          <span>Não foram encontradas estatísticas</span>
        </div>
      );
    }

    return (
      <Chart
        width={"100%"}
        height={"100%"}
        chartType={type}
        loader={<Loading />}
        data={data}
        options={{
          legend: showLegend ? "bottom" : "none"
        }}
      />
    );
  };

  render() {
    const { currentDateType, isDateSelectionOpen, data } = this.state;
    const { title, type, hideDateSelector } = this.props;
    const showLegend = type === "PieChart";
    const selectorValue = this.dateOptions[currentDateType].value;

    const chart = this.buildChart(data, type, showLegend);

    return (
      <>
        <PopupDateSelect
          isOpen={isDateSelectionOpen}
          onConfirm={this.onDateSelected}
          onCancel={this.onDateCanceled}
        />
        <div className="statistic-panel">
          <div className="statistic-header">
            <span>{title}</span>
            {hideDateSelector ? null : (
              <Selector
                className="statistic-selector"
                height={15}
                disableUppercase={true}
                options={this.dateOptions}
                value={selectorValue}
                onSelectorChange={this.onDateTypeChanged}
              />
            )}
          </div>
          <div className="statistic-content">{chart}</div>
        </div>
      </>
    );
  }
}

Statistic.propTypes = {
  baseUrl: PropTypes.string,
  title: PropTypes.string,
  hideDateSelector: PropTypes.bool,
  // Parses API data to chart readable data
  parseData: PropTypes.func.isRequired,

  // Available Types: https://www.npmjs.com/package/react-google-charts
  // Gráfico circular: PieChart https://react-google-charts.com/pie-chart
  // Gráfico linhas: LineChart / Line https://react-google-charts.com/line-chart
  // Gráfico barras: BarChart / Bar https://react-google-charts.com/bar-chart
  type: PropTypes.string
};

export default Statistic;
