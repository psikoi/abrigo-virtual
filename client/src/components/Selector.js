import React, { Component } from "react";
import Select from "react-select";

export class Selector extends Component {
  state = {
    selectedOption: null,
    multiple: false
  };

  handleChange = selectedOption => {
    this.setState({ selectedOption });
    this.props.onSelectorChange(selectedOption);
  };

  /**
   * The way react-select handles styling is strange,
   * this can't be done with regular css classes.
   * So for now, it has to be done this way.
   */
  style = {
    valueContainer: styles => ({
      ...styles,
      height: `${this.props.height || 45}px !important`
    }),
    indicatorSeparator: styles => ({
      ...styles,
      display: "none"
    }),
    control: (styles, state) => ({
      ...styles,
      boxShadow: "none",
      textTransform: this.props.disableUppercase ? "none" : "uppercase",
      padding: "0px 5px",
      fontFamily: "Raleway",
      fontSize: "12px",
      fontWeight: "500",
      borderRadius: "5px",
      color: "#9e9e9e",
      "&:hover": { borderColor: "#36a890" },
      border: "1px solid #9e9e9e"
    }),
    option: styles => ({
      ...styles,
      fontSize: "12px",
      textTransform: this.props.disableUppercase ? "none" : "uppercase"
    })
  };

  render() {
    const { selectedOption } = this.state;
    return (
      <div data-tip={this.props.dataTip}>
        <Select
          className={"select " + (this.props.className || "")}
          isSearchable={this.props.isSearchable || false}
          placeholder={this.props.placeholder || ""}
          styles={this.style}
          onChange={this.handleChange}
          options={this.props.options}
          value={
            Array.isArray(this.props.value)
              ? this.props.value
              : this.props.options.find(
                  option =>
                    option.value === (selectedOption || this.props.value)
                )
          }
          isMulti={this.props.isMulti}
          defaultValue={this.props.defaultValue || undefined}
        />
      </div>
    );
  }
}

export default Selector;
