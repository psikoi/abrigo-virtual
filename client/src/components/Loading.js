import React, { Component } from "react";
import "./styles/Loading.css";

export class Loading extends Component {
  render() {
    return (
      <img
        id="loading"
        style={this.props.customStyle}
        src="/img/loading.png"
        alt="Loading.."
      />
    );
  }
}

export default Loading;
