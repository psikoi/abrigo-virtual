import React, { Component } from "react";
import "./styles/AvatarCounter.css";

export class AvatarCounter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      names: undefined
    };
  }

  buildPopup = animals => {
    var strNames = "";
    animals.forEach((animal, index) => {
      if (index >= this.props.maxAvatars) {
        strNames += animal.animalName + "\n";
      }
    });
    this.setState({ names: strNames });
  };

  componentWillMount() {
    this.buildPopup(this.props.animals);
  }

  render() {
    const avatarCounterSize = {
      width: this.props.size || "30px",
      height: this.props.size || "30px",
      left: this.props.leftMargin
    };

    return (
      <div className="avatarCounter" style={avatarCounterSize}>
        <b data-toggle="tooltip" data-placement="top" title={this.state.names}>
          +{this.props.number}
        </b>
      </div>
    );
  }
}
export default AvatarCounter;
