import React, { Component } from "react";
import { slide as Menu } from "react-burger-menu";
import { Link, withRouter } from "react-router-dom";
import ProfileCard from "./ProfileCard";
import Popup from "../Popup";
import { logout } from "../../store/actions/sessions.action";
import { connect } from "react-redux";
import { AppContext } from "../../contexts/AppContext";

export class SideMenu extends Component {
  constructor() {
    super();
    this.closeLogoutPopup = this.closeLogoutPopup.bind(this);
    this.openLogoutPopup = this.openLogoutPopup.bind(this);
    this.logout = this.logout.bind(this);
  }

  state = {
    isMenuOpen: false,
    isLogoutPopupOpen: false
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.success) {
      this.context.update();
      window.location = "/";
    }
  }

  handleStateChange(state) {
    this.setState({ isMenuOpen: state.isOpen });
  }

  closeMenu() {
    this.setState({ isMenuOpen: false });
  }

  logout() {
    this.props.logout();
    this.closeLogoutPopup();
    this.closeMenu();
  }

  openLogoutPopup() {
    this.setState({
      isLogoutPopupOpen: true
    });
  }

  closeLogoutPopup() {
    this.setState({
      isLogoutPopupOpen: false
    });
  }

  render() {
    const { user, shelter } = this.context;

    if (!user) {
      return null;
    }

    return (
      <React.Fragment>
        <Menu
          right
          isOpen={this.state.isMenuOpen}
          onStateChange={state => this.handleStateChange(state)}
        >
          <ProfileCard onClick={() => this.closeMenu()} user={user} />
          <hr />

          <Link to="/colaboradores" onClick={() => this.closeMenu()}>
            <img src="/img/icons/group.svg" alt="Colaboradores" />
            Colaboradores
          </Link>

          <Link to="/tarefas" onClick={() => this.closeMenu()}>
            <img src="/img/icons/task.svg" alt="Tarefas" />
            Tarefas
          </Link>

          <Link to="/reforços" onClick={() => this.closeMenu()}>
            <img src="/img/icons/reinforcements.svg" alt="Reforços" />
            Reforços
          </Link>

          <Link to="/animais" onClick={() => this.closeMenu()}>
            <img src="/img/icons/paw.svg" alt="Animais" />
            Animais
          </Link>

          <Link to="/quarentenas" onClick={() => this.closeMenu()}>
            <img src="/img/icons/quarentenas.svg" alt="Quarentenas" />
            Quarentenas
          </Link>

          <Link to="/casotas" onClick={() => this.closeMenu()}>
            <img src="/img/icons/home.svg" alt="Casotas" />
            Casotas
          </Link>

          <Link to="/estatisticas" onClick={() => this.closeMenu()}>
            <img src="/img/icons/statistics.svg" alt="Estatísticas" />
            Estatísticas
          </Link>
          <hr />

          <div onClick={this.openLogoutPopup}>
            <img src="/img/icons/logout.svg" alt="Terminar sessão" />
            Terminar sessão
          </div>
          <hr />
          <Link to="/canil" onClick={() => this.closeMenu()}>
            <img src="/img/icons/shelter-info.svg" alt="Canil" />
            {shelter.name}
          </Link>
        </Menu>

        <Popup
          isOpen={this.state.isLogoutPopupOpen}
          confirmClass="btn-danger"
          confirmText="Terminar"
          title="Tem a certeza que pretende terminar a sua sessão?"
          onConfirm={this.logout}
          onCancel={this.closeLogoutPopup}
        />
      </React.Fragment>
    );
  }
}

SideMenu.contextType = AppContext;

const mapStateToProps = ({ logout }) => ({
  success: logout && logout.success
});

export default connect(
  mapStateToProps,
  { logout }
)(withRouter(SideMenu));
