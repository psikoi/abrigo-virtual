import React, { Component } from "react";
import Avatar from "../Avatar";
import { Link } from "react-router-dom";
import { translateType } from "../../utils/translation/employee.translation";

export class ProfileCard extends Component {
  render() {
    const { user } = this.props;

    if (!user) {
      return null;
    }

    const url = "/colaboradores/" + user.employeeId;

    return (
      <div className="profile-card" onClick={this.props.onClick}>
        <Avatar object={user} size="55px" />
        <Link to={url}>
          <p>
            <span id="nav-user-name">{user.name}</span>
            <span id="nav-user-type">{translateType(user)}</span>
          </p>
        </Link>
      </div>
    );
  }
}

export default ProfileCard;
