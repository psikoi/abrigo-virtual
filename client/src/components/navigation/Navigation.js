import React, { Component } from "react";
import "./Navigation.css";
import SideMenu from "./SideMenu";
import { Link, withRouter } from "react-router-dom";

export class Navigation extends Component {
  render() {
    return (
      <nav>
        <div className="nav">
          <Link to="/">
            <img src="/img/logo.png" alt="Abrigo Virtual" />
          </Link>
        </div>
        {this.props.location.pathname === "/iniciar-sessao" ? null : (
          <SideMenu />
        )}
      </nav>
    );
  }
}

export default withRouter(Navigation);
