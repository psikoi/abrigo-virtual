import React, { Component } from "react";
import PropTypes from "prop-types";
import "./styles/Schedule.css";
import {
  getWeek,
  formatDate,
  formatTimeBlock,
  formatDayOfWeek,
  getDailyBlocks,
  addDays,
  setHours
} from "../utils/dates";

export class Schedule extends Component {
  state = {
    days: [],
    selectedBlocks: []
  };

  getBaseDay() {
    return this.state.baseDay || new Date();
  }

  componentWillMount() {
    var {
      excludeWeekend,
      startHour,
      endHour,
      blocksPerHour,
      baseDay,
      importedRange,
      importedBlocks
    } = this.props;

    // Define default values for time block range
    startHour = startHour || 7;
    endHour = endHour || 19;
    blocksPerHour = blocksPerHour || 2;
    baseDay =
      (importedRange && importedRange.startDate) || (baseDay || new Date());

    this.setState(
      {
        baseDay,
        days: getWeek(baseDay, excludeWeekend),
        blocks: getDailyBlocks(startHour, endHour, blocksPerHour),
        startHour,
        endHour,
        blocksPerHour
      },
      () => {
        if (importedBlocks || importedRange) {
          this.setState({ selectedBlocks: this.parseBlocks(importedBlocks) });
        }

        this.setState({ staticBlocks: this.parseBlocks() });
      }
    );
  }

  componentWillReceiveProps(nextProps) {
    const { importedBlocks } = nextProps;
    if (importedBlocks) {
      this.setState({
        selectedBlocks: this.parseBlocks(importedBlocks)
      });
    } else {
      this.setState({ staticBlocks: this.parseBlocks() });
    }
  }

  componentWillUnmount() {
    this.setState({
      selectedBlocks: [],
      staticBlocks: []
    });
  }

  /**
   * Returns whether or not a given time block is selected
   */
  isBlockSelected(dayIndex, blockIndex) {
    const { selectedBlocks } = this.state;

    if (!selectedBlocks || selectedBlocks.length === 0) {
      return false;
    }

    for (var i = 0; i < selectedBlocks.length; i++) {
      var block = selectedBlocks[i];
      if (block.blockIndex === blockIndex && block.dayIndex === dayIndex) {
        return true;
      }
    }

    return false;
  }

  /**
   * Returns whether or not a given time block is selected
   */
  isBlockStatic(dayIndex, blockIndex) {
    const { staticBlocks } = this.state;

    if (!staticBlocks || staticBlocks.length === 0) {
      return false;
    }

    for (var i = 0; i < staticBlocks.length; i++) {
      var block = staticBlocks[i];
      if (block.blockIndex === blockIndex && block.dayIndex === dayIndex) {
        return true;
      }
    }

    return false;
  }

  /**
   * Selects a time block with the given coordinates,
   * checking for invalid selections
   */
  selectBlock(dayIndex, blockIndex) {
    const { selectedBlocks } = this.state;
    const { allowCrossDay, blockPastDates, importedStaticBlocks } = this.props;

    if (blockPastDates && this.isPast(dayIndex, blockIndex)) {
      return;
    }

    if (importedStaticBlocks && !this.isBlockStatic(dayIndex, blockIndex)) {
      return;
    }

    if (selectedBlocks && selectedBlocks.length > 0) {
      var lastBlock = selectedBlocks[selectedBlocks.length - 1];

      // Prevent dragging across different days
      if (!allowCrossDay && lastBlock.dayIndex !== dayIndex) {
        return;
      }

      // Prevent selecting unconsecutive blocks
      if (!allowCrossDay && Math.abs(lastBlock.blockIndex - blockIndex) !== 1) {
        return;
      }

      // Prevent selecting duplicate blocks
      if (this.isBlockSelected(dayIndex, blockIndex)) {
        return;
      }
    }

    this.setState({
      selectedBlocks: [...(selectedBlocks || []), { dayIndex, blockIndex }]
    });
  }

  /**
   * OnHover event for the time blocks.
   * If the user is dragging their mouse, this will
   * add the current block to the selected list
   */
  onHover(dayIndex, blockIndex) {
    if (!this.state.mouseDown) {
      return;
    }

    this.selectBlock(dayIndex, blockIndex);
  }

  /**
   * Begins a new selection, resetting the previous selection
   */
  beginSelection(dayIndex, blockIndex) {
    if (this.props.readOnly) {
      return;
    }

    if (this.props.blockPastDates && this.isPast(dayIndex, blockIndex)) {
      return;
    }

    if (this.isBlockSelected(dayIndex, blockIndex)) {
      this.deselectBlock(dayIndex, blockIndex);
      return;
    }

    const { allowCrossDay } = this.props;

    if (allowCrossDay) {
      this.setState({ mouseDown: true }, () => {
        this.selectBlock(dayIndex, blockIndex);
      });
    } else {
      this.setState({ mouseDown: true, selectedBlocks: [] }, () => {
        this.selectBlock(dayIndex, blockIndex);
      });
    }
  }

  deselectBlock(dayIndex, blockIndex) {
    var { selectedBlocks } = this.state;
    for (var i = 0; i < selectedBlocks.length; i++) {
      if (
        selectedBlocks[i].dayIndex === dayIndex &&
        selectedBlocks[i].blockIndex === blockIndex
      ) {
        selectedBlocks.splice(i, 1);
      }
    }
    this.setState({ selectedBlocks });
  }

  isPast(dayIndex, blockIndex) {
    const { date, time } = this.toAbsolute(dayIndex, blockIndex);
    date.setHours(time.hours);
    date.setMinutes(time.minutes);
    date.setSeconds(0);

    return date < new Date();
  }

  /**
   * Converts a relative time block (internal) to absolute
   */
  toRelative(date, hours, minutes) {
    const { startHour, blocksPerHour } = this.state;
    const fullMinutes = hours * 60 + minutes;
    const blockIndex = (fullMinutes - startHour * 60) / (60 / blocksPerHour);

    if (date instanceof Date) {
      const day = date.getDay();
      const fixedDay = day === 0 ? 6 : day - 1;
      return { dayIndex: fixedDay, blockIndex };
    }

    if (date > 7) {
      var dateObj = new Date(date);
      const day = dateObj.getDay();
      const fixedDay = day === 0 ? 6 : day - 1;
      return { dayIndex: fixedDay, blockIndex };
    }

    return { dayIndex: date === 0 ? 6 : date - 1, blockIndex };
  }

  /**
   * Converts a relative block to a absolute time block
   */
  toAbsolute(dayIndex, blockIndex) {
    const { blocksPerHour, startHour, days } = this.state;
    const { dateLessMode, returnDays } = this.props;

    if (days.length <= dayIndex) {
      throw new Error("Invalid day index " + dayIndex);
    }

    const blockMinutes = blockIndex * (60 / blocksPerHour);
    const adjustedMinutes = startHour * 60 + blockMinutes;

    const date = days[dayIndex];
    const day = date.getDay();
    const hours = startHour + Math.floor(blockMinutes / 60);
    const minutes = adjustedMinutes - hours * 60;

    if (dateLessMode || returnDays) {
      return { day, time: { hours, minutes } };
    } else {
      return { date, time: { hours, minutes } };
    }
  }

  /**
   * Converts the selected block list and sorts it by date
   */
  convertedBlocks() {
    const { selectedBlocks } = this.state;
    var convertedBlocks = [];
    const context = this;

    selectedBlocks.forEach(function({ dayIndex, blockIndex }) {
      convertedBlocks.push(context.toAbsolute(dayIndex, blockIndex));
    });

    convertedBlocks = convertedBlocks.sort(function(a, b) {
      if (a.date) {
        if (a.date.getTime() < b.date.getTime()) {
          return -1;
        }

        if (a.date.getTime() > b.date.getTime()) {
          return 1;
        }
      } else {
        if (a.day < b.day) {
          return -1;
        }

        if (a.day > b.day) {
          return 1;
        }
      }

      if (a.time.hours < b.time.hours) {
        return -1;
      }

      if (a.time.hours > b.time.hours) {
        return 1;
      }

      if (a.time.minutes < b.time.minutes) {
        return -1;
      }

      if (a.time.minutes > b.time.minutes) {
        return 1;
      }

      return 0;
    });

    return convertedBlocks;
  }

  parseBlocks(importedBlocks) {
    const { importedRange, importedStaticBlocks } = this.props;

    if (!importedBlocks && !importedRange && !importedStaticBlocks) {
      return;
    }

    if (importedRange) {
      const { startDate, endDate } = importedRange;
      const { blocksPerHour } = this.state;

      const minuteDiff = (endDate.getTime() - startDate.getTime()) / 1000 / 60;
      const blockSpan = minuteDiff / (60 / blocksPerHour);

      const startBlock = this.toRelative(
        startDate,
        startDate.getHours(),
        startDate.getMinutes()
      );

      var blocks = [];

      for (var i = 0; i < blockSpan; i++) {
        blocks.push({
          dayIndex: startBlock.dayIndex,
          blockIndex: startBlock.blockIndex + i
        });
      }

      return blocks;
    }

    const context = this;

    if (importedBlocks || (!importedBlocks && !importedStaticBlocks)) {
      var parsedBlocks = importedBlocks.map(({ date, hours, minutes }) => {
        return context.toRelative(date, hours, minutes);
      });

      return parsedBlocks;
    }

    if (importedStaticBlocks) {
      var parsedStaticBlocks = [];
      const { days } = this.state;
      setHours(days[0], 0, 0);
      setHours(days[6], 0, 0);

      importedStaticBlocks.forEach(({ date, hours, minutes }) => {
        if (date >= days[0] && date <= days[6])
          parsedStaticBlocks.push(context.toRelative(date, hours, minutes));
      });

      return parsedStaticBlocks;
    }
  }

  /**
   * Ends the current selection, executing the onSelection
   * callback if it's provided
   */
  endSelection() {
    this.setState({ mouseDown: false });

    const { onSelection } = this.props;

    if (onSelection) {
      onSelection(this.convertedBlocks());
    }
  }

  /**
   * Moves the currently displayed week forward
   * or backward depending on the given offset (-1 or 1)
   */
  moveWeek(offset) {
    const { excludeWeekend } = this.props;
    const baseDay = addDays(this.getBaseDay(), offset * 7);
    this.setState(
      {
        baseDay: baseDay,
        days: getWeek(baseDay, excludeWeekend),
        selectedBlocks: []
      },
      () => {
        this.setState({ staticBlocks: this.parseBlocks() });
      }
    );
  }

  clearSelection() {
    this.setState({ selectedBlocks: [] }, () => {
      this.endSelection();
    });
  }

  confirmSelection() {
    const { onConfirmSelection } = this.props;

    if (onConfirmSelection) {
      onConfirmSelection(this.convertedBlocks());
    }
  }

  blockClass(dayIndex, blockIndex) {
    const { importedStaticBlocks, blockPastDates } = this.props;
    const isStatic = this.isBlockStatic(dayIndex, blockIndex);
    const isSelected = this.isBlockSelected(dayIndex, blockIndex);

    if (
      (importedStaticBlocks && !isStatic) ||
      (blockPastDates && this.isPast(dayIndex, blockIndex))
    ) {
      return "blocked-block";
    }

    if (isSelected) {
      return "selected-block";
    }

    if (isStatic) {
      return "static-block";
    }

    return "";
  }

  render() {
    const { days, blocks, selectedBlocks } = this.state;
    const { reducedBlocks, readOnly } = this.props;

    const dayCount = days.length;
    const first = days[0];
    const last = days[dayCount - 1];
    const title = formatDate(first, true) + " - " + formatDate(last, true);

    const cellStyle = reducedBlocks ? { height: "20px" } : null;
    const hiddenStyle = { display: "none" };
    const clearBtnStyle =
      !readOnly && (selectedBlocks && selectedBlocks.length > 0)
        ? null
        : hiddenStyle;
    const dateLessStyle = this.props.dateLessMode ? hiddenStyle : null;
    return (
      <div className="schedule" style={{ maxHeight: this.props.maxHeight }}>
        <div className="header">
          <div style={dateLessStyle}>
            <button
              style={readOnly ? hiddenStyle : null}
              onClick={() => this.moveWeek(-1)}
            >
              <img src="/img/icons/arrow-left.svg" alt="<" />
            </button>
            <h6>{title}</h6>
            <button
              style={readOnly ? hiddenStyle : null}
              onClick={() => this.moveWeek(1)}
            >
              <img src="/img/icons/arrow-right.svg" alt=">" />
            </button>
          </div>

          <button
            id="clear-schedule"
            onClick={() => this.clearSelection()}
            style={clearBtnStyle}
          >
            Limpar
          </button>

          <button
            id="finish-schedule"
            onClick={() => this.confirmSelection()}
            style={clearBtnStyle}
          >
            Confirmar
          </button>
        </div>

        {/* Render column headers*/}
        <table>
          <thead>
            <tr>
              <th />
              {days.map(day => (
                <th key={day}>
                  <span>{formatDayOfWeek(day.getDay())}</span>
                  <span style={dateLessStyle}>{formatDate(day)}</span>
                </th>
              ))}
            </tr>
          </thead>
        </table>

        {/* Render rows*/}
        <div
          className="tbody-wrapper"
          onMouseUp={() => this.endSelection(false)}
        >
          <table>
            <tbody>
              {blocks.map((block, bIndex) => (
                <tr key={formatTimeBlock(block)}>
                  <td style={cellStyle}>{formatTimeBlock(block)}</td>
                  {days.map((day, dIndex) => (
                    <td
                      disabled={readOnly}
                      style={cellStyle}
                      key={bIndex + "-" + dIndex}
                      className={this.blockClass(dIndex, bIndex)}
                      onMouseDown={() => this.beginSelection(dIndex, bIndex)}
                      onMouseEnter={() => this.onHover(dIndex, bIndex)}
                    />
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

Schedule.propTypes = {
  // Data
  importedBlocks: PropTypes.array, // The default blocks to display
  importedRange: PropTypes.object, // {startDate, endDate}, it will display the blocks in between those dates

  // UI settings
  reducedBlocks: PropTypes.bool, // Display blocks as a 20px high cell
  maxHeight: PropTypes.string, // Maximum schedule height

  // Schedule range settings
  excludeWeekend: PropTypes.bool, // Exclude weekend days (Sat/Sun) from the week days
  startHour: PropTypes.number, // The starting hour of the schedule
  endHour: PropTypes.number, // The ending hour of the schedule
  blocksPerHour: PropTypes.number, // The amount of blocks per hour (Ex: 2 = 30 minute blocks)
  baseDay: PropTypes.instanceOf(Date), // The base day on which the shown week will be based

  // Callbacks
  onSelection: PropTypes.func, // The callback that gets executed when a selection is finished
  onConfirmSelection: PropTypes.func, // The callback that gets executed when a selection is confirmed

  // Functionality settings
  allowCrossDay: PropTypes.bool, // Allow the user to select blocks across different days
  blockPastDates: PropTypes.bool, // Block the user input on time blocks that have already passed
  readOnly: PropTypes.bool, // Read-only mode, this will disable any user input
  returnDays: PropTypes.bool, // If enabled, the schedule will return blocks in days of the week, not dates
  dateLessMode: PropTypes.bool // See returnDays + this will not display dates on the UI
};

export default Schedule;
