import React, { Component } from "react";

export class MessageContainer extends Component {
  hasMessage() {
    return !this.props.message || this.props.message === "";
  }
  render() {
    return (
      <div
        className={this.props.className}
        role="alert"
        style={{ display: this.hasMessage() ? "none" : "block" }}
      >
        {this.props.message}
      </div>
    );
  }
}

export default MessageContainer;
