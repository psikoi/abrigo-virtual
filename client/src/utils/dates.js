import { appendZero } from "./general";

export const months = [
  "Janeiro",
  "Fevereiro",
  "Março",
  "Abril",
  "Maio",
  "Junho",
  "Julho",
  "Agosto",
  "Setembro",
  "Outubro",
  "Novembro",
  "Dezembro"
];

export const days = [
  "Domingo",
  "Segunda-feira",
  "Terça-feira",
  "Quarta-feira",
  "Quinta-feira",
  "Sexta-feira",
  "Sábado"
];

/**
 * Adds days to a given date
 */
export function addDays(date, days) {
  date.setDate(date.getDate() + days);
  return date;
}

export function blockToDate(block) {
  var dayToDate = function(block) {
    var curDate = new Date();
    var nextDate = new Date();
    var curDay = curDate.getDay();
    var day = block.date ? block.date.getDay() : block.day;

    var isBehind =
      curDay > day ||
      (curDay === day &&
        (curDate.getHours() > block.time.hours ||
          (curDate.getHours() === block.time.hours &&
            curDate.getMinutes() >= block.time.minutes)));

    var indexDiff = day - curDay + (isBehind ? 7 : 0);
    nextDate.setDate(nextDate.getDate() + indexDiff);
    nextDate.setHours(block.time.hours);
    nextDate.setMinutes(block.time.minutes);
    nextDate.setSeconds(0);

    return nextDate;
  };

  return dayToDate(block);
}

/**
 * Returns a list of dates from the given day's week
 * (from monday to sunday).
 */
export function getWeek(day, excludeWeekend) {
  var date = day ? day : new Date();
  var days = [];

  var firstDay = new Date(date.getTime());
  addDays(firstDay, -(date.getDay() - 1));

  for (var i = 0; i < (excludeWeekend ? 5 : 7); i++) {
    var newDay = new Date(firstDay.getTime());
    addDays(newDay, i);
    days.push(newDay);
  }

  return days;
}

/**
 * Formats a given date.
 * Extended: 25 Abril
 * Non-extended: 25/04
 */
export function formatDate(date, extended) {
  if (extended) {
    return date.getDate() + " " + months[date.getMonth()];
  }

  return appendZero(date.getDate()) + "/" + appendZero(date.getMonth() + 1);
}

/**
 * Returns a date string with a format like:
 * 25/04/2019 or 25 Abril 2019 (extended true)
 */
export function formatDateLong(date, extended) {
  if (extended) {
    return formatDate(date, extended) + " " + date.getFullYear();
  }

  return formatDate(date, extended) + "/" + date.getFullYear();
}

/**
 * Returns a date string with a format like:
 * 25/04/2019 às 21:00
 * or 25 Abril 2019 às 21:00 (extended true)
 */
export function formatDatetimeLong(date, extended) {
  let hour = appendZero(date.getHours()) + ":" + appendZero(date.getMinutes());

  return formatDateLong(date, extended) + " às " + hour;
}

/**
 * Returns a time string with format like:
 * 21:00
 */
export function formatTimeBlock(block) {
  return appendZero(block.hours) + ":" + appendZero(block.minutes);
}

/**
 * Returns a time string with format like:
 * 21:00
 */
export function formatTime(date) {
  return appendZero(date.getHours()) + ":" + appendZero(date.getMinutes());
}

/**
 * Formats a day of the week:
 * Ex: 0 -> Dom
 * Ex: 3 -> Qua
 */
export function formatDayOfWeek(index) {
  return days[index].substr(0, 3);
}

/**
 * Returns a list of bi-hourly blocks from
 * the startHour to endHour.
 *
 * Ex:
 * getDailyBlocks(8, 10) returns:
 * 08:00, 08:30, 09:00, 09:30
 */
export function getDailyBlocks(startHour, endHour, blocksPerHour) {
  var blocks = [];
  const blockSize = 60 / blocksPerHour;

  for (var i = startHour; i < endHour; i++) {
    for (var ii = 0; ii < blocksPerHour; ii++) {
      var hours = i;
      var minutes = ii * blockSize;

      var block = {
        hours,
        minutes
      };

      blocks.push(block);
    }
  }

  return blocks;
}

/**
 * Set the date to the hours and minutes passed in parameters
 */
export function setHours(date, hours, minutes) {
  date.setHours(hours);
  date.setMinutes(minutes);
  return date;
}
