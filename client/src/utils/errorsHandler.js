export async function handleErrors(response) {
  if (!response.ok) {
    let responseText = await response.text();
    return Promise.reject({ code: response.status, responseText });
  }
}
