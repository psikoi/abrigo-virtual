module.exports = {
  LOGIN_EMAIL: "Inserir o seu endereço de email, exemplo (email@teste.teste)",
  LOGIN_PASSWORD: "Inserir a palavra passe atual",
  RECOVER_NEW_PASSWORD:
    "Inserir a nova palavra passe, deve ter no mínimo 8 caracteres",
  RECOVER_CONFIRM_PASSWORD:
    "Repetir a nova palavra passe inserida no campo acima",
  EMP_PHOTO: "Selecionar foto de perfil do utilizador",
  EMP_NAME: "Primeiro e último nome do utilizador",
  EMP_EMAIL: "Email do utilizador, formato exemplo (exemplo@email.com)",
  EMP_GENDER: "Selecionar o gênero do utilizador",
  EMP_ROLE: "Selecionar a função que o utilizador vai desempenhar no cANIMALl",
  EMP_CONTACT:
    "Número de telemóvel do utilizador (máximo 14 digitos e mínimo 9 dígitos)",
  EMP_BIRTHDATE: "Selecionar data nascimento do utilizador",
  EMP_ADDRESS: "Morada do utilizador",
  EMP_ADDITIONALINFO:
    "Informacao adicional relativa ao utilizador, exemplo (alergias, etc)",
  TASK_TITLE: "Inserir o título da tarefa",
  TASK_DESCRIPTION: "Inserir uma descrição detalhada da tarefa",
  TASK_EMPLOYEE:
    "Selecionar o funcionário a quem a tarefa deverá ser atribuida",
  TASK_DATE:
    "Selecionar a data que pretende que o colaborador realize a tarefa",
  ANIMAL_PHOTO: "Selecionar foto do animal",
  ANIMAL_GENDER: "Selecionar o gênero do animal",
  ANIMAL_TYPE: "Selecionar o tipo de animal",
  ANIMAL_SIZE: "Selecionar o porte do animal",
  ANIMAL_BREED: "Selecionar a raça do animal",
  ANIMAL_NAME: "Nome do animal",
  ANIMAL_ADDITIONALINFO:
    "Informacao adicional relativa ao animal, exemplo (cuidados extras a ter, etc)",
  ANIMAL_BIRTHDATE: "Selecionar data nascimento do animal",
  RELATIONSHIP_ANIMAL: "Selecionar animal envolvido na relação",
  RELATIONSHIP_VALUE: "Selecionar o tipo de relação entre os dois animais",
  QUARENTINE_ANIMAL: "Selecionar o animal para quarentena",
  QUARENTINE_CAUSE: "Selecionar a causa da quarentena",
  QUARENTINE_DESCRIPTION: "Inserir uma descrição detalhada da quarentena",
  KENNEL_CAPACITY: "Indice qual o nº de animais que a casota suporta",
  KENNEL_NAME: "Indice o nome da casota",
  KENNEL_ANIMALS: "Selecione os animais que vão pertencer a esta casota",
  SHELTER_NAME: "Nome do albergue, por exemplo 'Quinta do Mião'",
  SHELTER_ADDRESS: "Morada do albergue, por exemplo 'Rua do Albergue, Almada'"
};
