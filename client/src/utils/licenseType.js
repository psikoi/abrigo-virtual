export function isLicensePlus(licenseType) {
  return licenseType.toLowerCase() === "plus";
}
