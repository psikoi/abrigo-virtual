export function hasPermission(context, endpoint, action, id) {
  const { user, permissions } = context;

  // Context inproperly setup or user logged out
  if (!context || !user) {
    return false;
  }

  // Invalid endpoints or actions passed
  if (!permissions[endpoint] || !permissions[endpoint][action]) {
    return false;
  }

  // Always allow admin
  if (user.type === "admin") {
    return true;
  }

  // If the user id is given, only allow that user
  // Example: Edit Profile, should only be allowed for Admin
  // or the user itself
  if (id) {
    return user.employeeId === parseInt(id);
  }

  return permissions[endpoint][action].includes(user.type);
}
