export function translateType({ type, gender }) {
  const isFemale = gender && gender === "female";

  switch (type) {
    case "admin":
      return isFemale ? "Administradora" : "Administrador";
    case "employee":
      return isFemale ? "Funcionária" : "Funcionário";
    case "volunteer":
      return isFemale ? "Voluntária" : "Voluntário";
    default:
      return "N/A";
  }
}

export function translateStatus({ status, archived, gender }) {
  const isFemale = gender && gender === "female";

  if (archived) {
    return isFemale ? "Arquivada" : "Arquivado";
  }

  return status === 1 ? "Em serviço" : "Fora de serviço";
}

export function translateGender(gender) {
  switch (gender) {
    case "male":
      return "Masculino";
    case "female":
      return "Feminino";
    default:
      return "N/A";
  }
}

export function convertToColor({ status, archived }) {
  if (archived) {
    return "#7c7c7c";
  }

  return status === 1 ? "#7bbb79" : "#bb4040";
}
