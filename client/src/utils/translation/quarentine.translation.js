export function translateQuarentineStatus(status) {
  switch (status) {
    case "finished":
      return "Terminada";
    case "pending":
      return "Decorrente";
    case "canceled":
      return "Cancelada";
    default:
      return "N/A";
  }
}

export function translateQuarentineCause(cause) {
  switch (cause) {
    case "sick":
      return "Doente";
    case "vaccinated":
      return "Vacinação";
    case "medicalWatch":
      return "Vigia médica";
    case "fight":
      return "Agressividade";
    default:
      return "Outro";
  }
}
