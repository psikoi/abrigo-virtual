export function translateType({ type, gender }) {
  switch (type) {
    case "dog":
      return "Cão";
    case "cat":
      return "Gato";
    default:
      return "N/A";
  }
}

export function translateGender({ gender }) {
  switch (gender) {
    case "male":
      return "Macho";
    case "female":
      return "Fêmea";
    default:
      return "N/A";
  }
}

export function translateSize({ size }) {
  switch (size) {
    case "micro":
      return "Micro";
    case "small":
      return "Pequeno";
    case "medium":
      return "Médio";
    case "big":
      return "Grande";
    case "veryBig":
      return "Muito grande";
    default:
      return "Outro";
  }
}

export function translateQuarentineStatus({ kennelName, inQuarentine }) {
  if (inQuarentine) {
    return "Em quarentena";
  } else if (kennelName) {
    return kennelName;
  } else {
    return "---";
  }
}

export function convertToColor({ kennelName, inQuarentine }) {
  if (inQuarentine) {
    return "#bb4040";
  } else if (kennelName) {
    return "#4eb44c";
  } else {
    return "transparent";
  }
}
