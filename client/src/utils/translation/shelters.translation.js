export function translateType({ licenseType }) {
  switch (licenseType.toLowerCase()) {
    case "plus":
      return "Plus";
    case "basic":
      return "Básica";
    default:
      return "---";
  }
}
