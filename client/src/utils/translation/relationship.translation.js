export function translateValue({ value }) {
  switch (value) {
    case -10:
    case "-10":
      return "muito má";
    case -5:
    case "-5":
      return "má";
    case 2:
    case "2":
      return "boa";
    case 5:
    case "5":
      return "muito boa";
    default:
      return "desconhecida";
  }
}

export function convertToColor({ value }) {
  if (value > 0) {
    return "#4eb44c";
  } else if (value < 0) {
    return "#bb4040";
  } else {
    return "#7c7c7c";
  }
}
