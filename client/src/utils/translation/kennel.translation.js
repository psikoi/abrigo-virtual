export function convertToColor(capacity, size) { 
  return size < capacity ? "#7bbb79" : "#bb4040";
}

export function translateSize(capacity, size){ 
  var str;
  if(size < capacity )
    str = `Livre`;
  else
    str = `Lotado`;
  return `${str} (${size}/${capacity})`
}
