export function translateStatus(status) {
  switch (status) {
    case "pending":
      return "Pendente";
    case "accepted":
      return "Aceite";
    case "declined":
      return "Recusado";
    case "canceled":
      return "Cancelado";
    default:
      return "N/A";
  }
}

export function convertToColor(status) {
  switch (status) {
    case "pending":
      return "#eaaf0f";
    case "accepted":
      return "#7bbb79";
    case "declined":
      return "#bb4040";
    case "canceled":
      return "#bb4040";
    default:
      return "#7c7c7c";
  }
}
