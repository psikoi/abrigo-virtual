export function translateStatus(status) {
  switch (status) {
    case "completed":
      return "Concluida";
    case "pending":
      return "Pendente";
    case "canceled":
      return "Cancelada";
    default:
      return "N/A";
  }
}

export function convertToColor(status) {
  switch (status) {
    case "pending":
      return "#eaaf0f";
    case "completed":
      return "#7bbb79";
    case "canceled":
      return "#bb4040";
    case "finished":
      return "#7bbb79";
    default:
      return "#7c7c7c";
  }
}
