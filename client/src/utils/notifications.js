import toast from "cogo-toast";

export function notifySuccess(text) {
  toast.success(text);
}

export function notifyError(text) {
  toast.error(text);
}

export function notifyWarn(text) {
  toast.warn(text);
}

export function notifyInfo(text) {
  toast.info(text);
}
