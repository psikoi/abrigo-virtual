import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Loading from "./components/Loading";
import Navigation from "./components/navigation/Navigation";
import EmployeesIndex from "./components/routes/employees/index/Index";
import EmployeesCreate from "./components/routes/employees/create/Create";
import EmployeesEdit from "./components/routes/employees/edit/Edit";
import EmployeesProfile from "./components/routes/employees/profile/Profile";
import LoginPage from "./components/routes/account/Login";
import ResetPassword from "./components/routes/account/ResetPassword";
import ForgotPassword from "./components/routes/account/ForgotPassword";
import ChangePassword from "./components/routes/account/ChangePassword";
import ErrorPage from "./components/routes/errors/ErrorPage";
import ReinforcementsIndex from "./components/routes/reinforcements/Index";
import ReinforcementsCreate from "./components/routes/reinforcements/Create";
import ReinforcementsAccept from "./components/routes/reinforcements/Accept/Accept.js";
import TasksIndex from "./components/routes/tasks/index/Index";
import TasksCreate from "./components/routes/tasks/create/Create";
import TasksEdit from "./components/routes/tasks/edit/Edit";
import AnimalsIndex from "./components/routes/animals/index/Index";
import AnimalsCreate from "./components/routes/animals/create/Create";
import AnimalsEdit from "./components/routes/animals/edit/Edit";
import AnimalsProfile from "./components/routes/animals/profile/Profile";
import RelationshipsCreate from "./components/routes/animals/relationship/Create";
import RelationshipsEdit from "./components/routes/animals/relationship/Edit";
import QuarentinesIndex from "./components/routes/quarentines/index/Index";
import QuarentinesCreate from "./components/routes/quarentines/create/Create";
import QuarentinesEdit from "./components/routes/quarentines/edit/Edit";
import KennelsIndex from "./components/routes/kennels/index/Index";
import KennelsCreate from "./components/routes/kennels/create/Create";
import KennelEdit from "./components/routes/kennels/edit/Edit";
import Register from "./components/routes/account/Register";
import KennelCalculate from "./components/routes/kennels/calculate/Calculate";
import StatisticsIndex from "./components/routes/statistics/index/Index";
import LicenseDetails from "./components/routes/shelters/details/Details";
import { AppContext } from "./contexts/AppContext";
import "./App.css";

class App extends Component {
  state = {
    isLoading: true
  };

  componentWillMount() {
    this.updateContext();
  }

  updateContext = async () => {
    const permissions = await this.loadPermissions();
    const user = await this.loadLoggedUser();
    const shelter = await this.loadShelter();
    this.setState({
      user,
      permissions,
      shelter: shelter ? shelter.data : undefined,
      isLoading: false
    });
  };

  render() {
    if (this.state.isLoading) {
      return <Loading />;
    }

    let { permissions, user, shelter } = this.state;
    let update = this.updateContext;

    return (
      <div className="App">
        <AppContext.Provider value={{ permissions, user, shelter, update }}>
          <BrowserRouter>
            <Navigation />
            <Switch>
              {routes.map(({ path, component, requiresAuth }) => (
                <Route
                  key={path}
                  exact
                  path={requiresAuth && !user ? "/iniciar-sessao" : path}
                  component={component}
                />
              ))}
              <Route
                exact
                path="/"
                render={() => <Redirect to="/iniciar-sessao" />}
              />
              <Route render={() => <ErrorPage statusCode="404" />} />
            </Switch>
          </BrowserRouter>
        </AppContext.Provider>
      </div>
    );
  }

  async loadPermissions() {
    const url = "";
    const response = await fetch(`${url}/api/sessions/permissions-matrix/`);
    return response.json();
  }

  async loadLoggedUser() {
    const url = "";
    const response = await fetch(`${url}/api/sessions/current-user`, {
      credentials: "include",
      method: "GET"
    });
    return response.status === 200 ? response.json() : null;
  }

  async loadShelter() {
    const url = "";
    const response = await fetch(`${url}/api/shelters/by-id`, {
      credentials: "include",
      method: "GET"
    });
    return response.status === 200 ? response.json() : null;
  }
}

const routes = [
  {
    path: "/iniciar-sessao",
    component: LoginPage
  },
  {
    path: "/restaurar-palavra-passe",
    component: ResetPassword
  },
  {
    path: "/alterar-palavra-passe",
    component: ChangePassword,
    requiresAuth: true
  },
  {
    path: "/recuperar-palavra-passe",
    component: ForgotPassword
  },
  {
    path: "/colaboradores",
    component: EmployeesIndex,
    requiresAuth: true
  },
  {
    path: "/colaboradores/:id(\\d+)",
    component: EmployeesProfile,
    requiresAuth: true
  },
  {
    path: "/colaboradores/criar",
    component: EmployeesCreate,
    requiresAuth: true
  },
  {
    path: "/colaboradores/editar/:id",
    component: EmployeesEdit,
    requiresAuth: true
  },
  {
    path: "/tarefas",
    component: TasksIndex,
    requiresAuth: true
  },
  {
    path: "/tarefas/criar",
    component: TasksCreate,
    requiresAuth: true
  },
  {
    path: "/tarefas/editar/:id",
    component: TasksEdit,
    requiresAuth: true
  },
  {
    path: "/reforços",
    component: ReinforcementsIndex,
    requiresAuth: true
  },
  {
    path: "/reforços/solicitar",
    component: ReinforcementsCreate,
    requiresAuth: true
  },
  {
    path: "/reforços/pedido",
    component: ReinforcementsAccept,
    requiresAuth: true
  },
  {
    path: "/animais",
    component: AnimalsIndex,
    requiresAuth: true
  },
  {
    path: "/animais/criar",
    component: AnimalsCreate,
    requiresAuth: true
  },
  {
    path: "/animais/editar/:id(\\d+)",
    component: AnimalsEdit,
    requiresAuth: true
  },
  {
    path: "/animais/:id(\\d+)",
    component: AnimalsProfile,
    requiresAuth: true
  },
  {
    path: "/animais/:id(\\d+)/relacoes/criar",
    component: RelationshipsCreate,
    requiresAuth: true
  },
  {
    path: "/animais/relacoes/editar/:id(\\d+)",
    component: RelationshipsEdit,
    requiresAuth: true
  },
  {
    path: "/quarentenas",
    component: QuarentinesIndex
  },
  {
    path: "/casotas",
    component: KennelsIndex,
    requiresAuth: true
  },
  {
    path: "/quarentenas/criar",
    component: QuarentinesCreate,
    requiresAuth: true
  },
  {
    path: "/quarentenas/editar/:id([0-9])",
    component: QuarentinesEdit
  },
  {
    path: "/casotas/criar",
    component: KennelsCreate,
    requiresAuth: true
  },
  {
    path: "/casotas/editar/:id(\\d+)",
    component: KennelEdit,
    requiresAuth: true
  },
  {
    path: "/registo",
    component: Register
  },
  {
    path: "/casotas/calcular",
    component: KennelCalculate,
    requiresAuth: true
  },
  {
    path: "/estatisticas",
    component: StatisticsIndex,
    requiresAuth: true
  },
  {
    path: "/canil",
    component: LicenseDetails,
    requiresAuth: true
  }
];

export default App;
