import { handleErrors } from "../../utils/errorsHandler.js";
import { notifySuccess } from "../../utils/notifications";

export const actions = {
  GET_CREATE_RELATIONSHIP_REQUEST: "GET_CREATE_RELATIONSHIP_REQUEST",
  GET_CREATE_RELATIONSHIP_SUCCESS: "GET_CREATE_RELATIONSHIP_SUCCESS",
  GET_CREATE_RELATIONSHIP_FAILURE: "GET_CREATE_RELATIONSHIP_FAILURE",
  CREATE_RELATIONSHIP_REQUEST: "CREATE_RELATIONSHIP_REQUEST",
  CREATE_RELATIONSHIP_SUCCESS: "CREATE_RELATIONSHIP_SUCCESS",
  CREATE_RELATIONSHIP_FAILURE: "CREATE_RELATIONSHIP_FAILURE",
  EDIT_RELATIONSHIP_REQUEST: "EDIT_RELATIONSHIP_REQUEST",
  EDIT_RELATIONSHIP_SUCCESS: "EDIT_RELATIONSHIP_SUCCESS",
  EDIT_RELATIONSHIP_FAILURE: "EDIT_RELATIONSHIP_FAILURE",
  GET_ANIMAL_RELATIONSHIPS_REQUEST: "GET_ANIMAL_RELATIONSHIPS_REQUEST",
  GET_ANIMAL_RELATIONSHIPS_SUCCESS: "GET_ANIMAL_RELATIONSHIPS_SUCCESS",
  GET_ANIMAL_RELATIONSHIPS_FAILURE: "GET_ANIMAL_RELATIONSHIPS_FAILURE",
  GET_RELATIONSHIP_REQUEST: "GET_RELATIONSHIP_REQUEST",
  GET_RELATIONSHIP_SUCCESS: "GET_RELATIONSHIP_SUCCESS",
  GET_RELATIONSHIP_FAILURE: "GET_RELATIONSHIP_FAILURE",
  REMOVE_RELATIONSHIP_REQUEST: "REMOVE_RELATIONSHIP_REQUEST",
  REMOVE_RELATIONSHIP_SUCCESS: "REMOVE_RELATIONSHIP_SUCCESS",
  REMOVE_RELATIONSHIP_FAILURE: "REMOVE_RELATIONSHIP_FAILURE"
};

export const getCreateRelationship = () => async dispatch => {
  dispatch(getCreateRelationshipRequest());
  try {
    let payload = await fetch(
      `${""}/api/relationships/create`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(getCreateRelationshipSuccess(response.data));
  } catch (errorDic) {
    dispatch(getCreateRelationshipFailure(errorDic));
  }

  function getCreateRelationshipRequest() {
    return dispatch => {
      dispatch({
        type: actions.GET_CREATE_RELATIONSHIP_REQUEST
      });
    };
  }

  function getCreateRelationshipFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.GET_CREATE_RELATIONSHIP_FAILURE,
        error
      });
    };
  }

  function getCreateRelationshipSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.GET_CREATE_RELATIONSHIP_SUCCESS,
        payload: response
      });
    };
  }
};

export const createRelationship = params => async dispatch => {
  dispatch(createRelationshipRequest());
  try {
    let payload = await fetch(
      `${""}/api/relationships/`,
      {
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(params)
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(createRelationshipSuccess(response.data));
  } catch (errorDic) {
    dispatch(createRelationshipFailure(errorDic));
  }

  function createRelationshipRequest() {
    return dispatch => {
      dispatch({
        type: actions.CREATE_RELATIONSHIP_REQUEST
      });
    };
  }

  function createRelationshipFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_RELATIONSHIP_FAILURE,
        error
      });
    };
  }

  function createRelationshipSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_RELATIONSHIP_SUCCESS,
        payload: response
      });
      notifySuccess("Relação criada com successo");
    };
  }
};

export const editRelationship = (params, id) => async dispatch => {
  dispatch(editRelationshipRequest());
  try {
    let payload = await fetch(
      `${""}/api/relationships/${id}`,
      {
        credentials: "include",
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(params)
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(editRelationshipSuccess(response.data));
  } catch (errorDic) {
    dispatch(editRelationshipFailure(errorDic));
  }

  function editRelationshipRequest() {
    return dispatch => {
      dispatch({
        type: actions.EDIT_RELATIONSHIP_REQUEST
      });
    };
  }

  function editRelationshipFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.EDIT_RELATIONSHIP_FAILURE,
        error
      });
    };
  }

  function editRelationshipSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.EDIT_RELATIONSHIP_SUCCESS,
        payload: response
      });
      notifySuccess("Relação editada com successo");
    };
  }
};

export const getAnimalRelationships = id => async dispatch => {
  dispatch(getAnimalRelationshipsRequest());
  try {
    let payload = await fetch(
      `${""}/api/relationships/animal/${id}`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(getAnimalRelationshipsSuccess(response.data));
  } catch (errorDic) {
    dispatch(getAnimalRelationshipsFailure(errorDic));
  }

  function getAnimalRelationshipsRequest() {
    return dispatch => {
      dispatch({
        type: actions.GET_ANIMAL_RELATIONSHIPS_REQUEST
      });
    };
  }

  function getAnimalRelationshipsFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.GET_ANIMAL_RELATIONSHIPS_FAILURE,
        error
      });
    };
  }

  function getAnimalRelationshipsSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.GET_ANIMAL_RELATIONSHIPS_SUCCESS,
        payload: response
      });
    };
  }
};

export const getRelationship = id => async dispatch => {
  dispatch(getRelationshipRequest());
  try {
    let payload = await fetch(
      `${""}/api/relationships/${id}`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(getRelationshipSuccess(response.data));
  } catch (errorDic) {
    dispatch(getRelationshipFailure(errorDic));
  }

  function getRelationshipRequest() {
    return dispatch => {
      dispatch({
        type: actions.GET_RELATIONSHIP_REQUEST
      });
    };
  }

  function getRelationshipFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.GET_RELATIONSHIP_FAILURE,
        error
      });
    };
  }

  function getRelationshipSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.GET_RELATIONSHIP_SUCCESS,
        payload: response
      });
    };
  }
};

export const removeRelationship = id => async dispatch => {
  dispatch(removeRelationshipRequest());
  try {
    let payload = await fetch(
      `${""}/api/relationships/${id}`,
      {
        credentials: "include",
        method: "DELETE"
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(removeRelationshipSuccess(response.data));
  } catch (errorDic) {
    dispatch(removeRelationshipFailure(errorDic));
  }

  function removeRelationshipRequest() {
    return dispatch => {
      dispatch({
        type: actions.REMOVE_RELATIONSHIP_REQUEST
      });
    };
  }

  function removeRelationshipFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.REMOVE_RELATIONSHIP_FAILURE,
        error
      });
    };
  }

  function removeRelationshipSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.REMOVE_RELATIONSHIP_SUCCESS,
        payload: response
      });
    };
  }
};
