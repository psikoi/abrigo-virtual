import { handleErrors } from "../../utils/errorsHandler.js";

export const actions = {
  LOGIN_FAILURE: "LOGIN_FAILURE",
  LOGIN_SUCESS: "LOGIN_SUCESS",
  LOGIN_REQUEST: "LOGIN_REQUEST",
  LOGOUT_FAILURE: "LOGOUT_FAILURE",
  LOGOUT_SUCESS: "LOGOUT_SUCESS",
  LOGOUT_REQUEST: "LOGOUT_REQUEST",

  CHANGE_PASSWORD_FAILURE: "CHANGE_PASSWORD_FAILURE",
  CHANGE_PASSWORD_SUCCESS: "CHANGE_PASSWORD_SUCCESS",
  CHANGE_PASSWORD_REQUEST: "CHANGE_PASSWORD_REQUEST",

  FORGOT_PASSWORD_FAILURE: "FORGOT_PASSWORD_FAILURE",
  FORGOT_PASSWORD_SUCCESS: "FORGOT_PASSWORD_SUCCESS",
  FORGOT_PASSWORD_REQUEST: "FORGOT_PASSWORD_REQUEST",

  RESET_PASSWORD_REQUEST: "RESET_PASSWORD_REQUEST",
  RESET_PASSWORD_SUCCESS: "RESET_PASSWORD_SUCCESS",
  RESET_PASSWORD_FAILURE: "RESET_PASSWORD_FAILURE"
};

export const login = body => async dispatch => {
  dispatch(loginRequest());
  try {
    let payload = await fetch(
      `${""}/api/sessions/login`,
      {
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
      }
    );

    await handleErrors(payload);
    dispatch(loginSucess(true));
  } catch (errorDic) {
    dispatch(loginFailure(errorDic.responseText));
  }

  function loginRequest() {
    return dispatch => {
      dispatch({
        type: actions.LOGIN_REQUEST
      });
    };
  }

  function loginFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.LOGIN_FAILURE,
        error
      });
    };
  }

  function loginSucess(result) {
    return dispatch => {
      dispatch({
        type: actions.LOGIN_SUCESS,
        payload: result
      });
    };
  }
};

export const logout = () => async dispatch => {
  dispatch(logoutRequest());
  try {
    let payload = await fetch(
      `${""}/api/sessions/logout`,
      {
        credentials: "include",
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }
    );

    await handleErrors(payload);
    dispatch(logoutSuccess());
  } catch (errorDic) {
    dispatch(logoutFailure(errorDic.error));
  }

  function logoutRequest() {
    return dispatch => {
      dispatch({
        type: actions.LOGOUT_REQUEST
      });
    };
  }

  function logoutFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.LOGOUT_FAILURE,
        error
      });
    };
  }

  function logoutSuccess() {
    return dispatch => {
      dispatch({
        type: actions.LOGOUT_SUCESS,
        success: true
      });
    };
  }
};

export const fetchChangePassword = (
  currentPassword,
  password
) => async dispatch => {
  dispatch(fetchChangePasswordSubscriptionsRequest());
  try {
    let payload = await fetch(
      `${""}/api/sessions/changePassword`,
      {
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          currentPassword: currentPassword,
          password: password
        })
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(fetchChangePasswordSuccess(response));
  } catch (errorDic) {
    dispatch(fetchChangePasswordFailure(errorDic.responseText));
  }

  function fetchChangePasswordSubscriptionsRequest() {
    return dispatch => {
      dispatch({
        type: actions.CHANGE_PASSWORD_REQUEST
      });
    };
  }

  function fetchChangePasswordFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.CHANGE_PASSWORD_FAILURE,
        error
      });
    };
  }

  function fetchChangePasswordSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.CHANGE_PASSWORD_SUCCESS,
        response
      });
    };
  }
};

export const fetchForgotPassword = email => async dispatch => {
  dispatch(fetchForgotPasswordSubscriptionsRequest());
  try {
    let payload = await fetch(
      `${""}/api/sessions/forgotPassword`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          email: email
        })
      }
    );

    await handleErrors(payload);
    let result = await payload.json();
    dispatch(fetchForgotPasswordSuccess(result.message));
  } catch (errorDic) {
    dispatch(fetchForgotPasswordFailure(errorDic.responseText));
  }

  function fetchForgotPasswordSubscriptionsRequest() {
    return dispatch => {
      dispatch({
        type: actions.FORGOT_PASSWORD_REQUEST
      });
    };
  }

  function fetchForgotPasswordFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FORGOT_PASSWORD_FAILURE,
        error
      });
    };
  }

  function fetchForgotPasswordSuccess(message) {
    return dispatch => {
      dispatch({
        type: actions.FORGOT_PASSWORD_SUCCESS,
        payload: message
      });
    };
  }
};

export const fetchResetPassword = (token, password) => async dispatch => {
  dispatch(fetchResetPasswordSubscriptionsRequest());
  try {
    let payload = await fetch(
      `${""}/api/sessions/resetPassword`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          token: token,
          password: password
        })
      }
    );

    await handleErrors(payload);
    let result = await payload.json();
    dispatch(fetchResetPasswordSuccess(result.message));
  } catch (errorDic) {
    dispatch(fetchResetPasswordFailure(errorDic.responseText));
  }

  function fetchResetPasswordSubscriptionsRequest() {
    return dispatch => {
      dispatch({
        type: actions.RESET_PASSWORD_REQUEST
      });
    };
  }

  function fetchResetPasswordFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.RESET_PASSWORD_FAILURE,
        error
      });
    };
  }

  function fetchResetPasswordSuccess(message) {
    return dispatch => {
      dispatch({
        type: actions.RESET_PASSWORD_SUCCESS,
        payload: message
      });
    };
  }
};
