import { handleErrors } from "../../utils/errorsHandler.js";
import { notifySuccess } from "../../utils/notifications";

export const actions = {
  FETCH_KENNELS_FAILURE: "FETCH_KENNELS_FAILURE",
  FETCH_KENNELS_SUCCESS: "FETCH_KENNELS_SUCCESS",
  FETCH_KENNELS_REQUEST: "FETCH_KENNELS_REQUEST",
  CREATE_KENNEL_REQUEST: "CREATE_KENNEL_REQUEST",
  CREATE_KENNEL_SUCCESS: "CREATE_KENNEL_SUCCESS",
  CREATE_KENNEL_FAILURE: "CREATE_KENNEL_FAILURE",
  EDIT_KENNEL_REQUEST: "EDIT_KENNEL_REQUEST",
  EDIT_KENNEL_SUCCESS: "EDIT_KENNEL_SUCCESS",
  EDIT_KENNEL_FAILURE: "EDIT_KENNEL_FAILURE",
  FETCH_AVAILABLE_ANIMALS_FAILURE: "FETCH_AVAILABLE_ANIMALS_FAILURE",
  FETCH_AVAILABLE_ANIMALS_SUCCESS: "FETCH_AVAILABLE_ANIMALS_SUCCESS",
  FETCH_AVAILABLE_ANIMALS_REQUEST: "FETCH_AVAILABLE_ANIMALS_REQUEST",
  FETCH_KENNEL_FAILURE: "FETCH_KENNEL_FAILURE",
  FETCH_KENNEL_SUCCESS: "FETCH_KENNEL_SUCCESS",
  FETCH_KENNEL_REQUEST: "FETCH_KENNEL_REQUEST",
  DELETE_KENNEL_FAILURE: "DELETE_KENNEL_FAILURE",
  DELETE_KENNEL_SUCCESS: "DELETE_KENNEL_SUCCESS",
  DELETE_KENNEL_REQUEST: "DELETE_KENNEL_REQUEST",
  CALCULATE_REQUEST: "CALCULATE_REQUEST",
  CALCULATE_SUCCESS: "CALCULATE_SUCCESS",
  CALCULATE_FAILURE: "CALCULATE_FAILURE",
  REDISTRIBUTION_REQUEST: "REDISTRIBUTION_REQUEST",
  REDISTRIBUTION_SUCCESS: "REDISTRIBUTION_SUCCESS",
  REDISTRIBUTION_FAILURE: "REDISTRIBUTION_FAILURE"
};

export const fetchKennels = () => async dispatch => {
  dispatch(fetchKennelsRequest());
  try {
    let payload = await fetch(
      `${""}/api/kennels`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let kennels = await payload.json();
    dispatch(fetchKennelsSuccess(kennels));
  } catch (errorDic) {
    dispatch(fetchKennelsFailure(errorDic.responseText));
  }

  function fetchKennelsRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_KENNELS_REQUEST
      });
    };
  }

  function fetchKennelsFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_KENNELS_FAILURE,
        error
      });
    };
  }

  function fetchKennelsSuccess(kennels) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_KENNELS_SUCCESS,
        payload: kennels.data
      });
    };
  }
};

export const fetchAnimalsAvailable = id => async dispatch => {
  dispatch(fetchAnimalsAvailableRequest());
  try {
    let payload = await fetch(
      `${
        ""
      }/api/kennels/available-animals/${id || ""}`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let animals = await payload.json();
    dispatch(fetchAnimalsAvailableSuccess(animals));
  } catch (errorDic) {
    dispatch(fetchAnimalsAvailableFailure(errorDic.responseText));
  }

  function fetchAnimalsAvailableRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_AVAILABLE_ANIMALS_REQUEST
      });
    };
  }

  function fetchAnimalsAvailableFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_AVAILABLE_ANIMALS_FAILURE,
        error
      });
    };
  }

  function fetchAnimalsAvailableSuccess(animals) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_AVAILABLE_ANIMALS_SUCCESS,
        payload: animals.data
      });
    };
  }
};

export const createKennel = kennel => async dispatch => {
  dispatch(createKennelRequest());
  try {
    let payload = await fetch(
      `${""}/api/kennels`,
      {
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(kennel),
        method: "POST"
      }
    );

    await handleErrors(payload);

    dispatch(createKennelSuccess());
  } catch (errorDic) {
    dispatch(createKennelFailure(errorDic.responseText));
  }

  function createKennelRequest() {
    return dispatch => {
      dispatch({
        type: actions.CREATE_KENNEL_REQUEST
      });
    };
  }

  function createKennelFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_KENNEL_FAILURE,
        error
      });
    };
  }

  function createKennelSuccess() {
    return dispatch => {
      dispatch({
        type: actions.CREATE_KENNEL_SUCCESS
      });
      notifySuccess("Casota criada com sucesso!");
    };
  }
};

export const editKennel = kennel => async dispatch => {
  dispatch(editKennelRequest());
  try {
    let payload = await fetch(
      `${""}/api/kennels`,
      {
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(kennel),
        method: "PUT"
      }
    );

    await handleErrors(payload);

    dispatch(editKennelSuccess());
  } catch (errorDic) {
    dispatch(editKennelFailure(errorDic.responseText));
  }

  function editKennelRequest() {
    return dispatch => {
      dispatch({
        type: actions.EDIT_KENNEL_REQUEST
      });
    };
  }

  function editKennelFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.EDIT_KENNEL_FAILURE,
        error
      });
    };
  }

  function editKennelSuccess() {
    return dispatch => {
      dispatch({
        type: actions.EDIT_KENNEL_SUCCESS
      });
      notifySuccess("Casota editada com sucesso!");
    };
  }
};

export const fetchKennel = id => async dispatch => {
  dispatch(fetchKennelRequest());
  try {
    let payload = await fetch(
      `${""}/api/kennels/` + id,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let kennel = await payload.json();
    dispatch(fetchKennelSuccess(kennel));
  } catch (errorDic) {
    dispatch(fetchKennelFailure(errorDic));
  }

  function fetchKennelRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_KENNEL_REQUEST
      });
    };
  }

  function fetchKennelFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_KENNEL_FAILURE,
        error
      });
    };
  }

  function fetchKennelSuccess(kennel) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_KENNEL_SUCCESS,
        payload: kennel.data
      });
    };
  }
};

export const deleteKennel = id => async dispatch => {
  dispatch(deleteKennelRequest());
  try {
    let payload = await fetch(
      `${""}/api/kennels/` + id,
      {
        credentials: "include",
        method: "DELETE"
      }
    );

    await handleErrors(payload);
    let message = await payload.json();
    dispatch(deleteKennelSuccess(message.data));
  } catch (errorDic) {
    dispatch(deleteKennelFailure(errorDic.responseText));
  }

  function deleteKennelRequest() {
    return dispatch => {
      dispatch({
        type: actions.DELETE_KENNEL_REQUEST
      });
    };
  }

  function deleteKennelFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.DELETE_KENNEL_FAILURE,
        error
      });
    };
  }

  function deleteKennelSuccess(message) {
    return dispatch => {
      dispatch({
        type: actions.DELETE_KENNEL_SUCCESS,
        message
      });
    };
  }
};

export const calculateKennels = () => async dispatch => {
  dispatch(calculateKennelsRequest());
  try {
    let payload = await fetch(
      `${""}/api/relationships/optimize/5000`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let layout = await payload.json();
    dispatch(calculateKennelsSuccess(layout));
  } catch (errorDic) {
    dispatch(calculateKennelsFailure(errorDic.responseText));
  }

  function calculateKennelsRequest() {
    return dispatch => {
      dispatch({
        type: actions.CALCULATE_REQUEST
      });
    };
  }

  function calculateKennelsFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.CALCULATE_FAILURE,
        error
      });
    };
  }

  function calculateKennelsSuccess(layout) {
    return dispatch => {
      dispatch({
        type: actions.CALCULATE_SUCCESS,
        payload: layout.data
      });
    };
  }
};

export const redistributeKennels = changes => async dispatch => {
  dispatch(redistributeRequest());
  try {
    let payload = await fetch(
      `${""}/api/kennels/redistribute`,
      {
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(changes),
        method: "PUT"
      }
    );

    await handleErrors(payload);
    let data = await payload.json();
    dispatch(redistributeSuccess(data));
  } catch (errorDic) {
    dispatch(redistributeFailure(errorDic.responseText));
  }

  function redistributeRequest() {
    return dispatch => {
      dispatch({
        type: actions.REDISTRIBUTION_REQUEST
      });
    };
  }

  function redistributeFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.REDISTRIBUTION_FAILURE,
        error
      });
    };
  }

  function redistributeSuccess(layout) {
    return dispatch => {
      dispatch({
        type: actions.REDISTRIBUTION_SUCCESS,
        payload: layout.data
      });
      notifySuccess(layout.data);
    };
  }
};
