import { handleErrors } from "../../utils/errorsHandler.js";
import { notifySuccess } from "../../utils/notifications";

export const actions = {
  FETCH_ANIMALS_FAILURE: "FETCH_ANIMALS_FAILURE",
  FETCH_ANIMALS_SUCCESS: "FETCH_ANIMALS_SUCCESS",
  FETCH_ANIMALS_REQUEST: "FETCH_ANIMALS_REQUEST",
  CREATE_ANIMAL_REQUEST: "CREATE_ANIMAL_REQUEST",
  CREATE_ANIMAL_SUCCESS: "CREATE_ANIMAL_SUCCESS",
  CREATE_ANIMAL_FAILURE: "CREATE_ANIMAL_FAILURE",
  EDIT_ANIMAL_REQUEST: "EDIT_ANIMAL_REQUEST",
  EDIT_ANIMAL_SUCCESS: "EDIT_ANIMAL_SUCCESS",
  EDIT_ANIMAL_FAILURE: "EDIT_ANIMAL_FAILURE",
  GET_ANIMAL_REQUEST: "GET_ANIMAL_REQUEST",
  GET_ANIMAL_SUCCESS: "GET_ANIMAL_SUCCESS",
  GET_ANIMAL_FAILURE: "GET_ANIMAL_FAILURE",
  REMOVE_ANIMAL_REQUEST: "REMOVE_ANIMAL_REQUEST",
  REMOVE_ANIMAL_SUCCESS: "REMOVE_ANIMAL_SUCCESS",
  REMOVE_ANIMAL_FAILURE: "REMOVE_ANIMAL_FAILURE"
};

export const fetchAnimals = () => async dispatch => {
  dispatch(fetchAnimalsRequest());
  try {
    let payload = await fetch(
      `${""}/api/animals`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let animals = await payload.json();
    dispatch(fetchAnimalsSuccess(animals.data));
  } catch (errorDic) {
    dispatch(fetchAnimalsFailure(errorDic));
  }

  function fetchAnimalsRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_ANIMALS_REQUEST
      });
    };
  }

  function fetchAnimalsFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_ANIMALS_FAILURE,
        error
      });
    };
  }

  function fetchAnimalsSuccess(animals) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_ANIMALS_SUCCESS,
        payload: animals
      });
    };
  }
};

export const createAnimal = animal => async dispatch => {
  dispatch(createAnimalRequest());
  try {
    let payload = await fetch(
      `${""}/api/animals`,
      {
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(animal)
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(createAnimalSuccess(response.data));
  } catch (errorDic) {
    dispatch(createAnimalFailure(errorDic));
  }

  function createAnimalRequest() {
    return dispatch => {
      dispatch({
        type: actions.CREATE_ANIMAL_REQUEST
      });
    };
  }

  function createAnimalFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_ANIMAL_FAILURE,
        error
      });
    };
  }

  function createAnimalSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_ANIMAL_SUCCESS,
        payload: response
      });
      notifySuccess(response);
    };
  }
};

export const editAnimal = (id, animal) => async dispatch => {
  dispatch(editAnimalRequest());
  try {
    let payload = await fetch(
      `${""}/api/animals/${id}`,
      {
        credentials: "include",
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(animal)
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(editAnimalSuccess(response.data));
  } catch (errorDic) {
    dispatch(editAnimalFailure(errorDic));
  }

  function editAnimalRequest() {
    return dispatch => {
      dispatch({
        type: actions.EDIT_ANIMAL_REQUEST
      });
    };
  }

  function editAnimalFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.EDIT_ANIMAL_FAILURE,
        error
      });
    };
  }

  function editAnimalSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.EDIT_ANIMAL_SUCCESS,
        payload: response
      });
      notifySuccess(response);
    };
  }
};

export const getAnimal = id => async dispatch => {
  dispatch(getAnimalRequest());
  try {
    let payload = await fetch(
      `${""}/api/animals/${id}`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(getAnimalSuccess(response.data));
  } catch (errorDic) {
    dispatch(getAnimalFailure(errorDic));
  }

  function getAnimalRequest() {
    return dispatch => {
      dispatch({
        type: actions.GET_ANIMAL_REQUEST
      });
    };
  }

  function getAnimalFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.GET_ANIMAL_FAILURE,
        error
      });
    };
  }

  function getAnimalSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.GET_ANIMAL_SUCCESS,
        payload: response
      });
    };
  }
};

export const removeAnimal = id => async dispatch => {
  dispatch(removeAnimalRequest());
  try {
    let payload = await fetch(
      `${""}/api/animals/${id}`,
      {
        credentials: "include",
        method: "DELETE",
        headers: {
          "Content-Type": "application/json"
        }
      }
    );

    await handleErrors(payload);
    let message = await payload.json();
    dispatch(removeAnimalSuccess(message.data));
  } catch (errorDic) {
    dispatch(removeAnimalFailure(errorDic.responseText));
  }

  function removeAnimalRequest() {
    return dispatch => {
      dispatch({
        type: actions.REMOVE_ANIMAL_REQUEST
      });
    };
  }

  function removeAnimalFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.REMOVE_ANIMAL_FAILURE,
        error
      });
    };
  }

  function removeAnimalSuccess(message) {
    return dispatch => {
      dispatch({
        type: actions.REMOVE_ANIMAL_SUCCESS,
        message
      });
    };
  }
};
