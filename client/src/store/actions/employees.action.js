import { handleErrors } from "../../utils/errorsHandler.js";
import { notifyError, notifySuccess } from "../../utils/notifications";

export const actions = {
  FETCH_EMPLOYEES_FAILURE: "FETCH_EMPLOYEES_FAILURE",
  FETCH_EMPLOYEES_SUCCESS: "FETCH_EMPLOYEES_SUCCESS",
  FETCH_EMPLOYEES_REQUEST: "FETCH_EMPLOYEES_REQUEST",
  REMOVE_EMPLOYEE_FAILURE: "REMOVE_EMPLOYEE_FAILURE",
  REMOVE_EMPLOYEE_SUCCESS: "REMOVE_EMPLOYEE_SUCCESS",
  REMOVE_EMPLOYEE_REQUEST: "REMOVE_EMPLOYEE_REQUEST",
  CREATE_EMPLOYEE_FAILURE: "CREATE_EMPLOYEE_FAILURE",
  CREATE_EMPLOYEE_SUCCESS: "CREATE_EMPLOYEE_SUCCESS",
  CREATE_EMPLOYEE_REQUEST: "CREATE_EMPLOYEE_REQUEST",
  EDIT_EMPLOYEE_FAILURE: "EDIT_EMPLOYEE_FAILURE",
  EDIT_EMPLOYEE_SUCCESS: "EDIT_EMPLOYEE_SUCCESS",
  EDIT_EMPLOYEE_REQUEST: "EDIT_EMPLOYEE_REQUEST",
  GET_EMPLOYEE_FAILURE: "GET_EMPLOYEE_FAILURE",
  GET_EMPLOYEE_SUCCESS: "GET_EMPLOYEE_SUCCESS",
  GET_EMPLOYEE_REQUEST: "GET_EMPLOYEE_REQUEST",
  ARCHIVE_REACTIVATE_EMPLOYEE_FAILURE: "ARCHIVE_REACTIVATE_EMPLOYEE_FAILURE",
  ARCHIVE_REACTIVATE_EMPLOYEE_SUCCESS: "ARCHIVE_REACTIVATE_EMPLOYEE_SUCCESS",
  ARCHIVE_REACTIVATE_EMPLOYEE_REQUEST: "ARCHIVE_REACTIVATE_EMPLOYEE_REQUEST"
};

export const fetchEmployees = () => async dispatch => {
  dispatch(fetchEmployeesRequest());
  try {
    let payload = await fetch(
      `${""}/api/employees?include=status`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let employees = await payload.json();
    dispatch(fetchEmployeesSuccess(employees));
  } catch (errorDic) {
    dispatch(fetchEmployeesFailure(errorDic.responseText));
  }

  function fetchEmployeesRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_EMPLOYEES_REQUEST
      });
    };
  }

  function fetchEmployeesFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_EMPLOYEES_FAILURE,
        error
      });
    };
  }

  function fetchEmployeesSuccess(employees) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_EMPLOYEES_SUCCESS,
        payload: employees
      });
    };
  }
};

export const removeEmployee = id => async dispatch => {
  dispatch(removeEmployeeRequest());
  try {
    let payload = await fetch(
      `${""}/api/employees`,
      {
        credentials: "include",
        method: "DELETE",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          id: id
        })
      }
    );

    await handleErrors(payload);
    let message = await payload.json();
    dispatch(removeEmployeeSuccess(message.data));
  } catch (errorDic) {
    dispatch(removeEmployeeFailure(errorDic.responseText));
  }

  function removeEmployeeRequest() {
    return dispatch => {
      dispatch({
        type: actions.REMOVE_EMPLOYEE_REQUEST
      });
    };
  }

  function removeEmployeeFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.REMOVE_EMPLOYEE_FAILURE,
        error
      });
      notifyError(error);
    };
  }

  function removeEmployeeSuccess(message) {
    return dispatch => {
      dispatch({
        type: actions.REMOVE_EMPLOYEE_SUCCESS,
        message
      });
    };
  }
};

export const createEmployee = employee => async dispatch => {
  dispatch(createEmployeeRequest());
  try {
    let payload = await fetch(
      `${""}/api/employees`,
      {
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(employee)
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(createEmployeeSuccess(response.data));
  } catch (errorDic) {
    dispatch(createEmployeeFailure(errorDic));
  }

  function createEmployeeRequest() {
    return dispatch => {
      dispatch({
        type: actions.CREATE_EMPLOYEE_REQUEST
      });
    };
  }

  function createEmployeeFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_EMPLOYEE_FAILURE,
        error: error.responseText
      });
    };
  }

  function createEmployeeSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_EMPLOYEE_SUCCESS,
        payload: response
      });
      notifySuccess(response);
    };
  }
};

export const editEmployee = employee => async dispatch => {
  dispatch(editEmployeeRequest());
  try {
    let payload = await fetch(
      `${""}/api/employees`,
      {
        credentials: "include",
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(employee)
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(editEmployeeSuccess(response.data));
  } catch (errorDic) {
    dispatch(editEmployeeFailure(errorDic));
  }

  function editEmployeeRequest() {
    return dispatch => {
      dispatch({
        type: actions.EDIT_EMPLOYEE_REQUEST
      });
    };
  }

  function editEmployeeFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.EDIT_EMPLOYEE_FAILURE,
        error: error.responseText
      });
    };
  }

  function editEmployeeSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.EDIT_EMPLOYEE_SUCCESS,
        payload: response
      });
      notifySuccess(response);
    };
  }
};

export const getEmployee = id => async dispatch => {
  dispatch(fetchEmployeeRequest());
  try {
    let payload = await fetch(
      `${
        ""
      }/api/employees/${id}?include=status`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let employee = await payload.json();
    dispatch(fetchEmployeeSuccess(employee.data));
  } catch (errorDic) {
    dispatch(fetchEmployeeFailure(errorDic));
  }

  function fetchEmployeeRequest() {
    return dispatch => {
      dispatch({
        type: actions.GET_EMPLOYEE_REQUEST
      });
    };
  }

  function fetchEmployeeFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.GET_EMPLOYEE_FAILURE,
        error
      });
    };
  }

  function fetchEmployeeSuccess(employee) {
    return dispatch => {
      dispatch({
        type: actions.GET_EMPLOYEE_SUCCESS,
        payload: employee
      });
    };
  }
};

export const archiveEmployee = (id, archive) => async dispatch => {
  dispatch(archiveEmployeeRequest());
  try {
    let endpoint = archive ? "archive" : "reactivate";
    let payload = await fetch(
      `${""}/api/employees/${endpoint}`,
      {
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          id: id
        })
      }
    );

    await handleErrors(payload);
    let message = await payload.json();
    dispatch(archiveEmployeeSuccess(message.data));
  } catch (errorDic) {
    dispatch(archiveEmployeeFailure(errorDic.responseText));
  }

  function archiveEmployeeRequest() {
    return dispatch => {
      dispatch({
        type: actions.ARCHIVE_REACTIVATE_EMPLOYEE_REQUEST
      });
    };
  }

  function archiveEmployeeFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.ARCHIVE_REACTIVATE_EMPLOYEE_FAILURE,
        error
      });
      notifyError(error);
    };
  }

  function archiveEmployeeSuccess(message) {
    return dispatch => {
      dispatch({
        type: actions.ARCHIVE_REACTIVATE_EMPLOYEE_SUCCESS,
        message
      });
    };
  }
};
