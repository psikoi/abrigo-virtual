import { handleErrors } from "../../utils/errorsHandler.js";
import { notifySuccess } from "../../utils/notifications";

export const actions = {
  FETCH_QUARENTINES_FAILURE: "FETCH_QUARENTINES_FAILURE",
  FETCH_QUARENTINES_SUCCESS: "FETCH_QUARENTINES_SUCCESS",
  FETCH_QUARENTINES_REQUEST: "FETCH_QUARENTINES_REQUEST",
  CREATE_QUARENTINE_FAILURE: "CREATE_QUARENTINE_FAILURE",
  CREATE_QUARENTINE_SUCCESS: "CREATE_QUARENTINE_SUCCESS",
  CREATE_QUARENTINE_REQUEST: "CREATE_QUARENTINE_REQUEST",
  GET_ANIMAL_QUARENTINE_FAILURE: "GET_ANIMAL_QUARENTINE_FAILURE",
  GET_ANIMAL_QUARENTINE_SUCCESS: "GET_ANIMAL_QUARENTINE_SUCCESS",
  GET_ANIMAL_QUARENTINE_REQUEST: "GET_ANIMAL_QUARENTINE_REQUEST",
  GET_QUARENTINE_REQUEST: "GET_QUARENTINE_REQUEST",
  GET_QUARENTINE_FAILURE: "GET_QUARENTINE_FAILURE",
  GET_QUARENTINE_SUCCESS: "GET_QUARENTINE_SUCCESS",
  EDIT_QUARENTINE_REQUEST: "EDIT_QUARENTINE_REQUEST",
  EDIT_QUARENTINE_SUCCESS: "EDIT_QUARENTINE_SUCCESS",
  EDIT_QUARENTINE_FAILURE: "EDIT_QUARENTINE_FAILURE",
  FINISH_QUARENTINE_FAILURE: "FINISH_QUARENTINE_FAILURE",
  FINISH_QUARENTINE_SUCCESS: "FINISH_QUARENTINE_SUCCESS",
  CLEAR_FINISH_PROPS: "CLEAR_FINISH_PROPS",
  FETCH_ANIMAL_QUARENTINES_FAILURE: "FETCH_ANIMAL_QUARENTINES_FAILURE",
  FETCH_ANIMAL_QUARENTINES_SUCCESS: "FETCH_ANIMAL_QUARENTINES_SUCCESS",
  FETCH_ANIMAL_QUARENTINES_REQUEST: "FETCH_ANIMAL_QUARENTINES_REQUEST"
};

export const fetchQuarentines = () => async dispatch => {
  dispatch(fetchQuarentinesRequest());
  try {
    let payload = await fetch(
      `${""}/api/quarentines`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let quarentines = await payload.json();
    dispatch(fetchQuarentinesSuccess(quarentines));
  } catch (errorDic) {
    dispatch(fetchQuarentinesFailure(errorDic.responseText));
  }

  function fetchQuarentinesRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_QUARENTINES_REQUEST
      });
    };
  }

  function fetchQuarentinesFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_QUARENTINES_FAILURE,
        error
      });
    };
  }

  function fetchQuarentinesSuccess(quarentines) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_QUARENTINES_SUCCESS,
        payload: quarentines
      });
    };
  }
};

export const createQuarentine = quarentine => async dispatch => {
  dispatch(createQuarentineRequest());
  try {
    let payload = await fetch(
      `${""}/api/quarentines`,
      {
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(quarentine)
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(createQuarentineSuccess(response.data));
  } catch (errorDic) {
    dispatch(createQuarentineFailure(errorDic));
  }

  function createQuarentineRequest() {
    return dispatch => {
      dispatch({
        type: actions.CREATE_QUARENTINE_REQUEST
      });
    };
  }

  function createQuarentineFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_QUARENTINE_FAILURE,
        error: error.responseText
      });
    };
  }

  function createQuarentineSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_QUARENTINE_SUCCESS,
        payload: response
      });
      notifySuccess(response);
    };
  }
};

export const fetchAnimalsToQuarentine = () => async dispatch => {
  dispatch(fetchAnimalsToQuarentineRequest());
  try {
    let payload = await fetch(
      `${""}/api/quarentines/create`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let animals = await payload.json();
    dispatch(fetchAnimalsToQuarentineSuccess(animals.data));
  } catch (errorDic) {
    dispatch(fetchAnimalsToQuarentineFailure(errorDic));
  }

  function fetchAnimalsToQuarentineRequest() {
    return dispatch => {
      dispatch({
        type: actions.GET_ANIMAL_QUARENTINE_REQUEST
      });
    };
  }

  function fetchAnimalsToQuarentineFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.GET_ANIMAL_QUARENTINE_FAILURE,
        error
      });
    };
  }

  function fetchAnimalsToQuarentineSuccess(animals) {
    return dispatch => {
      dispatch({
        type: actions.GET_ANIMAL_QUARENTINE_SUCCESS,
        payload: animals
      });
    };
  }
};

export const getQuarentine = id => async dispatch => {
  dispatch(getQuarentineRequest());
  try {
    let payload = await fetch(
      `${""}/api/quarentines/edit/${id}`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let permission = await payload.json();
    dispatch(getQuarentineSuccess(permission.data));
  } catch (errorDic) {
    dispatch(getQuarentineFailure(errorDic));
  }

  function getQuarentineRequest() {
    return dispatch => {
      dispatch({
        type: actions.GET_QUARENTINE_REQUEST
      });
    };
  }

  function getQuarentineFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.GET_QUARENTINE_FAILURE,
        error
      });
    };
  }

  function getQuarentineSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.GET_QUARENTINE_SUCCESS,
        payload: response
      });
    };
  }
};

export const editQuarentine = (id, quarentine) => async dispatch => {
  dispatch(editQuarentineRequest());
  try {
    let payload = await fetch(
      `${""}/api/quarentines/edit/${id}`,
      {
        credentials: "include",
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(quarentine)
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(editQuarentineSuccess(response.data));
  } catch (errorDic) {
    dispatch(editQuarentineFailure(errorDic));
  }

  function editQuarentineRequest() {
    return dispatch => {
      dispatch({
        type: actions.EDIT_QUARENTINE_REQUEST
      });
    };
  }

  function editQuarentineFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.EDIT_QUARENTINE_FAILURE,
        error
      });
    };
  }

  function editQuarentineSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.EDIT_QUARENTINE_SUCCESS,
        payload: response
      });
      notifySuccess(response);
    };
  }
};

export const finishQuarentine = body => async dispatch => {
  try {
    let payload = await fetch(
      `${""}/api/quarentines`,
      {
        credentials: "include",
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
      }
    );

    await handleErrors(payload);
    let quarentine = await payload.json();
    dispatch(finishQuarentineSuccess(quarentine));
  } catch (errorDic) {
    dispatch(finishQuarentineFailure(errorDic.responseText));
  }

  function finishQuarentineFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FINISH_QUARENTINE_FAILURE,
        error
      });
    };
  }

  function finishQuarentineSuccess(res) {
    return dispatch => {
      dispatch({
        type: actions.FINISH_QUARENTINE_SUCCESS,
        payload: res
      });
    };
  }
};

export const clearFinishProps = () => async dispatch => {
  dispatch(clearFinishPropsRequest());

  function clearFinishPropsRequest() {
    return dispatch => {
      dispatch({
        type: actions.CLEAR_FINISH_PROPS
      });
    };
  }
};

export const fetchAnimalQuarentines = id => async dispatch => {
  dispatch(fetchAnimalQuarentinesRequest());
  try {
    let payload = await fetch(
      `${""}/api/quarentines/animals/${id}`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let quarentines = await payload.json();
    dispatch(fetchAnimalQuarentinesSuccess(quarentines.data));
  } catch (errorDic) {
    dispatch(fetchAnimalQuarentinesFailure(errorDic));
  }

  function fetchAnimalQuarentinesRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_ANIMAL_QUARENTINES_REQUEST
      });
    };
  }

  function fetchAnimalQuarentinesFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_ANIMAL_QUARENTINES_FAILURE,
        error
      });
    };
  }

  function fetchAnimalQuarentinesSuccess(quarentines) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_ANIMAL_QUARENTINES_SUCCESS,
        payload: quarentines
      });
    };
  }
};
