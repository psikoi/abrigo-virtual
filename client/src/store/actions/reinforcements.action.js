import { handleErrors } from "../../utils/errorsHandler.js";
import { blockToDate } from "../../utils/dates";

export const actions = {
  FETCH_REINFORCEMENTS_FAILURE: "FETCH_REINFORCEMENTS_FAILURE",
  FETCH_REINFORCEMENTS_SUCCESS: "FETCH_REINFORCEMENTS_SUCCESS",
  FETCH_REINFORCEMENTS_REQUEST: "FETCH_REINFORCEMENTS_REQUEST",
  FETCH_AVAILABLE_EMPLOYEES_REQUEST: "FETCH_AVAILABLE_EMPLOYEES_REQUEST",
  FETCH_AVAILABLE_EMPLOYEES_FAILURE: "FETCH_AVAILABLE_EMPLOYEES_FAILURE",
  FETCH_AVAILABLE_EMPLOYEES_SUCCESS: "FETCH_AVAILABLE_EMPLOYEES_SUCCESS",
  CREATE_REINFORCEMENTE_REQUEST: "CREATE_REINFORCEMENTE_REQUEST",
  CREATE_REINFORCEMENTE_SUCCESS: "CREATE_REINFORCEMENTE_SUCCESS",
  CREATE_REINFORCEMENTE_FAILURE: "CREATE_REINFORCEMENTE_FAILURE",
  CANCEL_REINFORCEMENTE_REQUEST: "CANCEL_REINFORCEMENTE_REQUEST",
  CANCEL_REINFORCEMENTE_SUCCESS: "CANCEL_REINFORCEMENTE_SUCCESS",
  CANCEL_REINFORCEMENTE_FAILURE: "CANCEL_REINFORCEMENTE_FAILURE",
  GET_REINFORCEMENT_BY_TOKEN_REQUEST: "GET_REINFORCEMENT_BY_TOKEN_REQUEST",
  GET_REINFORCEMENT_BY_TOKEN_SUCCESS: "GET_REINFORCEMENT_BY_TOKEN_SUCCESS",
  GET_REINFORCEMENT_BY_TOKEN_FAILURE: "GET_REINFORCEMENT_BY_TOKEN_FAILURE",
  ACCEPT_REINFORCEMENT_SUCCESS: "ACCEPT_REINFORCEMENT_SUCCESS",
  ACCEPT_REINFORCEMENT_FAILURE: "ACCEPT_REINFORCEMENT_FAILURE",
  DECLINE_REINFORCEMENT_SUCCESS: "DECLINE_REINFORCEMENT_SUCCESS",
  DECLINE_REINFORCEMENT_FAILURE: "DECLINE_REINFORCEMENT_FAILURE"
};

export const fetchReinforcements = () => async dispatch => {
  dispatch(fetchReinforcementsRequest());
  try {
    let payload = await fetch(
      `${""}/api/reinforcements`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let reinforcements = await payload.json();
    dispatch(fetchReinforcementsSuccess(reinforcements));
  } catch (err) {
    dispatch(fetchReinforcementsFailure(err.responseText));
  }

  function fetchReinforcementsRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_REINFORCEMENTS_REQUEST
      });
    };
  }

  function fetchReinforcementsFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_REINFORCEMENTS_FAILURE,
        error
      });
    };
  }

  function fetchReinforcementsSuccess(reinforcements) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_REINFORCEMENTS_SUCCESS,
        payload: reinforcements
      });
    };
  }
};

export const fetchAvailableEmployees = timeBlocks => async dispatch => {
  dispatch(fetchAvailableEmployeesRequest());
  try {
    const sTimeBlocks = JSON.stringify(timeBlocks);
    const url = "";
    let payload = await fetch(`${url}/api/employees/available/${sTimeBlocks}`, {
      credentials: "include",
      method: "GET"
    });

    await handleErrors(payload);
    let employees = await payload.json();
    dispatch(fetchAvailableEmployeesSuccess(employees));
  } catch (err) {
    dispatch(fetchAvailableEmployeesFailure(err.responseText));
  }

  function fetchAvailableEmployeesRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_AVAILABLE_EMPLOYEES_REQUEST
      });
    };
  }

  function fetchAvailableEmployeesFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_AVAILABLE_EMPLOYEES_FAILURE,
        error
      });
    };
  }

  function fetchAvailableEmployeesSuccess(reinforcements) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_AVAILABLE_EMPLOYEES_SUCCESS,
        payload: reinforcements
      });
    };
  }
};

export const createReinforcement = (
  employee,
  startBlock,
  endBlock
) => async dispatch => {
  dispatch(createReinforcementRequest());
  try {
    const url = "";
    let payload = await fetch(`${url}/api/reinforcements/`, {
      credentials: "include",
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        employeeId: employee.employeeId,
        startDate: blockToDate(startBlock),
        endDate: blockToDate(endBlock)
      })
    });

    await handleErrors(payload);
    dispatch(createReinforcementSuccess());
  } catch (err) {
    dispatch(createReinforcementFailure(err.responseText));
  }

  function createReinforcementRequest() {
    return dispatch => {
      dispatch({
        type: actions.CREATE_REINFORCEMENTE_REQUEST
      });
    };
  }

  function createReinforcementFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_REINFORCEMENTE_FAILURE,
        error
      });
    };
  }

  function createReinforcementSuccess() {
    return dispatch => {
      dispatch({
        type: actions.CREATE_REINFORCEMENTE_SUCCESS,
        payload: true
      });
    };
  }
};

export const cancelReinforcement = id => async dispatch => {
  dispatch(cancelReinforcementRequest());
  try {
    const url = "";
    let payload = await fetch(`${url}/api/reinforcements/`, {
      credentials: "include",
      method: "DELETE",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id
      })
    });

    await handleErrors(payload);
    dispatch(cancelReinforcementSuccess());
  } catch (err) {
    dispatch(cancelReinforcementFailure(err.responseText));
  }

  function cancelReinforcementRequest() {
    return dispatch => {
      dispatch({
        type: actions.CANCEL_REINFORCEMENTE_REQUEST
      });
    };
  }

  function cancelReinforcementFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.CANCEL_REINFORCEMENTE_FAILURE,
        error
      });
    };
  }

  function cancelReinforcementSuccess() {
    return dispatch => {
      dispatch({
        type: actions.CANCEL_REINFORCEMENTE_SUCCESS,
        payload: true
      });
    };
  }
};

export const getReinforcementByToken = token => async dispatch => {
  try {
    const url = "";
    let payload = await fetch(`${url}/api/reinforcements/${token}`, {
      credentials: "include",
      method: "GET"
    });

    await handleErrors(payload);
    let reinforcement = await payload.json();
    dispatch(getReinforcementByTokenSuccess(reinforcement));
  } catch (err) {
    dispatch(getReinforcementByTokenFailure(err.responseText));
  }

  function getReinforcementByTokenFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.GET_REINFORCEMENT_BY_TOKEN_FAILURE,
        error
      });
    };
  }

  function getReinforcementByTokenSuccess(reinforcement) {
    return dispatch => {
      dispatch({
        type: actions.GET_REINFORCEMENT_BY_TOKEN_SUCCESS,
        payload: reinforcement
      });
    };
  }
};

export const acceptReinforcement = (token, body) => async dispatch => {
  try {
    const url = "";
    let payload = await fetch(`${url}/api/reinforcements/accept/${token}`, {
      credentials: "include",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body),
      method: "POST"
    });

    await handleErrors(payload);
    let reinforcement = await payload.json();
    dispatch(acceptReinforcementSuccess(reinforcement));
  } catch (err) {
    dispatch(acceptReinforcementFailure(err.responseText));
  }

  function acceptReinforcementSuccess(reinforcement) {
    return dispatch => {
      dispatch({
        type: actions.ACCEPT_REINFORCEMENT_SUCCESS,
        payload: reinforcement
      });
    };
  }

  function acceptReinforcementFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.ACCEPT_REINFORCEMENT_FAILURE,
        error
      });
    };
  }
};

export const declineReinforcement = (token, body) => async dispatch => {
  try {
    const url = "";
    let payload = await fetch(`${url}/api/reinforcements/decline/${token}`, {
      credentials: "include",
      method: "POST"
    });

    await handleErrors(payload);
    let res = await payload.json();
    dispatch(declineReinforcementSuccess(res));
  } catch (err) {
    dispatch(declineReinforcementFailure(err.responseText));
  }

  function declineReinforcementSuccess(reinforcement) {
    return dispatch => {
      dispatch({
        type: actions.DECLINE_REINFORCEMENT_SUCCESS,
        payload: reinforcement
      });
    };
  }

  function declineReinforcementFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.DECLINE_REINFORCEMENT_FAILURE,
        error
      });
    };
  }
};
