import { handleErrors } from "../../utils/errorsHandler.js";
import { notifySuccess } from "../../utils/notifications";

export const actions = {
  FETCH_TASKS_FAILURE: "FETCH_TASKS_FAILURE",
  FETCH_TASKS_SUCCESS: "FETCH_TASKS_SUCCESS",
  FETCH_TASKS_REQUEST: "FETCH_TASKS_REQUEST",
  FINISH_TASK_FAILURE: "FINISH_TASK_FAILURE",
  FINISH_TASK_SUCCESS: "FINISH_TASK_SUCCESS",
  CLEAR_FINISH_PROPS: "CLEAR_FINISH_PROPS",
  FETCH_EMPLOYEE_TASKS_FAILURE: "FETCH_EMPLOYEE_TASKS_FAILURE",
  FETCH_EMPLOYEE_TASKS_SUCCESS: "FETCH_EMPLOYEE_TASKS_SUCCESS",
  FETCH_EMPLOYEE_TASKS_REQUEST: "FETCH_EMPLOYEE_TASKS_REQUEST",
  CREATE_TASK_PERMISSION_FAILURE: "CREATE_TASK_PERMISSION_FAILURE",
  CREATE_TASK_PERMISSION_SUCCESS: "CREATE_TASK_PERMISSION_SUCCESS",
  CREATE_TASK_REQUEST: "CREATE_TASK_REQUEST",
  CREATE_TASK_SUCCESS: "CREATE_TASK_SUCCESS",
  CREATE_TASK_FAILURE: "CREATE_TASK_FAILURE",
  GET_TASK_REQUEST: "GET_TASK_REQUEST",
  GET_TASK_FAILURE: "GET_TASK_FAILURE",
  GET_TASK_SUCCESS: "GET_TASK_SUCCESS",
  EDIT_TASK_REQUEST: "EDIT_TASK_REQUEST",
  EDIT_TASK_SUCCESS: "EDIT_TASK_SUCCESS",
  EDIT_TASK_FAILURE: "EDIT_TASK_FAILURE"
};

export const fetchTasks = () => async dispatch => {
  dispatch(fetchTasksRequest());
  try {
    let payload = await fetch(
      `${""}/api/tasks`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let tasks = await payload.json();
    dispatch(fetchTasksSuccess(tasks));
  } catch (errorDic) {
    dispatch(fetchTasksFailure(errorDic.responseText));
  }

  function fetchTasksRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_TASKS_REQUEST
      });
    };
  }

  function fetchTasksFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_TASKS_FAILURE,
        error
      });
    };
  }

  function fetchTasksSuccess(tasks) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_TASKS_SUCCESS,
        payload: tasks
      });
    };
  }
};

export const fetchEmployeeTasks = id => async dispatch => {
  dispatch(fetchEmployeeTasksRequest());
  try {
    let payload = await fetch(
      `${""}/api/tasks/employee/${id}`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let tasks = await payload.json();
    dispatch(fetchEmployeeTasksSuccess(tasks.data));
  } catch (errorDic) {
    dispatch(fetchEmployeeTasksFailure(errorDic));
  }

  function fetchEmployeeTasksRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_EMPLOYEE_TASKS_REQUEST
      });
    };
  }

  function fetchEmployeeTasksFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_EMPLOYEE_TASKS_FAILURE,
        error
      });
    };
  }

  function fetchEmployeeTasksSuccess(tasks) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_EMPLOYEE_TASKS_SUCCESS,
        payload: tasks
      });
    };
  }
};

export const finishTask = body => async dispatch => {
  try {
    let payload = await fetch(
      `${""}/api/tasks`,
      {
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(body),
        method: "PUT"
      }
    );

    await handleErrors(payload);
    let tasks = await payload.json();
    dispatch(finishTaskSuccess(tasks));
  } catch (errorDic) {
    dispatch(finishTaskFailure(errorDic.responseText));
  }

  function finishTaskFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FINISH_TASK_FAILURE,
        error
      });
    };
  }

  function finishTaskSuccess(res) {
    return dispatch => {
      dispatch({
        type: actions.FINISH_TASK_SUCCESS,
        payload: res
      });
    };
  }
};

export const clearFinishProps = () => async dispatch => {
  dispatch(clearFinishPropsRequest());

  function clearFinishPropsRequest() {
    return dispatch => {
      dispatch({
        type: actions.CLEAR_FINISH_PROPS
      });
    };
  }
};

export const createTaskPermission = () => async dispatch => {
  try {
    let payload = await fetch(
      `${""}/api/tasks/create-permission`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let permission = await payload.json();
    dispatch(createTaskSuccess(permission.data));
  } catch (errorDic) {
    dispatch(createTaskFailure(errorDic));
  }

  function createTaskFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_TASK_PERMISSION_FAILURE,
        payload: error
      });
    };
  }

  function createTaskSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_TASK_PERMISSION_SUCCESS,
        payload: response
      });
    };
  }
};

export const createTask = task => async dispatch => {
  dispatch(createTaskRequest());
  try {
    let payload = await fetch(
      `${""}/api/tasks`,
      {
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(task)
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(createTaskSuccess(response.data));
  } catch (errorDic) {
    dispatch(createTaskFailure(errorDic));
  }

  function createTaskRequest() {
    return dispatch => {
      dispatch({
        type: actions.CREATE_TASK_REQUEST
      });
    };
  }

  function createTaskFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_TASK_FAILURE,
        error
      });
    };
  }

  function createTaskSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_TASK_SUCCESS,
        payload: response
      });
      notifySuccess(response);
    };
  }
};

export const getTask = id => async dispatch => {
  dispatch(getTaskRequest());
  try {
    let payload = await fetch(
      `${""}/api/tasks/edit/${id}`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let permission = await payload.json();
    dispatch(getTaskSuccess(permission.data));
  } catch (errorDic) {
    dispatch(getTaskFailure(errorDic));
  }

  function getTaskRequest() {
    return dispatch => {
      dispatch({
        type: actions.GET_TASK_REQUEST
      });
    };
  }

  function getTaskFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.GET_TASK_FAILURE,
        error
      });
    };
  }

  function getTaskSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.GET_TASK_SUCCESS,
        payload: response
      });
    };
  }
};

export const editTask = (id, task) => async dispatch => {
  dispatch(editTaskRequest());
  try {
    let payload = await fetch(
      `${""}/api/tasks/edit/${id}`,
      {
        credentials: "include",
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(task)
      }
    );

    await handleErrors(payload);
    var response = await payload.json();
    dispatch(editTaskSuccess(response.data));
  } catch (errorDic) {
    dispatch(editTaskFailure(errorDic));
  }

  function editTaskRequest() {
    return dispatch => {
      dispatch({
        type: actions.EDIT_TASK_REQUEST
      });
    };
  }

  function editTaskFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.EDIT_TASK_FAILURE,
        error
      });
    };
  }

  function editTaskSuccess(response) {
    return dispatch => {
      dispatch({
        type: actions.EDIT_TASK_SUCCESS,
        payload: response
      });
      notifySuccess(response);
    };
  }
};
