import { handleErrors } from "../../utils/errorsHandler.js";

export const actions = {
  FETCH_LICENSE_FAILURE: "FETCH_LICENSE_FAILURE",
  FETCH_LICENSE_SUCCESS: "FETCH_LICENSE_SUCCESS",
  FETCH_LICENSE_REQUEST: "FETCH_LICENSE_REQUEST",
  REGISTER_FAILURE: "REGISTER_FAILURE",
  REGISTER_SUCCESS: "REGISTER_SUCCESS",
  REGISTER_REQUEST: "REGISTER_REQUEST"
};

export const fetchLicense = () => async dispatch => {
  dispatch(fetchLicenseRequest());
  try {
    let payload = await fetch(
      `${""}/api/shelters`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let license = await payload.json();
    dispatch(fetchLicenseSuccess(license.data));
  } catch (errorDic) {
    dispatch(fetchLicenseFailure(errorDic));
  }

  function fetchLicenseRequest() {
    return dispatch => {
      dispatch({
        type: actions.FETCH_LICENSE_REQUEST
      });
    };
  }

  function fetchLicenseFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_LICENSE_FAILURE,
        error
      });
    };
  }

  function fetchLicenseSuccess(license) {
    return dispatch => {
      dispatch({
        type: actions.FETCH_LICENSE_SUCCESS,
        payload: license
      });
    };
  }
};

export const register = body => async dispatch => {
  dispatch(registerRequest());
  try {
    let payload = await fetch(
      `${""}/api/shelters`,
      {
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
      }
    );

    await handleErrors(payload);
    dispatch(registerSucess(true));
  } catch (errorDic) {
    dispatch(registerFailure(errorDic.responseText));
  }

  function registerRequest() {
    return dispatch => {
      dispatch({
        type: actions.REGISTER_REQUEST
      });
    };
  }

  function registerFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.REGISTER_FAILURE,
        error
      });
    };
  }

  function registerSucess(result) {
    return dispatch => {
      dispatch({
        type: actions.REGISTER_SUCCESS,
        payload: result
      });
    };
  }
};
