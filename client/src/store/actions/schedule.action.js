import { handleErrors } from "../../utils/errorsHandler.js";
import { notifySuccess, notifyError } from "../../utils/notifications";

export const actions = {
  GET_SCHEDULE_SUCCESS: "GET_SCHEDULE_SUCCESS",
  GET_SCHEDULE_FAILURE: "GET_SCHEDULE_FAILURE",
  GET_SCHEDULE_REQUEST: "GET_SCHEDULE_REQUEST",
  CREATE_SCHEDULE_FAILURE: "CREATE_SCHEDULE_FAILURE",
  CREATE_SCHEDULE_SUCCESS: "CREATE_SCHEDULE_SUCCESS",
  GET_USER_SUCCESS: "GET_USER_SUCCESS",
  GET_USER_FAILURE: "GET_USER_FAILURE",
  GET_EMPLOYEE_AVAILABILITY_REQUEST: "GET_EMPLOYEE_AVAILABILITY_REQUEST",
  GET_EMPLOYEE_AVAILABILITY_FAILURE: "GET_EMPLOYEE_AVAILABILITY_FAILURE",
  GET_EMPLOYEE_AVAILABILITY_SUCCESS: "GET_EMPLOYEE_AVAILABILITY_SUCCESS"
};

export const getSchedule = (id, isAvailable) => async dispatch => {
  dispatch(getScheduleRequest());
  try {
    let payload = await fetch(
      `${""}/api/schedule/${id}${
        isAvailable ? "?type=availability" : "?type=full"
      }`,
      {
        credentials: "include",
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }
    );
    await handleErrors(payload);
    var res = await payload.json();
    dispatch(getScheduleSuccess(res));
  } catch (errorDic) {
    dispatch(getScheduleFailure(errorDic));
  }

  function getScheduleRequest() {
    return dispatch => {
      dispatch({
        type: actions.GET_SCHEDULE_REQUEST
      });
    };
  }

  function getScheduleFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.GET_SCHEDULE_FAILURE,
        error
      });
    };
  }

  function getScheduleSuccess(result) {
    return dispatch => {
      dispatch({
        type: actions.GET_SCHEDULE_SUCCESS,
        payload: result
      });
    };
  }
};

export const createSchedule = (body, isAvailable) => async dispatch => {
  try {
    let payload = await fetch(
      `${""}/api/schedule${
        isAvailable ? "?type=availability" : "?type=full"
      }`,
      {
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
      }
    );

    await handleErrors(payload);
    dispatch(createScheduleSuccess(true));
  } catch (errorDic) {
    dispatch(createScheduleFailure(errorDic));
  }

  function createScheduleFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_SCHEDULE_FAILURE,
        error
      });

      notifyError(error.responseText);
    };
  }

  function createScheduleSuccess(result) {
    return dispatch => {
      dispatch({
        type: actions.CREATE_SCHEDULE_SUCCESS,
        payload: result
      });

      notifySuccess("Horário alterado com sucesso");
    };
  }
};

export const getUser = id => async dispatch => {
  try {
    let payload = await fetch(
      `${""}/api/employees/${id}`,
      {
        credentials: "include",
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }
    );

    await handleErrors(payload);
    var res = await payload.json();
    dispatch(getUserSuccess(res));
  } catch (errorDic) {
    dispatch(getUserFailure(errorDic));
  }

  function getUserFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.GET_USER_FAILURE,
        error
      });
    };
  }

  function getUserSuccess(result) {
    return dispatch => {
      dispatch({
        type: actions.GET_USER_SUCCESS,
        payload: result
      });
    };
  }
};

export const employeeAvailability = body => async dispatch => {
  dispatch(getEmployeeAvailabilityRequest());
  try {
    let payload = await fetch(
      `${""}/api/schedule/availability`,
      {
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
      }
    );

    await handleErrors(payload);
    var res = await payload.json();
    dispatch(getEmployeeAvailabilitySuccess(res));
  } catch (errorDic) {
    dispatch(getEmployeeAvailabilityFailure(errorDic));
  }

  function getEmployeeAvailabilityRequest() {
    return dispatch => {
      dispatch({
        type: actions.GET_EMPLOYEE_AVAILABILITY_REQUEST
      });
    };
  }

  function getEmployeeAvailabilityFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.GET_EMPLOYEE_AVAILABILITY_FAILURE,
        error
      });
    };
  }

  function getEmployeeAvailabilitySuccess(result) {
    return dispatch => {
      dispatch({
        type: actions.GET_EMPLOYEE_AVAILABILITY_SUCCESS,
        payload: result
      });
    };
  }
};
