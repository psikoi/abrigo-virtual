import { handleErrors } from "../../utils/errorsHandler.js";

export const actions = {
  GET_NOTIFICATIONS_FAILURE: "GET_NOTIFICATIONS_FAILURE",
  GET_NOTIFICATIONS_SUCCESS: "GET_NOTIFICATIONS_SUCCESS",
  READ_NOTIFICATION_FAILURE: "READ_NOTIFICATION_FAILURE",
  READ_NOTIFICATION_SUCCESS: "READ_NOTIFICATION_SUCCESS",
  READ_ALL_SUCCESS: "READ_ALL_SUCCESS",
  READ_ALL_FAILURE: "READ_ALL_FAILURE"
};

export const getNotifications = employeeId => async dispatch => {
  try {
    let payload = await fetch(
      `${
        ""
      }/api/notifications/employee/${employeeId}`,
      {
        credentials: "include",
        method: "GET"
      }
    );

    await handleErrors(payload);
    let notifications = await payload.json();
    dispatch(getNotificationsSuccess(notifications));
  } catch (errorDic) {
    dispatch(getNotificationsFailure(errorDic.responseText));
  }

  function getNotificationsFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.GET_NOTIFICATIONS_FAILURE,
        error
      });
    };
  }

  function getNotificationsSuccess(notifications) {
    return dispatch => {
      dispatch({
        type: actions.GET_NOTIFICATIONS_SUCCESS,
        payload: notifications
      });
    };
  }
};

export const readNotification = notificationId => async dispatch => {
  try {
    let payload = await fetch(
      `${
        ""
      }/api/notifications/read/${notificationId}`,
      {
        credentials: "include",
        method: "PUT"
      }
    );

    await handleErrors(payload);
    let res = await payload.json();
    dispatch(readNotificationSuccess(res));
  } catch (errorDic) {
    dispatch(readNotificationFailure(errorDic.responseText));
  }

  function readNotificationFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.READ_NOTIFICATION_FAILURE,
        error
      });
    };
  }

  function readNotificationSuccess(res) {
    return dispatch => {
      dispatch({
        type: actions.READ_NOTIFICATION_SUCCESS,
        payload: res
      });
    };
  }
};

export const readAllNotifications = notificationIds => async dispatch => {
  try {
    let payload = await fetch(
      `${""}/api/notifications/read/`,
      {
        credentials: "include",
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(notificationIds)
      }
    );

    await handleErrors(payload);
    let res = await payload.json();
    dispatch(readAllSuccess(res));
  } catch (errorDic) {
    dispatch(readAllFailure(errorDic.responseText));
  }

  function readAllFailure(error) {
    return dispatch => {
      dispatch({
        type: actions.READ_ALL_FAILURE,
        error
      });
    };
  }

  function readAllSuccess(res) {
    return dispatch => {
      dispatch({
        type: actions.READ_ALL_SUCCESS,
        payload: res
      });
    };
  }
};
