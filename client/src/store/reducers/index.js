import {
  employees,
  removeEmployee,
  archiveEmployee,
  employee,
  createEmployee,
  editEmployee
} from "./employees.reducer";
import {
  login,
  logout,
  changePassword,
  forgotPassword,
  resetPassword
} from "./sessions.reducer";
import {
  reinforcements,
  availableEmployees,
  createReinforcement,
  cancelReinforcement,
  acceptReinforcement
} from "./reinforcements.reducer";
import { tasks, employeeTasks, createTask, editTask } from "./tasks.reducer";
import {
  fetchAnimals,
  createAnimal,
  editAnimal,
  removeAnimal,
  getAnimal
} from "./animals.reducer";
import { notifications, readNotifications } from "./notifications.reducer";
import { schedule, employeeAvailability } from "./schedule.reducer";
import {
  quarentines,
  createQuarentine,
  editQuarentine,
  animalQuarentines
} from "./quarentines.reducer";
import {
  createRelationship,
  editRelationship,
  getAnimalRelationships,
  getRelationship,
  removeRelationship
} from "./relationships.reducer";
import { kennels, kennelsActions } from "./kennels.reducer";
import { fetchLicense, register } from "./shelters.reducer";

export default {
  employees,
  changePassword,
  forgotPassword,
  resetPassword,
  login,
  logout,
  removeEmployee,
  reinforcements,
  availableEmployees,
  createReinforcement,
  archiveEmployee,
  tasks,
  employeeTasks,
  createTask,
  editTask,
  editEmployee,
  createEmployee,
  employee,
  schedule,
  employeeAvailability,
  cancelReinforcement,
  acceptReinforcement,
  notifications,
  readNotifications,
  createAnimal,
  editAnimal,
  quarentines,
  fetchAnimals,
  removeAnimal,
  getAnimal,
  createRelationship,
  editRelationship,
  getAnimalRelationships,
  getRelationship,
  removeRelationship,
  createQuarentine,
  editQuarentine,
  kennels,
  kennelsActions,
  register,
  animalQuarentines,
  fetchLicense
};
