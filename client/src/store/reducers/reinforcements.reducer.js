import { actions } from "../actions/reinforcements.action";

export function reinforcements(state = null, action) {
  switch (action.type) {
    case actions.FETCH_REINFORCEMENTS_SUCCESS:
      return { ...state, data: action.payload, isReinforcementsLoading: false };
    case actions.FETCH_REINFORCEMENTS_FAILURE:
      return { ...state, error: action.error, isReinforcementsLoading: false };
    case actions.FETCH_REINFORCEMENTS_REQUEST:
      return { ...state, isReinforcementsLoading: true };
    default:
      return state;
  }
}

export function availableEmployees(state = null, action) {
  switch (action.type) {
    case actions.FETCH_AVAILABLE_EMPLOYEES_SUCCESS:
      return { ...state, data: action.payload, isAvailableLoading: false };
    case actions.FETCH_AVAILABLE_EMPLOYEES_FAILURE:
      return { ...state, error: action.error, isAvailableLoading: false };
    case actions.FETCH_AVAILABLE_EMPLOYEES_REQUEST:
      return { ...state, isAvailableLoading: true };
    default:
      return state;
  }
}

export function createReinforcement(state = null, action) {
  switch (action.type) {
    case actions.CREATE_REINFORCEMENTE_SUCCESS:
      return { ...state, success: action.payload, isCreationLoading: false };
    case actions.CREATE_REINFORCEMENTE_FAILURE:
      return { ...state, error: action.error, isCreationLoading: false };
    case actions.CREATE_REINFORCEMENTE_REQUEST:
      return { ...state, isCreationLoading: true };
    default:
      return state;
  }
}

export function cancelReinforcement(state = null, action) {
  switch (action.type) {
    case actions.CANCEL_REINFORCEMENTE_SUCCESS:
      return { ...state, success: action.payload, isCancelLoading: false };
    case actions.CANCEL_REINFORCEMENTE_FAILURE:
      return { ...state, error: action.error, isCancelLoading: false };
    case actions.CANCEL_REINFORCEMENTE_REQUEST:
      return { ...state, isCancelLoading: true };
    default:
      return state;
  }
}

export function acceptReinforcement(state = null, action) {
  switch (action.type) {
    case actions.GET_REINFORCEMENT_BY_TOKEN_SUCCESS:
      return {
        ...state,
        success: action.payload,
        isGetReinforcementLoading: false
      };
    case actions.GET_REINFORCEMENT_BY_TOKEN_FAILURE:
      return {
        ...state,
        error: action.error,
        isGetReinforcementLoading: false
      };
    case actions.ACCEPT_REINFORCEMENT_SUCCESS:
      return {
        ...state,
        success: false,
        error: false,
        acceptSuccess: action.payload
      };
    case actions.ACCEPT_REINFORCEMENT_FAILURE:
      return {
        ...state,
        success: false,
        error: false,
        acceptError: action.error
      };
    case actions.DECLINE_REINFORCEMENT_SUCCESS:
      return {
        ...state,
        declineSuccess: true,
        declineError: false
      };
    case actions.DECLINE_REINFORCEMENT_FAILURE:
      return {
        ...state,
        declineError: action.error,
        declineSuccess: false
      };
    default:
      return state;
  }
}
