import { actions } from "../actions/schedule.action";

export function schedule(state = null, action) {
  switch (action.type) {
    case actions.GET_SCHEDULE_SUCCESS:
      return { ...state, getSuccess: action.payload, error: undefined };
    case actions.GET_SCHEDULE_FAILURE:
      return {
        ...state,
        getError: {
          getError: action.error.responseText,
          code: action.error.code
        },
        getSuccess: undefined
      };
    case actions.GET_SCHEDULE_REQUEST:
      return {
        ...state,
        createSuccess: undefined,
        getError: undefined
      };
    case actions.CREATE_SCHEDULE_SUCCESS:
      return {
        ...state,
        createSuccess: action.payload,
        createError: undefined
      };
    case actions.CREATE_SCHEDULE_FAILURE:
      return {
        ...state,
        createError: {
          createError: action.error.responseText,
          code: action.error.code
        },
        createSuccess: undefined
      };
    default:
      return state;
  }
}

export function employeeAvailability(state = null, action) {
  switch (action.type) {
    case actions.GET_EMPLOYEE_AVAILABILITY_SUCCESS:
      return { ...state, data: action.payload };
    case actions.GET_EMPLOYEE_AVAILABILITY_FAILURE:
      return {
        ...state,
        error: { code: action.error.code, message: action.error.responseText }
      };
    case actions.GET_EMPLOYEE_AVAILABILITY_REQUEST:
      return { ...state };
    default:
      return state;
  }
}
