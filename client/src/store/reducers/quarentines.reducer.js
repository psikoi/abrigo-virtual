import { actions } from "../actions/quarentines.action";

export function quarentines(state = null, action) {
  switch (action.type) {
    case actions.FETCH_QUARENTINES_SUCCESS:
      return { ...state, data: action.payload, isLoading: false };
    case actions.FETCH_QUARENTINES_FAILURE:
      return { ...state, error: action.error, isLoading: false };
    case actions.FETCH_QUARENTINES_REQUEST:
      return { ...state, isLoading: true };
    case actions.FINISH_QUARENTINE_FAILURE:
      return {
        ...state,
        finishError: action.error,
        finishSuccess: undefined
      };
    case actions.FINISH_QUARENTINE_SUCCESS:
      return {
        ...state,
        finishSuccess: action.payload,
        finishError: undefined
      };
    case actions.CLEAR_FINISH_PROPS:
      return {
        ...state,
        finishSuccess: undefined,
        finishError: undefined
      };
    default:
      return state;
  }
}

export function createQuarentine(state = null, action) {
  switch (action.type) {
    case actions.CREATE_QUARENTINE_SUCCESS:
      return { ...state, success: action.payload, isLoading: false };
    case actions.CREATE_QUARENTINE_FAILURE:
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        isLoading: false
      };
    case actions.CREATE_QUARENTINE_REQUEST:
      return {
        ...state,
        isLoading: true,
        success: undefined,
        error: undefined
      };
    case actions.GET_ANIMAL_QUARENTINE_SUCCESS:
      return {
        ...state,
        animals: action.payload,
        isLoading: false
      };
    case actions.GET_ANIMAL_QUARENTINE_FAILURE:
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        isLoading: false
      };
    case actions.GET_ANIMAL_QUARENTINE_REQUEST:
      return {
        ...state,
        isLoading: true,
        success: undefined,
        error: undefined
      };
    default:
      return state;
  }
}

export function editQuarentine(state = null, action) {
  switch (action.type) {
    case actions.EDIT_QUARENTINE_SUCCESS:
      return { ...state, success: action.payload, isLoading: false };
    case actions.EDIT_QUARENTINE_FAILURE:
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        isLoading: false
      };
    case actions.EDIT_QUARENTINE_REQUEST:
      return {
        ...state,
        isLoading: true,
        success: undefined,
        error: undefined
      };
    case action.GET_QUARENTINE_REQUEST:
      return {
        isLoading: true,
        success: undefined,
        error: undefined,
        quarentine: undefined,
        animals: undefined
      };
    case actions.GET_QUARENTINE_SUCCESS:
      return {
        ...state,
        permission: true,
        animals: action.payload.animals,
        quarentine: action.payload.quarentine,
        success: undefined,
        isLoading: false,
        error: undefined
      };
    case actions.GET_QUARENTINE_FAILURE:
      return {
        ...state,
        permission: false,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        success: undefined,
        isLoading: undefined
      };
    default:
      return state;
  }
}

export function animalQuarentines(state = null, action) {
  switch (action.type) {
    case actions.FETCH_ANIMAL_QUARENTINES_SUCCESS:
      return { ...state, quarentines: action.payload, isLoading: false };
    case actions.FETCH_ANIMAL_QUARENTINES_FAILURE:
      return {
        ...state,
        error: { code: action.error.code, message: action.error.responseText },
        isLoading: false
      };
    case actions.FETCH_ANIMAL_QUARENTINES_REQUEST:
      return { ...state, isLoading: true };
    default:
      return state;
  }
}
