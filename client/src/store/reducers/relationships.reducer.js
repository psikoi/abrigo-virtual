import { actions } from "../actions/relationships.action";

export function createRelationship(state = null, action) {
  switch (action.type) {
    case actions.GET_CREATE_RELATIONSHIP_SUCCESS:
      return {
        ...state,
        getCreateRelationshipSuccess: action.payload,
        getCreateRelationshipLoading: false
      };
    case actions.GET_CREATE_RELATIONSHIP_FAILURE:
      return {
        ...state,
        getCreateRelationshipFailure: {
          code: action.error.code,
          message: action.error.responseText
        },
        getCreateRelationshipLoading: false
      };
    case actions.GET_CREATE_RELATIONSHIP_REQUEST:
      return {
        ...state,
        getCreateRelationshipLoading: true,
        getCreateRelationshipSuccess: undefined,
        getCreateRelationshipFailure: undefined
      };
    case actions.CREATE_RELATIONSHIP_SUCCESS:
      return {
        ...state,
        success: action.payload,
        loading: false,

        getCreateRelationshipLoading: false,
        getCreateRelationshipSuccess: undefined,
        getCreateRelationshipFailure: undefined
      };
    case actions.CREATE_RELATIONSHIP_FAILURE:
      return {
        ...state,
        errorMessage: action.error.responseText,
        loading: false,

        getCreateRelationshipLoading: false,
        getCreateRelationshipSuccess: undefined,
        getCreateRelationshipFailure: undefined
      };
    case actions.CREATE_RELATIONSHIP_REQUEST:
      return {
        ...state,
        loading: true,
        success: undefined,
        errorMessage: undefined,

        getCreateRelationshipLoading: false,
        getCreateRelationshipSuccess: undefined,
        getCreateRelationshipFailure: undefined
      };
    default:
      return state;
  }
}

export function editRelationship(state = null, action) {
  switch (action.type) {
    case actions.EDIT_RELATIONSHIP_SUCCESS:
      return {
        ...state,
        editSuccess: action.payload,
        editLoading: false
      };
    case actions.EDIT_RELATIONSHIP_FAILURE:
      return {
        ...state,
        editError: action.error.responseText,
        editLoading: false
      };
    case actions.EDIT_RELATIONSHIP_REQUEST:
      return {
        ...state,
        editLoading: true,
        editSuccess: undefined,
        editError: undefined
      };
    default:
      return state;
  }
}

export function getAnimalRelationships(state = null, action) {
  switch (action.type) {
    case actions.GET_ANIMAL_RELATIONSHIPS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        isLoading: false
      };
    case actions.GET_ANIMAL_RELATIONSHIPS_FAILURE:
      return {
        ...state,
        error: action.error.responseText,
        isLoading: false
      };
    case actions.GET_ANIMAL_RELATIONSHIPS_REQUEST:
      return {
        ...state,
        isLoading: true,
        data: undefined,
        error: undefined
      };
    default:
      return state;
  }
}

export function getRelationship(state = null, action) {
  switch (action.type) {
    case actions.GET_RELATIONSHIP_SUCCESS:
      return {
        ...state,
        data: action.payload,
        isLoading: false
      };
    case actions.GET_RELATIONSHIP_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false
      };
    case actions.GET_RELATIONSHIP_REQUEST:
      return {
        ...state,
        isLoading: true,
        data: undefined,
        error: undefined
      };
    default:
      return state;
  }
}

export function removeRelationship(state = null, action) {
  switch (action.type) {
    case actions.REMOVE_RELATIONSHIP_SUCCESS:
      return {
        ...state,
        removeData: action.payload,
        removeLoading: false
      };
    case actions.REMOVE_RELATIONSHIP_FAILURE:
      return {
        ...state,
        removeError: action.error,
        removeLoading: false
      };
    case actions.REMOVE_RELATIONSHIP_REQUEST:
      return {
        ...state,
        removeLoading: true,
        removeData: undefined,
        removeError: undefined
      };
    default:
      return state;
  }
}
