import { actions } from "../actions/sessions.action";

export function login(state = null, action) {
  switch (action.type) {
    case actions.LOGIN_SUCESS:
      return { ...state, loginSuccess: action.payload, isLoginLoading: false };
    case actions.LOGIN_FAILURE:
      return { ...state, error: action.error, isLoginLoading: false };
    case actions.LOGIN_REQUEST:
      return { ...state, isLoginLoading: true };
    default:
      return state;
  }
}

export function logout(state = null, action) {
  switch (action.type) {
    case actions.LOGOUT_SUCESS:
      return {
        ...state,
        success: action.success,
        isLogoutLoading: false
      };
    case actions.LOGOUT_FAILURE:
      return { ...state, error: action.error, isLogoutLoading: false };
    case actions.LOGOUT_REQUEST:
      return { ...state, isLogoutLoading: true };
    default:
      return state;
  }
}

export function changePassword(state = null, action) {
  switch (action.type) {
    case actions.CHANGE_PASSWORD_SUCCESS:
      return { ...state, success: true, isLoading: false, error: undefined };
    case actions.CHANGE_PASSWORD_FAILURE:
      return { ...state, error: action.error, isLoading: false };
    case actions.CHANGE_PASSWORD_REQUEST:
      return { ...state, isLoading: true };
    default:
      return state;
  }
}

export function forgotPassword(state = null, action) {
  switch (action.type) {
    case actions.FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        message: action.payload,
        isLoading: false,
        error: undefined
      };
    case actions.FORGOT_PASSWORD_FAILURE:
      return { ...state, error: action.error, isLoading: false };
    case actions.FORGOT_PASSWORD_REQUEST:
      return { ...state, isLoading: true };
    default:
      return state;
  }
}

export function resetPassword(state = null, action) {
  switch (action.type) {
    case actions.RESET_PASSWORD_SUCCESS:
      return { ...state, message: action.payload, isLoading: false };
    case actions.RESET_PASSWORD_FAILURE:
      return { ...state, error: action.error, isLoading: false };
    case actions.RESET_PASSWORD_REQUEST:
      return { ...state, isLoading: true };
    default:
      return state;
  }
}
