import { actions } from "../actions/shelters.action";

export function fetchLicense(state = null, action) {
  switch (action.type) {
    case actions.FETCH_LICENSE_SUCCESS:
      return { ...state, license: action.payload, isLoading: false };
    case actions.FETCH_LICENSE_FAILURE:
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        isLoading: false
      };
    case actions.FETCH_LICENSE_REQUEST:
      return { ...state, isLoading: true };
    default:
      return state;
  }
}

export function register(state = null, action) {
  switch (action.type) {
    case actions.REGISTER_SUCCESS:
      return {
        ...state,
        success: action.payload,
        isLoading: false,
        error: undefined
      };
    case actions.REGISTER_FAILURE:
      return { ...state, error: action.error, isLoading: false };
    case actions.REGISTER_REQUEST:
      return { ...state, isLoading: true, error: undefined };
    default:
      return state;
  }
}
