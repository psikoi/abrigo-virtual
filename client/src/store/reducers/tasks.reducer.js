import { actions } from "../actions/tasks.action";

export function tasks(state = null, action) {
  switch (action.type) {
    case actions.FETCH_TASKS_SUCCESS:
      return { ...state, data: action.payload, isLoading: false };
    case actions.FETCH_TASKS_FAILURE:
      return { ...state, error: action.error, isLoading: false };
    case actions.FETCH_TASKS_REQUEST:
      return { ...state, isLoading: true };
    case actions.FINISH_TASK_FAILURE:
      return {
        ...state,
        finishError: action.error,
        finishSuccess: undefined
      };
    case actions.FINISH_TASK_SUCCESS:
      return {
        ...state,
        finishSuccess: action.payload,
        finishError: undefined
      };
    case actions.CLEAR_FINISH_PROPS:
      return {
        ...state,
        finishSuccess: undefined,
        finishError: undefined
      };
    default:
      return state;
  }
}

export function employeeTasks(state = null, action) {
  switch (action.type) {
    case actions.FETCH_EMPLOYEE_TASKS_SUCCESS:
      return { ...state, tasks: action.payload, isLoading: false };
    case actions.FETCH_EMPLOYEE_TASKS_FAILURE:
      return {
        ...state,
        error: { code: action.error.code, message: action.error.responseText },
        isLoading: false
      };
    case actions.FETCH_EMPLOYEE_TASKS_REQUEST:
      return { ...state, isLoading: true };
    default:
      return state;
  }
}

export function createTask(state = null, action) {
  switch (action.type) {
    case actions.CREATE_TASK_SUCCESS:
      return { ...state, success: action.payload, isLoading: false };
    case actions.CREATE_TASK_FAILURE:
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        isLoading: false
      };
    case actions.CREATE_TASK_REQUEST:
      return {
        ...state,
        isLoading: true,
        success: undefined,
        error: undefined
      };
    case actions.CREATE_TASK_PERMISSION_SUCCESS:
      return {
        ...state,
        permission: true,
        employees: action.payload,
        success: undefined
      };
    case actions.CREATE_TASK_PERMISSION_FAILURE:
      return {
        ...state,
        permission: false,
        error: action.payload,
        success: undefined
      };
    default:
      return state;
  }
}

export function editTask(state = null, action) {
  switch (action.type) {
    case actions.EDIT_TASK_SUCCESS:
      return { ...state, success: action.payload, isLoading: false };
    case actions.EDIT_TASK_FAILURE:
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        isLoading: false
      };
    case actions.EDIT_TASK_REQUEST:
      return {
        ...state,
        isLoading: true,
        success: undefined,
        error: undefined
      };
    case action.GET_TASK_REQUEST:
      return {
        isLoading: true,
        success: undefined,
        error: undefined,
        task: undefined,
        employees: undefined
      };
    case actions.GET_TASK_SUCCESS:
      return {
        ...state,
        permission: true,
        employees: action.payload.employees,
        task: action.payload.task,
        success: undefined,
        isLoading: false,
        error: undefined
      };
    case actions.GET_TASK_FAILURE:
      return {
        ...state,
        permission: false,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        success: undefined,
        isLoading: undefined
      };
    default:
      return state;
  }
}
