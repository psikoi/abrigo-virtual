import { actions } from "../actions/animals.action";

export function fetchAnimals(state = null, action) {
  switch (action.type) {
    case actions.FETCH_ANIMALS_SUCCESS:
      return { ...state, data: action.payload, isLoading: false };
    case actions.FETCH_ANIMALS_FAILURE:
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        isLoading: false
      };
    case actions.FETCH_ANIMALS_REQUEST:
      return { ...state, isLoading: true };
    default:
      return state;
  }
}

export function createAnimal(state = null, action) {
  switch (action.type) {
    case actions.CREATE_ANIMAL_SUCCESS:
      return { ...state, success: action.payload, isLoading: false };
    case actions.CREATE_ANIMAL_FAILURE:
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        isLoading: false
      };
    case actions.CREATE_ANIMAL_REQUEST:
      return {
        ...state,
        isLoading: true,
        success: undefined,
        error: undefined
      };
    default:
      return state;
  }
}

export function editAnimal(state = null, action) {
  switch (action.type) {
    case actions.EDIT_ANIMAL_SUCCESS:
      return { ...state, success: action.payload, isLoading: false };
    case actions.EDIT_ANIMAL_FAILURE:
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        isLoading: false
      };
    case actions.EDIT_ANIMAL_REQUEST:
      return {
        ...state,
        isLoading: true,
        success: undefined,
        error: undefined
      };
    case action.GET_ANIMAL_REQUEST:
      return {
        isLoading: true,
        success: undefined,
        error: undefined,
        animal: undefined
      };
    case actions.GET_ANIMAL_SUCCESS:
      return {
        ...state,
        permission: true,
        animal: action.payload,
        success: undefined,
        isLoading: false,
        error: undefined
      };
    case actions.GET_ANIMAL_FAILURE:
      return {
        ...state,
        permission: false,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        success: undefined,
        isLoading: undefined
      };
    default:
      return state;
  }
}

export function getAnimal(state = null, action) {
  switch (action.type) {
    case action.GET_ANIMAL_REQUEST:
      return {
        ...state,
        isLoading: true,
        data: undefined,
        error: undefined
      };
    case actions.GET_ANIMAL_SUCCESS:
      return {
        ...state,
        data: action.payload,
        isLoading: false,
        error: undefined
      };
    case actions.GET_ANIMAL_FAILURE:
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        data: undefined,
        isLoading: undefined
      };
    default:
      return state;
  }
}

export function removeAnimal(state = null, action) {
  switch (action.type) {
    case actions.REMOVE_ANIMAL_SUCCESS:
      return {
        ...state,
        success: action.message,
        isLoading: false
      };
    case actions.REMOVE_ANIMAL_FAILURE:
      return { ...state, error: action.error, isLoading: false };
    case actions.REMOVE_ANIMAL_REQUEST:
      return { ...state, isLoading: true };
    default:
      return state;
  }
}
