import { actions } from "../actions/kennels.action";

export function kennels(state = null, action) {
  switch (action.type) {
    case actions.FETCH_KENNELS_SUCCESS:
      return { ...state, data: action.payload, isLoading: false };
    case actions.FETCH_KENNELS_FAILURE:
      return { ...state, error: action.error, isLoading: false };
    case actions.FETCH_KENNELS_REQUEST:
      return { ...state, isLoading: true };
    default:
      return state;
  }
}

export function kennelsActions(state = null, action) {
  switch (action.type) {
    case actions.FETCH_AVAILABLE_ANIMALS_SUCCESS:
      return { ...state, animals: action.payload, isLoading: false };
    case actions.FETCH_AVAILABLE_ANIMALS_FAILURE:
      return { ...state, error: action.error, isLoading: false };
    case actions.FETCH_AVAILABLE_ANIMALS_REQUEST:
      return { ...state, error: null, success: null, isLoading: true };
    case actions.CREATE_KENNEL_SUCCESS:
      return { ...state, success: true, isLoading: false };
    case actions.CREATE_KENNEL_FAILURE:
      return {
        ...state,
        error: action.error,
        success: false,
        isLoading: false
      };
    case actions.CREATE_KENNEL_REQUEST:
      return { ...state, error: null, success: null, isLoading: true };
    case actions.EDIT_KENNEL_SUCCESS:
      return { ...state, success: true, isLoading: false };
    case actions.EDIT_KENNEL_FAILURE:
      return {
        ...state,
        error: action.error,
        success: false,
        isLoading: false
      };
    case actions.EDIT_KENNEL_REQUEST:
      return { ...state, error: null, success: null, isLoading: true };
    case actions.FETCH_KENNEL_SUCCESS:
      return { ...state, kennel: action.payload, isLoading: false };
    case actions.FETCH_KENNEL_FAILURE:
      return { ...state, error: action.error, kennel: null, isLoading: false };
    case actions.FETCH_KENNEL_REQUEST:
      return { ...state, error: null, kennel: null, isLoading: true };
    case actions.DELETE_KENNEL_SUCCESS:
      return { ...state, success: action.message, isLoading: false };
    case actions.DELETE_KENNEL_FAILURE:
      return {
        ...state,
        error: action.error,
        success: false,
        isLoading: false
      };
    case actions.DELETE_KENNEL_REQUEST:
      return { ...state, error: null, success: null, isLoading: true };
    case actions.CALCULATE_SUCCESS:
      return { ...state, data: action.payload };
    case actions.CALCULATE_FAILURE:
      return { ...state, error: action.error };
    case actions.CALCULATE_REQUEST:
      return {
        ...state,
        isLoading: true,
        data: undefined,
        error: undefined
      };
    case actions.REDISTRIBUTION_SUCCESS:
      return { ...state, data: action.payload };
    case actions.REDISTRIBUTION_FAILURE:
      return { ...state, error: action.error };
    case actions.REDISTRIBUTION_REQUEST:
      return {
        ...state,
        isLoading: true,
        data: undefined,
        error: undefined
      };
    default:
      return state;
  }
}
