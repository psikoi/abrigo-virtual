import { actions } from "../actions/notifications.action";

export function notifications(state = null, action) {
  switch (action.type) {
    case actions.GET_NOTIFICATIONS_SUCCESS:
      return {
        ...state,
        notifications: action.payload,
        notificationsError: undefined
      };
    case actions.GET_NOTIFICATIONS_FAILURE:
      return {
        ...state,
        notificationsError: action.error,
        notifications: undefined
      };
    default:
      return state;
  }
}

export function readNotifications(state = null, action) {
  switch (action.type) {
    case actions.READ_NOTIFICATION_SUCCESS:
      return { ...state, readSuccess: action.payload };
    case actions.READ_NOTIFICATION_FAILURE:
      return { ...state, readError: action.error };
    case actions.READ_ALL_SUCCESS:
      return { ...state, readAllSuccess: action.payload };
    case actions.READ_ALL_FAILURE:
      return { ...state, readAllError: action.error };
    default:
      return state;
  }
}
