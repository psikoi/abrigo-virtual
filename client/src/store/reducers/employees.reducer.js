import { actions } from "../actions/employees.action";

export function employees(state = null, action) {
  switch (action.type) {
    case actions.FETCH_EMPLOYEES_SUCCESS:
      return { ...state, data: action.payload, isEmployeesLoading: false };
    case actions.FETCH_EMPLOYEES_FAILURE:
      return { ...state, error: action.error, isEmployeesLoading: false };
    case actions.FETCH_EMPLOYEES_REQUEST:
      return { ...state, isEmployeesLoading: true };
    default:
      return state;
  }
}

export function removeEmployee(state = null, action) {
  switch (action.type) {
    case actions.REMOVE_EMPLOYEE_SUCCESS:
      return {
        ...state,
        success: action.message,
        isRemoveEmployeeLoading: false
      };
    case actions.FETCH_EMPLOYEES_FAILURE:
      return { ...state, error: action.error, isRemoveEmployeeLoading: false };
    case actions.FETCH_EMPLOYEES_REQUEST:
      return { ...state, isRemoveEmployeeLoading: true };
    default:
      return state;
  }
}

export function archiveEmployee(state = null, action) {
  switch (action.type) {
    case actions.ARCHIVE_REACTIVATE_EMPLOYEE_SUCCESS:
      return {
        ...state,
        success: action.message,
        isArchiveReactivateEmployeeLoading: false
      };
    case actions.ARCHIVE_REACTIVATE_EMPLOYEE_FAILURE:
      return {
        ...state,
        error: action.error,
        isArchiveReactivateEmployeeLoading: false
      };
    case actions.ARCHIVE_REACTIVATE_EMPLOYEE_REQUEST:
      return { ...state, isArchiveReactivateEmployeeLoading: true };
    default:
      return state;
  }
}

export function createEmployee(state = null, action) {
  switch (action.type) {
    case actions.CREATE_EMPLOYEE_SUCCESS:
      return { ...state, success: action.payload, isLoading: false };
    case actions.CREATE_EMPLOYEE_FAILURE:
      return { ...state, error: action.error, isLoading: false };
    case actions.CREATE_EMPLOYEE_REQUEST:
      return {
        ...state,
        isLoading: true,
        success: undefined,
        error: undefined
      };
    default:
      return state;
  }
}

export function editEmployee(state = null, action) {
  switch (action.type) {
    case actions.EDIT_EMPLOYEE_SUCCESS:
      return { ...state, success: action.payload, isLoading: false };
    case actions.EDIT_EMPLOYEE_FAILURE:
      return { ...state, error: action.error, isLoading: false };
    case actions.EDIT_EMPLOYEE_REQUEST:
      return {
        ...state,
        isLoading: true,
        success: undefined,
        error: undefined
      };
    default:
      return state;
  }
}

export function employee(state = null, action) {
  switch (action.type) {
    case actions.GET_EMPLOYEE_SUCCESS:
      return { ...state, data: action.payload, isLoading: false };
    case actions.GET_EMPLOYEE_FAILURE:
      return {
        ...state,
        error: {
          code: action.error.code,
          message: action.error.responseText
        },
        isLoading: false
      };
    case actions.GET_EMPLOYEE_REQUEST:
      return { ...state, isLoading: true, error: undefined, data: undefined };
    default:
      return state;
  }
}
