const port = 5000;
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const httpFunctions = require("./api/HTTPFunctions.js");
const cors = require("cors");
const cookieParser = require("cookie-parser");
require("dotenv").config();
const api = require("./api/routing.js");

app.use(cookieParser());

app.use(express.static("public"));

app.use(cors({ credentials: true, origin: process.env.REACT_APP_URL }));
app.use(express.json({ limit: "50mb", extended: true }));
app.use(express.urlencoded({ limit: "50mb", extended: true }));
app.use(bodyParser.json({ limit: "50mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

app.use("/api", api);

app.get("/healthz", function(req, res, next) {
  res.send(200);
});

app.get("*", function(req, res, next) {
  next(httpFunctions.notFound());
});

app.use(function(err, req, res, next) {
  // If err has no specified error code, set error code to 'Internal Server Error (500)'
  if (!err.statusCode) {
    err.statusCode = 500;
  }
  res.status(err.statusCode).send(err.message);
});

var server = app.listen(port, function() {
  var host =
    server.address().address === "::" ? "localhost" : server.address().address;
  var port = server.address().port;
  console.log("Abrigo Virtual server is listening at http://%s:%s", host, port);
});

module.exports = server;
