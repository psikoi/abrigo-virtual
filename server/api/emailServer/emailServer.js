const hbs = require("nodemailer-express-handlebars");
const email = "virtualabrigo@gmail.com";
const pass = "engenhariadesoftware";
const nodemailer = require("nodemailer");
const httpFunctions = require("../HTTPFunctions.js");
const utils = require("../utils");

// used to send email
var smtpTransport = nodemailer.createTransport({
  service: "Gmail",
  auth: {
    user: email,
    pass: pass
  }
});

// used to load the email templates
var handlebarsOptions = {
  viewEngine: {
    extName: ".html",
    partialsDir: __dirname + "/templates/",
    layoutsDir: __dirname + "/templates"
  },
  viewPath: __dirname + "/templates/",
  extName: ".html"
};

// add the template configurations to transport
smtpTransport.use("compile", hbs(handlebarsOptions));

function sendEmail(res, data, callback) {
  smtpTransport.sendMail(data, function(err) {
    if (err) {
      var err = httpFunctions.serverError();
      res.status(err.statusCode).send(err.message);
    } else {
      if (callback) {
        callback();
      } else {
        return res.json({
          message: "Email enviado, por favor verifique a sua conta de email."
        });
      }
    }
  });
}

function sendNewAccountEmail(email, password) {
  var data = {
    to: email,
    from: email,
    template: "new-account",
    subject: "Conta de utilizador - AbrigoVirtual",
    context: {
      password: password
    }
  };
  smtpTransport.sendMail(data);
}

function sendTaskNotification(task, employee, subject, message) {
  var data = {
    to: employee.email,
    from: email,
    template: "notify-task",
    subject,
    context: {
      title: task.title,
      description: task.description,
      name: employee.name,
      color: utils.convertToColor(task.status),
      startDate: utils.formatDateLong(new Date(task.startDate)),
      startHour: utils.formatTime(new Date(task.startDate)),
      endDate: utils.formatDateLong(new Date(task.endDate)),
      endHour: utils.formatTime(new Date(task.endDate)),
      status: utils.translateStatus(task.status),
      profileUrl:
        process.env.REACT_APP_URL + `/colaboradores/${employee.employeeId}`,
      tasksUrl: process.env.REACT_APP_URL + "/tarefas",
      message
    }
  };
  smtpTransport.sendMail(data);
}

function sendResetPasswordLink(res, employee, token, baseUrl) {
  var data = {
    to: employee.email,
    from: email,
    template: "forgot-password",
    subject: "Recuperar palavra-passe",
    context: {
      url: baseUrl + "/restaurar-palavra-passe?token=" + token,
      name: employee.name.split(" ")[0]
    }
  };
  sendEmail(res, data);
}

function sendResetPasswordConfirmation(res, employee) {
  var data = {
    to: employee.email,
    from: email,
    template: "reset-password",
    subject: "Palavra-passe recuperada",
    context: {
      name: employee.name.split(" ")[0]
    }
  };

  sendEmail(res, data);
}

function sendReinforcementRequest(res, employee, token, baseUrl, callback) {
  var data = {
    to: employee.email,
    from: email,
    template: "reinforcement-request",
    subject: "O abrigo virtual precisa de ti!",
    context: {
      url: baseUrl + "/reforços/pedido?token=" + token,
      name: employee.name.split(" ")[0]
    }
  };
  sendEmail(res, data, callback);
}

function sendChangeSchedule(res, employee) {
  var data = {
    to: employee.email,
    from: email,
    template: "change-schedule",
    subject: "O seu horário foi alterado.",
    context: {
      name: employee.name.split(" ")[0]
    }
  };
  sendEmail(res, data);
}

function sendCancelReinforcementRequest(res, employee, callback) {
  var data = {
    to: employee.email,
    from: email,
    template: "cancel-reinforcement-request",
    subject: "Cancelámos o pedido de reforço",
    context: {
      name: employee.name.split(" ")[0]
    }
  };
  sendEmail(res, data, callback);
}

function sendPartialSchedule(res, employee, startDate, endDate) {
  var data = {
    to: employee.email,
    from: email,
    template: "partial-schedule",
    subject: "Horário de reforço",
    context: {
      day: startDate.getDate(),
      month: utils.getMonthString(startDate.getMonth()),
      startHour: utils.dateHourString(startDate),
      endHour: utils.dateHourString(endDate)
    }
  };
  sendEmail(res, data);
}

module.exports.sendResetPasswordLink = sendResetPasswordLink;
module.exports.sendResetPasswordConfirmation = sendResetPasswordConfirmation;
module.exports.sendNewAccountEmail = sendNewAccountEmail;
module.exports.sendReinforcementRequest = sendReinforcementRequest;
module.exports.sendChangeSchedule = sendChangeSchedule;
module.exports.sendCancelReinforcementRequest = sendCancelReinforcementRequest;
module.exports.sendPartialSchedule = sendPartialSchedule;
module.exports.sendTaskNotification = sendTaskNotification;
