const bcrypt = require("bcrypt");
const saltRounds = 10;

function hashPassword(password) {
  var salt = bcrypt.genSaltSync(saltRounds);
  return bcrypt.hashSync(password, salt);
}

function comparePassword(password, hashPassword) {
  return bcrypt.compareSync(password, hashPassword);
}

module.exports.hashPassword = hashPassword;
module.exports.comparePassword = comparePassword;
