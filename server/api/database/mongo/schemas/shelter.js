const mongoose = require("mongoose");
const { Schema } = mongoose;
const { connect } = require("../db");
const { hashPassword } = require("../../../hashing");
const strings = require("../../../data/strings.json");

const shelterSchema = new Schema({
  name: String,
  address: String,
  licenseType: String,
  paymentDate: Date,
  expirationDate: Date
});

function model() {
  return mongoose.model("Shelter", shelterSchema);
}

function insert(name, address, licenseType, onSuccess, onFailure) {
  connect(() => {
    const Shelter = model();
    const paymentDate = new Date();
    const expirationDate = new Date().setDate(new Date().getDate() + 30);
    const newDocument = new Shelter({
      name,
      address,
      licenseType,
      paymentDate,
      expirationDate
    });

    newDocument.save((error, result) => {
      if (error && onFailure) {
        onFailure(error);
      }

      if (onSuccess) {
        onSuccess(result);
      }
    });
  });
}

function find(name, onSuccess, onFailure) {
  connect(() => {
    const Shelter = model();
    Shelter.find({ name: name }, (error, documents) => {
      if (error) {
        onFailure(error);
      }

      if (documents && documents.length > 0) {
        onSuccess(documents[0]);
      } else {
        onFailure("No results found");
      }
    });
  });
}

function findById(id, onSuccess, onFailure) {
  connect(() => {
    const Shelter = model();
    Shelter.find({ _id: id }, (error, documents) => {
      if (error) {
        onFailure(error);
      }

      if (documents && documents.length > 0) {
        onSuccess(documents[0]);
      } else {
        onFailure("No results found");
      }
    });
  });
}

function updateLicense(shelterId, license, onSuccess, onNotFound, onFailure) {
  connect(() => {
    const ShelterModel = model();

    ShelterModel.findOne({ _id: shelterId, licenseId: license.id }, function(
      err,
      foundObject
    ) {
      if (err) {
        onFailure(err);
      } else {
        if (!foundObject) {
          onNotFound(strings.shelterNotFounded);
        } else {
          foundObject.licenseType = license.type;
          foundObject.paymentDate = new Date();
          foundObject.expirationDate = new Date().setDate(
            new Date().getDate() + 30
          );

          foundObject.save(function(err, updatedObject) {
            if (err) {
              onFailure(err);
            } else {
              onSuccess(strings.licenseEditedSuccessfully);
            }
          });
        }
      }
    });
  });
}

function getShelterById(shelterId, onSuccess, onNotFound, onFailure) {
  connect(() => {
    const ShelterModel = model();

    ShelterModel.findOne({ _id: shelterId }, function(err, foundObject) {
      if (err) {
        onFailure(err);
      } else {
        if (!foundObject) {
          onNotFound(strings.shelterNotFounded);
        } else {
          onSuccess(foundObject);
        }
      }
    });
  });
}

module.exports.insert = insert;
module.exports.find = find;
module.exports.findById = findById;
module.exports.updateLicense = updateLicense;
module.exports.getShelterById = getShelterById;
