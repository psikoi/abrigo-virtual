const mongoose = require("mongoose");
const { Schema } = mongoose;
const { connect } = require("../db");

const userSchema = new Schema({
  email: String,
  shelterId: String
});

function model() {
  return mongoose.model("User", userSchema);
}

function insert(email, shelterId, onSuccess, onFailure) {
  connect(() => {
    const User = model();
    const newDocument = new User({ email, shelterId });

    newDocument.save((error, result) => {
      if (error && onFailure) {
        onFailure(error);
      }

      if (onSuccess) {
        onSuccess(result);
      }
    });
  });
}

function find(email, onSuccess, onFailure) {
  connect(() => {
    const User = model();
    User.find({ email: email }, (error, documents) => {
      if (error) {
        onFailure(error);
      }

      if (documents && documents.length > 0) {
        onSuccess(documents[0]);
      } else {
        onFailure("No results found");
      }
    });
  });
}

module.exports.insert = insert;
module.exports.find = find;
