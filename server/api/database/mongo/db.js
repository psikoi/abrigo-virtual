const mongoose = require("mongoose");
const server = `${process.env.DB_HOST}:${process.env.MONGO_PORT}`;

function connect(callback) {
  if (mongoose && mongoose.connection && mongoose.connection.readyState == 1) {
    callback(mongoose);
    return;
  }    
    
  const options = {
    useNewUrlParser: true
  };

  const onCallback = function() {
    callback ? callback(mongoose) : () => {};
  };

  mongoose.connect(
    `mongodb://${server}/${process.env.DB_DATABASE}`,
    options,
    onCallback
  );
}

module.exports.connect = connect;
