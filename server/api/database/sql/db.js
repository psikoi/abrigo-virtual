const fs = require("fs");
const path = require("path");
const mysql = require("mysql");
const baseDB = process.env.DB_DATABASE + "_";
const {
  getShelterId
} = require("../../modules/permissions/authTokenValidation");
const queries = require("./queries");
const insertUser = require("../mongo/schemas/user").insert;

const createDatabaseScripts = ["create.sql"];
const connectionMap = {};

/**
 * Returns an open connection, to a specific database
 * that is determined by the authentication token's
 * shelterId attribute.
 */
function getConnection(req) {
  const shelterId = typeof req === "string" ? req : getShelterId(req);

  if (connectionMap[shelterId]) {
    return connectionMap[shelterId];
  }

  const connection = mysql.createConnection({
    database: baseDB + shelterId,
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    multipleStatements: true
  });

  connection.connect(function(err) {
    if (err) {
      return console.error(`MYSQL connection error: ${err.message}`);
    }
  });

  connectionMap[shelterId] = connection;

  return connection;
}

/**
 * Returns a generic connection, meaning a
 * connection to the host, without a database
 */
function getGenericConnection() {
  const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    multipleStatements: true
  });

  connection.connect(function(err) {
    if (err) {
      return console.error(`MYSQL connection error: ${err.message}`);
    }
  });

  return connection;
}

function createSqlDatabase(id, callback) {
  resetSql(id, callback);
}

function resetSql(shelterId, callback) {
  var removed = 0;

  resetDatabase(shelterId, () => {
    // After it has been reset, run all
    // the scripts on that database
    const connection = getConnection(shelterId);
    createDatabaseScripts.forEach(script => {
      runScript(connection, script, shelterId, callback);
    });
  });
}

function resetDatabase(id, onReset) {
  const db = baseDB + id;
  const preScript = `DROP DATABASE IF EXISTS ${db}; CREATE DATABASE ${db}; USE ${db};`;
  const connection = getGenericConnection();
  connection.query(preScript, (err, results) => {
    if (err) {
      return;
    }

    onReset();
  });
}

function runScript(connection, script, id, callback) {
  try {
    const data = fs.readFileSync(
      path.join(__dirname, "../../../../database/scripts/" + script),
      "utf8"
    );
    connection.query(data, (err, results) => {
      if (err) {
        return;
      }

      callback();
    });
  } catch (err) {}
}

module.exports.getConnection = getConnection;
module.exports.getGenericConnection = getGenericConnection;
module.exports.baseDB = baseDB;
module.exports.createSqlDatabase = createSqlDatabase;
