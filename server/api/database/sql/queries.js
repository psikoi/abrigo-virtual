module.exports = {
  GET_ALL_EMPLOYEES:
    "SELECT e.*, a.* FROM employees e INNER JOIN accounts a on e.employeeId = a.employeeId WHERE a.removed = false",
  GET_ALL_ACCOUNTS: "SELECT * FROM accounts",
  GET_EMPLOYEE_BY_EMAIL:
    "SELECT e.*, a.* FROM employees e INNER JOIN accounts a on e.employeeId = a.employeeId WHERE a.removed = false AND a.email = ?",
  GET_EMPLOYEE_BY_ID:
    "SELECT e.*, a.* FROM employees e INNER JOIN accounts a on e.employeeId = a.employeeId WHERE a.removed = false  AND e.employeeId = ?",
  UPDATE_EMPLOYEE_PASSWORD_BY_EMAIL:
    "UPDATE accounts SET password = ? WHERE email = ?",
  CREATE_EMPLOYEE:
    "INSERT INTO employees(name, photoUrl, birthdate, contact, address, info, gender) VALUE(?, ?, ?, ?, ?, ?, ?);",
  CREATE_ACCOUNT:
    "INSERT INTO accounts(email, password, archived, removed, type, employeeId) VALUE(?, ?, FALSE, FALSE, ?, ?);",
  EDIT_EMPLOYEE:
    "UPDATE employees SET name = ?, birthdate = ?, contact = ?, address = ?, info = ?, gender = ? WHERE employeeId = ?;",
  EDIT_ACCOUNT: "UPDATE accounts SET email = ?, type = ? WHERE employeeId = ?;",
  DELETE_EMPLOYEE:
    "UPDATE employees SET name = '', photoUrl = '', contact = '', address = '', info = '', birthdate=NOW(), gender = 'other' WHERE employeeId = ?;",
  DELETE_ACCOUNT:
    "UPDATE accounts SET removed = true, email = ?, password = '', lastLogin = null, archived = true WHERE employeeId = ?;",
  GET_ACCOUNT_BY_EMAIL: "SELECT * FROM accounts WHERE email = ?",
  GET_ACCOUNT_BY_ID: "SELECT * FROM accounts WHERE accountId = ?",
  SET_ACCOUNT_LAST_LOGIN:
    "UPDATE accounts SET lastLogin = CURDATE() WHERE email = ?",
  BLOCK_ACCOUNT: "UPDATE accounts SET archived = true WHERE accountId = ?",
  GET_EMPLOYEE_REQUEST_RESET_PASSWORD:
    "SELECT e.*, a.* FROM employees e INNER JOIN accounts a on e.employeeId = a.employeeId WHERE a.resetPasswordToken = ? and a.resetPasswordExpires >= ?",
  UPDATE_RESET_PASSWORD_TOKEN:
    "UPDATE accounts SET resetPasswordToken = ?, resetPasswordExpires = ? where accountId = ?",
  UPDATE_EMPLOYEE_PASSWORD_BY_ACCOUNTID:
    "UPDATE accounts SET password = ? WHERE accountId = ?",
  UPDATE_EMPLOYEE_PHOTOURL_BY_ID:
    "UPDATE employees set photoUrl = ? where employeeId = ?",
  GET_USER_INFO_BY_EMAIL:
    "SELECT a.email, a.archived, a.removed, a.type, e.* FROM accounts a INNER JOIN employees e ON a.employeeId = e.employeeId WHERE email = ?",
  GET_ALL_REINFORCEMENTS:
    "SELECT r.*, e.*, a.* FROM reinforcement_requests r INNER JOIN employees e on r.employeeId = e.employeeId INNER JOIN accounts a on e.employeeId = a.employeeId",
  CREATE_REINFORCEMENT:
    "INSERT INTO reinforcement_requests(employeeId, status, startDate, endDate, token, tokenExpiration, byEmployeeId) VALUE(?, ?, ?, ?, ?, ?, ?)",
  ARCHIVE_ACCOUT: "UPDATE accounts SET archived = 1 where employeeId = ?",
  REACTIVATE_ACCOUT: "UPDATE accounts SET archived = 0 where employeeId = ?",
  GET_ALL_TASKS:
    "SELECT t.*,a.*,e.* FROM tasks t INNER JOIN accounts a on t.employeeId = a.employeeId INNER JOIN employees e on a.employeeId = e.employeeId",
  CREATE_SCHEDULE:
    "INSERT INTO schedule(date, hours, minutes, employeeId, type) VALUES ?;",
  GET_SCHEDULE_BY_EMPLOYEE:
    "SELECT s.date, s.hours, s.minutes FROM schedule s WHERE s.employeeId = ? and s.type = ?",
  GET_SCHEDULE_BY_EMPLOYEE_WITHOUT_TYPE:
    "SELECT s.date, s.hours, s.minutes FROM schedule s WHERE s.employeeId = ?",
  DELETE_SCHEDULE: "DELETE FROM `schedule` WHERE employeeId = ? and type = ?",
  GET_EMPLOYEE_TASKS:
    "SELECT t.*,a.*,e.* FROM tasks t INNER JOIN accounts a on t.employeeId = a.employeeId INNER JOIN employees e on a.employeeId = e.employeeId WHERE t.employeeId = ?",
  GET_REINFORCEMENT_BY_ID:
    "SELECT r.*, e.*, a.* FROM reinforcement_requests r INNER JOIN employees e on r.employeeId = e.employeeId INNER JOIN accounts a on e.employeeId = a.employeeId WHERE r.reinforcementRequestId = ?",
  CANCEL_REINFORCEMENT_REQUEST:
    "UPDATE `reinforcement_requests` SET status = 'canceled', token = null, tokenExpiration = null WHERE reinforcementRequestId = ?",
  FINISH_TASK:
    "UPDATE `tasks` SET finishDate = CURDATE(), status = ?, comment = ? WHERE taskId = ?",
  GET_TASK_BY_ID: "SELECT * FROM `tasks` WHERE taskId = ?",
  GET_REINFORCEMENT_BY_TOKEN:
    "SELECT r.*, e.*, a.* FROM reinforcement_requests r INNER JOIN employees e on r.employeeId = e.employeeId INNER JOIN accounts a on e.employeeId = a.employeeId WHERE r.token = ?",
  CREATE_REINFORCEMENT_SCHEDULE:
    "INSERT INTO schedule(date, hours, minutes, type, employeeId) VALUES ?;",
  ACCEPT_REINFORCEMENT_BY_TOKEN:
    "UPDATE reinforcement_requests SET replyDate = CURDATE(), status = 'accepted' WHERE token = ?;",
  DECLINE_REINFORCEMENT_BY_TOKEN:
    "UPDATE reinforcement_requests SET replyDate = CURDATE(), status = 'declined' WHERE token = ?;",
  GET_NOTIFICATIONS_BY_EMPLOYEE:
    "SELECT * FROM notifications WHERE employeeId = ?",
  READ_NOTIFICATION:
    "UPDATE notifications SET readDate = CURDATE() WHERE notificationId = ?;",
  CREATE_NOTIFICATION:
    "INSERT INTO notifications (creationDate, message, employeeId) VALUES(CURDATE(), ?, ?);",
  GET_NOTIFICATION_BY_ID:
    "SELECT * FROM notifications WHERE notificationId = ?",
  CREATE_TASK:
    "INSERT INTO tasks (employeeId, title, description, creationDate, startDate, endDate, status) value(?, ?, ?, CURDATE(), ?, ?, 'pending')",
  EDIT_TASK:
    "UPDATE tasks SET employeeId= ?, title = ?, description = ?, startDate = ?, endDate = ? where taskId = ?",
  GET_EMPLOYEE_TASKS_PENDING:
    "SELECT * from tasks where employeeId = ? and status='pending'",
  CREATE_ANIMAL:
    "INSERT INTO animals (name, photoUrl, birthdate, breed, size, info, gender, type, registerDate) VALUE(?, ?, ?, ?, ?, ?, ?, ?, ?);",
  UPDATE_ANIMAL_PHOTOURL_BY_ID:
    "UPDATE animals SET photoUrl = ? where animalId = ?",
  GET_ANIMAL_BY_ID: "SELECT * FROM animals WHERE animalId = ?",
  EDIT_ANIMAL:
    "UPDATE animals SET name = ?, photoUrl = ?, birthdate = ?, breed = ?, size = ?, info = ?, gender = ?, type = ? where animalId = ?",
  GET_QUARENTINES:
    "SELECT *, q.info as quarentineInfo, a.info as animalInfo, q.removed as quarentineRemoved, a.removed as animalRemoved from quarentines q INNER JOIN animals a ON q.animalId = a.animalId WHERE q.removed = 0",
  GET_ALL_ANIMALS: "SELECT * from animals where removed = false",
  GET_KENNELS_AND_QUARENTINES:
    "SELECT k.name, ka.animalId from kennels k inner join kennels_animals ka on k.kennelId = ka.kennelId;SELECT animalId FROM quarentines WHERE status = 'pending' and removed= false;",
  DELETE_ANIMAL:
    "UPDATE animals SET name = '', photoUrl = '', birthdate = NOW(), breed = '', size = 'other', info = '', gender = 'other', type = 'other', removed = true WHERE animalId = ?;",
  UPDATE_ANIMAL_QUARENTINES_REMOVED:
    "UPDATE quarentines SET removed = true where animalId = ?",
  CREATE_RELATIONSHIP:
    "INSERT INTO relationships (animalAId, animalBId, value) VALUES (?, ?, ?)",
  DELETE_RELATIONSHIP: "DELETE FROM relationships WHERE relationshipId = ?",
  GET_ANIMAL_A_RELATIONSHIPS:
    "SELECT a.name, a.photoUrl, r.value, r.relationshipId FROM relationships r INNER JOIN animals a ON a.animalId = r.animalBId WHERE r.animalAId = ?",
  GET_ANIMAL_B_RELATIONSHIPS:
    "SELECT a.name, a.photoUrl, r.value, r.relationshipId FROM relationships r INNER JOIN animals a ON a.animalId = r.animalAId WHERE r.animalBId = ?",
  EDIT_RELATIONSHIP:
    "UPDATE relationships SET animalAId = ?, animalBId = ?, value = ? WHERE relationshipId = ?",
  GET_ALL_ANIMALS_FOR_RELATIONSHIP:
    "SELECT a.animalId as id, a.name, a.photoUrl as photo from animals a where a.removed = false",
  GET_RELATIONSHIP_BOTH_ANIMALS:
    "SELECT * FROM relationships WHERE animalAId = ? AND animalBId = ? OR animalAId = ? AND animalBId = ?",
  GET_RELATIONSHIP_BY_ID:
    "SELECT * FROM relationships WHERE relationshipId = ?",
  CREATE_QUARENTINE:
    "INSERT INTO quarentines(startDate, status, animalId, info, cause) VALUE(CURDATE(), ?, ?, ?, ?);",
  GET_ANIMALS_KENNEL:
    "SELECT k.name, ka.animalId from kennels k inner join kennels_animals ka on k.kennelId = ka.kennelId;SELECT animalId FROM quarentines WHERE status = 'pending' and removed= false;",
  DELETE_ANIMAL:
    "UPDATE animals SET name = '', photoUrl = '', birthdate = NOW(), breed = '', size = 'other', info = '', gender = 'other', type = 'other', removed = true WHERE animalId = ?;",
  UPDATE_ANIMAL_QUARENTINES_REMOVED:
    "UPDATE quarentines SET removed = true where animalId = ?",
  GET_ANIMALS_NOT_IN_QUARENTINE:
    "SELECT * FROM animals WHERE animalId NOT IN (SELECT animalId from quarentines WHERE status = 'pending') AND removed = 0",
  GET_QUARENTINE_BY_ID: "SELECT * FROM quarentines WHERE quarentineId = ?",
  GET_ALL_ANIMALS_EDIT_QUARENTINE:
    "SELECT DISTINCT a.* from animals a LEFT JOIN quarentines q on q.animalId = a.animalId WHERE q.animalId IS NULL OR q.animalId = ?",
  EDIT_QUARENTINE:
    "UPDATE quarentines SET info = ?, cause = ?, animalId = ? WHERE quarentineId = ?",
  FINISH_QUARENTINE:
    "UPDATE `quarentines` SET endDate = CURDATE(), status = ? WHERE quarentineId = ?",
  GET_ALL_KENNELS:
    "SELECT k.*, a.animalId, a.photoUrl, a.name as animalName  FROM kennels k left join kennels_animals ka on k.kennelId = ka.kennelId left join animals a on a.animalId = ka.animalId",
  CREATE_KENNEL: "INSERT INTO kennels (capacity, name) values(?, ?)",
  EDIT_KENNEL: "UPDATE kennels set capacity = ?, name = ? where kennelId = ?",
  GET_KENNEL:
    "SELECT k.*,a.animalId, a.name as animalName,ka.kennelAnimalsId FROM kennels k left join kennels_animals ka on k.kennelId = ka.kennelId left join animals a on a.animalId = ka.animalId where k.kennelId = ?",
  DELETE_KENNEL: "DELETE FROM kennels where kennelId = ?",
  CREATE_KENNEL_ANIMALS:
    "INSERT INTO kennels_animals (kennelId, animalId) values ",
  DELETE_KENNEL_ANIMALS: "DELETE FROM kennels_animals where kennelId = ?",
  DELETE_KENNEL_ANIMALS_ALL:
    "DELETE FROM kennels_animals where kennelanimalsId in ",
  GET_KENNEL_ANIMALS:
    "SELECT * FROM kennels_animals WHERE animalId = ? and kennelId = ?",
  GET_AVAILABLE_ANIMALS:
    "SELECT * FROM animals where animalId not in (select animalId from kennels_animals);",
  GET_AVAILABLE_ANIMALS_TO_EDIT:
    "SELECT * FROM animals where animalId not in (select animalId from kennels_animals where kennelId != ?);"
};
