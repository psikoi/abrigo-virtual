module.exports = {
  GET_TASKS_PER_STATUS:
    "SELECT status, COUNT(*) AS count FROM tasks WHERE creationDate BETWEEN ? AND ? GROUP BY status",
  GET_COMPLETED_TASKS_BY_EMPLOYEE:
    "SELECT e.name, COUNT(*) AS count FROM employees e INNER JOIN tasks t ON e.employeeId = t.employeeId WHERE t.status = 'completed' AND t.creationDate BETWEEN ? AND ? GROUP BY e.employeeId",
  GET_PENDING_TASKS_BY_EMPLOYEE:
    "SELECT e.name, COUNT(*) AS count FROM employees e INNER JOIN tasks t ON e.employeeId = t.employeeId WHERE t.status = 'pending' AND t.creationDate BETWEEN ? AND ? GROUP BY e.employeeId",
  GET_ANIMALS_BY_SIZE:
    "SELECT size, COUNT(*) as count from ANIMALS GROUP BY size",
  GET_ANIMALS_BY_AGE:
    "SELECT YEAR(CURDATE()) - YEAR(birthdate) - IF(STR_TO_DATE(CONCAT(YEAR(CURDATE()), '-', MONTH(birthdate), '-', DAY(birthdate)) ,'%Y-%c-%e') > CURDATE(), 1, 0) AS age, COUNT(*) AS count FROM animals GROUP BY age;",
  GET_ANIMALS_BY_REGISTER_DATE:
    "SELECT CAST(registerDate AS DATE) AS register, COUNT(*) AS count FROM animals WHERE registerDate BETWEEN ? and ? GROUP BY register",
  GET_QUARENTINES_BY_ANIMAL_BY_DATE:
    "SELECT a.name, COUNT(*) as count FROM quarentines q INNER JOIN animals a ON q.animalId = a.animalId WHERE q.startDate BETWEEN ? AND ? GROUP BY q.animalId",
  GET_QUARENTINES_BY_REGISTER_DATE:
    "SELECT CAST(startDate AS DATE) AS register, COUNT(*) AS count FROM quarentines WHERE startDate BETWEEN ? and ? GROUP BY register",
  GET_QUARENTINES_BY_CAUSE:
    "SELECT cause, COUNT(*) as count FROM quarentines GROUP BY cause"
};
