const express = require("express");
const router = express.Router();

// Import new modules
const employeesAPI = require("./modules/employees.js");
const sessionsAPI = require("./modules/sessions.js");
const reinforcementsAPI = require("./modules/reinforcements.js");
const tasksAPI = require("./modules/tasks.js");
const scheduleAPI = require("./modules/schedule.js");
const notificationsAPI = require("./modules/notifications.js");
const animalsAPI = require("./modules/animals.js");
const quarentinesAPI = require("./modules/quarentines.js");
const relationshipsAPI = require("./modules/relationships.js");
const kennelsAPI = require("./modules/kennels");
const statisticsAPI = require("./modules/statistics.js");
const sheltersAPI = require("./modules/shelters");

// Add new modules to the router
router.use("/employees", employeesAPI);
router.use("/sessions", sessionsAPI);
router.use("/reinforcements", reinforcementsAPI);
router.use("/tasks", tasksAPI);
router.use("/schedule", scheduleAPI);
router.use("/notifications", notificationsAPI);
router.use("/animals", animalsAPI);
router.use("/quarentines", quarentinesAPI);
router.use("/relationships", relationshipsAPI);
router.use("/kennels", kennelsAPI);
router.use("/statistics", statisticsAPI);
router.use("/shelters", sheltersAPI);

router.get("/", function(req, res) {
  // Handle API index, maybe display a list of endpoints
  res.send("API index");
});
module.exports = router;
