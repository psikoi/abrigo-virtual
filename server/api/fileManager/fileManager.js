var imgur = require("imgur");
imgur.setClientId(process.env.CLIENTID);
imgur.setAPIUrl("https://api.imgur.com/3/");
imgur.setCredentials(
  process.env.EMAIL,
  process.env.IMGUR_PASSWORD,
  process.env.IMGUR_CLIENTID
);

function uploadBase64(id, image, callback, onSuccess, defaultMessage) {
  if (image && !image.match(urlRegex())) {
    image = image.substring(image.indexOf(",") + 1);
    imgur
      .uploadBase64(image)
      .then(function(json) {
        callback(id, { photoUrl: json.data.link }, onSuccess, defaultMessage);
      })
      .catch(function(err) {
        callback(id, { error: err }, onSuccess, defaultMessage);
      });
  } else callback(id, undefined, onSuccess, defaultMessage);
}

function urlRegex() {
  var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
  var regex = new RegExp(expression);
  return regex;
}
module.exports.uploadBase64 = uploadBase64;
