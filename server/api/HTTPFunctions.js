var strings = require("./data/strings.json");

exports.forbidden = function(message) {
  return buildError(message || strings.forbbidenRequest, 403);
};

exports.notFound = function(message) {
  return buildError(message || strings.notFound, 404);
};

exports.badRequest = function(message) {
  return buildError(message || strings.badRequest, 400);
};

exports.serverError = function() {
  return buildError(strings.internalServerError, 500);
};

function buildError(message, code) {
  let err = new Error(message);
  err.statusCode = code;
  return err;
}
