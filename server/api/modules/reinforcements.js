const express = require("express");
const router = express.Router();
const { getConnection } = require("../database/sql/db");
const queries = require("../database/sql/queries");
const utils = require("../utils");
const mysql = require("mysql");
const httpFunctions = require("../HTTPFunctions.js");
const emailServer = require("../emailServer/emailServer.js");
const crypto = require("crypto");
var strings = require("../data/strings.json");
const employeesAPI = require("./employees");
const { permissions } = require("./permissions/permissions");
const {
  checkTokenPermission,
  getDecodedUserByReq,
  ownAccountOnlyPermission
} = require("./permissions/authTokenValidation");
const {
  validateReinforcement
} = require("./validations/reinforcementsValidations");

var connection;

router.all("*", function(req, res, next) {
  connection = getConnection(req);
  next();
});

router.get("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.reinforcements.viewAll, req, res)) {
    return;
  }

  getReinforcementRequests(
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

router.get("/:id(\\d+)", function(req, res, next) {
  if (!checkTokenPermission(permissions.reinforcements.view, req, res)) {
    return;
  }

  getReinforcementRequest(
    req.params.id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

router.post("/", function(req, res, next) {
  createReinforcementRequest(
    req,
    res,
    req.body,
    function() {
      // On success
      res.status(200).send("OK");
    },
    function(err) {
      // On failure
      next(httpFunctions.serverError());
    },
    function(err) {
      // On bad request
      next(httpFunctions.badRequest(err));
    }
  );
});

router.delete("/", function(req, res, next) {
  const id = req.body.id;

  cancelReinforcementRequest(
    id,
    res,
    function() {
      // On success
      res.status(200).send("OK");
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

router.get("/:token", function(req, res, next) {
  getReinforcementRequestByToken(
    req.params.token,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(err) {
      // On failure
      next(err);
    },
    req,
    res
  );
});

router.post("/accept/:token", function(req, res, next) {
  const schedule = req.body;
  const decoded = getDecodedUserByReq(req);

  if (!schedule) {
    return next(httpFunctions.badRequest("No schedule sent"));
  }

  if (!decoded || !decoded.success) {
    return next(httpFunctions.badRequest("No user sent"));
  }

  acceptReinforcement(
    req,
    res,
    req.params.token,
    schedule,
    decoded.success,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(err) {
      // On failure
      next(err);
    }
  );
  res.status(200);
});

router.post("/decline/:token", function(req, res, next) {
  checkDeclineReinforcement(
    req,
    res,
    req.params.token,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(err) {
      // On failure
      next(err);
    }
  );
  res.status(200);
});

function cancelReinforcementRequest(id, res, onSuccess, onFailure) {
  getReinforcementRequest(
    id,
    function(reinforcement) {
      if (!reinforcement || reinforcement === null) {
        onFailure();
        return;
      }

      connection.beginTransaction(function(err) {
        if (err) {
          return onFailure(err);
        }

        var query = mysql.format(queries.CANCEL_REINFORCEMENT_REQUEST, [id]);

        connection.query(query, (err, result) => {
          if (err) {
            connection.rollback(function() {
              onFailure(err);
            });
          } else {
            emailServer.sendCancelReinforcementRequest(
              res,
              reinforcement.employee,
              onSuccess
            );
          }
        });
      });
    },
    function(err) {
      onFailure(err);
    }
  );
}

function createReinforcementRequest(
  req,
  res,
  reinforcement,
  onSuccess,
  onFailure,
  onBadRequest
) {
  const { employeeId } = reinforcement;

  employeesAPI.getEmployee(
    employeeId,
    employee => {
      // If employee exists
      if (employee) {
        var invalid = validateReinforcement(reinforcement);

        if (invalid) {
          onBadRequest(invalid);
        } else {
          const status = "pending";
          const token = crypto.randomBytes(20).toString("hex");
          const tokenExpires = new Date();
          tokenExpires.setDate(tokenExpires.getDate() + 1);

          const decoded = getDecodedUserByReq(req);

          if (!decoded || !decoded.success) {
            return onBadRequest("No user sent");
          }

          var query = mysql.format(queries.CREATE_REINFORCEMENT, [
            reinforcement.employeeId,
            status,
            new Date(reinforcement.startDate),
            new Date(reinforcement.endDate),
            token,
            tokenExpires,
            decoded.success.id
          ]);

          connection.query(query, (err, result) => {
            if (err) {
              onFailure(err);
            } else {
              var baseUrl = process.env.REACT_APP_URL;
              emailServer.sendReinforcementRequest(
                res,
                employee,
                token,
                baseUrl,
                onSuccess
              );
            }
          });
        }
      }
    },
    () => {
      next(httpFunctions.badRequest(strings.invalidEmployeeId));
    },
    null,
    connection
  );
}

function getReinforcementRequests(onSuccess, onFailure) {
  var query = mysql.format(queries.GET_ALL_REINFORCEMENTS);

  var prepareResults = function(result) {
    var preparedResults = [];

    result.forEach(function(e) {
      preparedResults.push(prepareReinforcement(e));
    });

    return preparedResults;
  };

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    }
    onSuccess(prepareResults(result));
  });
}

function getReinforcementRequest(id, onSuccess, onFailure) {
  var query = mysql.format(queries.GET_REINFORCEMENT_BY_ID, [id]);

  var prepareResults = function(result) {
    var preparedResults = [];

    result.forEach(function(e) {
      preparedResults.push(prepareReinforcement(e));
    });

    return preparedResults;
  };

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    }

    if (result.length == 0) {
      onSuccess([]);
    } else {
      onSuccess(prepareResults(result)[0]);
    }
  });
}

function prepareReinforcement(e) {
  var employee = {
    id: e.employeeId,
    email: e.email,
    password: e.password,
    lastLogin: utils.convertDate(e.lastLogin),
    archived: e.archived,
    removed: e.removed,
    type: e.type,
    resetPasswordToken: e.resetPasswordToken,
    resetPasswordExpires: utils.convertDate(e.resetPasswordExpires),
    name: e.name,
    photoUrl: e.photoUrl,
    birthdate: utils.convertDate(e.birthdate),
    contact: e.contact,
    address: e.address,
    info: e.info,
    gender: e.gender
  };

  var reinforcement = {
    id: e.reinforcementRequestId,
    employee,
    status: e.status,
    startDate: utils.convertDate(e.startDate),
    endDate: utils.convertDate(e.endDate),
    replyDate: utils.convertDate(e.replyDate),
    token: e.token,
    tokenExpiration: utils.convertDate(e.tokenExpiration)
  };

  return reinforcement;
}

function isReinforcementRequestValid(req, res, reinforcement, onFailure) {
  if (!ownAccountOnlyPermission(reinforcement.employeeId, req, res)) {
    return false;
  }

  if (reinforcement.tokenExpiration < new Date()) {
    onFailure(httpFunctions.badRequest(strings.reinforcementNotActive));
    return false;
  }

  if (reinforcement.status !== "pending") {
    onFailure(httpFunctions.badRequest(strings.reinforcementNotActive));
    return false;
  }

  return true;
}

function getReinforcementRequestByToken(token, onSuccess, onFailure, req, res) {
  var query = mysql.format(queries.GET_REINFORCEMENT_BY_TOKEN, [token]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.badRequest(err));
    }

    if (result.length == 0)
      return onFailure(httpFunctions.badRequest(strings.reinforcementNotFound));

    const reinforcement = result[0];

    if (!isReinforcementRequestValid(req, res, reinforcement, onFailure)) {
      return;
    }

    onSuccess(prepareReinforcement(reinforcement));
  });
}

function acceptReinforcement(
  req,
  res,
  token,
  schedule,
  user,
  onSuccess,
  onFailure
) {
  connection.beginTransaction(function(err) {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    return acceptReinforcementAddSchedule(
      req,
      res,
      token,
      schedule,
      user,
      onSuccess,
      onFailure
    );
  });
}

function acceptReinforcementAddSchedule(
  req,
  res,
  token,
  schedule,
  user,
  onSuccess,
  onFailure
) {
  var query = mysql.format(queries.GET_REINFORCEMENT_BY_TOKEN, [token]);
  connection.query(query, (err, result) => {
    if (err) {
      return dbRollbackCallback(onFailure, httpFunctions.serverError());
    }

    if (result.length == 0)
      return onFailure(httpFunctions.badRequest(strings.reinforcementNotFound));

    const reinforcement = result[0];

    if (!isReinforcementRequestValid(req, res, reinforcement, onFailure)) {
      dbRollback();
      return;
    }

    return updateReinforcement(
      token,
      schedule,
      user,
      onSuccess,
      onFailure,
      res,
      reinforcement
    );
  });
}

function updateReinforcement(
  token,
  schedule,
  user,
  onSuccess,
  onFailure,
  res,
  reinforcement
) {
  var query = mysql.format(queries.ACCEPT_REINFORCEMENT_BY_TOKEN, [token]);
  connection.query(query, (err, result) => {
    if (err) {
      return dbRollbackCallback(onFailure, httpFunctions.serverError());
    }
    return addSchedule(
      schedule,
      user,
      token,
      onSuccess,
      onFailure,
      res,
      reinforcement
    );
  });
}

function addSchedule(
  schedule,
  user,
  token,
  onSuccess,
  onFailure,
  res,
  reinforcement
) {
  var sqlArray = [];
  schedule.forEach(element => {
    sqlArray.push([
      element.day,
      element.time.hours,
      element.time.minutes,
      "partial",
      user.id
    ]);
  });
  var query = queries.CREATE_REINFORCEMENT_SCHEDULE;
  connection.query(query, [sqlArray], (err, result) => {
    if (err) {
      return dbRollbackCallback(onFailure, httpFunctions.serverError());
    }
    emailServer.sendPartialSchedule(
      res,
      user,
      reinforcement.startDate,
      reinforcement.endDate
    );

    return createAcceptanceNotification(
      strings.volunteerAcceptedRequest,
      token,
      onFailure,
      onSuccess,
      commitTransaction
    );
  });
}

function dbRollback() {
  connection.rollback();
}

function dbRollbackCallback(callback, param) {
  connection.rollback(function() {
    return callback(param);
  });
}

function commitTransaction(result, onSuccess, onFailure) {
  connection.commit(function(err) {
    if (err) {
      return dbRollbackCallback(onFailure, httpFunctions.serverError());
    } else {
      return onSuccess(result);
    }
  });
}

function checkDeclineReinforcement(req, res, token, onSuccess, onFailure) {
  var query = mysql.format(queries.GET_REINFORCEMENT_BY_TOKEN, [token]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    if (result.length == 0)
      return onFailure(httpFunctions.badRequest(strings.reinforcementNotFound));

    const reinforcement = result[0];

    if (!isReinforcementRequestValid(req, res, reinforcement, onFailure)) {
      return;
    }

    return declineReinforcement(req, token, onSuccess, onFailure);
  });
}

function declineReinforcement(req, token, onSuccess, onFailure) {
  const decoded = getDecodedUserByReq(req);

  if (!decoded || !decoded.success) {
    return onFailure(httpFunctions.badRequest("No user sent"));
  }

  var query = mysql.format(queries.DECLINE_REINFORCEMENT_BY_TOKEN, [token]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    if (result.affectedRows === 0) {
      return onFailure(
        httpFunctions.badRequest(strings.reinforcementNotDeclined)
      );
    }

    return createAcceptanceNotification(
      strings.volunteerDeclinedRequest,
      token,
      onFailure,
      onSuccess
    );
  });
}

function createAcceptanceNotification(
  message,
  token,
  onFailure,
  onSuccess,
  commit
) {
  var query = mysql.format(queries.GET_REINFORCEMENT_BY_TOKEN, [token]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    if (!result || result.length === 0) {
      return onFailure(httpFunctions.badRequest(strings.reinforcementNotFound));
    }

    var sentMessage = result[0].name + message;

    return addNotification(
      sentMessage,
      result[0].byEmployeeId,
      onFailure,
      onSuccess,
      commit
    );
  });
}

function addNotification(message, employeeId, onFailure, onSuccess, commit) {
  var query = mysql.format(queries.CREATE_NOTIFICATION, [message, employeeId]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    if (commit) {
      return commit(result, function(callback) {}, onFailure);
    }
    return onSuccess(result);
  });
}

module.exports = router;
