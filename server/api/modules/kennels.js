const express = require("express");
const router = express.Router();
const queries = require("../database/sql/queries");
const httpFunctions = require("../HTTPFunctions.js");
const { getConnection } = require("../database/sql/db");
const mysql = require("mysql");
var strings = require("../data/strings.json");
const { checkTokenPermission } = require("./permissions/authTokenValidation");
const {
  validateKennel,
  validateKennelId
} = require("./validations/kennelsValidations");
const { permissions } = require("./permissions/permissions");

var connection;

router.all("*", function(req, res, next) {
  connection = getConnection(req);
  next();
});

router.get("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.kennels.viewAll, req, res)) {
    return;
  }

  getKennels(
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

function getKennels(onSuccess, onFailure) {
  var query = mysql.format(queries.GET_ALL_KENNELS);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    }

    var kennels = formatKennelsAnimals(result);
    onSuccess(kennels);
  });
}

function formatKennelsAnimals(result) {
  var array = [];
  result.forEach(function(k) {
    if (!hasKennelId(array, k.kennelId))
      array.push({
        kennelId: k.kennelId,
        capacity: k.capacity,
        name: k.name,
        animals: []
      });
  });

  result.forEach(function(k) {
    if (k.animalId != null) {
      var animals = getKennelAnimals(array, k.kennelId);
      if (!hasKennelId(animals, k.animalId))
        animals.push({
          animalId: k.animalId,
          photoUrl: k.photoUrl,
          name: k.animalName,
          kennelAnimalsId: k.kennelAnimalsId
        });
    }
  });
  return array;
}

function getKennelAnimals(array, id) {
  var obj = null;
  array.forEach(function(ka) {
    if (ka.kennelId == id) obj = ka;
  });
  return obj.animals;
}

function hasKennelId(array, id) {
  var exists = false;
  array.forEach(function(k) {
    if (k.kennelId == id) exists = true;
  });
  return exists;
}

router.get("/:id(\\d+)", function(req, res, next) {
  var id = req.params.id;

  if (!checkTokenPermission(permissions.kennels.view, req, res)) {
    return;
  }

  getKennel(
    id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

function getKennel(id, onSuccess, onFailure, animals = null) {
  var query = mysql.format(queries.GET_KENNEL, [id]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    } else if (result.length == 0) {
      onFailure(httpFunctions.notFound());
    } else {
      var kennels = formatKennelsAnimals(result);
      onSuccess(kennels[0], animals);
    }
  });
}

router.post("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.kennels.create, req, res)) {
    return;
  }

  var newKennel = req.body;

  createKennel(
    newKennel,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

function createKennel(newKennel, onSuccess, onFailure, onBadRequest) {
  connection.beginTransaction(function(err) {
    if (err) {
      return onFailure();
    }

    var invalid = validateKennel(newKennel);
    if (invalid) {
      onBadRequest(invalid);
    } else {
      var query = mysql.format(queries.CREATE_KENNEL, [
        newKennel.capacity,
        newKennel.name
      ]);

      connection.query(query, (err, result) => {
        if (err) {
          connection.rollback(function() {
            onFailure(err);
          });
        } else
          createAnimalsKennel(
            result.insertId,
            newKennel,
            onSuccess,
            onFailure,
            onBadRequest
          );
      });
    }
  });
}

function createAnimalsKennel(
  kennelId,
  newKennel,
  onSuccess,
  onFailure,
  onBadRequest
) {
  var animals = newKennel.animals;
  var str = "";

  animals.forEach(function(animalId) {
    if (!Number.isInteger(animalId)) onBadRequest(strings.invalidAnimalId);
    else {
      str += `(${kennelId},${animalId}),`;
    }
  });
  str = str.substring(0, str.length - 1);
  createAnimalKennel(str, onFailure, onSuccess);
}

function createAnimalKennel(str, onFailure, onSuccess, isEdit = false) {
  var query = queries.CREATE_KENNEL_ANIMALS + " " + str;
  if (str.length == 0) commitNewKennelTrasaction(onSuccess, onFailure, isEdit);
  else {
    connection.query(query, (err, result) => {
      if (err) {
        rollback(err, onFailure);
      }
      commitNewKennelTrasaction(onSuccess, onFailure, isEdit);
    });
  }
}

function rollback(err, onFailure) {
  connection.rollback(function() {
    onFailure(err);
  });
}

function commitNewKennelTrasaction(onSuccess, onFailure, isEdit) {
  connection.commit(function(err) {
    if (err) {
      rollback(err, onFailure);
    } else {
      onSuccess(
        isEdit ? strings.kennelEditedSucefully : strings.kennelCreatedSucefully
      );
    }
  });
}

router.put("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.kennels.edit, req, res)) {
    return;
  }

  var editInfo = req.body;

  editKennel(
    editInfo,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

function editKennel(editInfo, onSuccess, onFailure, onBadRequest) {
  connection.beginTransaction(function(err) {
    if (err) {
      onFailure();
    }

    var invalid = validateKennel(editInfo);
    if (invalid) {
      onBadRequest(invalid);
    } else {
      var query = mysql.format(queries.EDIT_KENNEL, [
        Number(editInfo.capacity),
        editInfo.name,
        editInfo.kennelId
      ]);

      connection.query(query, (err, result) => {
        if (err) {
          connection.rollback(function() {
            onFailure(err);
          });
        } else editKennelAnimals(editInfo, onSuccess, onFailure, onBadRequest);
      });
    }
  });
}

function hasAnimalsId(id, animals) {
  for (var i = 0; i < animals.length; i++) {
    if (animals[i].animalId == id) {
      return true;
    }
  }
  return false;
}

function isToDelete(id, animals) {
  var isToDelete = true;
  for (var i = 0; i < animals.length; i++) {
    if (animals[i] == id) {
      isToDelete = false;
    }
  }
  return isToDelete;
}

function editKennelAnimals(editInfo, onSuccess, onFailure, onBadRequest) {
  var animals = editInfo.animals;
  var kennelId = editInfo.kennelId;
  getKennel(
    kennelId,
    function(kennelAnimals, animals) {
      var queries = { create: "", delete: "(" };

      kennelAnimals.animals.forEach(function(e) {
        var id = e.animalId;
        var kennelAnimalsId = e.kennelAnimalsId;
        if (id != null) {
          if (animals.length == 0) {
            queries.delete += kennelAnimalsId + ",";
          } else {
            for (var i = 0; i < animals.length; i++) {
              if (isToDelete(id, animals)) {
                queries.delete += kennelAnimalsId + ",";
              }
            }
          }
        }
      });

      animals.forEach(function(id) {
        if (!hasAnimalsId(id, kennelAnimals.animals)) {
          queries.create += `(${kennelId},${id}),`;
        }
      });

      queries.create = queries.create.substring(0, queries.create.length - 1);
      queries.delete =
        queries.delete.length == 1
          ? ""
          : queries.delete.substring(0, queries.delete.length - 1) + ")";

      deleteSetOfKennelAnimals(queries, onSuccess, onFailure);
    },
    onFailure,
    animals
  );
}

function deleteSetOfKennelAnimals(editObject, onSuccess, onFailure) {
  var query = queries.DELETE_KENNEL_ANIMALS_ALL + editObject.delete;
  if (editObject.delete.length == 0)
    createAnimalKennel(editObject.create, onFailure, onSuccess, true);
  else
    connection.query(query, (err, result) => {
      if (err) {
        connection.rollback(function() {
          onFailure(err);
        });
      } else createAnimalKennel(editObject.create, onFailure, onSuccess, true);
    });
}

router.delete("/:id(\\d+)", function(req, res, next) {
  if (!checkTokenPermission(permissions.kennels.delete, req, res)) {
    return;
  }

  var id = req.params.id;

  deleteKennelAnimals(
    id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

function deleteKennelAnimals(id, onSuccess, onFailure, onBadRequest) {
  connection.beginTransaction(function(err) {
    if (err) {
      return onFailure();
    }

    var invalid = validateKennelId(id);
    if (invalid) {
      onBadRequest(invalid);
    } else {
      var query = mysql.format(queries.DELETE_KENNEL_ANIMALS, [id]);

      connection.query(query, (err, result) => {
        if (err) {
          connection.rollback(function() {
            onFailure(err);
          });
        } else {
          deleteKennel(id, onSuccess, onFailure);
        }
      });
    }
  });
}

function deleteKennel(id, onSuccess, onFailure) {
  var query = mysql.format(queries.DELETE_KENNEL, [id]);

  connection.query(query, (err, result) => {
    if (err) {
      connection.rollback(function() {
        onFailure(err);
      });
    } else commitDeleteTrasaction(onSuccess, onFailure);
  });
}

function commitDeleteTrasaction(onSuccess, onFailure) {
  connection.commit(function(err) {
    if (err) {
      connection.rollback(function() {
        onFailure(err);
      });
    } else {
      onSuccess(strings.kennelDeletedSuccessfully);
    }
  });
}

router.get("/available-animals/:id(\\d+)", function(req, res, next) {
  if (!checkTokenPermission(permissions.kennels.viewAll, req, res)) {
    return;
  }

  getAvailabelAnimals(
    req.params.id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

router.get("/available-animals/", function(req, res, next) {
  if (!checkTokenPermission(permissions.kennels.viewAll, req, res)) {
    return;
  }

  getAvailabelAnimals(
    null,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

function getAvailabelAnimals(id, onSuccess, onFailure) {
  var query = id
    ? mysql.format(queries.GET_AVAILABLE_ANIMALS_TO_EDIT, [id])
    : mysql.format(queries.GET_AVAILABLE_ANIMALS);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    }

    onSuccess(result);
  });
}

router.put("/redistribute", function(req, res, next) {
  if (!checkTokenPermission(permissions.kennels.edit, req, res)) {
    return;
  }

  var redistributeInfo = req.body;

  redistribute(
    redistributeInfo,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

function redistribute(redistributeInfo, onSuccess, onFailure) {
  connection.beginTransaction(function(err) {
    if (err) {
      onFailure();
    }

    redistributeInfo.forEach(function(element, i) {
      var query = mysql.format(queries.REDISTRIBUTE, [
        element.to.kennelId,
        element.from.kennelId,
        element.animal.animalId
      ]);

      connection.query(query, (err, result) => {
        if (err) {
          connection.rollback(function() {
            onFailure(err);
          });
        }
        if (redistributeInfo.length - 1 === i) {
          connection.commit(function(err) {
            if (err) {
              rollback(err, onFailure);
            } else {
              onSuccess(strings.RedistributionSuccessfully);
            }
          });
        }
      });
    });
  });
}

module.exports = router;
