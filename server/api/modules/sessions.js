const express = require("express");
const { getConnection } = require("../database/sql/db");
const queries = require("../database/sql/queries");
const httpFunctions = require("../HTTPFunctions.js");
const mysql = require("mysql");
const hash = require("../hashing.js");
const jwt = require("jsonwebtoken");
const crypto = require("crypto");
const { validatePassword } = require("./validations/accountValidations");
const emailServer = require("../emailServer/emailServer.js");
const strings = require("../data/strings.json");
const { find } = require("../database/mongo/schemas/user");

const { permissions } = require("./permissions/permissions");
const {
  checkTokenPermission,
  getDecodedUserByReq,
  isAuthenticated,
  getCurrentUserEmail
} = require("./permissions/authTokenValidation");

var router = express.Router();
var connection;

router.all("*", function(req, res, next) {
  if (isAuthenticated(req)) {
    connection = getConnection(req);
  }
  next();
});

router.post("/login", function(req, res, next) {
  var email = req.body.email;
  var shelterId;

  var authenticate = function() {
    login(
      req,
      result => {
        // On success

        // Cookie token expires in roughly 10 years
        var yearInMiliseconds = 31536000000;

        var expiryDate = new Date(Number(new Date()) + yearInMiliseconds * 10);
        res
          .cookie("token", result, { expires: expiryDate, httpOnly: true })
          .sendStatus(200);
      },
      error => {
        // On failure
        next(error);
      },
      shelterId
    );
  };

  find(
    email,
    doc => {
      // Email is registered in the central database,
      // use the shelterId associated with that email
      // to get the specific SQL database connection
      shelterId = doc.shelterId;
      connection = getConnection(shelterId);
      authenticate();
    },
    err => {
      next(httpFunctions.badRequest(strings.accountNotFound));
    }
  );
});

router.get("/permissions-matrix", function(req, res, next) {
  res.send(JSON.stringify(permissions));
});

router.get("/logout", function(req, res, next) {
  logout(
    req,
    function(result) {
      // On success
      res.cookie("token", "", { httpOnly: true }).sendStatus(200);
    },
    function(error) {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

router.get("/current-user", function(req, res, next) {
  var email = getCurrentUserEmail(req, res);

  if (!email) {
    return;
  }

  var query = mysql.format(queries.GET_USER_INFO_BY_EMAIL, [email]);

  connection.query(query, (err, result) => {
    if (err) {
      return next(httpFunctions.serverError());
    }

    return res.status(200).send(JSON.stringify(result[0]));
  });
});

router.post("/changePassword", (req, res, next) => {
  if (!isAuthenticated(req)) {
    next(httpFunctions.badRequest("É necessário iniciar sessão"));
    return;
  }

  changePassword(req, res, next);
});

router.post("/forgotPassword", function(req, res, next) {
  forgotPassword(req, res, next);
});

router.post("/resetPassword", function(req, res, next) {
  resetPassword(req, res, next);
});

router.post("/block", function(req, res, next) {
  if (!checkTokenPermission(permissions.employee.block, req, res)) {
    return;
  }

  // safer to include id in req body
  // other options includes redirecting Admin to confirmation page
  var blockInfo = req.body;

  // Account has to be deleted first due to fk constraint
  blockAccount(
    blockInfo,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

function login(req, onSuccess, onFailure, shelterId) {
  var email = req.body.email;
  var password = req.body.password;

  var validationResult = validateLoginRequest(req, email, password);
  if (validationResult.Error) {
    return onFailure(httpFunctions.badRequest(validationResult.Error));
  }

  var query = mysql.format(queries.GET_ACCOUNT_BY_EMAIL, [email, password]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }
    return loginAccount(
      req,
      result,
      email,
      password,
      onSuccess,
      onFailure,
      shelterId
    );
  });
}

function validateLoginRequest(req, email, password) {
  if (isAuthenticated(req)) {
    return { Error: strings.alreadyLoggedIn };
  }

  if (!email || !password) {
    return { Error: strings.emailAndPasswordRequired };
  }

  return { Success: strings.validRequest };
}

function loginAccount(
  req,
  result,
  email,
  password,
  onSuccess,
  onFailure,
  shelterId
) {
  if (result == undefined) {
    return onFailure(httpFunctions.serverError());
  }

  var user = result[0];

  if (!user) {
    return onFailure(
      httpFunctions.badRequest(strings.incorrectEmailOrPassword)
    );
  }

  if (user.archived || user.removed) {
    return onFailure(httpFunctions.forbidden(strings.noAccessToPlatform));
  }

  if (
    !(user.email.toLowerCase() === email.toLowerCase()) ||
    !hash.comparePassword(password, user.password)
  ) {
    return onFailure(
      httpFunctions.badRequest(strings.incorrectEmailOrPassword)
    );
  }

  // Register last login
  var query = mysql.format(queries.SET_ACCOUNT_LAST_LOGIN, [user.email]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }
    return setSession(req, user, onSuccess, shelterId);
  });
}

function setSession(req, user, onSuccess, shelterId) {
  const payload = {
    email: user.email,
    type: user.type,
    id: user.employeeId,
    shelterId
  };
  const token = jwt.sign(payload, process.env.JWT_TOKEN_SECRET);
  return onSuccess(token);
}

function logout(req, onSuccess, onFailure) {
  // logout user
  return onSuccess(strings.loggedOut);
}

function changePassword(req, res, next) {
  var userResult = getDecodedUserByReq(req);
  if (userResult.error) {
    next(httpFunctions.badRequest(userResult.error));
    return;
  }

  var user = userResult.success;
  var currentPassword = req.body.currentPassword;
  var newPassword = req.body.password;

  if (!currentPassword) {
    next(httpFunctions.badRequest(strings.invalidCurrentPassword));
    return;
  }

  if (!newPassword) {
    next(httpFunctions.badRequest(strings.invalidNewPassword));
    return;
  }

  var query = mysql.format(queries.GET_EMPLOYEE_BY_EMAIL, [user.email]);

  connection.query(query, (err, result) => {
    if (err) {
      next(httpFunctions.serverError());
    }
    // if user exists and password match
    if (
      result.length > 0 &&
      hash.comparePassword(req.body.currentPassword, result[0].password)
    ) {
      updatePassword(user, req, res, next);
    } else {
      // if employee account was not found
      next(httpFunctions.notFound(strings.dataNotMatches));
    }
  });
}

function resetPassword(req, res, next) {
  var query = mysql.format(queries.GET_EMPLOYEE_REQUEST_RESET_PASSWORD, [
    req.body.token,
    new Date()
  ]);

  connection.query(query, (err, result) => {
    if (err) {
      next(httpFunctions.serverError());
    } else if (result.length == 0) {
      next(httpFunctions.badRequest(strings.invalidToken));
    } else {
      // if exists an employee with that reset password token
      updatePasswordAndSendEmail(req, res, next, result[0]);
    }
  });
}

function forgotPassword(req, res, next) {
  var query = mysql.format(queries.GET_EMPLOYEE_BY_EMAIL, [req.body.email]);

  connection.query(query, (err, result) => {
    if (err) {
      next(httpFunctions.serverError());
    } else if (result.length == 0) {
      next(httpFunctions.notFound(strings.accountNotFound));
    } else {
      createToken(req, res, next, result[0]);
    }
  });
}

function updatePassword(currUser, req, res, next) {
  var password = req.body.password;
  var query = mysql.format(queries.UPDATE_EMPLOYEE_PASSWORD_BY_EMAIL, [
    hash.hashPassword(password),
    currUser.email
  ]);

  var invalid = validatePassword(password);

  if (invalid) {
    next(httpFunctions.badRequest(invalid));
  } else {
    connection.query(query, (err, result) => {
      if (err) {
        next(httpFunctions.serverError());
      } else if (result.affectedRows > 0) {
        // If a password was changed in database
        res.status(200).json({ message: "OK" });
      } else {
        // if employee account was not found
        next(httpFunctions.notFound(strings.accountMayNotExists));
      }
    });
  }
}

function blockAccount(blockInfo, onSuccess, onFailure) {
  var validationResult = validateBlockRequest(blockInfo);
  if (validationResult.Error) {
    return onFailure(httpFunctions.badRequest(validationResult.Error));
  }

  var query = mysql.format(queries.BLOCK_ACCOUNT, [blockInfo.id]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }
    if (result.affectedRows == 0) {
      return onFailure(
        httpFunctions.badRequest("A conta com esse id não existe.")
      );
    }
    onSuccess(result);
  });
}

function validateBlockRequest(blockInfo) {
  if (!blockInfo.id) {
    return { Error: strings.idRequired };
  }

  if (isNaN(blockInfo.id)) {
    return { Error: strings.idShouldBeNumber };
  }

  return { Success: strings.validRequest };
}

function createToken(req, res, next, employee) {
  // create the random token
  var token = crypto.randomBytes(20).toString("hex");
  var baseUrl = process.env.REACT_APP_URL;
  var expireDate = new Date();
  expireDate.setDate(expireDate.getDate() + 1);

  // update on database the column reset_password_token
  updateResetPasswordToken(employee, token, expireDate, next);

  // send the link with the token generated
  emailServer.sendResetPasswordLink(res, employee, token, baseUrl);
}

function updateResetPasswordToken(account, token, expireDate, next) {
  var query = mysql.format(queries.UPDATE_RESET_PASSWORD_TOKEN, [
    token,
    expireDate,
    account.accountId
  ]);

  connection.query(query, (err, result) => {
    if (err) {
      next(httpFunctions.serverError());
    } else if (result.affectedRows == 0) {
      next(httpFunctions.notFound(strings.accountNotFound));
    }
  });
}

function updatePasswordAndSendEmail(req, res, next, employee) {
  var password = req.body.password;
  var invalid = validatePassword(password);

  if (invalid) {
    httpFunctions.badRequest(invalid);
  } else {
    var query = mysql.format(queries.UPDATE_EMPLOYEE_PASSWORD_BY_ACCOUNTID, [
      hash.hashPassword(req.body.password),
      employee.accountId
    ]);

    connection.query(query, (err, result) => {
      if (err) {
        next(httpFunctions.serverError());
      } else if (result.affectedRows > 0) {
        // reset password columns to empty
        updateResetPasswordToken(employee, null, null, next);

        // send email confirmation
        emailServer.sendResetPasswordConfirmation(res, employee);
      } else {
        next(httpFunctions.notFound(strings.noAccountAssociatedToToken));
      }
    });
  }
}

module.exports = router;
