const express = require("express");
const queries = require("../database/sql/queries");
const httpFunctions = require("../HTTPFunctions.js");
const { getConnection } = require("../database/sql/db");
const mysql = require("mysql");
const { permissions } = require("./permissions/permissions");
const { checkTokenPermission } = require("./permissions/authTokenValidation");
const {
  validateRelationship
} = require("./validations/relationshipsValidations");
var strings = require("../data/strings.json");
const Algorithm = require("./genetic/Algorithm");
var router = express.Router();

var connection;

router.all("*", function(req, res, next) {
  connection = getConnection(req);
  next();
});

router.get("/", function(req, res, next) {
  getRelationships(
    result => {
      // On success
      res.status(200).json({ data: result });
    },
    error => {
      // On failure
      next(error);
    }
  );
});

router.get("/optimize/:duration", function(req, res, next) {
  const duration = req.params.duration;

  if (!duration || duration < 0) {
    next();
    return;
  }

  getOptimalLayout(duration, result => {
    res.status(200).json({ data: result });
  });
});

router.post("/", function(req, res, next) {
  createRelationship(
    req.body,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    },
    req,
    res
  );
});

router.get("/create", function(req, res, next) {
  if (!checkTokenPermission(permissions.animal.create, req, res)) {
    return;
  }

  getAnimalForRelationshipCreation(
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

router.get("/animal/:id", function(req, res, next) {
  var id = req.params.id;

  getAnimalRelationships(
    id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    },
    req,
    res
  );
});

router.get("/:id", function(req, res, next) {
  var id = req.params.id;

  getRelationship(
    id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    },
    req,
    res
  );
});

router.delete("/:id", function(req, res, next) {
  var id = req.params.id;

  deleteRelationship(
    id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    },
    req,
    res
  );
});

router.put("/:id", function(req, res, next) {
  var id = req.params.id;

  editRelationship(
    id,
    req.body,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    },
    req,
    res
  );
});

function getRelationships(onSuccess, onFailure) {
  var query = mysql.format(queries.GET_RELATIONSHIPS);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    }
    onSuccess(result);
  });
}

function createRelationship(relationship, onSuccess, onFailure, req, res) {
  if (!checkTokenPermission(permissions.relationships.create, req, res)) {
    return;
  }

  var result = validateRelationship(relationship);
  if (result) {
    return onFailure(httpFunctions.badRequest(result));
  }

  return validateExistingAnimals(
    relationship,
    onSuccess,
    onFailure,
    function() {
      addRelationship(relationship, onSuccess, onFailure);
    }
  );
}

function validateExistingAnimals(relationship, onSuccess, onFailure, callback) {
  var query = mysql.format(queries.GET_ANIMAL_BY_ID, [relationship.animalAId]);

  connection.query(query, (err, result) => {
    var invalid = validateGetAnimalResult(err, result);
    if (invalid) {
      return onFailure(invalid);
    }

    var animalBQuery = mysql.format(queries.GET_ANIMAL_BY_ID, [
      relationship.animalBId
    ]);

    connection.query(animalBQuery, (err, result) => {
      var invalid = validateGetAnimalResult(err, result);
      if (invalid) {
        return onFailure(invalid);
      }

      var bothQuery = mysql.format(queries.GET_RELATIONSHIP_BOTH_ANIMALS, [
        relationship.animalAId,
        relationship.animalBId,
        relationship.animalBId,
        relationship.animalAId
      ]);

      connection.query(bothQuery, (err, result) => {
        if (err) {
          return onFailure(httpFunctions.serverError());
        }

        if (result && result[0]) {
          return onFailure(
            httpFunctions.badRequest(strings.relationshipAlreadyExists)
          );
        }

        callback();
      });
    });
  });
}

function validateGetAnimalResult(err, result) {
  if (err) {
    return httpFunctions.serverError();
  }

  if (!result || result.length === 0) {
    return httpFunctions.badRequest(strings.relationshipAnimalsNotFound);
  }

  if (result[0].removed) {
    return httpFunctions.badRequest(strings.relationshipAnimalIsRemoved);
  }
}

function addRelationship(relationship, onSuccess, onFailure) {
  var query = mysql.format(queries.CREATE_RELATIONSHIP, [
    relationship.animalAId,
    relationship.animalBId,
    relationship.value
  ]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    onSuccess(result);
  });
}

function getAnimalForRelationshipCreation(onSuccess, onFailure) {
  var query = mysql.format(queries.GET_ALL_ANIMALS_FOR_RELATIONSHIP);

  connection.query(query, (err, animals) => {
    if (err) {
      return onFailure(err);
    }

    onSuccess(animals);
  });
}

function getRelationship(id, onSuccess, onFailure, req, res) {
  if (!checkTokenPermission(permissions.relationships.view, req, res)) {
    return;
  }

  var query = mysql.format(queries.GET_RELATIONSHIP_BY_ID, [id]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    if (!result || result.length === 0) {
      return onFailure(httpFunctions.notFound(strings.relationshipNotFound));
    }

    onSuccess(result);
  });
}

function getAnimalRelationships(id, onSuccess, onFailure, req, res) {
  if (!checkTokenPermission(permissions.relationships.viewAnimal, req, res)) {
    return;
  }

  var query = mysql.format(queries.GET_ANIMAL_A_RELATIONSHIPS, [id]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    getAnimalBRelationships(id, onSuccess, onFailure, result);
  });
}

function getAnimalBRelationships(id, onSuccess, onFailure, prevRelationships) {
  var query = mysql.format(queries.GET_ANIMAL_B_RELATIONSHIPS, [id]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    var both = prevRelationships.concat(result);
    onSuccess(both);
  });
}

function deleteRelationship(id, onSuccess, onFailure, req, res) {
  if (!checkTokenPermission(permissions.relationships.delete, req, res)) {
    return;
  }

  var query = mysql.format(queries.DELETE_RELATIONSHIP, [id]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    if (!result.affectedRows) {
      return onFailure(httpFunctions.badRequest(strings.relationshipNotFound));
    }

    onSuccess(result);
  });
}

function editRelationship(id, relationship, onSuccess, onFailure, req, res) {
  if (!checkTokenPermission(permissions.relationships.edit, req, res)) {
    return;
  }

  var result = validateRelationship(relationship);
  if (result) {
    return onFailure(httpFunctions.badRequest(result));
  }

  proceedEditRelationship(id, relationship, onSuccess, onFailure);
}

function proceedEditRelationship(id, relationship, onSuccess, onFailure) {
  var query = mysql.format(queries.EDIT_RELATIONSHIP, [
    relationship.animalAId,
    relationship.animalBId,
    relationship.value,
    id
  ]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    if (!result.affectedRows) {
      return onFailure(httpFunctions.badRequest(strings.relationshipNotFound));
    }

    onSuccess(result);
  });
}

function getOptimalLayout(duration, onSuccess) {
  var findKennelId = function(kennels, id) {
    var kennel;
    kennels.forEach(k => {
      if (k.kennelId === id) {
        kennel = k;
      }
    });
    return kennel;
  };

  var findAnimalId = function(animals, id) {
    var animal;
    animals.forEach(a => {
      if (a.animalId === id) {
        animal = a;
      }
    });
    return animal;
  };

  var normalize = function(result, animals, kennels) {
    result.kennels.forEach(k => {
      k.name = findKennelId(kennels, k.id).name;

      const fetchedAnimals = [];

      k.animals.forEach(a => {
        fetchedAnimals.push(findAnimalId(animals, a));
      });

      k.animals = fetchedAnimals;
    });

    result.changes.forEach(c => {
      c.animal = findAnimalId(animals, c.animal);
      c.from = findKennelId(kennels, c.from);
      c.to = findKennelId(kennels, c.to);
    });

    var sortedKennels = result.kennels.sort((a, b) => {
      return a.score - b.score;
    });

    result.worst = sortedKennels[0];
    result.best = sortedKennels[result.kennels.length - 1];

    return result;
  };

  var start = (animals, kennels, relationships) => {
    const algorithm = new Algorithm(animals, kennels, relationships);
    algorithm.run(duration, result => {
      onSuccess(normalize(result, animals, kennels));
    });
  };

  var getRelationships = (onSuccess, onFailure) => {
    var query = mysql.format(queries.GET_RELATIONSHIPS);

    connection.query(query, (err, result) => {
      if (err) {
        return onFailure(err);
      }
      onSuccess(result);
    });
  };

  var getAnimals = (onSuccess, onFailure) => {
    var query = mysql.format(queries.GET_ALL_ANIMALS);

    connection.query(query, (err, result) => {
      if (err) {
        return onFailure(err);
      }
      onSuccess(result);
    });
  };

  var getKennels = (onSuccess, onFailure) => {
    var query = mysql.format(queries.GET_ALL_KENNELS);

    connection.query(query, (err, result) => {
      if (err) {
        return onFailure(err);
      }
      onSuccess(result);
    });
  };

  getKennels(kennels => {
    getRelationships(relationships => {
      getAnimals(animals => {
        start(animals, kennels, relationships);
      });
    });
  });
}

module.exports = router;
