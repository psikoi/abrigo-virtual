const express = require("express");
const mysql = require("mysql");
const queries = require("../statisticQueries.js");
const { getConnection } = require("../database/sql/db");
const httpFunctions = require("../HTTPFunctions.js");
const { checkTokenPermission } = require("./permissions/authTokenValidation");
const { permissions } = require("./permissions/permissions");

var router = express.Router();

var connection;

router.all("*", function(req, res, next) {
  connection = getConnection(req);
  next();
});

function filterToDateInterval(filter) {
  var endDate = new Date().toLocaleDateString();
  var startDate = new Date();

  switch (filter) {
    case "week":
      startDate.setDate(startDate.getDate() - 7);
      break;
    case "month":
      startDate.setMonth(startDate.getMonth() - 1);
      break;
    case "year":
      startDate.setFullYear(startDate.getFullYear() - 1);
      break;
    case "all":
    default:
      startDate.setFullYear(1);
      break;
  }

  return [startDate.toLocaleDateString(), endDate];
}

function filterToDate(filter) {
  var start = new Date(filter);
  var end = new Date(filter);
  end.setDate(end.getDate() + 1);

  return [start.toLocaleDateString(), end.toLocaleDateString()];
}

function getDates(filter) {
  if (Date.parse(filter)) {
    return filterToDate(filter);
  }

  return filterToDateInterval(filter);
}

router.get("/tasks-per-status/:filter", function(req, res, next) {
  if (!checkTokenPermission(permissions.statistics.view, req, res)) {
    return;
  }

  var filter = req.params.filter;

  getDatedStatistics(
    queries.GET_TASKS_PER_STATUS,
    getDates(filter),
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

router.get("/completed-tasks-by-employee/:filter", function(req, res, next) {
  if (!checkTokenPermission(permissions.statistics.view, req, res)) {
    return;
  }

  var filter = req.params.filter;

  getDatedStatistics(
    queries.GET_COMPLETED_TASKS_BY_EMPLOYEE,
    getDates(filter),
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

router.get("/pending-tasks-by-employee/:filter", function(req, res, next) {
  if (!checkTokenPermission(permissions.statistics.view, req, res)) {
    return;
  }

  var filter = req.params.filter;

  getDatedStatistics(
    queries.GET_PENDING_TASKS_BY_EMPLOYEE,
    getDates(filter),
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

router.get("/get-animals-by-size/", function(req, res, next) {
  if (!checkTokenPermission(permissions.statistics.view, req, res)) {
    return;
  }

  getStatistics(
    queries.GET_ANIMALS_BY_SIZE,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

router.get("/get-animals-by-age/", function(req, res, next) {
  if (!checkTokenPermission(permissions.statistics.view, req, res)) {
    return;
  }

  getStatistics(
    queries.GET_ANIMALS_BY_AGE,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

router.get("/get-animals-by-register-date/:filter", function(req, res, next) {
  if (!checkTokenPermission(permissions.statistics.view, req, res)) {
    return;
  }

  var filter = req.params.filter;

  getDatedStatistics(
    queries.GET_ANIMALS_BY_REGISTER_DATE,
    getDates(filter),
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    },
    function(result) {
      return parseRegisterDate(result, filter);
    }
  );
});

router.get("/get-quarentines-by-animal/:filter", function(req, res, next) {
  if (!checkTokenPermission(permissions.statistics.view, req, res)) {
    return;
  }

  var filter = req.params.filter;

  getDatedStatistics(
    queries.GET_QUARENTINES_BY_ANIMAL_BY_DATE,
    getDates(filter),
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

router.get("/get-quarentines-by-register-date/:filter", function(
  req,
  res,
  next
) {
  if (!checkTokenPermission(permissions.statistics.view, req, res)) {
    return;
  }

  var filter = req.params.filter;

  getDatedStatistics(
    queries.GET_QUARENTINES_BY_REGISTER_DATE,
    getDates(filter),
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    },
    function(result) {
      return parseRegisterDate(result, filter);
    }
  );
});

router.get("/get-quarentines-by-cause/", function(req, res, next) {
  if (!checkTokenPermission(permissions.statistics.view, req, res)) {
    return;
  }

  getStatistics(
    queries.GET_QUARENTINES_BY_CAUSE,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

function getDatedStatistics(query, dates, onSuccess, onFailure, callback) {
  var query = mysql.format(query, dates);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError(err));
    }

    if (!result || result.length === 0)
      return onFailure(httpFunctions.badRequest("Not found"));

    if (callback) {
      return onSuccess(callback(result));
    }

    onSuccess(result);
  });
}

function getStatistics(query, onSuccess, onFailure) {
  var query = mysql.format(query);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError(err));
    }

    if (!result || result.length === 0)
      return onFailure(httpFunctions.badRequest("Not found"));

    onSuccess(result);
  });
}

function parseRegisterDate(result, filter) {
  switch (filter) {
    case "week":
      result = getWeeklyRegisters(result);
      break;
    case "month":
      result = getMonthlyRegisters(result);
      break;
    case "year":
      result = getAnualRegisters(result);
      break;
    case "all":
      result = getAllRegisters(result);
      break;
    default:
      result = getAllRegisters(result);
      break;
  }

  return result;
}

function getAllRegisters(result) {
  var res = [];

  result.forEach(function(e) {
    var year = new Date(e.register).getFullYear();
    var yearInRes = false;

    res.forEach(function(i, index) {
      if (i.year === year) {
        i.count += e.count;
        yearInRes = true;
      }
    });

    if (!yearInRes) {
      res.push({ type: "all", year: year, count: e.count });
    }
  });

  for (var k = 1; k < res.length; k++) {
    res[k].count += res[k - 1].count;
  }

  return res;
}

function getAnualRegisters(result) {
  var res = [];
  var startMonth = new Date().getMonth();

  for (var i = 0; i < 13; i++) {
    var obj = {
      type: "year",
      month: monthIndexToString(startMonth),
      count: res[res.length - 1] ? res[res.length - 1].count : 0
    };

    result.forEach(function(e) {
      var month = new Date(e.register).getMonth();

      if (month === startMonth) {
        obj.count += e.count;
      }
    });

    res.push(obj);

    startMonth++;
    if (startMonth === 12) {
      startMonth = 0;
    }
  }

  return res;
}

function getMonthlyRegisters(result) {
  var currDate = new Date();
  var firstDay = new Date(
    currDate.getFullYear(),
    currDate.getMonth() - 1,
    currDate.getDate()
  );
  var dates = groupIntoWeeks(firstDay);

  var res = [];
  for (var i = 0; i < dates.length - 1; i++) {
    var currDate = new Date(dates[i]);
    if (i) {
      currDate.setDate(currDate.getDate() + 1);
    }

    var obj = {
      type: "month",
      interval: buildIntervalString(currDate, dates[i + 1]),
      count: res[res.length - 1] ? res[res.length - 1].count : 0
    };

    result.forEach(function(e) {
      var first = currDate;
      var last = dates[i + 1];

      if (e.register >= first && e.register <= last) {
        obj.count += e.count;
      }
    });

    res.push(obj);
  }

  return res;
}

function getWeeklyRegisters(result) {
  var currDate = new Date();
  var firstDay = new Date(
    currDate.getFullYear(),
    currDate.getMonth(),
    currDate.getDate() - 7
  );
  var dates = getDatesBetween(firstDay, currDate);

  var res = [];

  dates.forEach(function(e) {
    var obj = {
      type: "week",
      day: buildDateString(e),
      count: res[res.length - 1] ? res[res.length - 1].count : 0
    };

    result.forEach(function(i) {
      var day = new Date(i.register).getDate();

      if (day === e.getDate()) {
        obj.count += i.count;
      }
    });

    res.push(obj);
  });

  return res;
}

function getDatesBetween(start, end) {
  for (var arr = [], dt = start; dt <= end; dt.setDate(dt.getDate() + 1)) {
    arr.push(new Date(dt));
  }
  return arr;
}

function groupIntoWeeks(first) {
  var dates = [];
  dates.push(new Date(first));

  for (var i = 0; i < 4; i++) {
    first.setDate(first.getDate() + 8);
    dates.push(new Date(first));
  }

  return dates;
}

function buildDateString(d) {
  return "" + d.getDate() + "/" + (d.getMonth() + 1);
}

function buildIntervalString(d1, d2) {
  return (
    "" +
    d1.getDate() +
    "/" +
    (d1.getMonth() + 1) +
    " - " +
    d2.getDate() +
    "/" +
    (d2.getMonth() + 1)
  );
}

function monthIndexToString(index) {
  switch (index) {
    case 0:
      return "Jan";
    case 1:
      return "Fev";
    case 2:
      return "Mar";
    case 3:
      return "Abr";
    case 4:
      return "Mai";
    case 5:
      return "Jun";
    case 6:
      return "Jul";
    case 7:
      return "Ago";
    case 8:
      return "Set";
    case 9:
      return "Out";
    case 10:
      return "Nov";
    case 11:
      return "Dez";
    default:
      return "";
  }
}

module.exports = router;
