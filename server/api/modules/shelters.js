const express = require("express");
const httpFunctions = require("../HTTPFunctions.js");
const {
  getDecodedUserByReq,
  getShelterId
} = require("./permissions/authTokenValidation");
const { validateLicense } = require("./validations/licenceValidations");
const {
  updateLicense,
  getShelterById
} = require("../database/mongo/schemas/shelter");
const { getConnection, createSqlDatabase } = require("../database/sql/db");
const strings = require("../data/strings.json");
const shelter = require("../database/mongo/schemas/shelter");
const user = require("../database/mongo/schemas/user");
const { createEmployee } = require("./employees");
const {
  validateShelter,
  validateUser
} = require("./validations/shelterValidations");

var router = express.Router();

router.get("/by-id", function(req, res, next) {
  var shelterId = getShelterId(req);

  getShelter(
    shelterId,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

function getShelter(shelterId, onSuccess, onFailure) {
  shelter.findById(
    shelterId,
    function(doc) {
      return onSuccess(doc);
    },
    function() {
      return onFailure(httpFunctions.badRequest("No shelter found"));
    }
  );
}

router.post("/", function(req, res, next) {
  var body = req.body;

  createShelter(
    body,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

function createShelter(body, onSuccess, onFailure, onBadRequest) {
  var shelterInfo = body.shelter;
  var userInfo = body.user;

  var shelterValidation = validateShelter(shelterInfo);
  if (shelterValidation) {
    return onBadRequest(shelterValidation);
  }

  var userValidation = validateUser(userInfo);
  if (userValidation) {
    return onBadRequest(userValidation);
  }

  validateExistingInformation(
    shelterInfo,
    userInfo,
    onSuccess,
    onFailure,
    onBadRequest
  );
}

function validateExistingInformation(
  shelterInfo,
  userInfo,
  onSuccess,
  onFailure,
  onBadRequest
) {
  shelter.find(
    shelterInfo.name,
    function() {
      return onBadRequest(strings.shelterNameAlreadyExists);
    },
    function() {
      user.find(
        userInfo.email,
        function() {
          return onBadRequest(strings.shelterAdminEmailAlreadyExists);
        },
        function() {
          shelter.insert(
            shelterInfo.name,
            shelterInfo.address,
            "basic",
            function(res) {
              createDatabase(userInfo, res, onSuccess, onFailure, onBadRequest);
            },
            onFailure
          );
        }
      );
    }
  );
}

function createDatabase(
  userInfo,
  result,
  onSuccess,
  onFailure,
  onBadRequest,
  req
) {
  var shelterId = result._id.toString();
  createSqlDatabase(shelterId, function() {
    insertUser(shelterId, userInfo, onSuccess, onFailure, onBadRequest);
  });
}

function insertUser(shelterId, userInfo, onSuccess, onFailure, onBadRequest) {
  var connection = getConnection(shelterId);
  var newEmployee = {
    name: userInfo.name,
    email: userInfo.email,
    photoUrl: "",
    contact: "000000000",
    address: "---",
    info: "",
    gender: "other",
    type: "admin",
    birthdate: new Date(1900, 01, 01)
  };

  createEmployee(
    newEmployee,
    onSuccess,
    onFailure,
    onBadRequest,
    shelterId,
    connection
  );
}

// middleware function to check if is a admin request
var isAdmin = (req, res, next) => {
  var user = getDecodedUserByReq(req).success;

  if (!user || user.type !== "admin") {
    let err = httpFunctions.forbidden();
    res.status(err.statusCode).send(err.message);
  } else {
    next();
  }
};

router.get("/", isAdmin, function(req, res, next) {
  getShelterById(
    getShelterId(req),
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    },
    function(message) {
      // On bad request
      next(httpFunctions.notFound(message));
    }
  );
});

router.put("/license", isAdmin, function(req, res, next) {
  changeLicense(
    getShelterId(req),
    req.body,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    },
    function(message) {
      // On bad request
      next(httpFunctions.notFound(message));
    }
  );
});

function changeLicense(
  shelterId,
  license,
  onSuccess,
  onFailure,
  onBadRequest,
  onNotFound
) {
  var invalid = validateLicense(license);

  if (invalid) {
    onBadRequest(invalid);
  } else if (license.type != license.lastType) {
    updateLicense(shelterId, license, onSuccess, onNotFound, onFailure);
  } else {
    onSuccess(strings.licenseEditedSuccessfully);
  }
}

module.exports = router;
