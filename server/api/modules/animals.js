const express = require("express");
const router = express.Router();
const { getConnection } = require("../database/sql/db");
const queries = require("../database/sql/queries");
const httpFunctions = require("../HTTPFunctions.js");
const mysql = require("mysql");
var strings = require("../data/strings.json");
var fileManager = require("../fileManager/fileManager.js");
var util = require("util");
const { validateAnimal } = require("./validations/animalsValidations.js");
const { permissions } = require("./permissions/permissions");
const { checkTokenPermission } = require("./permissions/authTokenValidation");

var connection;

router.all("*", function(req, res, next) {
  connection = getConnection(req);
  next();
});

router.post("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.animal.create, req, res)) {
    return;
  }

  var animal = req.body;

  createAnimal(
    animal,
    result => {
      // On success
      res.status(200).json({ data: result });
    },
    () => {
      // On failure
      next(httpFunctions.serverError());
    },
    message => {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

router.get("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.animal.viewAll, req, res)) {
    return;
  }

  getAnimals(
    result => {
      // On success
      res.status(200).json({ data: result });
    },
    () => {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

router.get("/:id(\\d+)", function(req, res, next) {
  if (!checkTokenPermission(permissions.animal.view, req, res)) {
    return;
  }

  getAnimal(
    req.params.id,
    result => {
      // On success
      res.status(200).json({ data: result });
    },
    () => {
      // On failure
      next(httpFunctions.serverError());
    },
    message => {
      // On not found
      next(httpFunctions.notFound(message));
    }
  );
});

router.put("/:id(\\d+)", function(req, res, next) {
  if (!checkTokenPermission(permissions.animal.edit, req, res)) {
    return;
  }

  editAnimal(
    req.params.id,
    req.body,
    result => {
      // On success
      res.status(200).json({ data: result });
    },
    () => {
      // On failure
      next(httpFunctions.serverError());
    },
    message => {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

router.delete("/:id(\\d+)", function(req, res, next) {
  if (!checkTokenPermission(permissions.animal.delete, req, res)) {
    return;
  }

  deleteAnimal(
    req.params.id,
    result => {
      // On success
      res.status(200).json({ data: result });
    },
    () => {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

function createAnimal(animal, onSuccess, onFailure, onBadRequest) {
  var invalid = validateAnimal(animal);

  if (invalid) {
    onBadRequest(invalid);
  } else {
    connection.beginTransaction(function(err) {
      if (err) {
        return onFailure();
      }

      var currDate = new Date();
      currDate.setHours(0, 0, 0, 0);

      var query = mysql.format(queries.CREATE_ANIMAL, [
        animal.name,
        animal.photoUrl,
        new Date(animal.birthdate),
        animal.breed,
        animal.size,
        animal.info,
        animal.gender,
        animal.type,
        currDate
      ]);

      connection.query(query, (err, result) => {
        if (err) {
          connection.rollback(function() {
            onFailure(err);
          });
        }
        connection.commit(function(err) {
          if (err) {
            rollback(err);
          } else {
            fileManager.uploadBase64(
              result.insertId,
              animal.image,
              setPhotoUrl,
              onSuccess,
              strings.animalCreatedSuccessfully
            );
          }
        });
      });
    });
  }
}

function setPhotoUrl(id, result, onSuccess, defaultMessage) {
  if (!result) {
    onSuccess(defaultMessage);
  } else if (result.photoUrl) {
    var query = mysql.format(queries.UPDATE_ANIMAL_PHOTOURL_BY_ID, [
      result.photoUrl,
      id
    ]);

    connection.query(query, (err, result) => {
      if (err) {
        onSuccess(defaultMessage + " " + strings.noPhotoUploaded);
      } else {
        onSuccess(defaultMessage);
      }
    });
  } else {
    onSuccess(defaultMessage + " " + strings.noPhotoUploaded);
  }
}

function editAnimal(id, animal, onSuccess, onFailure, onBadRequest) {
  var invalid = validateAnimal(animal);

  if (invalid) {
    onBadRequest(invalid);
  } else {
    connection.beginTransaction(function(err) {
      if (err) {
        return onFailure();
      }

      var query = mysql.format(queries.EDIT_ANIMAL, [
        animal.name,
        animal.photoUrl,
        new Date(animal.birthdate),
        animal.breed,
        animal.size,
        animal.info,
        animal.gender,
        animal.type,
        id
      ]);

      connection.query(query, (err, result) => {
        if (err) {
          connection.rollback(function() {
            onFailure(err);
          });
        }
        connection.commit(function(err) {
          if (err) {
            rollback(err);
          } else {
            fileManager.uploadBase64(
              id,
              animal.image,
              setPhotoUrl,
              onSuccess,
              strings.animalEditedSuccessfully
            );
          }
        });
      });
    });
  }
}

function getAnimals(onSuccess, onFailure) {
  var query = mysql.format(queries.GET_ALL_ANIMALS);

  connection.query(query, (err, animals) => {
    if (err) {
      return onFailure(err);
    }

    query = mysql.format(queries.GET_KENNELS_AND_QUARENTINES);

    connection.query(query, (err, result) => {
      if (err) {
        return onFailure(err);
      }

      animals.forEach(animal => {
        // check if is in quarentine
        animal.inQuarentine = result[1].some(
          quarentine => quarentine.animalId == animal.animalId
        );

        // get the kennel name
        result[0].forEach(kennel => {
          if (kennel.animalId == animal.animalId) {
            animal.kennelName = kennel.name;
          }
        });
      });

      onSuccess(animals);
    });
  });
}

function getAnimal(id, onSuccess, onFailure, onNotFound) {
  var query = mysql.format(queries.GET_ANIMAL_BY_ID, [id]);

  connection.query(query, (err, animal) => {
    if (err) {
      return onFailure(err);
    } else if (animal.length == 0 || animal[0].removed) {
      return onNotFound(util.format(strings.dataNotFound, "animal"));
    }

    query = mysql.format(queries.GET_KENNELS_AND_QUARENTINES);

    connection.query(query, (err, result) => {
      if (err) {
        return onFailure(err);
      }

      animal = animal[0];

      // check if is in quarentine
      animal.inQuarentine = result[1].some(
        quarentine => quarentine.animalId == animal.animalId
      );

      // get the kennel name
      result[0].forEach(kennel => {
        if (kennel.animalId == animal.animalId) {
          animal.kennelName = kennel.name;
        }
      });

      onSuccess(animal);
    });
  });
}

function deleteAnimal(animalId, onSuccess, onFailure) {
  var query = mysql.format(queries.DELETE_ANIMAL, [animalId]);

  connection.query(query, (err, result) => {
    if (err) {
      onFailure(err);
    } else {
      setQuarentinesRemoved(animalId, onSuccess, onFailure);
    }
  });
}

function setQuarentinesRemoved(animalId, onSuccess, onFailure) {
  var query = mysql.format(queries.UPDATE_ANIMAL_QUARENTINES_REMOVED, [
    animalId
  ]);

  connection.query(query, (err, result) => {
    if (err) {
      onFailure(err);
    } else {
      onSuccess(strings.animalRemovedSucessfully);
    }
  });
}
module.exports = router;
