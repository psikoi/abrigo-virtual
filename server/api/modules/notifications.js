const express = require("express");
const { getConnection } = require("../database/sql/db");
const mysql = require("mysql");
const queries = require("../database/sql/queries");
const httpFunctions = require("../HTTPFunctions.js");
const {
  getDecodedUserByReq,
  ownAccountOnlyPermission
} = require("./permissions/authTokenValidation");
var strings = require("../data/strings.json");

var router = express.Router();
var connection;

router.all("*", function(req, res, next) {
  connection = getConnection(req);
  next();
});

router.get("/employee/:id(\\d+)", function(req, res, next) {
  const id = parseInt(req.params.id);
  if (!ownAccountOnlyPermission(id, req, res)) {
    return;
  }

  getNotificationsByEmployee(
    id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

router.put("/read/:id(\\d+)", function(req, res, next) {
  const result = getDecodedUserByReq(req);

  if (!result || !result.success) {
    res.status(403).send(result.error);
    return false;
  }

  checkReadNotification(
    req.params.id,
    result.success,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

// reads all
router.put("/read", function(req, res, next) {
  readAllNotifications(
    req.body,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

function getNotificationsByEmployee(id, onSuccess, onFailure) {
  var query = mysql.format(queries.GET_NOTIFICATIONS_BY_EMPLOYEE, [id]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError(err));
    }

    if (result.length === 0)
      return onFailure(
        httpFunctions.badRequest(strings.noEmployeeNotifications)
      );

    onSuccess(result);
  });
}

function checkReadNotification(id, currUser, onSuccess, onFailure) {
  var query = mysql.format(queries.GET_NOTIFICATION_BY_ID, [id]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError(err));
    }

    if (!result || result.length === 0)
      return onFailure(httpFunctions.badRequest("Not found"));

    if (result[0].employeeId !== currUser.id) {
      return onFailure(httpFunctions.forbidden());
    }

    return readNotification(id, onSuccess, onFailure);
  });
}

function readNotification(id, onSuccess, onFailure) {
  var query = mysql.format(queries.READ_NOTIFICATION, [id]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError(err));
    }

    if (result.affectedRows === 0)
      return onFailure(httpFunctions.badRequest("Not found"));

    onSuccess(result);
  });
}

async function readAllNotifications(notifications, onSuccess, onFailure) {
  if (!notifications || notifications.length === 0) {
    return onFailure(httpFunctions.badRequest(strings.noNotificationsSent));
  }

  var query = "";
  notifications.forEach(e => {
    query += mysql.format(queries.READ_NOTIFICATION, [e.id]);
  });

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }
    return onSuccess();
  });
}

module.exports = router;
