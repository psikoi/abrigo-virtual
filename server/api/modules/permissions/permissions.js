const accountTypes = {
  ADMIN: "admin",
  EMPLOYEE: "employee",
  VOLUNTEER: "volunteer"
};

// Empty permissions mean only Admin can execute the action
// Adding Admin to array is redudant because checkPermission checks if user is admin
// Actions that everyone can perform as long as they are logged in should be registered
// Actions that everyone can perfom even when not logged in don't need to be registered, since there will be no validation anyways
const permissions = {
  employee: {
    create: [],
    edit: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    delete: [],
    block: [],
    viewAll: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    viewAvailable: [],
    archive: [],
    reactivate: [],
    view: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    viewSchedule: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    scheduleAction: [],
    viewAvailableSchedule: [accountTypes.VOLUNTEER],
    viewPartialSchedule: [accountTypes.VOLUNTEER],
    scheduleAvailableAction: [accountTypes.VOLUNTEER]
  },
  animal: {
    create: [accountTypes.EMPLOYEE],
    edit: [accountTypes.EMPLOYEE],
    view: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    viewAll: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    delete: []
  },
  reinforcements: {
    viewAll: [],
    view: [accountTypes.VOLUNTEER]
  },
  tasks: {
    viewAll: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    finish: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    create: [],
    edit: []
  },
  tasksEmployee: {
    viewAll: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    view: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    create: [],
    edit: []
  },
  schedule: {
    getAvailability: []
  },
  quarentines: {
    viewAll: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    create: [accountTypes.EMPLOYEE],
    edit: [accountTypes.EMPLOYEE],
    finish: [accountTypes.EMPLOYEE]
  },
  relationships: {
    create: [],
    delete: [],
    viewAnimal: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    edit: [],
    view: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    viewAll: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER]
  },
  kennels: {
    viewAll: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    view: [accountTypes.EMPLOYEE, accountTypes.VOLUNTEER],
    create: [accountTypes.EMPLOYEE],
    edit: [accountTypes.EMPLOYEE],
    delete: []
  },
  statistics: {
    view: []
  }
};

function checkPermission(array, user, adminCountes) {
  //user isn't logged in
  if (user === undefined) {
    return false;
  }

  var type = user.type;

  if (type === accountTypes.ADMIN) {
    return adminCountes ? true : false;
  }

  return array.includes(type);
}

module.exports.permissions = permissions;
module.exports.checkPermission = checkPermission;
