const jwt = require("jsonwebtoken");
const { checkPermission } = require("./permissions");

function getToken(req) {
  return (
    req.body.token ||
    req.query.token ||
    req.headers["x-access-token"] ||
    req.cookies.token
  );
}

function getShelterId(req) {
  const token = getToken(req);

  if (!token) {
    return { error: "Não se encontra autenticado" };
  }

  var result;

  jwt.verify(token, process.env.JWT_TOKEN_SECRET, (err, decoded) => {
    if (err) {
      result = { error: "Sessão inválida" };
    } else {
      result = decoded.shelterId;
    }
  });

  return result;
}
module.exports.getShelterId = getShelterId;

function getDecodedUser(token) {
  var result;

  if (!token) {
    result = { error: "Não se encontra autenticado" };
  } else {
    jwt.verify(token, process.env.JWT_TOKEN_SECRET, function(err, decoded) {
      if (err) {
        result = { error: "Sessão inválida" };
      } else {
        result = { success: decoded };
      }
    });
  }

  return result;
}

function getDecodedUserByReq(req) {
  return getDecodedUser(getToken(req));
}

module.exports.getDecodedUserByReq = getDecodedUserByReq;

const checkTokenPermission = function(allowed, req, res) {
  var result = checkToken(allowed, true, req, res);

  if (!result.allowed && !result.error) {
    res.status(403).send("Não tem permissão para efetuar esta ação");
  }

  return result.allowed;
};

module.exports.checkTokenPermission = checkTokenPermission;

function checkToken(allowed, adminCounts, req, res) {
  const token = getDecodedUser(getToken(req));
  if (token.error) {
    res.status(403).send(token.error);
    return { error: token.error };
  }
  return {
    allowed: checkPermission(allowed, token.success, adminCounts),
    token: token
  };
}

const checkTokenOwnAccount = function(allowed, id, req, res) {
  var result = checkToken(allowed, true, req, res);

  var user = result.token.success;

  var dontHavePermission =
    (result.allowed &&
      !result.error &&
      !((user.type !== "admin" && user.id == id) || user.type === "admin")) ||
    (!result.allowed && !result.error);

  if (dontHavePermission) {
    res.status(403).send("Não tem permissão para efetuar esta ação");
  }

  return !dontHavePermission;
};

module.exports.checkTokenOwnAccount = checkTokenOwnAccount;

const isAuthenticated = function(req) {
  var token = getToken(req);
  return !(token === undefined || token === "");
};

module.exports.isAuthenticated = isAuthenticated;

const getCurrentUserEmail = function(req, res) {
  var token = getDecodedUser(getToken(req));
  if (token.error) {
    res.status(403).send(token.error);
    return "";
  } else {
    return token.success.email;
  }
};

module.exports.getCurrentUserEmail = getCurrentUserEmail;

const ownAccountOnlyPermission = function(id, req, res) {
  var result = getDecodedUserByReq(req);

  if (!result || !result.success) {
    res.status(403).send(result.error);
    return false;
  }

  var user = result.success;

  if (user.id !== id) {
    res.status(403).send("Não tem permissão para efetuar esta ação");
    return false;
  }

  return true;
};

module.exports.ownAccountOnlyPermission = ownAccountOnlyPermission;

const checkTokenPermissionIgnoringAdmin = function(allowed, req, res) {
  var result = checkToken(allowed, false, req, res);

  if (!result.allowed && !result.error) {
    res.status(403).send("Não tem permissão para efetuar esta ação");
  }

  return result.allowed;
};

module.exports.checkTokenPermissionIgnoringAdmin = checkTokenPermissionIgnoringAdmin;
