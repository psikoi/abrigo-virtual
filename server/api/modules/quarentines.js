const express = require("express");
const router = express.Router();
const queries = require("../database/sql/queries");
const { getConnection } = require("../database/sql/db");
const mysql = require("mysql");
const utils = require("../utils");
var util = require("util");
const { permissions } = require("./permissions/permissions");
const { checkTokenPermission } = require("./permissions/authTokenValidation");
const {
  validateQuarentine,
  validateFinishQuarentineRequest
} = require("./validations/quarentinesValidation");
var strings = require("../data/strings.json");
const httpFunctions = require("../HTTPFunctions.js");

var connection;

router.all("*", function(req, res, next) {
  connection = getConnection(req);
  next();
});

router.get("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.quarentines.viewAll, req, res)) {
    return;
  }

  getQuarentines(
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

router.get("/create", function(req, res, next) {
  if (!checkTokenPermission(permissions.quarentines.create, req, res)) {
    return;
  }

  getAnimalsNotInQuarentine(
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

router.post("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.quarentines.create, req, res)) {
    return;
  }

  var newQuarentine = req.body;

  createQuarentine(
    newQuarentine,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

router.get("/edit/:id([0-9])", function(req, res, next) {
  if (!checkTokenPermission(permissions.quarentines.edit, req, res)) {
    return;
  }

  var id = req.params.id;

  getQuarentineToEdit(
    id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On not found
      next(httpFunctions.notFound(message));
    }
  );
});

router.put("/edit/:id([0-9])", function(req, res, next) {
  if (!checkTokenPermission(permissions.quarentines.edit, req, res)) {
    return;
  }

  var id = req.params.id;

  editQuarentine(
    id,
    req.body,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

router.put("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.quarentines.finish, req, res)) {
    return;
  }

  checkFinishQuarentine(
    req.body,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

router.get("/animals/:id(\\d+)", function(req, res, next) {
  if (!checkTokenPermission(permissions.quarentines.viewAll, req, res)) {
    return;
  }

  var id = req.params.id;

  getAnimalQuarentines(
    id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

function getAnimalQuarentines(id, onSuccess, onFailure) {
  var query = mysql.format(queries.GET_ANIMAL_QUARENTINES, [id]);

  var prepareResults = function(result) {
    var preparedResults = [];

    result.forEach(function(e) {
      preparedResults.push(prepareQuarentines(e));
    });

    return preparedResults;
  };

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    }
    onSuccess(prepareResults(result));
  });
}

function createQuarentine(quarentine, onSuccess, onFailure, onBadRequest) {
  var invalid = validateQuarentine(quarentine);

  if (invalid) {
    return onBadRequest(invalid);
  }

  var query = mysql.format(queries.CREATE_QUARENTINE, [
    "pending",
    quarentine.animalId,
    quarentine.info,
    quarentine.cause
  ]);

  connection.query(query, (err, result) => {
    if (err) {
      let animalNotExists = err.message.includes("foreign key");
      return animalNotExists
        ? onBadRequest(strings.animalNotFound)
        : onFailure(err);
    }
    onSuccess(strings.quarentineCreatedSuccessfully);
  });
}

function getAnimalsNotInQuarentine(onSuccess, onFailure) {
  var query = mysql.format(queries.GET_ANIMALS_NOT_IN_QUARENTINE);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    }
    onSuccess(result);
  });
}

function getQuarentines(onSuccess, onFailure, extras) {
  var query = mysql.format(queries.GET_QUARENTINES);

  var prepareResults = function(result) {
    var preparedResults = [];

    result.forEach(function(e) {
      preparedResults.push(prepareQuarentines(e));
    });

    return preparedResults;
  };

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    }
    onSuccess(prepareResults(result));
  });
}

function prepareQuarentines(e) {
  var animal = {
    id: e.animalId,
    name: e.name,
    photoUrl: e.photoUrl,
    birthdate: utils.convertDate(e.birthdate),
    breed: e.breed,
    size: e.size,
    info: e.animalInfo,
    gender: e.gender,
    registerDate: utils.convertDate(e.registerDate),
    removed: e.animalRemoved,
    type: e.type
  };

  var quarentine = {
    id: e.quarentineId,
    animal,
    startDate: utils.convertDate(e.startDate),
    endDate: utils.convertDate(e.endDate),
    status: e.status,
    info: e.quarentineInfo,
    cause: e.cause,
    removed: e.quarentineRemoved
  };

  return quarentine;
}

function getQuarentineToEdit(id, onSuccess, onFailure, onNotFound) {
  var query = mysql.format(queries.GET_QUARENTINE_BY_ID, [id]);

  connection.query(query, (err, quarentineResult) => {
    if (err) {
      return onFailure(err);
    } else if (quarentineResult.length === 0) {
      onNotFound(util.format(strings.dataNotFound, "quarentena"));
    } else {
      query = mysql.format(queries.GET_ALL_ANIMALS_EDIT_QUARENTINE, [
        quarentineResult[0].animalId
      ]);

      connection.query(query, (err, animalsResult) => {
        if (err) {
          return onFailure(err);
        }
        onSuccess({
          quarentine: quarentineResult[0],
          animals: animalsResult
        });
      });
    }
  });
}

function editQuarentine(id, quarentine, onSuccess, onFailure, onBadRequest) {
  var invalid = validateQuarentine(quarentine);

  if (invalid) {
    return onBadRequest(invalid);
  }

  var query = mysql.format(queries.EDIT_QUARENTINE, [
    quarentine.info,
    quarentine.cause,
    quarentine.animalId,
    id
  ]);

  connection.query(query, (err, result) => {
    if (err) {
      let animalNotExists = err.message.includes("foreign key");
      return animalNotExists
        ? onBadRequest(strings.animalNotFound)
        : onFailure(err);
    }
    onSuccess(strings.quarentineEditedSuccessfully);
  });
}

function checkFinishQuarentine(body, onSuccess, onFailure) {
  var err = validateFinishQuarentineRequest(body.id, body.status);

  if (err) {
    return onFailure(httpFunctions.badRequest(err));
  }

  var query = mysql.format(queries.FINISH_QUARENTINE, [body.status, body.id]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    if (result.affectedRows) {
      return onSuccess(true);
    }

    return onFailure(httpFunctions.badRequest(strings.quarentineNotFound));
  });
}

module.exports = router;
