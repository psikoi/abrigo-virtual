const express = require("express");
const { getConnection } = require("../database/sql/db");
const mysql = require("mysql");
const queries = require("../database/sql/queries");
const utils = require("../utils");
var util = require("util");
const httpFunctions = require("../HTTPFunctions.js");
const {
  checkTokenPermission,
  checkTokenOwnAccount
} = require("./permissions/authTokenValidation");
const { permissions } = require("./permissions/permissions");
const {
  validateFinishTaskRequest,
  validateTask
} = require("./validations/tasksValidations");
const { getEmployeeAvailability } = require("./schedule");
const strings = require("../data/strings.json");
const emailServer = require("../emailServer/emailServer.js");

var router = express.Router();
var connection;

router.all("*", function(req, res, next) {
  connection = getConnection(req);
  next();
});

router.get("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.tasks.viewAll, req, res)) {
    return;
  }

  getTasks(
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

router.get("/create-permission", function(req, res, next) {
  if (!checkTokenPermission(permissions.tasks.create, req, res)) {
    return;
  }

  getEmployees(
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

router.post("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.tasks.create, req, res)) {
    return;
  }

  createTask(
    req.body,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

router.get("/edit/:id(\\d+)", function(req, res, next) {
  if (!checkTokenPermission(permissions.tasks.edit, req, res)) {
    return;
  }

  var id = req.params.id;

  getTaskToEdit(
    id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On not found
      next(httpFunctions.notFound(message));
    }
  );
});

router.put("/edit/:id(\\d+)", function(req, res, next) {
  if (!checkTokenPermission(permissions.tasks.edit, req, res)) {
    return;
  }

  var id = req.params.id;

  editTask(
    id,
    req.body,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

router.get("/employee/:id(\\d+)", function(req, res, next) {
  if (!checkTokenPermission(permissions.tasksEmployee.viewAll, req, res)) {
    return;
  }

  var id = req.params.id;

  getEmployeeTasks(
    id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

router.get("/:id(\\d+)", function(req, res, next) {
  if (!checkTokenPermission(permissions.tasks.view, req, res)) {
    return;
  }

  var id = req.params.id;

  if (!id) {
    next(httpFunctions.badRequest(message));
  }

  getTask(
    id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

router.put("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.tasks.finish, req, res)) {
    return;
  }

  checkFinishTask(
    req.body,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    },
    req,
    res
  );
});

function createTask(task, onSuccess, onFailure, onBadRequest) {
  var invalid = validateTask(task);

  if (invalid) {
    onBadRequest(invalid);
  } else {
    connection.beginTransaction(function(err) {
      if (err) {
        return onFailure();
      } else {
        var startDate = new Date(task.startDate);
        var endDate = new Date(task.endDate);
        var query = mysql.format(queries.CREATE_TASK, [
          task.employeeId,
          task.title,
          task.description,
          startDate,
          endDate
        ]);

        getEmployeeAvailability(
          {
            employeeId: task.employeeId,
            onBadRequest,
            onSuccess,
            onFailure,
            query,
            startDate,
            endDate,
            successMessage: strings.createTaskSucessfull,
            task
          },
          execReplaceTask,
          onFailure
        );
      }
    });
  }
}

function execReplaceTask(blocks, parameters) {
  const {
    query,
    startDate,
    endDate,
    onSuccess,
    onBadRequest,
    onFailure,
    successMessage
  } = parameters;

  if (!isEmployeeAvailable(blocks, startDate, endDate)) {
    onBadRequest(strings.employeeNotAvailable);
    return;
  }
  connection.query(query, (err, result) => {
    if (err) {
      let employeeNotExists = err.message.includes("foreign key");
      connection.rollback(
        employeeNotExists ? strings.employeeNotFoundTask : err,
        employeeNotExists ? onBadRequest : onFailure
      );
    } else
      connection.commit(function(err) {
        if (err) {
          connection.rollback(err);
        } else {
          notifyEmployee(parameters);

          onSuccess(successMessage);
        }
      });
  });
}

function notifyEmployee(parameters) {
  const { employeeId, lastEmployeeId } = parameters;

  var query = mysql.format(queries.GET_EMPLOYEE_BY_ID, [employeeId]);

  connection.query(query, (err, result) => {
    if (err || result.length == 0) {
      return;
    }

    const employee = result[0];
    var subject;
    var message;

    if (employeeId !== lastEmployeeId) {
      // new task created or user change on task
      subject = strings.assignedTaskSubject;
      message = strings.assignedTask;
    } else {
      // task edited and employee is the same
      subject = strings.assignedTaskChangedSubject;
      message = strings.assignedTaskChanged;
    }

    emailServer.sendTaskNotification(
      parameters.task,
      employee,
      subject,
      message
    );
  });
}

function editTask(id, task, onSuccess, onFailure, onBadRequest) {
  var invalid = validateTask(task);

  if (invalid) {
    onBadRequest(invalid);
  } else {
    connection.beginTransaction(function(err) {
      if (err) {
        return onFailure();
      } else {
        var startDate = new Date(task.startDate);
        var endDate = new Date(task.endDate);
        var query = mysql.format(queries.EDIT_TASK, [
          task.employeeId,
          task.title,
          task.description,
          startDate,
          endDate,
          id
        ]);

        getEmployeeAvailability(
          {
            employeeId: task.employeeId,
            lastEmployeeId: task.lastEmployeeId,
            taskId: id,
            onBadRequest,
            onSuccess,
            onFailure,
            query,
            startDate,
            endDate,
            successMessage: strings.editTaskSucessfull,
            task
          },
          execReplaceTask,
          onFailure
        );
      }
    });
  }
}

function isEmployeeAvailable(blocks, startDate, endDate) {
  if (!blocks || blocks.length == 0) {
    return false;
  }

  blocks = blocks.filter(
    block => block.date >= startDate && block.date < endDate
  );

  let expectedNumberBlocks = Math.round((endDate - startDate) / 1800000);

  return blocks.length === expectedNumberBlocks;
}

function getEmployees(onSuccess, onFailure) {
  var query = mysql.format(queries.GET_ALL_EMPLOYEES);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    }
    onSuccess(result);
  });
}

function getTaskToEdit(id, onSuccess, onFailure, onNotFound) {
  var query = mysql.format(queries.GET_TASK_BY_ID, [id]);

  connection.query(query, (err, taskResult) => {
    if (err) {
      return onFailure(err);
    } else if (taskResult.length == 0) {
      onNotFound(util.format(strings.dataNotFound, "tarefa"));
    } else {
      query = mysql.format(queries.GET_ALL_EMPLOYEES);

      connection.query(query, (err, employeesResult) => {
        if (err) {
          return onFailure(err);
        }
        onSuccess({ task: taskResult[0], employees: employeesResult });
      });
    }
  });
}

function getTasks(onSuccess, onFailure) {
  var query = mysql.format(queries.GET_ALL_TASKS);

  prepareResultsAndExec(query, onSuccess, onFailure);
}

function prepareResultsAndExec(query, onSuccess, onFailure) {
  var prepareResults = function(result) {
    var preparedResults = [];

    result.forEach(function(e) {
      var employee = {
        id: e.employeeId,
        email: e.email,
        password: e.password,
        lastLogin: utils.convertDate(e.lastLogin),
        archived: e.archived,
        removed: e.removed,
        type: e.type,
        resetPasswordToken: e.resetPasswordToken,
        resetPasswordExpires: utils.convertDate(e.resetPasswordExpires),
        name: e.name,
        photoUrl: e.photoUrl,
        birthdate: utils.convertDate(e.birthdate),
        contact: e.contact,
        address: e.address,
        info: e.info,
        gender: e.gender
      };

      var task = {
        id: e.taskId,
        title: e.title,
        description: e.description,
        comment: e.comment,
        creationDate: utils.convertDate(e.creationDate),
        startDate: utils.convertDate(e.startDate),
        endDate: utils.convertDate(e.endDate),
        finishDate: utils.convertDate(e.finishDate),
        status: e.status,
        employee
      };

      preparedResults.push(task);
    });

    return preparedResults;
  };

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    }
    onSuccess(prepareResults(result));
  });
}

function getEmployeeTasks(id, onSuccess, onFailure) {
  var query = mysql.format(queries.GET_EMPLOYEE_TASKS, [id]);

  prepareResultsAndExec(query, onSuccess, onFailure);
}

function checkFinishTask(body, onSuccess, onFailure, req, res) {
  var err = validateFinishTaskRequest(body.id, body.status, body.comment);

  if (err) {
    return onFailure(httpFunctions.badRequest(err));
  }

  var query = mysql.format(queries.GET_TASK_BY_ID, [body.id]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    if (!result) {
      return onFailure(httpFunctions.badRequest(strings.taskNotFound));
    }

    if (
      !checkTokenOwnAccount(
        permissions.tasks.finish,
        result[0].employeeId,
        req,
        res
      )
    ) {
      return;
    }

    return finishTask(body, onSuccess, onFailure);
  });
}

function finishTask(body, onSuccess, onFailure) {
  var query = mysql.format(queries.FINISH_TASK, [
    body.status,
    body.comment,
    body.id
  ]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    if (result.affectedRows) {
      return onSuccess(true);
    }

    return onFailure(httpFunctions.badRequest(strings.taskNotFound));
  });
}

module.exports = router;
