const Layout = require("./Layout");

class Population {
  constructor(animals, kennels, relationships) {
    this.layouts = [];
    this.animals = animals;
    this.relationships = relationships;
    this.kennels = kennels;
  }

  seed(size) {
    for (var i = 0; i < size; i++) {
      this.addNew();
    }
  }

  addNew() {
    const layout = new Layout();
    layout.randomize(this.animals, this.kennels);
    layout.calculateFitness(this.relationships, this.kennels);

    this.layouts.push(layout);
  }

  getAverageFitness() {
    var sum = 0;

    this.layouts.forEach(l => {
      sum += l.fitness;
    });

    return sum / this.layouts.length;
  }

  getBest() {
    var max = -999;
    var index = -1;

    this.layouts.forEach((l, i) => {
      if (l.fitness > max) {
        index = i;
        max = l.fitness;
      }
    });

    return index;
  }

  getWorst() {
    var min = 999;
    var index = -1;

    this.layouts.forEach((l, i) => {
      if (l.fitness < min) {
        index = i;
        min = l.fitness;
      }
    });

    return index;
  }

  evolve() {
    const toRemove = this.layouts.length / 30;

    // Prune the worst 30%, add new randomized individuals
    for (var i = 0; i < toRemove; i++) {
      const worst = this.getWorst();
      this.layouts.splice(worst, 1);

      this.addNew();
    }

    this.layouts.forEach((l, index) => {
      const child = l.copy();
      child.mutate();
      child.calculateFitness(this.relationships, this.kennels);

      if (child.fitness > l.fitness) {
        this.layouts[index] = child;
      }
    });
  }
}

module.exports = Population;
