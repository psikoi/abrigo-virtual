class Layout {
  constructor(fitness, kennels) {
    this.kennels = kennels ? kennels : [];
    this.fitness = fitness ? fitness : 0;
  }

  randomize(animals, kennels) {
    this.kennels = kennels.map(k => k.copyEmpty());

    var randomInt = function(max) {
      return Math.floor(Math.random() * max);
    };

    animals.forEach(a => {
      var kennel;

      while (!kennel || !kennel.hasRoom()) {
        kennel = this.kennels[randomInt(this.kennels.length)];
      }

      kennel.animals.push(a);
    });
  }

  standardDeviation(values) {
    var average = function(data) {
      var sum = data.reduce((sum, value) => {
        return sum + value;
      }, 0);
      return sum / data.length;
    };

    var avg = average(values);

    var squareDiffs = values.map(function(value) {
      return Math.pow(value - avg, 2);
    });

    var avgSquareDiff = average(squareDiffs);

    return Math.sqrt(avgSquareDiff);
  }

  calculateFitness(relationships, originalLayout) {
    var calculateChanges = () => {
      var changes = [];
      originalLayout.forEach((k, kIndex) => {
        k.animals.forEach(animal => {
          if (!this.kennels[kIndex].contains(animal)) {
            var toIndex = -1;

            this.kennels.forEach((kk, kkIndex) => {
              if (kk.contains(animal)) {
                toIndex = kkIndex;
              }
            });

            changes.push({
              animal,
              from: kIndex + 1,
              to: toIndex + 1
            });
          }
        });
      });
      return changes;
    };

    var calculateScore = () => {
      var sum = 0;

      this.kennels.forEach(k => {
        sum += k.evaluate(relationships);
      });
      return sum;
    };

    var calculateStd = () => {
      const sizes = this.kennels.map(k => k.animals.length);
      return this.standardDeviation(sizes);
    };

    this.std = calculateStd();
    this.changes = calculateChanges();
    this.score = calculateScore();

    var size = this.kennels.length;
    var wScore = this.score / size;
    var wChanges = this.changes.length / size;

    this.fitness = (wScore - this.std - wChanges) * 10;
    this.unbiasedFitness = (wScore - this.std) * 10;
  }

  mutate() {
    const iterations = Math.floor(Math.random() * 5);

    var randomInt = function(max) {
      return Math.floor(Math.random() * max);
    };

    for (var i = 0; i < iterations; i++) {
      var kennelA = this.kennels[randomInt(this.kennels.length)];
      var kennelB;

      while (!kennelB || kennelA === kennelB) {
        kennelB = this.kennels[randomInt(this.kennels.length)];
      }

      if (Math.random() < 0.5) {
        const swapIndex = randomInt(
          Math.min(kennelA.animals.length, kennelB.animals.length)
        );

        if (swapIndex > 0) {
          const animalA = kennelA.animals[swapIndex];
          const animalB = kennelB.animals[swapIndex];

          kennelA.remove(animalA);
          kennelB.remove(animalB);
          kennelA.add(animalB);
          kennelB.add(animalA);
        }
      } else {
        if (kennelA.animals.length === 0) {
          return;
        }

        // Find an available secondary kennel
        var tries = 0;
        while (tries < 50 && (!kennelB.hasRoom() || kennelA === kennelB)) {
          tries++;
          kennelB = this.kennels[randomInt(this.kennels.length)];
        }

        if (kennelB.hasRoom()) {
          const moveIndex = randomInt(kennelA.animals.length);
          const animal = kennelA.animals[moveIndex];

          kennelA.remove(animal);
          kennelB.add(animal);
        }
      }
    }
  }

  copy() {
    const kennels = [];

    this.kennels.forEach(k => {
      kennels.push(k.copy());
    });

    return new Layout(this.fitness, kennels);
  }

  toString() {
    var s = "";

    this.kennels.forEach(k => {
      s += JSON.stringify(k.animals) + " ";
    });

    return s + " - Fitness: " + this.fitness;
  }
}

module.exports = Layout;
