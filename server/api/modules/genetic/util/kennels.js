const Kennel = require("../Kennel");

function reduce(kennels) {
  const reduced = [];

  kennels.forEach(k => {
    const match = find(reduced, k.kennelId);

    if (match) {
      match.animals.push(k.animalId);
    } else {
      const kennel = new Kennel(k.kennelId, k.capacity, [k.animalId]);
      reduced.push(kennel);
    }
  });

  return reduced;
}

function find(reduced, id) {
  var match;

  reduced.forEach(k => {
    if (k.id === id) {
      match = k;
    }
  });

  return match;
}

module.exports.reduce = reduce;
