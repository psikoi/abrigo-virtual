function buildMatrix(animals, relationships) {
  const size = animals.length;
  const matrix = emptyMatrix(size);

  relationships.forEach(relationship => {
    const { animalAId, animalBId, value } = relationship;
    matrix[animalAId - 1][animalBId - 1] = parseInt(value);
    matrix[animalBId - 1][animalAId - 1] = parseInt(value);
  });

  return matrix;
}

function emptyMatrix(size) {
  values = [];
  for (var i = 0; i < size; i++) {
    values.push(new Array(size).fill(0));
  }

  return values;
}

module.exports.buildMatrix = buildMatrix;
