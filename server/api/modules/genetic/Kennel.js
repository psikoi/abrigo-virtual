class Kennel {
  constructor(id, capacity, animals) {
    this.id = id;
    this.capacity = capacity;
    this.animals = animals;
  }

  evaluate(relationships) {
    if (this.animals.length < 2) {
      this.score = 0;
      return this.score;
    }

    var score = 0;

    this.animals.forEach(a => {
      this.animals.forEach(b => {
        if (a !== b) {
          score += relationships[a - 1][b - 1];
        }
      });
    });

    this.score = score / 2;

    return this.score;
  }

  hasRoom() {
    return this.animals.length < this.capacity;
  }

  remove(animal) {
    for (var i = 0; i < this.animals.length; i++) {
      if (this.animals[i] === animal) {
        this.animals.splice(i, 1);
      }
    }
  }

  contains(animal) {
    return this.animals.includes(animal);
  }

  add(animal) {
    this.animals.push(animal);
  }

  copy() {
    const animals = [];

    this.animals.forEach(a => {
      animals.push(a);
    });

    return new Kennel(this.id, this.capacity, animals);
  }

  copyEmpty() {
    return new Kennel(this.id, this.capacity, []);
  }
}

module.exports = Kennel;
