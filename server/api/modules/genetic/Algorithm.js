const { buildMatrix } = require("./util/matrix");
const { reduce } = require("./util/kennels");
const Layout = require("./Layout");
const Population = require("./Population");

const POPULATION_SIZE = 100;

class Algorithm {
  constructor(animals, kennels, relationships) {
    this.animals = animals.map(a => a.animalId);
    this.relationships = buildMatrix(this.animals, relationships);
    this.kennels = reduce(kennels);
    this.generations = 0;

    this.originalLayout = new Layout(0, this.kennels);
    this.originalLayout.calculateFitness(this.relationships, this.kennels);
  }

  run(duration, onFinish) {
    const { animals, kennels, relationships, originalLayout } = this;

    this.population = new Population(animals, kennels, relationships);
    this.population.seed(POPULATION_SIZE);

    const start = new Date().getTime();

    while (new Date().getTime() - start < duration) {
      this.generations++;
      this.population.evolve();
    }

    var best = this.population.layouts[this.population.getBest()];
    best.favourable = originalLayout.unbiasedFitness < best.unbiasedFitness;

    onFinish(best);
  }
}

module.exports = Algorithm;
