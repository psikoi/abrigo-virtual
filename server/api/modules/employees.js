const express = require("express");
const router = express.Router();
const { getConnection } = require("../database/sql/db");
const queries = require("../database/sql/queries");
const httpFunctions = require("../HTTPFunctions.js");
const mysql = require("mysql");
const hash = require("../hashing.js");
const emailServer = require("../emailServer/emailServer.js");
const utils = require("../utils");
const strings = require("../data/strings.json");
const fileManager = require("../fileManager/fileManager.js");
const {
  validateEmployee,
  validateId
} = require("./validations/employeesValidations");
const scheduleAPI = require("./schedule");
const { permissions } = require("./permissions/permissions");
const {
  checkTokenPermission,
  checkTokenOwnAccount,
  getShelterId
} = require("./permissions/authTokenValidation");
const { insert, find } = require("../database/mongo/schemas/user");

var connection;

router.all("*", function(req, res, next) {
  connection = getConnection(req);
  next();
});

// get all employees
router.get("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.employee.viewAll, req, res)) {
    return;
  }

  const extras = req.query.include || [];

  getEmployees(
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    extras
  );
});

router.get("/:id(\\d+)", function(req, res, next) {
  var id = req.params.id;

  if (!checkTokenOwnAccount(permissions.employee.view, id, req, res)) {
    return;
  }

  const extras = req.query.include || [];

  getEmployee(
    id,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function(error) {
      // On failure
      next(error);
    },
    extras
  );
});

router.get("/available/:timeBlocks", function(req, res, next) {
  if (!checkTokenPermission(permissions.employee.viewAvailable, req, res)) {
    return;
  }

  if (!req.params.timeBlocks) {
    next(httpFunctions.badRequest(strings.invalidTimeBlocks));
    return;
  }

  const timeBlocks = JSON.parse(req.params.timeBlocks);

  if (timeBlocks.length === 0) {
    next(httpFunctions.badRequest(strings.invalidTimeBlocks));
    return;
  }

  getAvailableEmployees(
    timeBlocks,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

router.put("/", function(req, res, next) {
  var editInfo = req.body;

  if (
    !checkTokenOwnAccount(
      permissions.employee.edit,
      editInfo.employeeId,
      req,
      res
    )
  ) {
    return;
  }

  editEmployee(
    editInfo,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

router.post("/archive", function(req, res, next) {
  if (!checkTokenPermission(permissions.employee.archive, req, res)) {
    return;
  }

  var archiveInfo = req.body;

  archiveAccount(
    archiveInfo,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

router.post("/reactivate", function(req, res, next) {
  if (!checkTokenPermission(permissions.employee.reactivate, req, res)) {
    return;
  }

  var reactivateInfo = req.body;

  reactivateAccount(
    reactivateInfo,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

router.post("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.employee.create, req, res)) {
    return;
  }

  var newEmployee = req.body;

  createEmployee(
    newEmployee,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    },
    req,
    connection
  );
});

router.put("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.employee.edit, req, res)) {
    return;
  }

  var editInfo = req.body;

  editEmployee(
    editInfo,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    }
  );
});

function getAvailableEmployees(timeBlocks, onSuccess, onFailure) {
  var query = mysql.format(queries.GET_ALL_EMPLOYEES);

  // When schedule endpoints are ready, replace this
  // with actual database data
  var tempSchedule = [
    { day: 1, time: { hours: 7, minutes: 0 } },
    { day: 1, time: { hours: 7, minutes: 30 } },
    { day: 1, time: { hours: 8, minutes: 0 } },
    { day: 1, time: { hours: 8, minutes: 30 } },
    { day: 1, time: { hours: 9, minutes: 0 } },
    { day: 1, time: { hours: 9, minutes: 30 } },
    { day: 1, time: { hours: 10, minutes: 0 } },
    { day: 1, time: { hours: 10, minutes: 30 } }
  ];

  var calculateScore = function(employee) {
    var score = 0;
    tempSchedule.forEach(sBlock => {
      timeBlocks.forEach(tBlock => {
        if (
          sBlock.day === tBlock.day &&
          sBlock.time.hours === tBlock.time.hours &&
          sBlock.time.minutes === tBlock.time.minutes
        ) {
          score++;
        }
      });
    });
    return score;
  };

  var prepareScore = function(employees) {
    employees.forEach(employee => {
      employee.score = Math.random(); //calculateScore(employee);
    });

    employees = employees.filter(e => e.type === "volunteer");

    employees.sort(function(a, b) {
      return b.score - a.score;
    });

    return employees.slice(0, 5);
  };

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    }
    onSuccess(prepareScore(result));
  });
}

function getEmployees(onSuccess, onFailure, extras) {
  var query = mysql.format(queries.GET_ALL_EMPLOYEES);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    }

    var employees = result;

    if (extras && extras.includes("status")) {
      const employeeIds = employees.map(e => e.employeeId);

      // If the api requests the employees' statuses,
      // fetch the statuses and then return the employees
      scheduleAPI.getStatus(
        employeeIds,
        statuses => {
          statuses.forEach((status, index) => {
            // Não me julguem, o .status tá por alguma razão
            // pouco estável, e ao fazer a mesma ação das duas
            // maneiras, já funciona.. não sei
            employees[index]["status"] = status;
            employees[index].status = status;
          });
          onSuccess(employees);
        },
        err => {
          onFailure(err);
        },
        connection
      );
    } else {
      onSuccess(employees);
    }
  });
}

function getEmployee(id, onSuccess, onFailure, extras, externalConnection) {
  var query = mysql.format(queries.GET_EMPLOYEE_BY_ID, [id]);

  var conn = externalConnection || connection;

  conn.query(query, (err, result) => {
    if (err) {
      return onFailure(err);
    } else if (result.length == 0) {
      onFailure(httpFunctions.notFound());
    } else {
      const employee = result[0];

      if (extras && extras.includes("status")) {
        // If the api requests the employee's status,
        // fetch the status and then return the employee
        scheduleAPI.getStatus(
          [id],
          status => {
            employee.status = status[0];
            onSuccess(employee);
          },
          err => {
            onFailure(err);
          },
          conn
        );
      } else {
        onSuccess(employee);
      }
    }
  });
}

function createEmployee(
  newEmployee,
  onSuccess,
  onFailure,
  onBadRequest,
  req,
  conn
) {
  if (conn) {
    connection = conn;
  }

  connection.beginTransaction(function(err) {
    if (err) {
      return onFailure();
    }

    var invalid = validateEmployee(newEmployee);
    if (invalid) {
      onBadRequest(invalid);
    } else {
      find(
        newEmployee.email,
        function() {
          return onBadRequest(strings.shelterAdminEmailAlreadyExists);
        },
        function() {
          var query = mysql.format(queries.CREATE_EMPLOYEE, [
            newEmployee.name,
            newEmployee.photoUrl,
            new Date(newEmployee.birthdate),
            newEmployee.contact,
            newEmployee.address,
            newEmployee.info,
            newEmployee.gender
          ]);

          connection.query(query, (err, result) => {
            if (err) {
              connection.rollback(function() {
                onFailure(err);
              });
            } else
              createAccount(
                result.insertId,
                newEmployee,
                onSuccess,
                onFailure,
                onBadRequest,
                req
              );
          });
        }
      );
    }
  });
}

function createAccount(
  employeeId,
  newEmployee,
  onSuccess,
  onFailure,
  onBadRequest,
  req
) {
  var passwordGenerated = utils.generateRandomPassword();
  var newPassword = hash.hashPassword(passwordGenerated);
  var query = mysql.format(queries.CREATE_ACCOUNT, [
    newEmployee.email,
    newPassword,
    newEmployee.type,
    employeeId
  ]);

  connection.query(query, (err, result) => {
    if (err) {
      let duplicatedEmail = err.message.includes("Duplicate");
      rollback(
        duplicatedEmail ? strings.emailAlreadyInUse : err,
        duplicatedEmail ? onBadRequest : onFailure
      );
    } else
      commitNewAccountTrasaction(
        employeeId,
        newEmployee,
        passwordGenerated,
        onSuccess,
        req
      );
  });
}

function commitNewAccountTrasaction(
  employeeId,
  newEmployee,
  passwordGenerated,
  onSuccess,
  req
) {
  connection.commit(function(err) {
    if (err) {
      rollback(err);
    } else {
      const shelterId = typeof req === "string" ? req : getShelterId(req);
      const email = newEmployee.email;
      // Add the new user to the central MongoDB database
      insert(
        email,
        shelterId,
        () => {
          emailServer.sendNewAccountEmail(newEmployee.email, passwordGenerated);

          fileManager.uploadBase64(
            employeeId,
            newEmployee.image,
            setPhotoUrl,
            onSuccess,
            strings.employeeCreatedSuccessfully
          );
        },
        err => {
          throw err;
        }
      );
    }
  });
}

function rollback(err, onFailure) {
  connection.rollback(function() {
    onFailure(err);
  });
}

function setPhotoUrl(id, result, onSuccess, defaultMessage) {
  if (!result) {
    onSuccess(defaultMessage);
  } else if (result.photoUrl) {
    var query = mysql.format(queries.UPDATE_EMPLOYEE_PHOTOURL_BY_ID, [
      result.photoUrl,
      id
    ]);

    connection.query(query, (err, result) => {
      if (err) {
        onSuccess(defaultMessage + " " + strings.noPhotoUploaded);
      } else {
        onSuccess(defaultMessage);
      }
    });
  } else {
    onSuccess(defaultMessage + " " + strings.noPhotoUploaded);
  }
}

function editEmployee(editInfo, onSuccess, onFailure, onBadRequest) {
  connection.beginTransaction(function(err) {
    if (err) {
      return onFailure();
    }

    var invalid = validateEmployee(editInfo);
    if (invalid) {
      onBadRequest(invalid);
    } else {
      var query = mysql.format(queries.EDIT_EMPLOYEE, [
        editInfo.name,
        new Date(editInfo.birthdate),
        editInfo.contact,
        editInfo.address,
        editInfo.info,
        editInfo.gender,
        editInfo.employeeId
      ]);

      connection.query(query, (err, result) => {
        if (err) {
          connection.rollback(function() {
            onFailure(err);
          });
        } else editAccount(editInfo, onSuccess, onFailure, onBadRequest);
      });
    }
  });
}

function editAccount(editInfo, onSuccess, onFailure, onBadRequest) {
  var query = mysql.format(queries.EDIT_ACCOUNT, [
    editInfo.email,
    editInfo.type,
    editInfo.employeeId
  ]);

  connection.query(query, (err, result) => {
    if (err) {
      let duplicatedEmail = err.message.includes("Duplicate");
      rollback(
        duplicatedEmail ? strings.emailAlreadyInUse : err,
        duplicatedEmail ? onBadRequest : onFailure
      );
    } else {
      connection.commit(function(err) {
        if (err) {
          rollback(err);
        } else {
          fileManager.uploadBase64(
            editInfo.employeeId,
            editInfo.image,
            setPhotoUrl,
            onSuccess,
            strings.employeeEditSuccessfully
          );
        }
      });
    }
  });
}

router.delete("/", function(req, res, next) {
  if (!checkTokenPermission(permissions.employee.delete, req, res)) {
    return;
  }
  var deleteInfo = req.body;

  deleteEmployee(
    deleteInfo,
    function(result) {
      // On success
      res.status(200).json({ data: result });
    },
    function() {
      // On failure
      next(httpFunctions.serverError());
    },
    function(message) {
      // On bad request
      next(httpFunctions.badRequest(message));
    }
  );
});

function deleteEmployee(deleteInfo, onSuccess, onFailure, onBadRequest) {
  connection.beginTransaction(function(err) {
    if (err) {
      return onFailure();
    }

    var invalid = validateId(deleteInfo.id);
    if (invalid) {
      onBadRequest(invalid);
    } else {
      var query = mysql.format(queries.DELETE_EMPLOYEE, [deleteInfo.id]);

      connection.query(query, (err, result) => {
        if (err) {
          connection.rollback(function() {
            onFailure(err);
          });
        } else {
          deleteAccount(deleteInfo, onSuccess, onFailure);
        }
      });
    }
  });
}

function deleteAccount(deleteInfo, onSuccess, onFailure) {
  let deleteMessage = "Funcionário removido " + deleteInfo.id;
  var query = mysql.format(queries.DELETE_ACCOUNT, [
    deleteMessage,
    deleteInfo.id
  ]);

  connection.query(query, (err, result) => {
    if (err) {
      connection.rollback(function() {
        onFailure(err);
      });
    } else commitDeleteAccountTrasaction(onSuccess, onFailure);
  });
}

function commitDeleteAccountTrasaction(onSuccess, onFailure) {
  connection.commit(function(err) {
    if (err) {
      connection.rollback(function() {
        onFailure(err);
      });
    } else {
      onSuccess(strings.employeeDeletedSuccessfully);
    }
  });
}

function archiveAccount(archiveInfo, onSuccess, onFailure, onBadRequest) {
  var query = mysql.format(queries.ARCHIVE_ACCOUT, [archiveInfo.id]);

  var invalid = validateId(archiveInfo.id);
  if (invalid) {
    onBadRequest(invalid);
  } else {
    connection.query(query, (err, result) => {
      if (err) {
        return onFailure(err);
      }
      onSuccess(strings.employeeArchiveSuccessfully);
    });
  }
}

function reactivateAccount(reactivateInfo, onSuccess, onFailure, onBadRequest) {
  var query = mysql.format(queries.REACTIVATE_ACCOUT, [reactivateInfo.id]);

  var invalid = validateId(reactivateInfo.id);
  if (invalid) {
    onBadRequest(invalid);
  } else {
    connection.query(query, (err, result) => {
      if (err) {
        return onFailure(err);
      }
      onSuccess(strings.employeeReactivateSuccessfully);
    });
  }
}

module.exports = router;
module.exports.getEmployee = getEmployee;
module.exports.createEmployee = createEmployee;
