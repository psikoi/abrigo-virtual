const { isInvalidString } = require("./generalValidations");

exports.validatePassword = function(password) {
  const minLength = 8;
  const maxLength = 25;

  if (isInvalidString(password)) {
    return strings.validPasswordRequested;
  } else if (password.length < minLength) {
    return `A palavra-chave não pode ter menos que ${minLength} caracteres.`;
  } else if (password.length > maxLength) {
    return `A palavra-chave não pode ter mais que ${maxLength} caracteres.`;
  }
};
