const { isInvalidString, isEmailValid } = require("./generalValidations");
var strings = require("../../data/strings.json");

exports.validateShelter = function({ name, address }) {
  const maxAddressLength = 160;
  const maxNameLength = 50;

  if (isInvalidString(name)) {
    return strings.shelterNameRequired;
  }

  if (name.length > maxNameLength) {
    return strings.shelterNameTooLong;
  }

  if (isInvalidString(address)) {
    return strings.shelterAddressRequired;
  }

  if (address.length > maxAddressLength) {
    return strings.shelterAddressTooLong;
  }
};

exports.validateUser = function({ name, email }) {
  const maxNameLength = 50;
  const maxEmailStringLength = 100;

  if (isInvalidString(name)) {
    return strings.validNameRequested;
  }

  if (name.length > maxNameLength) {
    return strings.maxEmployeeNameLength;
  }

  if (isInvalidString(email) || !isEmailValid(email)) {
    return strings.validEmailRequested;
  }

  if (email.length > maxEmailStringLength) {
    return strings.maxEmailLength;
  }
};
