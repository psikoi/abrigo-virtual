var strings = require("../../data/strings.json");
const { isInvalidString } = require("./generalValidations");
var util = require("util");

const finishTaskStatus = {
  completed: "completed",
  pending: "pending",
  canceled: "canceled"
};

exports.validateFinishTaskRequest = function(id, status, comment) {
  if (!id) {
    return strings.idNotSentError;
  }

  if (!status) {
    return strings.invalidTaskRequiredFields;
  }

  if (isNaN(id)) {
    return strings.idShouldBeNumber;
  }

  if (
    status != finishTaskStatus.completed &&
    status != finishTaskStatus.canceled
  ) {
    return strings.invalidStatus;
  }

  if (comment && comment.length > 255) {
    return strings.invalidTaskCommentTooLong;
  }
};

exports.validateTask = function(task) {
  const maxTitleLength = 50;
  const maxDescriptionLength = 250;

  if (isInvalidString(task.title)) {
    return util.format(strings.validRequestedNeeded, strings.title);
  }

  if (task.title.length > maxTitleLength) {
    return util.format(strings.maxLength, strings.title, maxTitleLength);
  }

  if (isInvalidString(task.description)) {
    return util.format(strings.validRequestedNeeded, strings.description);
  }

  if (task.description.length > maxDescriptionLength) {
    return util.format(
      strings.maxLength,
      strings.description,
      maxDescriptionLength
    );
  }

  if (!task.employeeId) {
    return util.format(strings.validRequestedNeeded, strings.employees);
  }

  if (isNaN(task.employeeId)) {
    return util.format(strings.validTypeNeeded, strings.employees, "números");
  }

  if (
    task.startDate == null ||
    isNaN(new Date(task.startDate).getDate()) ||
    task.endDate == null ||
    isNaN(new Date(task.endDate).getDate())
  )
    return strings.validDateRequested;

  var startDate = new Date(task.startDate).getTime();
  var endDate = new Date(task.endDate).getTime();
  var currDate = Date.now();

  if (startDate < currDate || endDate < currDate || endDate < startDate)
    return strings.validDateRequested;
};
