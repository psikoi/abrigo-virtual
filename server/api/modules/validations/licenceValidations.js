var strings = require("../../data/strings.json");
const { isInvalidString } = require("./generalValidations");
var util = require("util");

function isLicenseTypeValid(type) {
  if (!type) {
    return false;
  }

  var typeFormated = type.toLowerCase();
  return typeFormated === "plus" || typeFormated === "basic";
}

exports.validateLicense = function(license) {
  if (!isLicenseTypeValid(license.type)) {
    return util.format(strings.validFieldRequested, strings.licenseType);
  }
};
