exports.isInvalidString = function(str) {
  return !str || str == null || str === "";
};

exports.isEmailValid = function(email) {
  var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return regex.test(String(email).toLowerCase());
};

exports.validate = function(condition, message, onBadRequest, rollback) {
  if (condition) {
    onBadRequest(message);
    if (rollback) {
      rollback();
    }
    return;
  }
};
