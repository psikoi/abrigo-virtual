exports.isGenderValid = function(gender) {
  if (!gender) {
    return false;
  }

  var genderFormated = gender.toLowerCase();
  return (
    genderFormated === "male" ||
    genderFormated === "female" ||
    genderFormated === "other"
  );
};
