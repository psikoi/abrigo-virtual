const { isInvalidString } = require("./generalValidations");
const { isGenderValid } = require("./genderValidations");
var strings = require("../../data/strings.json");
var util = require("util");

function isAnimalTypeValid(type) {
  if (!type) {
    return false;
  }

  var typeFormated = type.toLowerCase();
  return (
    typeFormated === "dog" || typeFormated === "cat" || typeFormated === "other"
  );
}

function isAnimalSizeValid(size) {
  if (!size) {
    return false;
  }

  var sizeFormated = size.toLowerCase();
  return (
    sizeFormated === "micro" ||
    sizeFormated === "small" ||
    sizeFormated === "medium" ||
    sizeFormated === "big" ||
    sizeFormated === "veryBig" ||
    sizeFormated === "other"
  );
}

exports.validateAnimal = function(animal) {
  const maxNameLength = 50;
  const maxBreedLength = 50;
  const maxInfoStringLength = 255;

  if (isInvalidString(animal.name)) {
    return strings.validNameRequested;
  }

  if (animal.name.length > maxNameLength) {
    return util.format(strings.maxLength, strings.name, maxNameLength);
  }

  if (isInvalidString(animal.breed)) {
    return util.format(strings.validFieldRequested, strings.breed);
  }

  if (animal.breed.length > maxBreedLength) {
    return util.format(strings.maxLength, strings.breed, maxBreedLength);
  }

  if (!isAnimalSizeValid(animal.size)) {
    return util.format(strings.validFieldRequested, strings.size);
  }

  if (!isGenderValid(animal.gender)) {
    return strings.validGenderRequested;
  }

  if (!isAnimalTypeValid(animal.type)) {
    return util.format(strings.validTypeRequested, strings.animal);
  }

  if (animal.birthdate == null || isNaN(new Date(animal.birthdate).getDate()))
    return strings.validDateRequested;

  var animalBirthdate = new Date(animal.birthdate);
  var currDate = Date.now();

  if (animalBirthdate >= currDate) return strings.validBirthdateRequested;

  if (!isGenderValid(animal.gender)) {
    return strings.validGenderRequested;
  }

  if (animal.info && animal.info.length > maxInfoStringLength) {
    return strings.maxInfoLength;
  }

  if (animal.photoUrl && animal.photoUrl.length > 250) {
    return strings.photoUrlInvalid;
  }
};
