const strings = require("../../data/strings.json");

exports.validateReinforcement = function(reinforcement) {
  const startDate = new Date(reinforcement.startDate);
  const endDate = new Date(reinforcement.endDate);
  const curDate = new Date();

  if (!startDate || startDate == null || startDate < curDate) {
    return strings.invalidStartDate;
  }

  if (!endDate || endDate == null || endDate < curDate || endDate < startDate) {
    return strings.invalidEndDate;
  }
};
