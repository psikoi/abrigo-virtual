const { isInvalidString } = require("./generalValidations");
var strings = require("../../data/strings.json");

exports.validateKennelId = function(id) {
  if (!id || id === "" || id < 0) {
    return strings.invalidKennelId;
  }
};

exports.validateKennel = function(kennel) {
  const maxKennelNameLength = 15;

  if (isInvalidString(kennel.name)) {
    return strings.validKennelNameRequested;
  }

  if (kennel.name.length > maxKennelNameLength) {
    return strings.maxKennelNameLength;
  }

  if (kennel.capacity == null || kennel.capacity == "" || kennel.capacity < 0) {
    return strings.kennelCapacityInvalid;
  }

  if (kennel.capacity < kennel.animals.length) {
    return strings.kennelLimitCapacity;
  }
};
