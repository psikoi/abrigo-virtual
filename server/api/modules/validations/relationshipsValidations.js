var strings = require("../../data/strings.json");

exports.validateRelationship = function(relationship) {
  if (!relationship) {
    return strings.noRelationshipSent;
  }

  if (!relationship.animalAId || !relationship.animalBId) {
    return strings.relationshipAnimalsNotSent;
  }

  if (!relationship.value) {
    return strings.relationshipValueNotSent;
  }

  if (relationship.animalAId === relationship.animalBId) {
    return strings.relationshipSameAnimalId;
  }

  if (isNaN(relationship.value)) {
    return strings.relationshipValueNotANumber;
  }
};
