const { isInvalidString, isEmailValid } = require("./generalValidations");
const { isGenderValid } = require("./genderValidations");
var strings = require("../../data/strings.json");

function isEmployeeTypeValid(type) {
  if (!type) {
    return false;
  }

  var typeFormated = type.toLowerCase();
  return (
    typeFormated === "admin" ||
    typeFormated === "employee" ||
    typeFormated === "volunteer"
  );
}

function isValidContact(contact) {
  return (
    /^\+? ([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/.test(contact) ||
    /^\+?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/.test(contact) ||
    /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3,})$/.test(contact)
  );
}

exports.validateId = function(id) {
  if (!id || id === "" || id < 0) {
    return strings.invalidEmployeeId;
  }
};

exports.validateEmployee = function(employee) {
  const maxAddressLength = 160;
  const maxNameLength = 50;
  const maxContactLength = 15;
  const minContactLength = 9;
  const maxEmailStringLength = 100;
  const maxInfoStringLength = 250;

  if (isInvalidString(employee.name)) {
    return strings.validNameRequested;
  }

  if (employee.name.length > maxNameLength) {
    return strings.maxEmployeeNameLength;
  }

  if (isInvalidString(employee.email) || !isEmailValid(employee.email)) {
    return strings.validEmailRequested;
  }

  if (employee.email.length > maxEmailStringLength)
    return strings.maxEmailLength;

  if (!isGenderValid(employee.gender)) {
    return strings.validGenderRequested;
  }

  if (!isEmployeeTypeValid(employee.type)) {
    return strings.employeeTypeValidRequested;
  }

  if (isInvalidString(employee.contact) || !isValidContact(employee.contact)) {
    return strings.validContactRequested;
  }

  if (employee.contact.length > maxContactLength) {
    return strings.maxContactLength;
  }

  if (employee.contact.length < minContactLength) {
    return strings.minContactLength;
  }

  if (
    employee.birthdate == null ||
    isNaN(new Date(employee.birthdate).getDate())
  )
    return strings.validDateRequested;

  var employeeBirthdate = new Date(employee.birthdate);
  var currDate = Date.now();

  if (employeeBirthdate >= currDate) return strings.validBirthdateRequested;

  var ageDiffMs = currDate - employeeBirthdate.getTime();
  var ageDate = new Date(ageDiffMs);

  if (Math.abs(ageDate.getUTCFullYear() - 1970) < 10) return strings.minAge;

  if (isInvalidString(employee.address)) {
    return strings.validAddressRequested;
  }

  if (employee.address.length > maxAddressLength) {
    return strings.maxAddressLength;
  }

  if (!isGenderValid(employee.gender)) {
    return strings.validGenderRequested;
  }

  if (employee.info && employee.info.length > maxInfoStringLength) {
    return strings.maxInfoLength;
  }

  if (employee.photoUrl && employee.photoUrl.length > 250) {
    return strings.photoUrlInvalid;
  }
};
