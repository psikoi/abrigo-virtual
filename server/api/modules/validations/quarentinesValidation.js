const { isInvalidString } = require("./generalValidations");
var strings = require("../../data/strings.json");
var util = require("util");

const finishQuarentineStatus = {
  finished: "finished",
  canceled: "canceled"
};

isCauseValid = function(cause) {
  if (!cause) {
    return false;
  }

  var causeFormated = cause.toLowerCase();
  return (
    causeFormated === "sick" ||
    causeFormated === "vaccinated" ||
    causeFormated === "medicalwatch" ||
    causeFormated === "fight" ||
    causeFormated === "undefined"
  );
};

exports.validateQuarentine = function(quarentine) {
  const maxInfoLength = 255;

  if (isInvalidString(quarentine.info)) {
    return util.format(strings.validRequestedNeeded, strings.info);
  }

  if (quarentine.info.length > maxInfoLength) {
    return util.format(strings.maxLength, strings.info, maxInfoLength);
  }

  if (!quarentine.animalId) {
    return util.format(strings.validRequestedNeeded, strings.animal);
  }

  if (isNaN(quarentine.animalId)) {
    return util.format(strings.validTypeNeeded, strings.animal, "números");
  }

  if (!isCauseValid(quarentine.cause)) {
    return util.format(strings.validTypeRequested, strings.cause);
  }
};

exports.validateFinishQuarentineRequest = function(id, status) {
  if (!id) {
    return strings.idNotSentError;
  }

  if (!status) {
    return strings.invalidTaskRequiredFields;
  }

  if (isNaN(id)) {
    return strings.idShouldBeNumber;
  }

  if (
    status != finishQuarentineStatus.finished &&
    status != finishQuarentineStatus.canceled
  ) {
    return strings.invalidStatus;
  }
};
