const express = require("express");
const router = express.Router();
const { getConnection } = require("../database/sql/db");
const queries = require("../database/sql/queries");
const strings = require("../data/strings.json");
const mysql = require("mysql");
const httpFunctions = require("../HTTPFunctions.js");
const { permissions } = require("./permissions/permissions");
const {
  checkTokenPermission,
  checkTokenPermissionIgnoringAdmin,
  checkTokenOwnAccount
} = require("./permissions/authTokenValidation");
const emailServer = require("../emailServer/emailServer");

var connection;

router.all("*", function(req, res, next) {
  connection = getConnection(req);
  next();
});

router.post("/", function(req, res, next) {
  var type = req.query.type || "";
  var hasPermissions;
  if (type === "availability") {
    hasPermissions = checkTokenPermissionIgnoringAdmin(
      permissions.employee.scheduleAvailableAction,
      req,
      res
    );
  } else {
    hasPermissions = checkTokenPermission(
      permissions.employee.scheduleAction,
      req,
      res
    );
  }

  if (!hasPermissions) {
    return;
  }

  checkSchedule(
    req,
    res,
    function(result) {
      // On success
      res.status(200).send();
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

router.post("/availability", function(req, res, next) {
  if (!checkTokenPermission(permissions.schedule.getAvailability, req, res)) {
    return;
  }

  getEmployeeAvailability(
    req.body,
    function(result) {
      // On success
      res.status(200).send(result);
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

function checkSchedule(req, res, onSuccess, onFailure) {
  if (req.body === undefined || req.body.length === 0) {
    return onFailure(httpFunctions.badRequest(strings.noScheduleSent));
  }

  var employeeId = req.body[0].employeeId;
  var type = req.query.type;
  var query = mysql.format(queries.GET_SCHEDULE_BY_EMPLOYEE, [
    employeeId,
    type
  ]);
  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }
    return createSchedule(req, res, result, onSuccess, onFailure);
  });
}

function createSchedule(req, res, result, onSuccess, onFailure) {
  var shouldDelete = false;

  if (result !== undefined && result.length) {
    shouldDelete = true;
  }
  connection.beginTransaction(function(err) {
    if (err) {
      return onFailure(err);
    }

    if (shouldDelete) {
      return deleteThenAddSchedule(req, res, onSuccess, onFailure);
    } else {
      return addSchedule(req, res, onSuccess, onFailure);
    }
  });
}

function deleteThenAddSchedule(req, res, onSuccess, onFailure) {
  var employeeId = req.body[0].employeeId;
  var type = req.query.type;
  var query = mysql.format(queries.DELETE_SCHEDULE, [employeeId, type]);
  connection.query(query, (err, result) => {
    if (err) {
      return dbRollback(onFailure, err);
    }
    return addSchedule(req, res, onSuccess, onFailure);
  });
}

function addSchedule(req, res, onSuccess, onFailure) {
  var sqlArray = [];
  var employeeId;
  req.body.forEach(element => {
    if (!employeeId) {
      employeeId = element.employeeId;
    }

    var date = element.date ? new Date(element.date).getTime() : element.day;

    sqlArray.push([
      date,
      element.time.hours,
      element.time.minutes,
      element.employeeId,
      req.query.type
    ]);
  });

  var query = queries.CREATE_SCHEDULE;

  connection.query(query, [sqlArray], (err, result) => {
    if (err) {
      return dbRollback(onFailure, httpFunctions.serverError());
    }
    sendChangeScheduleEmail(res, employeeId);
    return commitTransaction(result, onSuccess, onFailure);
  });
}

function dbRollback(callback, param) {
  connection.rollback(function() {
    return callback(param);
  });
}

function commitTransaction(result, onSuccess, onFailure) {
  connection.commit(function(err) {
    if (err) {
      return dbRollback(onFailure, err);
    } else {
      return onSuccess(result);
    }
  });
}

function sendChangeScheduleEmail(res, employeeId) {
  var query = queries.GET_EMPLOYEE_BY_ID;

  connection.query(query, [employeeId], (err, result) => {
    emailServer.sendChangeSchedule(res, result[0]);
  });
}

function getScheduleByEmployee(employeeId, type, onSuccess, onFailure) {
  var query = mysql.format(queries.GET_EMPLOYEE_BY_ID, [employeeId]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    if (result === undefined || !result.length) {
      return onFailure(httpFunctions.notFound(strings.accountDoesntExist));
    }

    return fetchSchedule(employeeId, type, onSuccess, onFailure);
  });
}

function fetchSchedule(
  employeeId,
  type,
  onSuccess,
  onFailure,
  externalConnection
) {
  var query = mysql.format(queries.GET_SCHEDULE_BY_EMPLOYEE, [
    employeeId,
    type
  ]);

  const conn = externalConnection || connection;

  conn.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }
    return onSuccess(result);
  });
}

router.get("/:id", function(req, res, next) {
  var employeeId = parseInt(req.params.id);
  var type = req.query.type;

  if (
    !checkTokenOwnAccount(
      type === "full"
        ? permissions.employee.viewSchedule
        : type === "partial"
        ? permissions.employee.viewPartialSchedule
        : permissions.employee.viewAvailableSchedule,
      employeeId,
      req,
      res
    )
  ) {
    return;
  }

  getScheduleByEmployee(
    employeeId,
    type,
    function(result) {
      // On success
      res.status(200).send(result);
    },
    function(error) {
      // On failure
      next(error);
    }
  );
});

function isAvailable(employeeId, onSuccess, onFailure, externalConnection) {
  var checkPartial = function(blocks) {
    var found = false;

    blocks.forEach(({ date, hours, minutes }) => {
      var blockStart = new Date(Date.parse(`${date} ${hours}:${minutes}:00`));
      var blockEnd = new Date(blockStart.getTime() + 1000 * 60 * 30);
      var currentDate = new Date();

      if (blockStart < currentDate && blockEnd > currentDate) {
        found = true;
      }
    });

    return found;
  };

  var checkFull = function(blocks) {
    if (!blocks || blocks.length === 0) {
      return false;
    }

    var found = false;

    blocks.forEach(({ date, hours, minutes }) => {
      var currentDate = new Date();
      var dayOfWeek = currentDate.getDay();

      if (date == dayOfWeek) {
        var fakeDate = new Date();
        fakeDate.setHours(hours);
        fakeDate.setMinutes(minutes);
        fakeDate.setSeconds(0);

        var fakeDateEnd = new Date(fakeDate.getTime() + 1000 * 60 * 30);

        if (currentDate > fakeDate && currentDate < fakeDateEnd) {
          found = true;
        }
      }
    });

    return found;
  };

  fetchSchedule(
    employeeId,
    "partial",
    blocks => {
      if (blocks.length > 0) {
        if (checkPartial(blocks)) {
          onSuccess(1);
        }
      }

      fetchSchedule(
        employeeId,
        "full",
        blocks => {
          onSuccess(checkFull(blocks) ? 1 : 0);
        },
        err => {
          onFailure(err);
        },
        externalConnection
      );
    },
    err => {
      onFailure(err);
    },
    externalConnection
  );
}

function getStatus(employeeIds, onSuccess, onFailure, externalConnection) {
  if (!employeeIds) {
    return onFailure();
  }

  var statuses = [];

  employeeIds.forEach((id, index) => {
    isAvailable(
      id,
      status => {
        statuses[index] = status;
        if (index === employeeIds.length - 1) {
          onSuccess(statuses);
        }
      },
      () => {
        onFailure();
      },
      externalConnection
    );
  });
}

function getEmployeeAvailability(parameters, onSuccess, onFailure) {
  /* Parse a block to date */
  var parseBlockToDate = function(startDate, block) {
    const { hours, minutes } = block;
    const day = block.date;
    var date = new Date(startDate);

    if (isNaN(day)) {
      date = new Date(day);
    } else {
      date = new Date(
        date.setDate(
          date.getDate() + ((parseInt(day) + (7 - date.getDay())) % 7)
        )
      );
    }

    date.setHours(hours);
    date.setMinutes(minutes);

    return { date, hours, minutes };
  };

  /* Get next dates until the endDate */
  var getNextDates = function(blocks, block, endDate) {
    var date = block.date;
    var hours = block.hours;
    var minutes = block.minutes;

    for (date; date <= endDate; new Date(date.setDate(date.getDate() + 7))) {
      blocks.push({ date: new Date(date), hours, minutes });
    }
    return blocks;
  };

  /* execute queries*/
  var blocks = [];
  var { employeeId, startDate, endDate } = parameters;
  var query = mysql.format(queries.GET_SCHEDULE_BY_EMPLOYEE_WITHOUT_TYPE, [
    employeeId
  ]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    startDate = startDate ? new Date(startDate) : new Date();
    endDate = endDate
      ? new Date(endDate)
      : new Date().setDate(new Date().getDate() + 365);

    result.forEach(block => {
      let parsedBlock = parseBlockToDate(startDate, block);
      if (!isNaN(block.date)) {
        blocks = getNextDates(blocks, parsedBlock, endDate);
      } else {
        blocks.push(parsedBlock);
      }
    });

    return removeBlocksOccupied(blocks, onSuccess, onFailure, parameters);
  });
}

function removeBlocksOccupied(blocks, onSuccess, onFailure, parameters) {
  var query = mysql.format(queries.GET_EMPLOYEE_TASKS_PENDING, [
    parameters.employeeId
  ]);

  connection.query(query, (err, result) => {
    if (err) {
      return onFailure(httpFunctions.serverError());
    }

    result.forEach(task => {
      if (task.taskId !== parseInt(parameters.taskId)) {
        var startDate = new Date(task.startDate);
        var endDate = new Date(task.endDate);
        blocks = blocks.filter(
          block => block.date < startDate || block.date >= endDate
        );
      }
    });

    onSuccess(blocks, parameters);
  });
}
module.exports = router;
module.exports.getEmployeeAvailability = getEmployeeAvailability;
module.exports.getStatus = getStatus;
