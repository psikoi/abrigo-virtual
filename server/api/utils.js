exports.capitalize = function(s) {
  return s.charAt(0).toUpperCase() + s.slice(1);
};

exports.convertDate = function(string) {
  return string ? new Date(Date.parse(string)) : null;
};

exports.generateRandomPassword = function() {
  var getRandomNumberString = function(min, max) {
    return Math.floor(Math.random() * max) + min;
  };

  var password = "";

  const numberSections = 3;
  const numbersInSections = 3;
  const minDigit = 0;
  const maxDigit = 10; //maxDigit is exclusive.
  var sectionSeparator = "-";

  for (let i = 0; i < numberSections; i++) {
    for (let x = 0; x < numbersInSections; x++) {
      password += getRandomNumberString(minDigit, maxDigit);
    }

    //Last section
    if (i == numbersInSections - 1) {
      password += getRandomNumberString(minDigit, maxDigit);
      break;
    }

    password += sectionSeparator;
  }

  return password;
};

exports.getMonthString = function(month) {
  switch (month) {
    case 0:
      return "Janeiro";
    case 1:
      return "Fevereiro";
    case 2:
      return "Março";
    case 3:
      return "Abril";
    case 4:
      return "Maio";
    case 5:
      return "Junho";
    case 6:
      return "Julho";
    case 7:
      return "Agosto";
    case 8:
      return "Setembro";
    case 9:
      return "Outubro";
    case 10:
      return "Novembro";
    case 11:
      return "Dezembro";
    default:
      return "";
  }
};

exports.translateStatus = function(status) {
  switch (status) {
    case "completed":
      return "Concluida";
    case "canceled":
      return "Cancelada";
    default:
      return "Pendente";
  }
};

exports.convertToColor = function(status) {
  switch (status) {
    case "completed":
      return "#7bbb79";
    case "canceled":
      return "#bb4040";
    default:
      return "#eaaf0f";
  }
};

exports.dateHourString = function(date) {
  var minutes = date.getMinutes();

  if (minutes == "0") {
    minutes += "0";
  }
  return `${date.getHours()}:${minutes}`;
};

exports.formatTime = function(date) {
  return appendZero(date.getHours()) + ":" + appendZero(date.getMinutes());
};

exports.formatDateLong = function(date, extended) {
  if (extended) {
    return formatDate(date, extended) + " " + date.getFullYear();
  }

  return formatDate(date, extended) + "/" + date.getFullYear();
};

function appendZero(num) {
  return num < 10 ? "0" + num : num;
}

function formatDate(date, extended) {
  if (extended) {
    return date.getDate() + " " + months[date.getMonth()];
  }

  return appendZero(date.getDate()) + "/" + appendZero(date.getMonth() + 1);
}

module.exports.appendZero = appendZero;
module.exports.formatDate = formatDate;
