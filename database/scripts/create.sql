CREATE TABLE `accounts`(
`accountId` BIGINT AUTO_INCREMENT ,
`email` VARCHAR(100) UNIQUE NOT NULL,
`password` VARCHAR(200) NOT NULL,
`lastLogin` DATETIME,
`archived` BOOL,
`removed` BOOL,
`type` ENUM('admin','employee','volunteer'),
`resetPasswordToken` VARCHAR(50),
`resetPasswordExpires` DATE,
`employeeId` BIGINT NOT NULL,
PRIMARY KEY (`accountId`));

CREATE TABLE `employees`(
`employeeId` BIGINT AUTO_INCREMENT ,
`name` VARCHAR(50) NOT NULL,
`photoUrl` VARCHAR(250),
`birthdate` DATE NOT NULL,
`contact` VARCHAR(15) NOT NULL,
`address` VARCHAR(255) NOT NULL,
`info` VARCHAR(255),
`gender` ENUM('male','female','other'),
PRIMARY KEY (`employeeId`));

CREATE TABLE `reinforcement_requests`(
`reinforcementRequestId` BIGINT AUTO_INCREMENT,
`employeeId` BIGINT NOT NULL,
`status` ENUM('accepted', 'declined', 'pending', 'canceled'),
`startDate` DATETIME NOT NULL,
`endDate` DATETIME NOT NULL,
`replyDate` DATETIME,
`token` VARCHAR(50),
`tokenExpiration` DATE,
`byEmployeeId` BIGINT NOT NULl,
PRIMARY KEY (`reinforcementRequestId`));

CREATE TABLE `tasks`(
`taskId` BIGINT AUTO_INCREMENT,
`employeeId` BIGINT NOT NULL,
`title` VARCHAR(50) NOT NULL,
`description` VARCHAR(255) NOT NULL,
`comment` VARCHAR(255),
`creationDate` DATETIME NOT NULL,
`startDate` DATETIME NOT NULL,
`endDate` DATETIME NOT NULL,
`finishDate` DATETIME,
`status` ENUM('completed','pending','canceled'),
PRIMARY KEY (taskId));

CREATE TABLE `schedule` (
`scheduleId` BIGINT AUTO_INCREMENT NOT NULL,
`date` LONG NOT NULL,
`hours` INT NOT NULL,
`minutes` INT NOT NULL,
`type` ENUM('full','partial','availability') NOT NULL,
`employeeId` BIGINT NOT NULL,
PRIMARY KEY (scheduleId));

CREATE TABLE `notifications` (
`notificationId` BIGINT AUTO_INCREMENT NOT NULL,
`creationDate` DATETIME NOT NULL,
`readDate` DATETIME,
`message` VARCHAR(255) NOT NULL,
`url` VARCHAR(100),
`employeeId` BIGINT NOT NULL,
PRIMARY KEY (notificationId));

CREATE TABLE `animals`(
`animalId` BIGINT AUTO_INCREMENT,
`name` VARCHAR(50) NOT NULL,
`photoUrl` VARCHAR(250),
`birthdate` DATE NOT NULL,
`breed` VARCHAR(50) NOT NULL,
`size` ENUM('micro','small','medium','big', 'veryBig', 'other'),
`info` VARCHAR(255),
`gender` ENUM('male','female','other'),
`registerDate` DATETIME,
`removed` BOOL DEFAULT FALSE,
`type` ENUM('dog','cat'),
PRIMARY KEY (`animalId`));

CREATE TABLE `quarentines`(
`quarentineId` BIGINT AUTO_INCREMENT ,
`startDate` DATE NOT NULL,
`endDate` DATE,
`status` ENUM('finished','pending','canceled'),
`info` VARCHAR(255),
`cause` VARCHAR(50) NOT NULL,
`animalId` BIGINT NOT NULL,
`removed` BOOL DEFAULT FALSE,
PRIMARY KEY (`quarentineId`));

CREATE TABLE `relationships`(
`relationshipId` BIGINT AUTO_INCREMENT,
`animalAId` BIGINT NOT NULL,
`animalBId` BIGINT NOT NULL,
`value` VARCHAR(15) NOT NULL,
PRIMARY KEY (`relationshipId`));
    
CREATE TABLE `kennels`(
`kennelId` BIGINT AUTO_INCREMENT AUTO_INCREMENT,
`capacity` INT NOT NULL,
`name` VARCHAR(15)  NOT NULL,
PRIMARY KEY (`kennelId`));

CREATE TABLE `kennels_animals`(
`kennelAnimalsId` BIGINT NOT NULL AUTO_INCREMENT,
`kennelId` BIGINT NOT NULL,
`animalId` BIGINT NOT NULL,
PRIMARY KEY (`kennelAnimalsId`));

CREATE TABLE `shelters`(
`shelterId` BIGINT AUTO_INCREMENT,
`name` VARCHAR(50) NOT NULL,
`address` VARCHAR(255) NOT NULL,
`contact` VARCHAR(15) NOT NULL,
`licenseType` ENUM('premium','standard'),
`lastUpdateDate` DATETIME,
PRIMARY KEY (`shelterId`));

CREATE TABLE `permissions`(
`permissionId` BIGINT AUTO_INCREMENT,
`shelterId` BIGINT NOT NULL,
`name` VARCHAR(50) NOT NULL,
`allowed` BOOL DEFAULT FALSE,
PRIMARY KEY (`permissionId`));

ALTER TABLE `accounts`
ADD CONSTRAINT `account_employee`
FOREIGN KEY (`employeeId`) REFERENCES `employees`(`employeeId`);

ALTER TABLE `reinforcement_requests`
ADD CONSTRAINT `reinforcement_employee`
FOREIGN KEY (`employeeId`) REFERENCES `employees`(`employeeId`);

ALTER TABLE `reinforcement_requests`
ADD CONSTRAINT `reinforcement_byEmployee`
FOREIGN KEY (`byEmployeeId`) REFERENCES `employees`(`employeeId`);

ALTER TABLE `tasks`
ADD CONSTRAINT `tasks_employees`
FOREIGN KEY (`employeeId`) REFERENCES `employees`(`employeeId`);

ALTER TABLE `schedule`
ADD CONSTRAINT `schedule_employee`
FOREIGN KEY (`employeeId`) REFERENCES `employees`(`employeeId`);

ALTER TABLE `notifications`
ADD CONSTRAINT `notifications_employee`
FOREIGN KEY (`employeeId`) REFERENCES `employees`(`employeeId`);

ALTER TABLE `quarentines`
ADD CONSTRAINT `quarentines_animal`
FOREIGN KEY (`animalId`) REFERENCES `animals`(`animalId`);

ALTER TABLE `relationships`
ADD CONSTRAINT `relationships_animalA`
FOREIGN KEY (`animalAId`) REFERENCES `animals`(`animalId`);

ALTER TABLE `relationships`
ADD CONSTRAINT `relationships_animalB`
FOREIGN KEY (`animalBId`) REFERENCES `animals`(`animalId`);

ALTER TABLE `kennels_animals`
ADD CONSTRAINT `animals_kennel`
FOREIGN KEY (`kennelId`) REFERENCES `kennels`(`kennelId`);

ALTER TABLE `kennels_animals`
ADD CONSTRAINT `kennels_animal`
FOREIGN KEY (`animalId`) REFERENCES `animals`(`animalId`);

ALTER TABLE `permissions`
ADD CONSTRAINT `permissions_shelter`
FOREIGN KEY (`shelterId`) REFERENCES `shelters`(`shelterId`);
