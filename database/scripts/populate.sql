INSERT INTO `employees`(`name`, `birthdate`, `contact`, `address`, `gender`) 
VALUE('Admin', '1974-04-25', '931627845', 'Rua Geronimo de Sousa', 'male');

INSERT INTO `employees`(`name`, `birthdate`, `contact`,`address`, `info`, `gender`) 
VALUE('Adilson Soares','1989-07-03', '912134683','Rua luisa Todi', 'Alergico a bananas','female');

INSERT INTO `employees`(`name`, `photoUrl`, `birthdate`, `contact`,`address`, `info`, `gender`) 
VALUE('Rui Nunes', 'https://i.imgur.com/mu5y0Pw.jpg','1989-07-03', '912134683','Rua luisa Todi', 'Alergico a bananas','female');

INSERT INTO `employees`(`name`, `photoUrl`, `birthdate`, `contact`, `address`, `gender`) 
VALUE('Hugo Ferreira', 'https://i.imgur.com/5PCU4SD.jpg', '1996-12-06', '923647318', 'Rua da escola primaria', 'male');

INSERT INTO `employees`(`name`, `photoUrl`, `birthdate`, `contact`, `address`, `gender`) 
VALUE('Paula Candeias','https://scontent.flis5-1.fna.fbcdn.net/v/t1.0-9/57280448_10215743980684770_667707328211976192_n.jpg?_nc_cat=105&_nc_ht=scontent.flis5-1.fna&oh=4e101be9889cfcae48a1dde81fb2f5e2&oe=5D2E1E1B','1963-5-5','966615478','Rua da Filosofia','female');

INSERT INTO `employees`(`name`, `photoUrl`, `birthdate`, `contact`, `address`, `gender`) 
VALUE('Luis Cardoso', 'https://scontent.flis5-1.fna.fbcdn.net/v/t1.0-9/58461274_385503325510938_1161950762813620224_n.jpg?_nc_cat=106&_nc_ht=scontent.flis5-1.fna&oh=3959c08072adffb5e20321f4d9f0e12b&oe=5D6D8AEE', '1953-12-06', '965326586', 'Rua da Dom Afonso Henriques', 'male');

INSERT INTO `employees`(`name`, `photoUrl`, `birthdate`, `contact`, `address`, `gender`) 
VALUE('Anabela Pereira', 'https://scontent.flis5-1.fna.fbcdn.net/v/t1.0-9/56500743_10211101729737386_2480320807163658240_n.jpg?_nc_cat=101&_nc_ht=scontent.flis5-1.fna&oh=11ed5cc522a2563b873ad6c3bf867c49&oe=5D3D85C7', '1970-12-06', '931456975', 'Rua Rui de Pina', 'female');

INSERT INTO `accounts`(`email`, `password`, `lastLogin`, `archived`, `removed`, `type`, `employeeId`)
VALUE('admin@admin.pt', '$2b$10$5s2x0ehPr4qx7N6IBpYpyuCQyhJ9Osdf7dkH3zvuHTkltQQ0YUgCi', CURDATE(), false, false, 'admin', 1);

INSERT INTO `accounts`(`email`, `password`, `lastLogin`, `archived`, `removed`, `type`, `employeeId`)
VALUE('adilson@gmail.pt', '$2b$10$5s2x0ehPr4qx7N6IBpYpyuCQyhJ9Osdf7dkH3zvuHTkltQQ0YUgCi', CURDATE(), false, false, 'employee', 2);

INSERT INTO `accounts`(`email`, `password`, `lastLogin`, `archived`, `removed`, `type`, `employeeId`)
VALUE('rui@email.pt', '$2b$10$5s2x0ehPr4qx7N6IBpYpyuCQyhJ9Osdf7dkH3zvuHTkltQQ0YUgCi', CURDATE(), false, false, 'employee', 3);

INSERT INTO `accounts`(`email`, `password`, `lastLogin`, `archived`, `removed`, `type`, `employeeId`)
VALUE('tiago.afsantos@hotmail.com', '$2b$10$5s2x0ehPr4qx7N6IBpYpyuCQyhJ9Osdf7dkH3zvuHTkltQQ0YUgCi', CURDATE(), false, false, 'volunteer', 4);

INSERT INTO `accounts`(`email`, `password`, `lastLogin`, `archived`, `removed`, `type`, `employeeId`)
VALUE('paulaCandeias@gmail.pt', '$2b$10$5s2x0ehPr4qx7N6IBpYpyuCQyhJ9Osdf7dkH3zvuHTkltQQ0YUgCi', CURDATE(), false, false, 'employee', 5);

INSERT INTO `accounts`(`email`, `password`, `lastLogin`, `archived`, `removed`, `type`, `employeeId`)
VALUE('luisCardoso@gmail.pt', '$2b$10$5s2x0ehPr4qx7N6IBpYpyuCQyhJ9Osdf7dkH3zvuHTkltQQ0YUgCi', CURDATE(), true, false, 'volunteer', 6);

INSERT INTO `accounts`(`email`, `password`, `lastLogin`, `archived`, `removed`, `type`, `employeeId`)
VALUE('anabelaPereira@gmail.pt', '$2b$10$5s2x0ehPr4qx7N6IBpYpyuCQyhJ9Osdf7dkH3zvuHTkltQQ0YUgCi', CURDATE(), false, false, 'employee', 7);

INSERT INTO `reinforcement_requests`(`employeeId`, `status`, `startDate`, `endDate`, `token`, `tokenExpiration`, `byEmployeeId`)
VALUES(1, 'pending', '2019-04-30 08:00', '2019-04-30 12:00', 't1', STR_TO_DATE('30,5,2030','%d,%m,%Y'), 1);

INSERT INTO `reinforcement_requests`(`employeeId`, `status`, `startDate`, `endDate`,  `token`, `tokenExpiration`, `byEmployeeId`)
VALUES(2, 'canceled', '2019-05-02 09:30', '2019-05-02 15:00', 't2', STR_TO_DATE('30,5,2000','%d,%m,%Y'), 1);

INSERT INTO `reinforcement_requests`(`employeeId`, `status`, `startDate`, `endDate`, `byEmployeeId`)
VALUES(3, 'accepted', '2019-04-22 11:30', '2019-04-22 15:00', 1);

INSERT INTO `reinforcement_requests`(`employeeId`, `status`, `startDate`, `endDate`, `byEmployeeId`)
VALUES(3, 'accepted', '2019-05-17 11:30', '2019-05-17 15:00', 1);

INSERT INTO `reinforcement_requests`(`employeeId`, `status`, `startDate`, `endDate`, `byEmployeeId`)
VALUES(6, 'declined', '2019-05-20 08:00', '2019-05-20 12:00', 1);

INSERT INTO `tasks`(`employeeId`,`title`,`description`,`creationDate`,`startDate`,`endDate`,`status`)
values (2,'Tosquia ao Rudy','Pelo curto, mas mais alongado na parte de cima da cabeça','2019-05-20 12:00','2019-05-29 12:00','2019-05-29 12:00','pending');

INSERT INTO `tasks`(`employeeId`,`title`,`description`,`creationDate`,`startDate`,`endDate`,`status`)
values (3,'Tosquia à Mimi','Curtar apenas as pontas','2019-05-20 10:00','2019-05-29 11:00','2019-05-29 12:00','pending');

INSERT INTO `tasks`(`employeeId`,`title`,`description`,`creationDate`,`startDate`,`endDate`,`status`)
values (3,'Tosquia ao Rui','O mesmo corte de sempre','2019-05-20 12:00','2019-05-15 10:30','2019-05-15 12:00','pending');

INSERT INTO `tasks`(`employeeId`,`title`,`description`,`creationDate`,`startDate`,`endDate`,`status`)
values (3,'Tosquia ao Rui 2','O corte deve ser o mesmo','2019-05-20 12:00','2019-05-29 13:30','2019-05-29 14:00','pending');

INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (1, 8, 30, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (1, 9, 00, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (1, 9, 30, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (1, 10, 00, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (1, 10, 30, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (1, 11, 00, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (1, 11, 30, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (1, 12, 00, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (1, 12, 30, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (3, 10, 30, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (3, 11, 00, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (3, 11, 30, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (3, 12, 00, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (3, 12, 30, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (3, 13, 00, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (3, 13, 30, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (5, 10, 30, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (5, 11, 00, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (5, 11, 30, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (5, 12, 00, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (5, 12, 30, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (5, 13, 00, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (5, 13, 30, 'full', 3);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (2, 10, 30, 'availability', 4);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (2, 11, 00, 'availability', 4);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (2, 11, 30, 'availability', 4);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (2, 12, 00, 'availability', 4);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (2, 12, 30, 'availability', 4);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (2, 13, 00, 'availability', 4);
INSERT INTO `schedule` (`date`, `hours`, `minutes`, `type`, `employeeId`) VALUES (2, 13, 30, 'availability', 4);


INSERT INTO `notifications`(`creationDate`, `message`, `employeeId`)
	VALUES('2019-05-14 08:00', 'Notificação n1', 1);
    
INSERT INTO `notifications`(`creationDate`, `message`, `employeeId`)
	VALUES('2019-05-14 08:00', 'Notificação n2', 1);
    
INSERT INTO `notifications`(`creationDate`, `message`, `employeeId`)
	VALUES('2019-05-14 08:00', 'Notificação n3', 1);

INSERT INTO `notifications`(`creationDate`, `message`, `employeeId`)
	VALUES('2019-05-14 08:00', 'Notificação n4', 1);

INSERT INTO `notifications`(`creationDate`, `readDate`, `message`, `employeeId`)
	VALUES('2019-05-14 08:00', '2019-05-14 12:00', 'Notificação n5', 1);

INSERT INTO `notifications`(`creationDate`, `readDate`, `message`, `employeeId`)
	VALUES('2019-05-14 08:00', '2019-05-14 12:00', 'Notificação n6', 1);
    
INSERT INTO `notifications`(`creationDate`, `readDate`, `message`, `employeeId`)
	VALUES('2019-05-14 08:00', '2019-05-14 12:00', 'Notificação n7', 1);

INSERT INTO `animals`(`name`,`birthdate`,`breed`,`size`,`info`,`gender`,`registerDate`,`removed`,`type`)
	VALUES('Rudy','2008-09-01','Caniche','medium','Cor Branca','male','2019-05-14 12:00',0,'dog');

INSERT INTO `animals`(`name`,`birthdate`,`breed`,`size`,`info`,`gender`,`registerDate`,`removed`,`type`)
	VALUES('Tobias','2008-09-01','Caniche','medium','Cor Branca','male','2019-05-14 12:00',0,'dog');
    
INSERT INTO `animals`(`name`,`birthdate`,`breed`,`size`,`info`,`gender`,`registerDate`,`removed`,`type`)
	VALUES('Tyson','2008-09-01','Caniche','medium','Cor Branca','male','2019-05-14 12:00',0,'dog');

INSERT INTO `animals`(`name`,`birthdate`,`breed`,`size`,`info`,`gender`,`registerDate`,`removed`,`type`)
	VALUES('Princesa','2008-09-01','Bulldog','medium','Cor Branca','female','2019-05-14 12:00',0,'dog');
	
INSERT INTO `animals`(`name`,`birthdate`,`breed`,`size`,`info`,`gender`,`registerDate`,`removed`,`type`)
	VALUES('Pantufa','2008-09-01','Pug','medium','Cor Branca','male','2019-05-14 12:00',0,'dog');

INSERT INTO `animals`(`name`,`birthdate`,`breed`,`size`,`info`,`gender`,`registerDate`,`removed`,`type`)
	VALUES('Cookie','2008-09-01','Rafeiro','medium','Cor Branca','male','2019-05-14 12:00',0,'dog');
	
INSERT INTO `animals`(`name`,`birthdate`,`breed`,`size`,`info`,`gender`,`registerDate`,`removed`,`type`)
	VALUES('Winie','2008-09-01','Persa','medium','Cor Branca','female','2019-05-14 12:00',0,'cat');

INSERT INTO `quarentines`(`startDate`, `status`, `cause`,`animalId`,`removed`)
	VALUES('2019-05-19','pending', 'sick',1,0);
    
INSERT INTO `quarentines`(`startDate`,`endDate`,`status`,`cause`,`animalId`,`removed`)
	VALUES('2019-05-19','2019-05-30','canceled','medicalWatch',2,0);
    
INSERT INTO `quarentines`(`startDate`,`endDate`,`status`,`cause`,`animalId`,`removed`)
	VALUES('2019-05-19','2019-05-30','finished','fight',3,0);

INSERT INTO `kennels` (`capacity`, `name`) values(1, 'C01');
INSERT INTO `kennels` (`capacity`, `name`) values(2, 'C02');
INSERT INTO `kennels` (`capacity`, `name`) values(3, 'C03');
INSERT INTO `kennels` (`capacity`, `name`) values(4, 'C04');

INSERT INTO `kennels_animals` (`kennelId`, `animalId`) VALUES(1, 1);
INSERT INTO `kennels_animals` (`kennelId`, `animalId`) VALUES(2, 2);
INSERT INTO `kennels_animals` (`kennelId`, `animalId`) VALUES(3, 3);
INSERT INTO `kennels_animals` (`kennelId`, `animalId`) VALUES(3, 4);
INSERT INTO `kennels_animals` (`kennelId`, `animalId`) VALUES(3, 5);
INSERT INTO `kennels_animals` (`kennelId`, `animalId`) VALUES(4, 6);
INSERT INTO `kennels_animals` (`kennelId`, `animalId`) VALUES(4, 7);

INSERT INTO `relationships` (`animalAId`, `animalBId`, `value`) VALUES(1, 2, 1);
INSERT INTO `relationships` (`animalAId`, `animalBId`, `value`) VALUES(3, 2, 5);
INSERT INTO `relationships` (`animalAId`, `animalBId`, `value`) VALUES(4, 1, -5);
INSERT INTO `relationships` (`animalAId`, `animalBId`, `value`) VALUES(6, 3, -10);
INSERT INTO `relationships` (`animalAId`, `animalBId`, `value`) VALUES(5, 4, 1);
INSERT INTO `relationships` (`animalAId`, `animalBId`, `value`) VALUES(4, 3, 5);
INSERT INTO `relationships` (`animalAId`, `animalBId`, `value`) VALUES(5, 1, 1);