require("dotenv").config();

const fs = require("fs");
const colors = require("colors");
const mysql = require("mysql");
const queries = require("../server/api/database/sql/queries");
const db = require("../server/api/database/sql/db");
const { connect } = require("../server/api/database/mongo/db");
const { insert } = require("../server/api/database/mongo/schemas/shelter");
const insertUser = require("../server/api/database/mongo/schemas/user").insert;
const { getConnection, getGenericConnection, baseDB } = db;

const shelterIds = [];

// All default testing shelters
const shelters = [
  { name: "Quinta do mião", address: "Avenida", licenseType: "plus" },
  { name: "Canil de Lisboa", address: "Rua", licenseType: "basic" }
];

// If new scripts get added later on,
// add them to this to-run queue
const scripts = ["create.sql", "populate.sql"];

// Connect to the database host, with no
// particular database
const genericConnection = getGenericConnection();

clearDatabases();

/**
 * Add all the sql users to the mongo database
 */
function addUsers() {
  const connection = getConnection(shelterIds[0]);

  connection.query(queries.GET_ALL_ACCOUNTS, (err, result) => {
    if (err) {
      throw err;
    }

    result.forEach(a => {
      shelterIds.forEach(s => {
        insertUser(a.email, s);
      });
    });
  });
}

function clearDatabases() {
  console.log("Removing old databases");

  const showQuery = "SHOW SCHEMAS";
  const connection = getGenericConnection();

  var removeDb = (name, callback) => {
    const removeQuery = `DROP DATABASE IF EXISTS ${name}`;
    connection.query(removeQuery, (err, result) => {
      if (err) {
        throw err;
      }
      callback();
    });
  };

  connection.query(showQuery, (err, result) => {
    if (err) {
      console.log(err);
    }

    var toRemove = 0;
    var removed = 0;

    result.forEach(r => {
      const name = r.Database;

      if (name.includes("abrigovirtual")) {
        toRemove++;
        removeDb(name, () => {
          removed++;
          if (removed === toRemove) {
            resetMongo();
          }
        });
      }
    });

    if (toRemove === removed) {
      resetMongo();
    }
  });
}

function resetMongo() {
  console.log("Resetting MongoDB");
  connect(({ connection }) => {
    // Clear the mongo database
    connection.db.dropDatabase(() => {
      // Insert all default shelters
      // this will ensure a connection to the
      // previously deleted database, re-creating it.
      shelters.forEach(({ name, address, licenseType }) => {
        insert(name, address, licenseType, result => {
          shelterIds.push(result.id);

          if (shelterIds.length === shelters.length) {
            // If all shelters have been inserted
            // Initiate the SQL database resetting
            resetSql();
          }
        });
      });
    });
  });
}

function resetSql() {
  console.log("Resetting SQL");

  var removed = 0;

  shelterIds.forEach(id => {
    // First, reset the database
    resetDatabase(id, () => {
      // After it has been reset, run all
      // the scripts on that database
      const connection = getConnection(id);
      scripts.forEach(script => {
        runScript(connection, script, id, () => {
          removed++;

          // If all scripts have been run on all databases
          // add all sql users to mongo
          if (removed === shelterIds.length * scripts.length) {
            addUsers();
          }
        });
      });
    });
  });
}

function resetDatabase(id, onReset) {
  const db = baseDB + id;
  const preScript = `DROP DATABASE IF EXISTS ${db}; CREATE DATABASE ${db}; USE ${db};`;

  genericConnection.query(preScript, (err, results) => {
    if (err) {
      console.log(colors.red(err));
      return;
    }

    onReset();
  });
}

function runScript(connection, script, id, callback) {
  try {
    const data = fs.readFileSync(__dirname + "/scripts/" + script, "utf8");

    connection.query(data, (err, results) => {
      if (err) {
        console.log(colors.red(err));
        return;
      }

      console.log(
        colors.green(`SUCCESSFULLY EXECUTED: `) + script + " - Database #" + id
      );

      callback();
    });
  } catch (err) {
    console.log(colors.red(err));
  }
}
